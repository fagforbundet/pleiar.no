# Build system for Pleiar.no

Unlike many JavaScript-projects, Pleiar.no uses a Makefile-based build system,
rather than a npm-based one. Webpack handles most of the building, with some
additional tasks run by the makefile. `make production` will build a ready
production build of pleiar.no into build/, while `make development` does the
same for a debugging build, and `make devserver` starts a development server.
`make test` runs our tests.

What follows is a description of how it is built. Note that the makefile
targets does all of this for you already, and this is mainly intended to help
you understand what does what so that you can modify it.

## Data compilation

As a dependency before webpack can do its job, the data files need to be
prepared. This is done by `./scripts/data-compiler.js`. This script reads the
YAML and markdown data from the data repository, performs conversion, indexing,
sorting and whatever else is required to prepare the data for use and then
outputs it as JSON which is imported into the app. Because of this step, you
can't just run webpack when building.

## Webpack

Webpack handles most of our build. It calls babel to transpile the JS, and
bundles up our deps. It also builds the CSS, minifies and gzips files. The
webpack setup is fairly standard.

## react-snap

Once webpack is done, we run react-snap on the resulting tree to get
pre-rendered versions of our static pages, to smooth out the initial load.
There's a somewhat hacky workaround script run after react-snap that updates
the page descriptions (scripts/meta-description.pl).

## Additional steps

The Makefile will minify and gzip the appmanifest, and it will call
scripts/build-sitemap.sh to build a sitemap.xml for production instances (this
last step is skipped for development).
