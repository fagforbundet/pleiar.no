# Extended Markdown Syntax for Pleiar.no

See the [Markdown syntax
reference](https://daringfireball.net/projects/markdown/syntax) for general
Markdown syntax information.

Pleiar.no supports, if the page enables it in the MarkdownRenderer, some
special link syntax, which can be used to call rendering of pleiar.no
components directly from markdown. The syntax consists of "components" and
"internal links", with different prefixes to trigger the syntax.

## Components

Triggering of this syntax is done by making a link whose first character is
`!`. Though not required, it is also common to leave the link text empty. So a
component import would look something like `[](!component)`.

### Display entry from the dictionary

To display an entry from the dictionary, use the link target `!dict/NAME`
Where **NAME** is the value of `expression` for the entry you want to display.
Keep in mind that you need to use URL encoding if it includes spaces. So, if
you wanted to link to an entry called "CRP", you would do `[](!dict/CRP)`,
while if it was called "C-Reactive Protein" it would be
`[](!dict/C-Reactive%20Protein)`.

### Display lab value tables

To display a set of tables with all values from the lab values list, use
`!labValues`. So, the full code would be `[](!labValues)`.

### Display an index from the handbook

There are two variants available to displaying an index from the handbook,
`!handbookIndexOpen` and `!handbookIndexPlain`. The syntax is the same.

#### !handbookIndexOpen
Use `!handbookIndexOpen/PATH` where `PATH` is the path to the handbook entry,
as would be used in URLs (ie.  /Section/Subsection/). The full code would look
something like: `[](!handbookIndexOpen/Section/Subsection)`.

Note that this will not display an accordion, but just an open list. It will
also skip any toplevel articles, because those aren't really compatible with
being displayed in an index, so make sure you take this into account when
building the handbook tree.

#### !handbookIndexPlain

Use `!handbookIndexPlain/PATH` where `PATH` is the path to the handbook entry,
as would be used in URLs (ie.  /Section/Subsection/). The full code would look
something like: `[](!handbookIndexPlain/Section/Subsection)`.

Note that this will display a simple `<ul>` list, without section "headers"
(but with links to said sections).

### Embed a vimeo video

Use `!vimdeo/ID` wher `ID` is the Vimeo video ID. Will be responsive. Remember
to permit player.vimeo.com in CSP.

### Embed a YouTube video

Use `!youtube/ID` where `ID` is the YouTube video ID. Will be responsive.
Remember to permit youtube.com in CSP.

## Internal links

Triggering of this syntax is done by making a link whose first character is
`:`. The link text will be used as usual. `[Kommunikasjon og
relasjonsarbeid](:handbook/Miljoarbeid/Kommunikasjon_og_relasjonsarbeid)` would
generate a link to `/handbok/Miljoarbeid/Kommunikasjon_og_relasjonsarbeid`, and
is essentially the same as
`[…](/handbok/Miljoarbeid/Kommunikasjon_og_relasjonsarbeid)`. The advantage is
that it generates portable links, which are not bound to internal
implementation details like which path the handbook gets rendered at. Thus you
should prefer using internal link syntax instead of writing out the full path.

Another advantage to using this syntax is that it will validate that the
handbook entry you are linking to actually exists during the data tests. The
tests will fail if you're linking to something that doesn't exist.

### Handbook

To link to an entry in the handbook, use `:handbook/PATH` where PATH is the
internal path to the entry in the handbook. For instance to link to the file in
the dataset that lives at
`handbok/Miljoarbeid/Kommunikasjon_og_relasjonsarbeid.md` you would write
`:handbook/Miljoarbeid/Kommunikasjon_og_relasjonsarbeid`.

## Image text

By default the text for an image (`![text](/img.png)`) will be used as a
descriptive text beneath the image. You can override this and force it to be
used only as alt text by prefixing the text with #.

`![#Hello world](/img.png)` - the image will not have a text beneath it, but
will have `alt="Hello world"`.

`![Hello world](/img.png)` - the image will have `alt="Hello world"` *and*
"Hello world" as the image text underneath the image.

## Forcing a link to open in a new window

Triggering of this syntax is done by making a link whose first character is
`+`.  The link text will be used as usual. `[Last ned](+/files/file.pdf)` would
generate a link to `/files/file.pdf`, but which is `target="_blank"` and with
the class `external-link` (like other external links).
