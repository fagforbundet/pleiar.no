# Part of Pleiar.no - a collection of tools for nurses
#
# Copyright (C) Eskild Hustvedt 2017-2018
# Copyright (C) Fagforbundet 2019-2021
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

default: help

SHELL:=/bin/bash
YARN=$(shell if which yarnpkg &>/dev/null; then echo yarnpkg; else echo yarn;fi)
NODEJS=$(shell if which nodejs &>/dev/null; then echo nodejs; else echo node;fi; if [ "$$CI" == "" ]; then echo "--enable-source-maps";fi)
WGET=$(shell if [ "$$CI" != "" ]; then echo "wget --no-verbose"; else echo "wget";fi)
PRENAME=$(shell if which prename &>/dev/null; then echo "prename"; elif which "file-rename"; then echo "file-rename";else echo "rename";fi)
YARN_RUN=$(YARN) run -s
include .Makefile.incl

help: _help depCheck
# This target outputs a formatted list of all of the "public" targets in this Makefile
_help: FMT="%-18s%s\\n"
_help:
	@echo "Pleiar.no makefile targets:"
	@printf "$(FMT)" "production" "Builds a ready-to use production version of Pleiar.no into build/"
	@printf "$(FMT)" "development" "Builds a development version of Pleiar.no into build/"
	@printf "$(FMT)" "staging" "Builds a staging version of Pleiar.no into build/"
	@printf "$(FMT)" "devserver" "Starts a webpack-dev-server"
	@printf "$(FMT)" "analyze" "Builds pleiar.no and runs webpack-bundle-analyzer"
	@printf "$(FMT)" "apidoc" "Builds the API documentation using documentation.js into apidocs/"
	@printf "$(FMT)" "version" "Displays version information about the local and production versions of pleiar.no"
# This does a (stupid) check for our main dependencies, namely that yarn is
# installed and that the node_modules directory insists.
depCheck: _requiresData
	@if ! which $(YARN) &>/dev/null; then echo "Error: Pleiar.no requires yarn to be installed. See https://yarnpkg.com/lang/en/docs/install/";exit 1;fi
	@if [ ! -e "./node_modules" ]; then echo "Error: You must install the pleiar.no dependencies. Run: $(YARN) install";exit 1;fi
	@if [ ! -e "./flow-typed" ]; then echo "Warning: flow typedefinitions (flow-typed) not installed";echo "Run $(YARN) flow-typed install to fix this.";fi
	@if [ -d "./.git/hooks" ]; then ln -sf "$(shell pwd)/scripts/pre-push" .git/hooks/pre-push; fi

# ===
# Build targets
# ===

# Builds a complete development version of pleiar.no, not for deployment
development: depCheck clean packDev snap prepareSiteTree _devSiteTree compressEverything
# Builds a quick development version of pleiar.no, without react-snap, not for deployment
developmentLazy: depCheck clean packDev prepareSiteTree _devSiteTree
# Builds a complete deployment-ready staging version of pleiar.no
staging: depCheck distclean packStaging snap prepareSiteTree _devSiteTree compressEverything
# Builds a complete, deployment-ready version of pleiar.no
production: distclean buildProduction
# The actual worker for the production target, but only uses 'clean' not 'distclean'
# to avoid rebuilding external indexes. The usual production target calls distclean and then
# this.
buildProduction: depCheck clean packDist snap sitemap prepareSiteTree compressEverything
# Builds a development version of pleiar.no with webpack
packDev: depCheck data
	PLEIAR_ENV=development NODE_ENV=development $(YARN_RUN) webpack -d --mode development $(PLEIAR_WEBPACK_OPTIONS)
# Builds a staging version of pleiar.no with webpack
packStaging: depCheck data
	PLEIAR_ENV=staging NODE_ENV=production $(YARN_RUN) webpack --mode production $(PLEIAR_WEBPACK_OPTIONS)
# Builds a production version of pleiar.no with webpack
packDist: depCheck data
	PLEIAR_ENV=production NODE_ENV=production $(YARN_RUN) webpack --mode production $(PLEIAR_WEBPACK_OPTIONS)
# Cleans up the tree
clean:
	rm -rf build
	rm -f scripts/build-sitemap.js scripts/link-validator.js scripts/postprocess-html-snapshots.js scripts/external-site-indexer.js scripts/prepare-static-tree.js scripts/data-compiler.mjs data/datasett/nutricalc-data.json data/datasett/external-resources.json data/datasett/dictionary.json data/datasett/lab.json data/handbok/compiled.json data/handbok/handbok.json data/search-index.json data/.*cache
	rm -rf public apidocs flow-coverage bundle-analyzer coverage public .src-nodecompatible .scripts-nodecompatible
distclean: clean
	rm -f data/externallyIndexed.json
# Builds two trees that are mirrors of src/ and scripts/ where the flow types
# have been stripped, and where each .js is renamed .mjs. This enables the
# scripts to be able to be run directly using node, without passing it through
# a webpack compiler, reducing the number of moving parts for those scripts.
nodejsCompatibleTree:
	rm -rf .src-nodecompatible .scripts-nodecompatible
	mkdir -p .src-nodecompatible .scripts-nodecompatible
	$(YARN_RUN) flow-remove-types --quiet src/ --out-dir ./.src-nodecompatible/
	$(YARN_RUN) flow-remove-types --quiet scripts/ --out-dir ./.scripts-nodecompatible/
	perl -pi -e 's{/src/(.*)\.js}{/.src-nodecompatible/$$1.mjs}g;s{(\./.*)\.js}{$$1.mjs}g;s{webpack.config.mjs}{webpack.config.js};s{mjson}{json}g;' $$(find .*-nodecompatible -iname '*.js')
	find .*-nodecompatible -iname '*.js'| $(PRENAME) 's/\.js$$/.mjs/'
refreshNodejsCompatible:
	@for f in src/*.js src/*/*.js scripts/*js; do\
		other="$$(echo $$f | perl -p -E 's{src/}{.src-nodecompatible/}g; s{\.js$$}{.mjs}')";\
		if [ "$$f" -nt "$$other" ]; then\
			echo "$$f -nt $$other";\
			make --no-print-directory nodejsCompatibleTree; exit 0;\
		fi;\
	done
# Traverses the local pleiar.no with react-snap to build a static version of
# the site. Used to build a production version of the code
snap: _snap _snap_verify_build _snap_noscript_hack _snap_postprocess_script
# Runs react-snap
_snap: refreshNodejsCompatible
	sync
	@# This is a hack to make sure that react-snap runs on a pristine copy
	@# during both runs
	mv build build.base
	rsync -az build.base/ build/
	mkdir -p build/snap
	timeout 8m $(NODEJS) ./scripts/build-screenshots.js
	mv build/snap build.base/
	rm -rf build
	mv build.base build
# 	# This is a hack to work around react-snap erroring out with a ECONNRESET
# 	# without there actually being a problem.
	$(YARN_RUN) react-snap || $(YARN_RUN) react-snap
	@# Use the 200 page for everything. Right now we just use react-snap to
	@# build a directory tree because having a site with logged in/logged out
	@# states where the server doesn't know the state results in problems
	@# rehydrating trees.
	$(NODEJS) ./.scripts-nodecompatible/prepare-static-tree.src.mjs
# 	# We don't want to use a cached om/system page, it does not make sense
	cp build/200.html build/om/system/index.html
# 	# Manually create the auth and app installation pages
	mkdir -p build/auth/logout build/om/app
	cp build/200.html build/auth/index.html && cp build/200.html build/auth/logout/index.html
	cp build/200.html build/om/app/index.html
# This is a hack to work around react-snap not handling <noscript> properly (it
# HTML escapes everything inside <noscript>, breaking our pretty alert box)
_snap_noscript_hack:
	find build/ -iname '*.html' -print0 |xargs -0 -- perl -pi -e s'#<noscript></noscript>#<noscript><div class="alert alert-danger text-center" role="alert">Pleiar.no nyttar JavaScript. Du må slå på JavaScript i din nettlesar for at sida skal fungere.</div></noscript>#g'
_snap_verify_build:
	@echo "Verifying snap build..."
	@ret=0; for file in $(shell find build/ -iname '*.html'|grep -v '/mailto:'|grep -v 404); do\
		if grep -q "404 feil:" "$$file"; then\
			echo "$$file: 404 error";ret=1;\
		fi;\
	done; exit $$ret
# Runs our meta description generation script to update the react-snap-written
# HTML pages with a page-specific meta description
_snap_postprocess_script: refreshNodejsCompatible
	$(NODEJS) ./.scripts-nodecompatible/postprocess-html-snapshots.src.mjs
# Compresses everything that webpack hasn't already compressed
compressEverything:
	find build/ -regextype egrep -iregex '.*\.(html|xml|txt|svg|ttf|eot)$$' -print0 | xargs -0 -- gzip -k
	find build/ -regextype egrep -iregex '.*\.(js|css|html|xml|txt|svg|ttf|eot)$$' -print0 | xargs -0 -- brotli -k
# Makes some final preparations of the build tree before deployment
prepareSiteTree:
	cd build;ln -s apple-ticon450x.png apple-touch-icon.png
	find build/ -iname '*~' -print0 |xargs -0 -- rm -f
_devSiteTree:
	echo -e "User-agent: *\nDisallow: /" >> build/robots.txt
	perl -pi -e 's{"Pleiar.no"}{"Pleiar.no (dev)"}g' build/appmanifest.json
	rm -f build/appmanifest.json.gz; gzip --keep build/appmanifest.json
# Starts a development server
devserver: depCheck data _beforeDevServer
	@if which parallel &>/dev/null && which inotifywait &>/dev/null; then\
		make --no-print-directory _runDevParalell; \
	else\
		echo -e "\n\n****\nWARNING: parallel not installed, will not monitor data changes!\n****\n\n";\
		sleep 5;\
		make --no-print-directory _runDevServer;\
	fi
_runDevParalell:
	parallel --line-buffer make ::: _runDevServer delayedDataWatch
_runDevServer:
	PLEIAR_ENV=development NODE_ENV=development $(YARN_RUN) webpack serve --env PLEIAR_ENV=development --node-env development --mode development $(PLEIAR_WEBPACK_DEV_SERVER_ARGS)
server: devserver
# Compiles the data
.PHONY: data
festData:
	[ -e data/fest.json ] || $(WGET) -O data/fest.json "https://fagforbundet.gitlab.io/pfet/pfet.out.json"
externallyIndexed:
	[ -e data/externallyIndexed.json ] || make --no-print-directory externalIndexer
data: festData externallyIndexed dataCompiler
dataCompiler: refreshNodejsCompatible
	$(NODEJS) ./.scripts-nodecompatible/data-compiler.src.mjs
updateFEST:
	if [ -e data/fest.json ]; then mv data/fest.json pfet.out.json; $(WGET) -N "https://fagforbundet.gitlab.io/pfet/pfet.out.json"; mv pfet.out.json data/fest.json;else make --no-print-directory data;fi
delayedDataWatch: data
	sleep 10
	@make --no-print-directory _dataWatch
dataWatch: data _dataWatch
_dataWatch:
	while :; do inotifywait -e modify -e move -e create -e close_write -r --excludei '(\.swp|\.json|~)$$' scripts/data-compiler.src.js data/;if ! ps ux|grep -v grep|egrep -q '(git push|make (test|development|production|data|coverage))'; then date;make --no-print-directory data;fi;done
# Switches quickly between public and private data
switchData:
	[ -e data ]
	if [ -e data.public ]; then \
		mv -v data data.private;\
		mv -v data.public data;\
	elif [ -e data.private ]; then \
		mv -v data data.public ;\
		mv -v data.private data;\
	fi
# Builds our documentation
# The replacements to src/searcher.js is a workaround for documentation.js
# issue #1224
apidoc: depCheck data
	rm -rf apidocs
	$(YARN_RUN) documentation build --shallow $$(find src/ -iname '*.js') -f html -o apidocs

DEVDOC_TEMPLATE='<html><head><title>%s</title><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"></head><body><div class="container"><div class="row"><div class="col">%s</div></div></div></body></html>'
# Builds our developer documentation into public/
devdoc: depCheck clean data apidoc testCoverage flowCoverage analyzeStatic
	mkdir -p public public/docs
	mv apidocs flow-coverage bundle-analyzer public
	mv coverage/lcov-report public/coverage && rm -rf coverage
	printf $(DEVDOC_TEMPLATE) "Pleiar.no development documentation" "<h1>Pleiar.no developer documentation</h1><a href="apidocs">API Documentation</a><br /><a href="docs">Other documentation</a><br /><a href="coverage">Test coverage</a><br /><a href="flow-coverage">Flow coverage</a><br /><a href="bundle-analyzer">Webpack bundle analyzer report</a><br /><br /><a href="https://gitlab.com/fagforbundet/pleiar.no">Gitlab repo</a>" > public/index.html
	echo -n "User-agent: *\nDisallow: /\n" > public/robots.txt
	for file in $$(find public -iname '*.html'); do\
		if ! grep -q 'nofollow' "$$file"; then\
			perl -pi -e 's{<head>}{<head><meta name="robots" content="noarchive,noindex,nofollow" />}g' "$$file";\
		fi;\
	done

# ===
# Tests
# ===

# Default parameters for jest
JEST_PARAMS?=
# The global test target
testNoFlow: depCheck jest eslint
test: JEST_PARAMS+=--testPathIgnorePatterns=fest\\.data
test: testNoFlow flow
# Verify types with flow
flow:
	@# FIXME: Temporary hack to remove broken flow-typed types
	@rm -f flow-typed/npm/morgan_v1.x.x.js flow-typed/npm/body-parser_v1.x.x.js
	@if [ -d flow-typed ]; then echo "$(YARN_RUN) flow check --include-warnings"; $(YARN_RUN) flow check --include-warnings;fi
# Flow coverage report
flowCoverage: depCheck data
	$(YARN_RUN) flow-coverage-report -t html -i 'src/**/*.js'
# Verify syntax with eslint.
eslint:
	@# Using shell echo to expand the glob before running the rule, so that
	@# it's obvious what eslint is processing.
	$(YARN_RUN) eslint --ignore-pattern node_modules scripts/ src/ tests/ $(shell echo data*/) $(PLEIAR_ESLINT_PARAMS)
# Run testsuite
jest: data
	$(YARN_RUN) jest $(JEST_PARAMS)
# Run testsuite but just with data
testData: JEST_PARAMS+=--testPathIgnorePatterns=fest\\.data
testData: testDataFull
testDataFull: JEST_PARAMS+=--testPathPattern=\\.data
testDataFull: jest
# Run tests with up-to-date flow definitions
distTest: _distTestPre clean test buildProduction
_distTestPre:
	if [ -d flow-typed ]; then $(YARN_RUN) flow-typed update >/dev/null;fi
snyk:
	snyk test --dev --file=yarn.lock || snyk wizard --dev --file=yarn.lock
# Verbose test runner
testVerbose: JEST_PARAMS+=--verbose
testVerbose: jest
# Test runner that updates jest snapshots
testUpdate: JEST_PARAMS+=-u
testUpdate: jest
# Build a test and flow coverage report
testCoverage: JEST_PARAMS+=--coverage --testPathIgnorePatterns='(\.data|integration)'
testCoverage: depCheck jest
# Tests data links
testDataLinks: refreshNodejsCompatible data
	echo -e 'nodejs_conf = openssl_init\n\n[openssl_init]\nssl_conf = ssl_sect\n\n[ssl_sect]\nsystem_default = system_default_sect\n\n[system_default_sect]\nOptions = UnsafeLegacyRenegotiation\n' > ./.openssl-legacy.cnf
	$(NODEJS) --openssl-config ./.openssl-legacy.cnf ./.scripts-nodecompatible/link-validator.src.mjs
linkcheck: testDataLinks

# ===
# Various helpers
# ===

EXTERNAL_KILLTIME=1m
EXTERNAL_TERMTIME=2m
# Runs our indexer
externalIndexer: refreshNodejsCompatible
	timeout -k $(EXTERNAL_TERMTIME) $(EXTERNAL_KILLTIME) $(NODEJS) --unhandled-rejections=strict ./.scripts-nodecompatible/external-site-indexer.src.mjs || \
	timeout -k $(EXTERNAL_TERMTIME) $(EXTERNAL_KILLTIME) $(NODEJS) --unhandled-rejections=strict ./.scripts-nodecompatible/external-site-indexer.src.mjs
externalIndexerPermissive: EXTERNAL_TERMTIME=10m
externalIndexerPermissive: EXTERNAL_TERMTIME=4m
externalIndexerPermissive: externalIndexer

# Uses our script to generate a sitemap
sitemap: refreshNodejsCompatible
	$(NODEJS) ./.scripts-nodecompatible/build-sitemap.src.mjs
# Analyzes the codebase with webpack-bundle-analyzer
prepAnalyze: depCheck clean data
	mkdir -p build
	PLEIAR_ENV=staging NODE_ENV=production $(YARN_RUN) webpack --mode production --stats=detailed --json > build/stats.json
analyze: prepAnalyze
	$(YARN_RUN) webpack-bundle-analyzer ./build/stats.json
analyzeStatic: prepAnalyze
	rm -rf bundle-analyzer && mkdir bundle-analyzer
	$(YARN_RUN) webpack-bundle-analyzer -r bundle-analyzer/index.html -m static ./build/stats.json
# Gives a quick comparison of the remote (deployed) version of pleiar.no with
# our local one
REMOTE_SITE?=www.pleiar.no
version:
	@echo -n "Local version: "
	@git rev-parse --short=4 HEAD
	@echo -n "Remote version: "
	@mojo get https://$(REMOTE_SITE) 'footer a' 'text'|cat|head -n1|perl -pe 's/.*versjon\s+(\S+)\s+.*/$$1/i'
# This checks that we have a dataset cloned
_requiresData:
	@if [ ! -d "data/.git" ]; then echo "No dataset appears to be present.";echo "Cloning the public/free pleiar.no dataset:";git clone git@gitlab.com:fagforbundet/pleiar.no-fritt-innhald.git data;fi
switchNodeModules:
	if echo "$$OSTYPE" |grep -q darwin; then\
		if [ -e "node_modules.mac" ]; then\
			mv -v node_modules node_modules.linux;\
			mv -v node_modules.mac node_modules;\
		fi;\
		echo "node_modules is mac";\
	else\
		if [ -e "node_modules.linux" ]; then\
			mv -v node_modules node_modules.mac;\
			mv -v node_modules.linux node_modules;\
		fi;\
		echo "node_modules is linux";\
	fi
# ===
# CI wrappers
# ===

# Various wrappers to help with some quirks with gitlab CI (sometimes commands
# may fail randomly instead of due to actual errors)
ciTest: JEST_PARAMS+=--testPathIgnorePatterns=\\.data --silent --runInBand
ciTest: testNoFlow
ciTestData: JEST_PARAMS=--runInBand --silent
ciTestData: testDataFull
ciProduction:
	@make --no-print-directory production || make --no-print-directory production
ciStaging:
	@make --no-print-directory staging || make --no-print-directory staging
