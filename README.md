# Pleiar.no

Pleiar.no is a set of tools for nurses to use in their day-to-day work, or
nursing students to use as learning aids. They are all in Norwegian (though the
source code and repository is commented in english).

Pleiar.no is written in javascript, uses React.js, Redux.js and a variant of
Bootstrap 4 for React applications called reactstrap. The CSS is written in
SASS, and the whole thing is compiled using webpack. Utilities of note that are
used include babel, flow and react-snap.

## Using this repository

Unlike many JavaScript-based projects, Pleiar.no uses a regular Makefile,
rather than package.json "scripts". Run "make help" to view all of the targets.
If you just want to quickly get a development server, run "make devserver". See
[docs/BuildSystem.md](docs/BuildSystem.md) for more information about the build
system.

### Dependencies

Pleiar.no uses yarn as its dependency manager. Install yarn, and then run "yarn
install" to install all of Pleiar.no's dependencies locally into
./node_modules/

### Data set

The site requires a dataset. The dataset includes the data for the various
features on pleiar.no. The production version of pleiar.no has an internal
non-free dataset. There's a simple free dataset available at
https://gitlab.com/fagforbundet/pleiar.no-fritt-innhald - this repo will
auto-checkout this dataset if you don't check another out yourself. The dataset
also includes various branding elements.

### git branches

The branch "production" is the one currently deployed on pleiar.no. "main" is
the default branch in which development occurs. Other branches are related to
specific changes or features and are usually deleted once merged.

## License

Pleiar.no is Copyright © Eskild Hustvedt 2018-2019, Fagforbundet 2019

Pleiar.no is free software: you can redistribute it and/or modify it under the
terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
details.

### Permission for Fagforbundet to combine AGPL code with non-free data

Pleiar.no is licensed under the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or (at your
option) any later version, with the following clarification and special
exception:

As a special exception, the copyright holders of this software grant the
Norwegian organization Fagforbundet permission to import data into this
software to produce a compiled version of Pleiar.no that uses said data.  The
combined work must still comply with the GNU Affero General Public License, but
the license will not extend to the imported data, which will retain its
original license, and the work as a whole may not be used for commercial
purposes. If you modify this software, you may extend this exception to your
version of the software, but you are not obliged to do so. If you do not wish
to do so, delete this exception statement from your version.
