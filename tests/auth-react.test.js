/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
jest.unmock('../src/auth.js');
import * as React from 'react';
import auth from '../src/auth';
import { getComponentWith, checkSnapshot, mockLocation } from './test-helpers';
import { RequiresAuth, AuthOverlay, Authenticator, InlineAuthBlock } from '../src/react-components/auth';

// just-enough react-router-dom for Authenticator to render
jest.mock('react-router-dom', () => {
    return {
        Redirect (props)
        {
            return props.to;
        }
    };
});

// We monkey-patch the auth object to mock it. These define the return values
// from each method (default values defined in the beforeEach block).
const authMockRet = {};
// Reset monkey patches before each test
beforeEach( () => {
    authMockRet.setAuthStatusTo = true;
    authMockRet.initialize = true;
    authMockRet.isAuthenticated = true;
    authMockRet.invalidate = null;
    authMockRet.authenticate = true;
    // Also remove the localStorage we might use
    delete window.localStorage.pleiarAuthRedirect;
    // Monkey-patching. Yes, we reset the patches before each test (instead of
    // just resetting it)
    auth.initialize = jest.fn( () => {
        return authMockRet.initialize;
    });
    auth.isAuthenticated = jest.fn( () => {
        return authMockRet.isAuthenticated;
    });
    auth.invalidate= jest.fn( () => {
        return authMockRet.invalidate;
    });
    auth.authenticate = jest.fn( () => {
        authMockRet.isAuthenticated = authMockRet.setAuthStatusTo;
        return authMockRet.authenticate;
    });
    delete window.MutationObserver;
});

describe('RequiresAuth', () => {
    test('Authenticated', () => {
        const comp = getComponentWith(<RequiresAuth>
            Something
        </RequiresAuth>, { enzyme: true });
        expect(comp.find('.authOverlay')).toHaveLength(0);
        expect(comp.find('.authn')).toHaveLength(0);
        expect(comp.find('.requiresAuthentication')).toHaveLength(0);
    });
    test('Not authenticated', () => {
        authMockRet.isAuthenticated = false;
        const comp = getComponentWith(<RequiresAuth>
            Something
        </RequiresAuth>, { enzyme: true });
        expect(comp.find('.authOverlay')).toHaveLength(1);
        expect(comp.find('.authn')).toHaveLength(1);
        expect(comp.find('.requiresAuthentication')).toHaveLength(1);
    });
    test('Not authenticated snapshot', () => {
        authMockRet.isAuthenticated = false;
        checkSnapshot(<RequiresAuth>
            Something
        </RequiresAuth>);
    });
    test('Not authenticated with MutationObserver', () => {
        authMockRet.isAuthenticated = false;
        window.MutationObserver = jest.fn(() => {
            return {
                observe: jest.fn(),
                disconnect: jest.fn()
            };
        });
        const comp = getComponentWith(<RequiresAuth>
            Something
        </RequiresAuth>, { enzyme: true });
        expect(comp.find('.authn')).toHaveLength(1);
        expect(comp.find('.requiresAuthentication')).toHaveLength(1);
        expect(window.MutationObserver).toHaveBeenCalled();
        // Make some changes
        const authn = comp.find('.authn').getDOMNode();
        const requiresAuthentication = comp.find('.requiresAuthentication').getDOMNode();
        authn.className = "";
        requiresAuthentication.className = "";
        const cb = window.MutationObserver.mock.calls[0][0];
        cb([{
            target: authn,
            type: "attributes",
            attributeName: "class"
        }]);
        expect(authn.className).not.toBe("");
        cb([{
            target: requiresAuthentication,
            type: "attributes",
            attributeName: "class"
        }]);
        expect(requiresAuthentication.className).not.toBe("");
        const observerInstance = comp.instance().observer;
        comp.unmount();
        expect(observerInstance.disconnect).toHaveBeenCalled();
    });
});

describe('AuthOverlay', () => {
    test('snapshot', () => {
        checkSnapshot(<AuthOverlay />);
    });
    test('Setting redirect', () => {
        // We don't actually need it, we just want to check the localStorage
        getComponentWith(<AuthOverlay />);
        expect(window.localStorage.pleiarAuthRedirect).toBe(location.pathname);
    });
});

describe('InlineAuthBlock', () => {
    test('snapshot', () => {
        checkSnapshot(<AuthOverlay />);
    });
    test('Setting redirect', () => {
        // We don't actually need it, we just want to check the localStorage
        getComponentWith(<InlineAuthBlock attemptedAccess="test" />);
        expect(window.localStorage.pleiarAuthRedirect).toBe(location.pathname);
    });
});

describe('Authenticator', () => {
    test('Auto-redirect to / when already logged in', () => {
        const comp = getComponentWith(<Authenticator />,{
            enzyme: true
        });
        expect(comp.find('Redirect')).toHaveLength(1);
        expect(comp.find('Redirect').props().to).toBe("/");
    });
    test('Auto-redirect to pleiarAuthRedirect when already logged in', () => {
        window.localStorage.pleiarAuthRedirect = "/test";
        const comp = getComponentWith(<Authenticator />,{
            enzyme: true
        });
        expect(comp.find('Redirect')).toHaveLength(1);
        expect(comp.find('Redirect').props().to).toBe("/test");
        expect(window.localStorage.pleiarAuthRedirect).toBe(undefined);
    });
    test('Successfully authenticate', () => {
        mockLocation();
        location.href = 'http://example.com/auth?jwt=test';
        authMockRet.isAuthenticated = false;
        const comp = getComponentWith(<Authenticator />,{
            enzyme: true
        });
        expect(auth.authenticate).toHaveBeenCalledWith('test');
        expect(comp.find('Redirect')).toHaveLength(1);
        expect(comp.find('Redirect').props().to).toBe("/");
    });
    test('Fail to authenticate', () => {
        mockLocation();
        location.href = 'http://example.com/auth?jwt=test';
        authMockRet.isAuthenticated = false;
        authMockRet.setAuthStatusTo = false;
        const comp = getComponentWith(<Authenticator />,{
            enzyme: true
        });
        expect(auth.authenticate).toHaveBeenCalledWith('test');
        // Should display the overlay again
        expect(comp.find('AuthFailed')).toHaveLength(1);
    });
    test('Logout', () => {
        mockLocation();
        location.pathname = '/auth/logout';
        const comp = getComponentWith(<Authenticator />,{
            enzyme: true
        });
        expect(auth.invalidate).toHaveBeenCalled();
        expect(comp.find('Redirect')).toHaveLength(1);
        expect(comp.find('Redirect').props().to).toBe("/");
    });
});
