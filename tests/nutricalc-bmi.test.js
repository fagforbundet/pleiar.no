/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('reactstrap');
import NutriCalcBMI from '../src/react-components/nutricalc-bmi';
import { changeFieldValue, prepareEnv, checkSnapshot, getComponentWith, getRealRedux } from './test-helpers';
import { toggleRounding, toggleFormulaVisibility } from '../src/actions/global';
import { setHeight } from '../src/actions/nutricalc-bmi';
import { setWeight } from '../src/actions/nutricalc-shared.js';

const getResultFromComp = (comp) =>
{
    const resultElement = comp.find('#nutriCalcBMIResult');
    expect( resultElement ).toHaveLength(1);
    const result = resultElement.text().replace(/\s/g,'').replace(/,/g,'.');
    return Number.parseFloat(result);
};

prepareEnv();

test('Basic rendering of NutriCalc BMI calculator', () => {
    checkSnapshot(NutriCalcBMI,{ redux: true });
    expect(global.__title).toMatch('Ernæringskalkulator - BMI');
});

test("redux store", () => {
    const store = getRealRedux();
    // Default state
    expect( store.getState().nutriCalc.bmi ).toMatchObject({
        height: 180
    });
    expect( store.getState().nutriCalc.shared ).toMatchObject({
        weight: 75,
    });

    store.dispatch(setWeight("50"));
    expect( store.getState().nutriCalc.shared.weight ).toBe("50");

    store.dispatch(setHeight("175"));
    expect( store.getState().nutriCalc.bmi.height ).toBe("175");
});

test('NutriCalcBMI calculations', () => {
    const {component, store} = getComponentWith(NutriCalcBMI,{
        redux: true,
        includeReduxStore: true,
        enzyme: true
    });

    // By default we have 4 calculator description lines (two headers, two descriptions)
    expect(component.find('.calculator-descriptions small .row')).toHaveLength(4);

    expect( getResultFromComp(component) ).toBe(23.1);

    store.dispatch(setWeight("50"));
    expect( getResultFromComp(component) ).toBe(15.4);

    store.dispatch(setHeight("160"));
    expect( getResultFromComp(component) ).toBe(19.5);

    store.dispatch(toggleRounding());
    expect( getResultFromComp(component) ).toBe(19.531249999999996);
    store.dispatch(toggleRounding());

    // Enable formulas
    store.dispatch(toggleFormulaVisibility());
    // (also toggle rounding off since the last line will be rounding if not)
    store.dispatch(toggleRounding());
    expect( component.find('.calculator-descriptions small .row').last().find('.formula').last().text() ).toMatch(/Vekt.*Høyde.*Høyde/);

    expect( component.find('.invisible') ).toHaveLength(0);
    expect( component.find('.calculator-descriptions') ).toHaveLength(1);
    store.dispatch(setWeight("0"));
    expect( getResultFromComp(component) ).toBe(0);
    expect( component.find('.calculator-descriptions') ).toHaveLength(0);
    expect( component.find('.invisible') ).toHaveLength(1);
});

test('NutriCalcBMI interpretations', () => {
    const {component, store} = getComponentWith(NutriCalcBMI,{
        redux: true,
        includeReduxStore: true,
        enzyme: true
    });

    const resultElement = component.find('#nutriCalcBMInterp');
    expect(resultElement).toHaveLength(1);
    expect( resultElement.text() ).toMatch(/normalvektig/);
    store.dispatch(setWeight("50"));
    expect( resultElement.text() ).toMatch(/undervektig/);
    store.dispatch(setWeight("85"));
    expect( resultElement.text() ).toMatch(/overvektig/);
    store.dispatch(setWeight("102"));
    expect( resultElement.text() ).toMatch(/fedme, grad 1/);
    store.dispatch(setWeight("120"));
    expect( resultElement.text() ).toMatch(/fedme, grad 2/);
    store.dispatch(setWeight("140"));
    expect( resultElement.text() ).toMatch(/fedme, grad 3/);
});

test('UI interaction', () => {
    const {component, store} = getComponentWith(NutriCalcBMI,{
        redux: true,
        includeReduxStore: true,
        enzyme: true
    });

    changeFieldValue(component.find('#weight').first(),200);
    expect(store.getState().nutriCalc.shared.weight).toBe(200);

    changeFieldValue(component.find('#height').first(),100);
    expect(store.getState().nutriCalc.bmi.height).toBe(100);
});
