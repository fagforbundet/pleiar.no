/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import MedSearcher from '../src/med-synonym-searcher';
import fest from '../data/fest.json';
import { getFakeMedSearcherData } from './test-helpers';

test('Inheritance', () => {
    expect(MedSearcher).toHaveProperty('onInitialize');
    expect(MedSearcher).toHaveProperty('initialize');
    expect(MedSearcher).toHaveProperty('hasInitialized');
});

test('Initialization', () => {
    expect(MedSearcher.fest).toBe(null);
    expect( () => {
        MedSearcher._loadedData( fest );
    }).not.toThrow();
    expect(MedSearcher.fest).not.toBe(null);
});

describe('_searchMatch', () => {
    test('Base return value', () => {
        expect(MedSearcher._searchMatch(['test'], { name: 'Hello world' })).toMatchObject({
            points: 0,
            hit: false,
        });
    });
    test('Simple match', () => {
        expect(MedSearcher._searchMatch(['test'], { name: 'test med' })).toMatchObject({
            points: 3,
            hit: true,
        });
    });
    test('Simple match, insensitive', () => {
        expect(MedSearcher._searchMatch(['test'], { name: 'tEsT med' })).toMatchObject({
            points: 3,
            hit: true,
        });
    });
    test('Non-start match', () => {
        expect(MedSearcher._searchMatch(['est'], { name: 'test med' })).toMatchObject({
            points: 1,
            hit: true,
        });
    });
    test('Multi match', () => {
        expect(MedSearcher._searchMatch(['test'], { name: 'test med' })).toMatchObject({
            points: 3,
            hit: true,
        });
        expect(MedSearcher._searchMatch(['med'], { name: 'test med' })).toMatchObject({
            points: 1,
            hit: true,
        });
        expect(MedSearcher._searchMatch(['test','med'], { name: 'test med' })).toMatchObject({
            points: 4,
            hit: true,
        });
    });
    test('Non-start multi match', () => {
        expect(MedSearcher._searchMatch(['est'], { name: 'test med' })).toMatchObject({
            points: 1,
            hit: true,
        });
        expect(MedSearcher._searchMatch(['med'], { name: 'test med' })).toMatchObject({
            points: 1,
            hit: true,
        });
        expect(MedSearcher._searchMatch(['est','med'], { name: 'test med' })).toMatchObject({
            points: 2,
            hit: true,
        });
    });
    test('Matches one word but not the other', () => {
        expect(MedSearcher._searchMatch(['test'], { name: 'test med' })).toMatchObject({
            points: 3,
            hit: true,
        });
        expect(MedSearcher._searchMatch(['nope'], { name: 'test med' })).toMatchObject({
            points: 0,
            hit: false,
        });
        expect(MedSearcher._searchMatch(['test','nope'], { name: 'test med' })).toMatchObject({
            points: 0,
            hit: false,
        });
    });
});

describe('With fake data', () => {
    test('Urasaring', () => {
        MedSearcher._loadedData( getFakeMedSearcherData() );
        let results;
        expect( () => {
            results = MedSearcher.search('Urasaring');
        }).not.toThrow();
        expect(results).toMatchObject({
            with: expect.any(Array),
            without: expect.any(Array),
            length: expect.any(Number)
        });

        expect(results.with.length).toBe(1);
        expect(results.without.length).toBe(0);
        const entry = results.with.shift();

        expect(entry).toMatchObject({
            name: "Urasaring 100 mg",
            points: 3,
            synonyms: [ 'Ursaring 100 mg', 'Nincada 100 mg' ],
            atc: 'TEST'
        });
    });
    test('Ursaring', () => {
        MedSearcher._loadedData( getFakeMedSearcherData() );
        const results = MedSearcher.search('Ursaring');

        expect(results.with.length).toBe(1);
        expect(results.without.length).toBe(0);
        const entry = results.with.shift();

        expect(entry).toMatchObject({
            name: "Ursaring 100 mg",
            points: 3,
            synonyms: [ 'Urasaring 100 mg', 'Nincada 100 mg' ],
            atc: 'TEST'
        });
    });
    test('Nincada', () => {
        MedSearcher._loadedData( getFakeMedSearcherData() );
        const results = MedSearcher.search('Ursaring');

        expect(results.with.length).toBe(1);
        expect(results.without.length).toBe(0);
        const entry = results.with.shift();

        expect(entry).toMatchObject({
            name: "Ursaring 100 mg",
            points: 3,
            synonyms: [ 'Urasaring 100 mg', 'Nincada 100 mg' ],
            atc: 'TEST'
        });
    });
    test('Vulpix', () => {
        MedSearcher._loadedData( getFakeMedSearcherData() );
        const results = MedSearcher.search('Vulpix');

        expect(results.with.length).toBe(0);
        expect(results.without.length).toBe(1);
        const entry = results.without.shift();

        expect(entry).toMatchObject({
            name: "Vulpix 200 mg",
            points: 3,
            atc: "TEST",
            synonyms: expect.any(Array),
        });
        expect(entry.synonyms.length).toBe(0);
    });
    test('Azurill', () => {
        MedSearcher._loadedData( getFakeMedSearcherData() );
        const results = MedSearcher.search('Azurill');

        expect(results.with.length).toBe(2);
        expect(results.without.length).toBe(1);

        expect(results.with[0]).toMatchObject({
            name: "Azurill 20 mg",
            points: 3,
            atc: "TEST-0-1",
            synonyms: expect.arrayContaining(['Gliscor 20 mg']),
        });
        expect(results.with[0].synonyms.length).toBe(1);

        expect(results.with[1]).toMatchObject({
            name: "Azurill 10 mg",
            points: 3,
            atc: "TEST-0-1",
            synonyms: expect.arrayContaining(['Gliscor 10 mg']),
        });
        expect(results.with[1].synonyms.length).toBe(1);

        expect(results.without[0]).toMatchObject({
            name: "Azurill 40 mg",
            points: 3,
            atc: 'TEST',
            synonyms: expect.any(Array),
        });
        expect(results.without[0].synonyms.length).toBe(0);
    });
});

describe('With real data', () => {
    const withAndWithoutSynonyms = [ 'Paracet', 'Panodil' ];
    const withSynonyms = [ 'Venlafaxin', 'Metoprolol', 'Cetirizin', 'Ibux', 'Ondansetron', 'Oxynorm', 'Cefotaxim', 'Oxycontin', 'Morfin', 'Naproxen', 'Sarotex', 'Relpax', 'Ery-Max', 'Symbicort', 'Montelukast', 'Albyl-E', 'Allopur', 'Dalacin', 'Clindamycin', 'Tadalafil', 'Cialis', 'Sumatriptan' ];
    const shouldHaveResults = [ 'Sobril', 'Vincristine', 'Bacimycin', 'Lidocaine', 'Atenolol', 'Zoladex', 'Penicillin', 'Nitroglycerin'];
    test.each(withAndWithoutSynonyms)('Should have results with and without synonyms: %s', (syn) => {
        MedSearcher._loadedData( fest );
        let results;
        expect( () => {
            results = MedSearcher.search(syn);
        }).not.toThrow();
        expect(results).toMatchObject({
            with: expect.any(Array),
            without: expect.any(Array),
            length: expect.any(Number)
        });
        expect(results.with.length).toBeGreaterThan(0);
        expect(results.without.length).toBeGreaterThan(0);
        expect(results.with.length).toBeLessThan(20);
        expect(results.without.length).toBeLessThan(20);
        expect(results.length).toBe( results.with.length + results.without.length );

        for(const entry of results.with)
        {
            expect(entry).toMatchObject({
                name: expect.any(String)
            });
            if(entry.exchangeGroup !== undefined)
            {
                expect(entry.exchangeGroup).toBe(expect.any(Number));
            }
        }

        for(const entry of results.without)
        {
            expect(entry).toMatchObject({
                name: expect.any(String)
            });
            if(entry.exchangeGroup !== undefined)
            {
                expect(entry.exchangeGroup).toBe(expect.any(Number));
            }
        }
    });
    test.each(withSynonyms)('Should have results with synonyms: %s', (syn) => {
        MedSearcher._loadedData( fest );
        let results;
        expect( () => {
            results = MedSearcher.search(syn);
        }).not.toThrow();
        expect(results).toMatchObject({
            with: expect.any(Array),
            without: expect.any(Array),
            length: expect.any(Number)
        });
        expect(results.with.length).toBeGreaterThan(0);
        expect(results.with.length).toBeLessThan(20);
        expect(results.length).toBe( results.with.length + results.without.length );

        for(const entry of results.with)
        {
            expect(entry).toMatchObject({
                name: expect.any(String)
            });
            if(entry.exchangeGroup !== undefined)
            {
                expect(entry.exchangeGroup).toBe(expect.any(Number));
            }
        }
    });
    test.each(shouldHaveResults)('Should have some sort of results: %s', (syn) => {
        MedSearcher._loadedData( fest );
        let results;
        expect( () => {
            results = MedSearcher.search(syn);
        }).not.toThrow();
        expect(results).toMatchObject({
            with: expect.any(Array),
            without: expect.any(Array),
            length: expect.any(Number)
        });
        expect(results.with.length + results.without.length ).toBeGreaterThan(0);
        expect(results.with.length + results.without.length ).toBeLessThan(20);
        expect(results.length).toBe( results.with.length + results.without.length );

        for(const entry of results.with)
        {
            expect(entry).toMatchObject({
                name: expect.any(String)
            });
            if(entry.exchangeGroup !== undefined)
            {
                expect(entry.exchangeGroup).toBe(expect.any(Number));
            }
        }

        for(const entry of results.without)
        {
            expect(entry).toMatchObject({
                name: expect.any(String)
            });
            if(entry.exchangeGroup !== undefined)
            {
                expect(entry.exchangeGroup).toBe(expect.any(Number));
            }
        }
    });
});

describe('Initialization data format tests', () => {
    test('Too new data version', () => {
        expect( () => {
            MedSearcher._loadedData({
                exchangeList: [],
                drugs: [],
                fVer: 999,
                fCompV: 999,
            });
        }).toThrow();
    });
    test('New version with compatibility', () => {
        expect( () => {
            MedSearcher._loadedData({
                exchangeList: [],
                drugs: [],
                fVer: 999,
                fCompV: 1,
            });
        }).not.toThrow();
    });
    test('Missing data version', () => {
        expect( () => {
            MedSearcher._loadedData({
                exchangeList: [],
                drugs: [],
            });
        }).toThrow();
    });
    test('Invalid data version', () => {
        expect( () => {
            MedSearcher._loadedData({
                exchangeList: [],
                drugs: [],
                fVer: "not working",
                fCompV: "hello",
            });
        }).toThrow();
    });
});

describe('ATC detection', () => {
    test.each(fest.drugs)('Should not detect medication as ATC code: %s', (drug) => {
        const firstName = drug.name.replace(/^(\S+)\s+.*/,'$1');
        expect( MedSearcher.looksLikeATC( drug.name ) ).toBe(false);
        expect( MedSearcher.looksLikeATC( firstName ) ).toBe(false);
        for(let no = 1; no <= 3; no++)
        {
            expect( MedSearcher.looksLikeATC( firstName.substr(0,no) ) ).toBe(false);
        }
    });

    // Build a list of ATC codes
    const ATCCodes = {};

    for(const drug of fest.drugs)
    {
        /** The ATC code */
        let drugATC = drug.atc;

        /** If we have no ATC code directly on the drug, fetch it from the synonyms list */
        if(drugATC === undefined)
        {
            /** Exchange groups for drug, if any */
            const exchangeGroup = drug.exchangeGroup;
            /* The exchangeGroup should be a number, and we skip this entry
             * if we've already processed the same exchangeGroup */
            if(typeof(exchangeGroup) === 'number')
            {
                /** A list of synonyms for this drug */
                const synonyms = fest.exchangeList[exchangeGroup];
                /** ATC code for the synonym list */
                drugATC = synonyms.atc;
            }
        }
        ATCCodes[ drugATC ] = true;
    }

    test.each(Object.keys(ATCCodes))('Should detect ATC code as such: %s', (drugATC) => {
            expect( MedSearcher.looksLikeATC( drugATC) ).toBe(true);
    });

    test('Forced detection', () => {
        expect( MedSearcher.looksLikeATC('ATC: Hello World') ).toBe(true);
        expect( MedSearcher.looksLikeATC('ATC Hello World') ).toBe(true);
        expect( MedSearcher.looksLikeATC('ATCHello World') ).toBe(true);
        expect( MedSearcher.looksLikeATC('aTcHello World') ).toBe(true);
        expect( MedSearcher.looksLikeATC('acHello World') ).toBe(false);
    });

    test('Invalid parameters', () => {
        expect( MedSearcher.looksLikeATC('') ).toBe(false);
        expect( MedSearcher.looksLikeATC(null) ).toBe(false);
        expect( MedSearcher.looksLikeATC(undefined) ).toBe(false);
    });
});
