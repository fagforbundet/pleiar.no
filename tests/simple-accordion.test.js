/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import SimpleAccordion from '../src/react-components/shared/simple-accordion';
import { getComponentWith, checkSnapshot } from './test-helpers';
import { Collapse } from 'reactstrap';

const fakeAccordion = [
    {
        header: "Element one",
        render: "Render one",
    },
    {
        header: "Element two",
        render: "Render two",
    },
    {
        header: "Element three",
        render: "Render three",
    },
    {
        header: "Element four",
        render: "Render four",
    },
];

test('SimpleAccordion basic rendering', () =>
{
    checkSnapshot(<SimpleAccordion elements={fakeAccordion} />);
});

test('SimpleAccordion with react elements', () =>
{
    const elements = [
        {
            header: "One",
            render: <div className="this-is-rendered">true</div>
        }
    ];
    const accordion = getComponentWith(<SimpleAccordion elements={elements} />, { enzyme: true });
    expect(accordion.find('.this-is-rendered')).toHaveLength(1);
});

test('SimpleAccordion functionality', () => {
    const accordion = getComponentWith(<SimpleAccordion elements={fakeAccordion} />, { enzyme: true });
    expect(accordion.find(Collapse)).toHaveLength(fakeAccordion.length);
    expect(accordion.find('.state-open')).toHaveLength(0);

    accordion.find('.AccordionHeader').at(0).simulate('click');
    expect(accordion.find('.state-open')).toHaveLength(1);
    expect(accordion.find('.AccordionHeader').at(0).is('.state-open')).toBeTruthy();
    expect(accordion.find(Collapse).at(0).props().isOpen).toBe(true);
    expect(accordion.find(Collapse).at(1).props().isOpen).toBe(false);

    accordion.find('.AccordionHeader').at(2).simulate('click');
    expect(accordion.find('.state-open')).toHaveLength(1);
    expect(accordion.find('.AccordionHeader').at(2).is('.state-open')).toBeTruthy();
    expect(accordion.find(Collapse).at(2).props().isOpen).toBe(true);
    expect(accordion.find(Collapse).at(1).props().isOpen).toBe(false);
    expect(accordion.find(Collapse).at(0).props().isOpen).toBe(false);

    accordion.find('.AccordionHeader').at(2).simulate('click');
    expect(accordion.find('.state-open')).toHaveLength(0);
    expect(accordion.find(Collapse).at(2).props().isOpen).toBe(false);
});

test('SimpleAccordion with alwaysOpen=true', () => {
    const accordion = getComponentWith(<SimpleAccordion elements={fakeAccordion} alwaysOpen={true} />, { enzyme: true });
    expect(accordion.find('.state-open')).toHaveLength(4);
    expect(accordion.find('.AccordionHeader').at(0).is('.state-open')).toBeTruthy();
    expect(accordion.find('.AccordionHeader').at(1).is('.state-open')).toBeTruthy();
    expect(accordion.find('.AccordionHeader').at(2).is('.state-open')).toBeTruthy();
    expect(accordion.find('.AccordionHeader').at(3).is('.state-open')).toBeTruthy();
    expect(accordion.find('.always-open')).toHaveLength(1);
});
