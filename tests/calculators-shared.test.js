/**
 * @prettier
 */
/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019-2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 * @flow
 */

import {
    CalcExplanation,
    CalcExplanationLine,
    CalcExplanationDescription,
    CalcExplanationCalculation,
    CalcExplanationFormula,
    Fraction,
    FractionResult,
    FractionComponent,
} from "../src/react-components/calculators-shared";
import { getComponentWith, checkSnapshot } from "./test-helpers";
import { toggleRounding, toggleFormulaVisibility } from "../src/actions/global";
import { Input } from "reactstrap";
import React from "react";

const compSettingsWRedux = {
    redux: {
        wrap: true,
    },
};

describe("FractionResult", () => {
    test("Base rendering", () => {
        checkSnapshot(<FractionResult>Test</FractionResult>);
    });
});
describe("Fraction", () => {
    test("Base rendering", () => {
        checkSnapshot(<Fraction top="top" bottom="bottom" />);
    });
});

describe("FractionComponent", () => {
    test("Base rendering", () => {
        checkSnapshot(<FractionComponent>Test</FractionComponent>);
    });
});

describe("CalcExplanationFormula", () => {
    test("Base rendering", () => {
        checkSnapshot(
            <CalcExplanationFormula>Test</CalcExplanationFormula>,
            compSettingsWRedux,
        );
    });
});

describe("CalcExplanationCalculation", () => {
    test("Base rendering", () => {
        checkSnapshot(
            <CalcExplanationCalculation>Test</CalcExplanationCalculation>,
            compSettingsWRedux,
        );
    });
});

describe("CalcExplanationDescription", () => {
    test("Base rendering", () => {
        checkSnapshot(
            <CalcExplanationDescription>Test</CalcExplanationDescription>,
            compSettingsWRedux,
        );
    });
});

describe("CalcExplanationLine", () => {
    test("Base rendering", () => {
        checkSnapshot(<CalcExplanationLine>Test</CalcExplanationLine>);
    });
});

describe("CalcExplanation", () => {
    test("Redux changes", () => {
        const { component, store } = getComponentWith(
            <CalcExplanation hasAdditionalCalculations={false}>
                <CalcExplanationLine key="bmi">
                    <CalcExplanationDescription>
                        Uterkning av BMI (kg/m²)
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        <Fraction top="top" bottom="bottom" />
                        <FractionResult>= bmi</FractionResult>
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula>
                        <Fraction top="Vekt" bottom="Høyde × Høyde" />
                    </CalcExplanationFormula>
                </CalcExplanationLine>
            </CalcExplanation>,
            {
                redux: true,
                includeReduxStore: true,
                enzyme: true,
            },
        );
        expect(
            component.find(".additionalCalculationsToggle input"),
        ).toHaveLength(0);

        expect(component.find(".roundingToggle").find(Input)).toHaveLength(1);
        expect(
            component.find(".roundingToggle").find(Input).prop("checked"),
        ).toBeFalsy();

        expect(
            component.find(".formulaVisibilityToggle").find(Input),
        ).toHaveLength(1);
        expect(
            component
                .find(".formulaVisibilityToggle")
                .find(Input)
                .prop("checked"),
        ).toBeFalsy();

        store.dispatch(toggleRounding());
        expect(component.find(".roundingToggle").find(Input)).toBeTruthy();

        store.dispatch(toggleFormulaVisibility());
        expect(
            component
                .find(".formulaVisibilityToggle")
                .find(Input)
                .prop("checked"),
        ).toBeTruthy();
    });
    test("mobileOverflowScrolling", () => {
        const { component, store } = getComponentWith(
            <CalcExplanation hasAdditionalCalculations={false}>
                <CalcExplanationLine key="bmi">
                    <CalcExplanationDescription>
                        Uterkning av BMI (kg/m²)
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        <Fraction top="top" bottom="bottom" />
                        <FractionResult>= bmi</FractionResult>
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula>
                        <Fraction top="Vekt" bottom="Høyde × Høyde" />
                    </CalcExplanationFormula>
                </CalcExplanationLine>
            </CalcExplanation>,
            {
                redux: true,
                includeReduxStore: true,
                enzyme: true,
            },
        );
        expect(component.find(".mobileOverflowScrolling")).toHaveLength(0);
        store.dispatch(toggleFormulaVisibility());
        expect(component.find(".mobileOverflowScrolling")).toHaveLength(1);
    });
    describe("additionalCalculations", () => {
        test("additionalCalculations=false", () => {
            const { component } = getComponentWith(
                <CalcExplanation
                    hasAdditionalCalculations={true}
                    additionalCalculationsToggle={() => {}}
                >
                    Test
                </CalcExplanation>,
                {
                    redux: true,
                    includeReduxStore: true,
                    enzyme: true,
                },
            );
            expect(
                component.find(".additionalCalculationsToggle"),
            ).toHaveLength(1);
            expect(
                component
                    .find(".additionalCalculationsToggle")
                    .find(Input)
                    .prop("checked"),
            ).toBeFalsy();
        });
        test("additionalCalculations=true", () => {
            const { component } = getComponentWith(
                <CalcExplanation
                    hasAdditionalCalculations={true}
                    additionalCalculations={true}
                    additionalCalculationsToggle={() => {}}
                >
                    Test
                </CalcExplanation>,
                {
                    redux: true,
                    includeReduxStore: true,
                    enzyme: true,
                },
            );
            expect(
                component.find(".additionalCalculationsToggle"),
            ).toHaveLength(1);
            expect(
                component.find(".additionalCalculationsToggle").find(Input),
            ).toHaveLength(1);
            expect(
                component
                    .find(".additionalCalculationsToggle")
                    .find(Input)
                    .prop("checked"),
            ).toBeTruthy();
        });
        test("interaction", () => {
            const onChange = jest.fn();
            const { component } = getComponentWith(
                <CalcExplanation
                    hasAdditionalCalculations={true}
                    additionalCalculations={true}
                    additionalCalculationsToggle={onChange}
                >
                    Test
                </CalcExplanation>,
                {
                    redux: true,
                    includeReduxStore: true,
                    enzyme: true,
                },
            );
            expect(onChange).not.toHaveBeenCalled();
            component
                .find(".additionalCalculationsToggle input")
                .first()
                .simulate("change");
            expect(onChange).toHaveBeenCalled();
        });
    });
    test("interaction", () => {
        const { component, store } = getComponentWith(
            <CalcExplanation hasAdditionalCalculations={true}>
                <CalcExplanationLine key="bmi">
                    <CalcExplanationDescription>
                        Uterkning av BMI (kg/m²)
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        <Fraction top="top" bottom="bottom" />
                        <FractionResult>= bmi</FractionResult>
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula>
                        <Fraction top="Vekt" bottom="Høyde × Høyde" />
                    </CalcExplanationFormula>
                </CalcExplanationLine>
            </CalcExplanation>,
            {
                redux: true,
                includeReduxStore: true,
                enzyme: true,
            },
        );

        expect(store.getState().global).toMatchObject({
            formulaVisibility: false,
            round: true,
        });

        component
            .find(".calculator-descriptions .roundingToggle input")
            .first()
            .simulate("change");

        expect(store.getState().global).toMatchObject({
            formulaVisibility: false,
            round: false,
        });

        component
            .find(".calculator-descriptions .formulaVisibilityToggle input")
            .first()
            .simulate("change");

        expect(store.getState().global).toMatchObject({
            formulaVisibility: true,
            round: false,
        });
    });
});
