/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import { Collapse } from 'reactstrap';
import { getComponentWith, checkSnapshot, checkRoutedSnapshotWithRealRedux, prepareEnv, getRealRedux, silentFunc } from './test-helpers';
import { NEWS, NEWSExplanationLine, NEWSWhatIsInfo, NEWSConnected, BPOptions, pulseOptions, tempOptions, rfOptions, SpO2Options, SpO2OptionsAlternate, consciousnessOptions } from '../src/react-components/news';
import { BPValues, pulseValues, tempValues, RFValues, SpO2Values, consciousnessValues } from  '../src/reducers/news';
import { setNewsBP, setNewsPulse, setNewsTemp, setNewsRF, setNewsSpO2, setNewsConsciousness, toggleNewsO2, toggleNewsAlternateO2Table } from '../src/actions/news';

// We stub ToolContainer so that we don't have to bother with handling
// react-router in all the tests
jest.mock('../src/react-components/tools.js', () => {
    return {
        ToolContainer (props)
        {
            return <div className={"app-"+props.tool}>{props.children}</div>;
        }
    };
});

prepareEnv();

const onChangeDummies = {
    onBPUpdate: jest.fn(),
    onPulseUpdate: jest.fn(),
    onTempUpdate: jest.fn(),
    onRFUpdate: jest.fn(),
    onSpO2Update: jest.fn(),
    onConsciousnessUpdate: jest.fn(),
    onToggleO2: jest.fn(),
    onToggleAlternateSPO2: jest.fn()
};
const zeroPointsSetting = {
    BP: '111-219',
    pulse: '51-90',
    temp: '36.1-38.0',
    RF: '12-20',
    SpO2: 'over95',
    consciousness: 'awake',
    usingO2: false,
};

const componentSettings = {
    enzyme: true
};

describe('Basic rendering', () => {
    test('Default', () => {
        checkSnapshot(<NEWS />);
    });
    test('0 points', () => {
        checkSnapshot(<NEWS {...zeroPointsSetting} {...onChangeDummies} />);
    });
    test('1+ points', () => {
        checkSnapshot(<NEWS BP="101-110" pulse="51-90" temp="36.1-38.0" RF="12-20" SpO2="over95" consciousness="awake" {...onChangeDummies} />);
    });
    test('5+ points', () => {
        checkSnapshot(<NEWS BP="101-110" pulse="41-50" temp="35.1-36.0" RF="9-11" SpO2="92-93" consciousness="awake" {...onChangeDummies} />);
    });
    test('7+ points', () => {
        checkSnapshot(<NEWS BP="101-110" pulse="41-50" temp="38.1-39.0" RF="21-24" SpO2="92-93" consciousness="awake" {...onChangeDummies} />);
    });
    test('Single 3 pointer', () => {
        checkSnapshot(<NEWS {...zeroPointsSetting} consciousness="confused" {...onChangeDummies} />);
    });
    test('3 pointer + 1-pointer', () => {
        checkSnapshot(<NEWS {...zeroPointsSetting} SpO2="92-93" consciousness="confused" {...onChangeDummies} />);
    });
});

describe('Functionality', () => {
    test('Alternate O2-table info-link', () => {
        const component = getComponentWith(<NEWS {...onChangeDummies} />, componentSettings);
        expect(component.find('#whatIsAltO2')).toHaveLength(1);
        expect(component.find('#whatIsAltO2').text()).toBe("Hva er dette?");
        component.find('#whatIsAltO2').simulate('click');
        expect(component.find('#whatIsAltO2')).toHaveLength(1);
        expect(component.find('#whatIsAltO2').text()).not.toBe("Hva er dette?");
        expect(component.find('#whatIsAltO2').text()).toMatch(/Forklaring til alternativ tolkning/);
    });
    test('Alternate O2-table checkbox', () => {
        onChangeDummies.onToggleAlternateSPO2.mockClear();
        const component = getComponentWith(<NEWS alternateO2Table={false} {...onChangeDummies} />, componentSettings);
        const checkbox = component.find('[name="alternateO2Table"]').at(0);
        expect(checkbox.prop('checked')).toBe(false);
        expect(onChangeDummies.onToggleAlternateSPO2).not.toHaveBeenCalled();
        checkbox.simulate('change');
        expect(onChangeDummies.onToggleAlternateSPO2).toHaveBeenCalled();
    });
    test('O2 checkbox', () => {
        onChangeDummies.onToggleO2.mockClear();
        const component = getComponentWith(<NEWS usingO2={false} {...onChangeDummies} />, componentSettings);
        const checkbox = component.find('[name="usingO2"]').at(0);
        expect(checkbox.prop('checked')).toBe(false);
        expect(onChangeDummies.onToggleO2).not.toHaveBeenCalled();
        checkbox.simulate('change');
        expect(onChangeDummies.onToggleO2).toHaveBeenCalled();
    });
});

describe('NEWSExplanationLine', () => {
    test.each([1,2,3])('Score result=%d', (score) => {
        checkSnapshot(<NEWSExplanationLine key="test-key" score={score} explanation="test-explanation" />, componentSettings);
    });
    test('Header syntax without rightStyle', () => {
        checkSnapshot(<NEWSExplanationLine key="test-key" left="left" right="right" style="test-style" />, componentSettings);
    });
    test('Header syntax with rightStyle', () => {
        checkSnapshot(<NEWSExplanationLine key="test-key" left="left" right="right" style="base-style" rightStyle="right-style" />, componentSettings);
    });
    test('Error handling', () => {
        expect( silentFunc(() => {
            getComponentWith(<NEWSExplanationLine key="test-key" score={99} explanation="test-explanation" /> );
        })).toThrow(/unhandled score/);
    });
});

describe('NEWSWhatIsInfo', () => {
    test('Basic rendering', () => {
        checkSnapshot(<NEWSWhatIsInfo />, componentSettings);
    });
    test('Collapse functionality', () => {
        const comp = getComponentWith(<NEWSWhatIsInfo />, componentSettings);
        const instance = comp.instance();
        expect(instance.state.visible).toBe(false);
        expect(comp.find(Collapse).prop('isOpen')).toBe(false);
        comp.find('.likeLink').simulate('click');
        expect(instance.state.visible).toBe(true);
        expect(comp.find(Collapse).prop('isOpen')).toBe(true);
    });
});

describe('Calculation', () => {
    describe('calculateNEWSParameterScore error handling', () => {
        const testStructure = [
            {
                key: 'testCallback',
                resultCalculator: jest.fn(),
            },
            {
                key: 'testNoExplanation',
                score: 3
            },
        ];
        test('Missing calculatorCallbackValue', () => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies}  />,componentSettings);
            expect(() => {
                component.instance().calculateNEWSParameterScore([],[],'test','testCallback',testStructure);
            }).toThrow('resultCalculator found in options but no calculatorCallbackValue provided');
        });
        test('Missing explanation', () => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies}  />,componentSettings);
            expect(() => {
                component.instance().calculateNEWSParameterScore([],[],'test','testNoExplanation',testStructure);
            }).toThrow('No valid explanation provided');
        });
        test('Invalid entry', () => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies}  />,componentSettings);
            expect(() => {
                component.instance().calculateNEWSParameterScore([],[],'test','does-not-exist',testStructure);
            }).toThrow('not found in provided NEWSOptionList');
        });
    });
    describe('Matches expected calculation based on data', () => {
        test.each(BPOptions)('BP option %#', (option) => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies} BP={option.key} />,componentSettings);
            expect(component.instance().calculateNewsScore()).toMatchObject({
                score: option.score
            });
        });
        test.each(pulseOptions)('pulse option %#', (option) => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies} pulse={option.key} />,componentSettings);
            expect(component.instance().calculateNewsScore()).toMatchObject({
                score: option.score
            });
        });
        test.each(tempOptions)('temp option %#', (option) => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies} temp={option.key} />,componentSettings);
            expect(component.instance().calculateNewsScore()).toMatchObject({
                score: option.score
            });
        });
        test.each(rfOptions)('RF option %#', (option) => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies} RF={option.key} />,componentSettings);
            expect(component.instance().calculateNewsScore()).toMatchObject({
                score: option.score
            });
        });
        test('usingO2', () => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies} usingO2={true} />,componentSettings);
            expect(component.instance().calculateNewsScore()).toMatchObject({
                score: 2
            });
        });
        test.each(SpO2Options)('SpO2 (default) option %#', (option) => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies} SpO2={option.key} />,componentSettings);
            expect(component.instance().calculateNewsScore()).toMatchObject({
                score: option.score
            });
        });
        test.each(SpO2OptionsAlternate)('SpO2 (alternate) without O2 option %#', (option) => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies} SpO2={option.key} usingO2={false} alternateO2Table={true} />,componentSettings);
            let score = option.score;
            if(option.resultCalculator)
            {
                const calculated = option.resultCalculator(false);
                expect(calculated).toMatchObject({
                    score: expect.any(Number),
                });
                expect(calculated).not.toMatchObject({
                    score: option.resultCalculator(true).score
                });
                if(calculated.explanation)
                {
                    expect(calculated).toMatchObject({
                        explanation: expect.stringMatching(/\S/),
                    });
                }
                score = calculated.score;
            }
            expect(component.instance().calculateNewsScore()).toMatchObject({
                score
            });
        });
        test.each(SpO2OptionsAlternate)('SpO2 (alternate) with O2 option %#', (option) => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies} SpO2={option.key} usingO2={true} alternateO2Table={true} />,componentSettings);
            let score = option.score;
            if(option.resultCalculator)
            {
                const calculated = option.resultCalculator(true);
                expect(calculated).toMatchObject({
                    score: expect.any(Number),
                });
                expect(calculated).not.toMatchObject({
                    score: option.resultCalculator(false).score
                });
                if(calculated.explanation)
                {
                    expect(calculated).toMatchObject({
                        explanation: expect.stringMatching(/\S/),
                    });
                }
                score = calculated.score;
            }
            expect(component.instance().calculateNewsScore()).toMatchObject({
                // +2 for the extra points for O2
                score: score+2
            });
        });
        test.each(consciousnessOptions)('consciousness option %#', (option) => {
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies} consciousness={option.key} />,componentSettings);
            expect(component.instance().calculateNewsScore()).toMatchObject({
                score: option.score
            });
        });
    });
    describe('Test cases', () => {
        const testCases = [
            {
                BP: '91-100',
                pulse: 'under41',
                expectedScore: 5
            },
            // Example from https://nhsconnect.github.io/FHIR-NEWS2/index.html
            {
                RF              : '21-24',     // 21
                SpO2            : '92-93',     // 93%
                alternateO2Table: false,       // Default scale
                usingO2         : false,       // On air
                BP              : '111-219',   // 120
                pulse           : '91-110',    // 95
                temp            : '38.1-39.0', // 38.5
                consciousness   : 'awake',     // Alert
                expectedScore   : 6,
            },
        ];
        test.each(testCases)('test %j', (testCase) => {
            const { expectedScore, ...props } = testCase;
            const component = getComponentWith(<NEWS {...zeroPointsSetting} {...onChangeDummies} {...props} />,componentSettings);
            expect(component.instance().calculateNewsScore()).toMatchObject({
                score: expectedScore
            });
        });
    });
});

describe('Redux', () => {
    test('Default state', () => {
        const store = getRealRedux();
        expect(store.getState().news).toMatchObject({
            BP              : null,
            pulse           : null,
            temp            : null,
            RF              : null,
            SpO2            : null,
            consciousness   : null,
            usingO2         : false,
            alternateO2Table: false,
        });
    });
    test.each(Object.keys(BPValues))('BP value %s', (value) => {
        const store = getRealRedux();
        expect(store.getState().news).toMatchObject({
            BP: null
        });
        store.dispatch(setNewsBP(value));
        expect(store.getState().news).toMatchObject({
            BP: value
        });
        // The key should equal its value
        expect(BPValues[value]).toBe(value);
    });
    test.each(Object.keys(pulseValues))('pulse value %s', (value) => {
        const store = getRealRedux();
        expect(store.getState().news).toMatchObject({
            pulse: null
        });
        store.dispatch(setNewsPulse(value));
        expect(store.getState().news).toMatchObject({
            pulse: value
        });
        // The key should equal its value
        expect(pulseValues[value]).toBe(value);
    });
    test.each(Object.keys(tempValues))('temp value %s', (value) => {
        const store = getRealRedux();
        expect(store.getState().news).toMatchObject({
            temp: null
        });
        store.dispatch(setNewsTemp(value));
        expect(store.getState().news).toMatchObject({
            temp: value
        });
        // The key should equal its value
        expect(tempValues[value]).toBe(value);
    });
    test.each(Object.keys(RFValues))('RF value %s', (value) => {
        const store = getRealRedux();
        expect(store.getState().news).toMatchObject({
            RF: null
        });
        store.dispatch(setNewsRF(value));
        expect(store.getState().news).toMatchObject({
            RF: value
        });
        // The key should equal its value
        expect(RFValues[value]).toBe(value);
    });
    test.each(Object.keys(SpO2Values))('SpO2 value %s', (value) => {
        const store = getRealRedux();
        expect(store.getState().news).toMatchObject({
            SpO2: null
        });
        store.dispatch(setNewsSpO2(value));
        expect(store.getState().news).toMatchObject({
            SpO2: value
        });
        // The key should equal its value
        expect(SpO2Values[value]).toBe(value);
    });
    test.each(Object.keys(consciousnessValues))('consciousness value %s', (value) => {
        const store = getRealRedux();
        expect(store.getState().news).toMatchObject({
            consciousness: null
        });
        store.dispatch(setNewsConsciousness(value));
        expect(store.getState().news).toMatchObject({
            consciousness: value
        });
        // The key should equal its value
        expect(consciousnessValues[value]).toBe(value);
    });
    test('usingO2', () => {
        const store = getRealRedux();
        expect(store.getState().news).toMatchObject({
            usingO2: false
        });
        store.dispatch(toggleNewsO2());
        expect(store.getState().news).toMatchObject({
            usingO2: true
        });
        store.dispatch(toggleNewsO2());
        expect(store.getState().news).toMatchObject({
            usingO2: false
        });
    });
    test('alternateO2Table', () => {
        const store = getRealRedux();
        expect(store.getState().news).toMatchObject({
            alternateO2Table: false
        });
        store.dispatch(toggleNewsAlternateO2Table());
        expect(store.getState().news).toMatchObject({
            alternateO2Table: true
        });
        store.dispatch(toggleNewsAlternateO2Table());
        expect(store.getState().news).toMatchObject({
            alternateO2Table: false
        });
    });
    test('Invalid parameters', () => {
        expect( () => {
            setNewsBP('invalid');
        }).toThrow(/Invalid.*value/);
        expect( () => {
            setNewsSpO2('invalid');
        }).toThrow(/Invalid.*value/);
        expect( () => {
            setNewsRF('invalid');
        }).toThrow(/Invalid.*value/);
        expect( () => {
            setNewsPulse('invalid');
        }).toThrow(/Invalid.*value/);
        expect( () => {
            setNewsTemp('invalid');
        }).toThrow(/Invalid.*value/);
        expect( () => {
            setNewsConsciousness('invalid');
        }).toThrow(/Invalid.*value/);
    });
});

function getConnectedNEWS ()
{
    const {component,store} = getComponentWith(NEWSConnected,{
        router: true,
        redux: true,
        includeReduxStore: true,
        route: '/verktoy/news',
        previousPath: '/verktoy',
        ...componentSettings
    });
    const instance = component.find(NEWS).at(0).instance();
    return { component, store, instance };
}

describe('Connected NEWS', () => {
    test('Basic rendering', () => {
        checkRoutedSnapshotWithRealRedux('/verktoy/news','/verktoy',NEWSConnected);
    });
    describe("Callbacks", () => {
        const callbackTests = [
            {
                CB: 'onBPUpdate',
                key: 'BP',
                testValue: '111-219'
            },
            {
                CB: 'onPulseUpdate',
                key: 'pulse',
                testValue: '51-90',
            },
            {
                CB: 'onTempUpdate',
                key: 'temp',
                testValue: '36.1-38.0',
            },
            {
                CB: 'onRFUpdate',
                key: 'RF',
                testValue: '12-20',
            },
            {
                CB: 'onSpO2Update',
                key: 'SpO2',
                testValue: 'over95'
            },
            {
                CB: 'onConsciousnessUpdate',
                key: 'consciousness',
                testValue: 'awake'
            },
            {
                CB: 'onToggleO2',
                key: 'usingO2',
                testValue: true,
                default: false
            },
            {
                CB: 'onToggleAlternateSPO2',
                key: 'alternateO2Table',
                testValue: true,
                default: false
            }
        ];
        test.each(callbackTests)("%j", (cbTest) =>
        {
            const { store, instance } = getConnectedNEWS();
            const defaultVal = cbTest.default !== undefined ? cbTest.default : null;
            expect(store.getState().news[cbTest.key]).toBe(defaultVal);
            expect(instance.props[cbTest.key]).toBe(defaultVal);
            expect(instance.props[cbTest.CB]).toBeInstanceOf(Function);
            instance.props[cbTest.CB](cbTest.testValue);
            expect(store.getState().news[cbTest.key]).toBe(cbTest.testValue);
            expect(instance.props[cbTest.key]).toBe(cbTest.testValue);
        });
        test('Toggling the alternate O2 table should reset SpO2', () => {
            const { store, instance } = getConnectedNEWS();
            expect(store.getState().news).toMatchObject({
                alternateO2Table: false,
                SpO2: null
            });
            instance.props.onSpO2Update('over95');
            expect(store.getState().news).toMatchObject({
                alternateO2Table: false,
                SpO2: 'over95'
            });
            instance.props.onToggleAlternateSPO2();
            expect(store.getState().news).toMatchObject({
                alternateO2Table: true,
                SpO2: null
            });
            instance.props.onSpO2Update('over96');
            expect(store.getState().news).toMatchObject({
                alternateO2Table: true,
                SpO2: 'over96',
            });
            instance.props.onToggleAlternateSPO2();
            expect(store.getState().news).toMatchObject({
                alternateO2Table: false,
                SpO2: null
            });
        });
    });
});
