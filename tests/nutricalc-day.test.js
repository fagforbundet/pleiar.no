/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('reactstrap');
import React from 'react';
import Select from 'react-select';

import { prepareEnv, checkSnapshot, getRealRedux, changeFieldValue } from './test-helpers';

import { Collapse } from 'reactstrap';

import { Provider } from 'react-redux';

import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

Enzyme.configure({ adapter: new Adapter() });

import NutriCalcDay from '../src/react-components/nutricalc-day';
import { removeEntry, reset, setEntryValue, toggleFoodList, setMinimum } from '../src/actions/nutricalc-day';

prepareEnv();

const testEntry = {
    name: "Calogen naturell (30 ml)",
    kcal: 135
};

const getNutriCalcDayWithRedux = () =>
{
    const store = getRealRedux();
    const component = Enzyme.mount(
        <Provider store={store}>
            <NutriCalcDay />
        </Provider>
    );

    store.subscribe(() => {
        // It won't auto-update it during testing, so we manually refresh on any event
        component.update();
    });
    return { store, component };
};

const getNumberFromEntry = (entry) =>
{
    expect(entry).toHaveLength(1);
    const result = entry.text().replace(/\s/g,'').replace(/,/g,'.').replace('kcal','').replace('=','').replace('−','-');
    return Number.parseFloat(result);
};

test('Basic rendering of the nutricalc-day calculator', () => {
    checkSnapshot(NutriCalcDay,{enzyme: true, redux: true});
    expect(global.__title).toMatch('Ernæringskalkulator - inntak');
});

test('Displaying the list of foods', () => {
    const {store,component} = getNutriCalcDayWithRedux();
    store.dispatch(toggleFoodList());
    expect(component).toMatchSnapshot();
});

test('Checking calculation in nutricalc-day', () => {
    const {store,component} = getNutriCalcDayWithRedux();
    store.dispatch(setEntryValue(0,"food",testEntry.name));
    store.dispatch(setEntryValue(1,"food",testEntry.name));
    store.dispatch(setEntryValue(2,"food",testEntry.name));

    // We should have four lines (three with entries, one for "new entry")
    expect( component.find('#nutriCalcDayEntries .row') ).toHaveLength(4);
    // The total should be the kcal of the entries * 3
    expect( getNumberFromEntry(component.find('#nutriCalcDayTotal').first()) ).toBe( testEntry.kcal * 3 );

    // Change the number of items for line 0 to 1.5
    store.dispatch(setEntryValue(0,"items",1.5));
    // We should still have four lines
    expect( component.find('#nutriCalcDayEntries .row') ).toHaveLength(4);
    // But the total should now be *3.5
    expect( getNumberFromEntry(component.find('#nutriCalcDayTotal').first()) ).toBe( testEntry.kcal * 3.5 );

    // The total of the first line should be kcal*1.5
    expect( getNumberFromEntry(component.find('#nutriCalcDayEntries .row').first().find('.nutriCalcDayLineTotal')) ).toBe( testEntry.kcal * 1.5 );

    // Change the kcal for line 0 to 150
    store.dispatch(setEntryValue(0,"kcal",150));
    // The total of the first line should be 150*1.5
    expect( getNumberFromEntry(component.find('#nutriCalcDayEntries .row').first().find('.nutriCalcDayLineTotal')) ).toBe( 150 * 1.5 );
    // The summarized total should now be kcal*2+(150*1.5)
    expect( getNumberFromEntry(component.find('#nutriCalcDayTotal').first()) ).toBe( (testEntry.kcal * 2)+(150*1.5) );

    // The total of .nutriCalcDayLineTotal should be the same as #nutriCalcDayTotal
    let lineTotal = 0;
    component.find('#nutriCalcDayEntries .row').forEach( (node) => {
        if(node.find('.nutriCalcDayLineTotal').text() != "")
        {
            lineTotal += getNumberFromEntry(node.find('.nutriCalcDayLineTotal'));
        }
    });
    expect(lineTotal).toBe( getNumberFromEntry(component.find('#nutriCalcDayTotal').first()) );

    // Test that the diff matches the total and the minimum
    let diff = lineTotal-(store.getState().nutriCalc.day.minimum);
    expect(diff).toBe( getNumberFromEntry(component.find('#nutriCalcDayDiff').first()) );
    // And that it updates if we change it
    store.dispatch(setMinimum(1000));
    diff = lineTotal-1000;
    expect(diff).toBe( getNumberFromEntry(component.find('#nutriCalcDayDiff').first()) );
    // And that it renders with a text-secondary diffColor if it is the same as the requirement
    store.dispatch(setMinimum(lineTotal));
    expect( component.find('#nutriCalcDayDiff').first().prop('className') ).toBe('text-secondary');
});

test("redux store", () => {
    const store = getRealRedux();
    // Default state
    expect( store.getState().nutriCalc.day ).toMatchObject({
        isFoodListOpen: false,
        entries: [],
        minimum: 2400
    });
    // Set the minimum to 1000
    store.dispatch(setMinimum(1000));
    // Update should have hit the store
    expect( store.getState().nutriCalc.day.minimum).toBe(1000);

    store.dispatch(toggleFoodList());
    expect( store.getState().nutriCalc.day.isFoodListOpen).toBe(true);

    store.dispatch(setEntryValue(0,"food",testEntry.name));
    store.dispatch(setEntryValue(1,"food",testEntry.name));
    expect(store.getState().nutriCalc.day).toMatchSnapshot();
    store.dispatch(setEntryValue(0,"items",1.5));
    expect(store.getState().nutriCalc.day).toMatchSnapshot();
    store.dispatch(setEntryValue(1,"kcal",190));
    expect(store.getState().nutriCalc.day).toMatchSnapshot();

    store.dispatch(removeEntry(0));
    expect(store.getState().nutriCalc.day.entries).toHaveLength(1);

    store.dispatch(reset());
    expect(store.getState().nutriCalc.day.entries).toHaveLength(0);
});

test('UI interaction', () => {
    const {component,store} = getNutriCalcDayWithRedux();

    // This is the closest we can get to "interacting" with the select component
    // without a full browser. It works well enough for our purposes, as it tests
    // the callback we care about.
    const triggerSelectChange = (rootComponent,selector,newValue) =>
    {
        let comp = rootComponent;
        if(selector !== null)
        {
            comp = rootComponent.find(selector).first();
        }
        comp.find(Select).props().onChange({ name: newValue, value: newValue });
    };

    // Test creating a select entry
    triggerSelectChange(component,'#nutriCalcDayEntries .row',"Kaffe");
    expect(store.getState().nutriCalc.day.entries).toHaveLength(1);
    expect(store.getState().nutriCalc.day.entries[0].food).toBe("Kaffe");
    expect(store.getState().nutriCalc.day.entries[0].kcal).toBe(0);

    // Test changing a select entry
    triggerSelectChange(component,'#nutriCalcDayEntries .row',"Tine meierismør (12g pakke)");
    expect(store.getState().nutriCalc.day.entries).toHaveLength(1);
    expect(store.getState().nutriCalc.day.entries[0].food).toBe("Tine meierismør (12g pakke)");
    expect(store.getState().nutriCalc.day.entries[0].kcal).toBe(89);

    // Test adding another entry
    triggerSelectChange(component.find('#nutriCalcDayEntries .row').at(1),null,"Kaffe");
    expect(store.getState().nutriCalc.day.entries).toHaveLength(2);
    expect(store.getState().nutriCalc.day.entries[0].food).toBe("Tine meierismør (12g pakke)");
    expect(store.getState().nutriCalc.day.entries[0].kcal).toBe(89);
    expect(store.getState().nutriCalc.day.entries[1].food).toBe("Kaffe");
    expect(store.getState().nutriCalc.day.entries[1].kcal).toBe(0);

    // Test changing the number of items
    expect(store.getState().nutriCalc.day.entries[0].items).toBe(1);
    changeFieldValue(component.find('#nutriCalcDayEntries .row .nutriCalcDayItems input').at(0),2);
    expect(store.getState().nutriCalc.day.entries[0].items).toBe("2");
    changeFieldValue(component.find('#nutriCalcDayEntries .row .nutriCalcDayItems input').at(0),"2.5");
    expect(store.getState().nutriCalc.day.entries[0].items).toBe("2.5");

    // Test changing the kcal
    expect(store.getState().nutriCalc.day.entries[0].kcal).toBe(89);
    changeFieldValue(component.find('#nutriCalcDayEntries .row .nutriCalcDayKcalBox input').at(0),200);
    expect(store.getState().nutriCalc.day.entries[0].kcal).toBe("200");

    // Test changing the minimum
    changeFieldValue(component.find('#minimumKcalDay').at(0),50);
    expect( store.getState().nutriCalc.day.minimum).toBe(50);
    // At this point, the UI should also be displaying green for the diff
    expect( component.find('#nutriCalcDayDiff').first().hasClass('text-success') ).toBeTruthy();

    // Test removing an entry
    component.find('#nutriCalcDayEntries .row .remove-icon').at(0).simulate('click');
    expect(store.getState().nutriCalc.day.entries).toHaveLength(1);
    expect(store.getState().nutriCalc.day.entries[0].food).toBe("Kaffe");

    // Test adding a custom entry
    triggerSelectChange(component.find('#nutriCalcDayEntries .row').last(),null,"Annet/Eigendefinert");
    expect(store.getState().nutriCalc.day.entries).toHaveLength(2);
    expect(store.getState().nutriCalc.day.entries[1].food).toBe("");
    changeFieldValue(component.find('#nutriCalcDayEntries .row .nutriCalcDayName input').at(1),"Testing");
    expect(store.getState().nutriCalc.day.entries[1].food).toBe("Testing");

    // Test resetting the UI
    component.find('.reset-button').at(0).simulate('click');
    expect(store.getState().nutriCalc.day.entries).toHaveLength(0);

    // Test showing the food list
    expect( store.getState().nutriCalc.day.isFoodListOpen).toBe(false);
    expect( component.find('#toggleFoodListButton').first().closest('.row').find(Collapse).props().isOpen ).toBeFalsy();
    component.find('#toggleFoodListButton').at(0).simulate('click');
    expect( component.find('#toggleFoodListButton').first().closest('.row').find(Collapse).props().isOpen ).toBeTruthy();
    expect( store.getState().nutriCalc.day.isFoodListOpen).toBe(true);
    component.find('#toggleFoodListButton').at(0).simulate('click');
    expect( store.getState().nutriCalc.day.isFoodListOpen).toBe(false);
    expect( component.find('#toggleFoodListButton').first().closest('.row').find(Collapse).props().isOpen ).toBeFalsy();
});
