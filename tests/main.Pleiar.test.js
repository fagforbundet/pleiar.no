/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('reactstrap');
import React from 'react';
import { Pleiar, OfflinePluginRuntime } from '../src/react-components/main';
import { Alert } from 'reactstrap';
import { checkSnapshot, prepareEnv, getComponentWith, getRealRedux, mockLocation } from './test-helpers';

prepareEnv();

/*
 * We have to mock all toplevel components, because Pleiar is our toplevel
 * component and calls everything underneath it.
 */
jest.mock('../src/react-components/frontpage.js', () => {return () => { return <div>FRONT PAGE</div>; };});
jest.mock('../src/react-components/menu.js', () => {return () => { return <div>MENU</div>; };});
jest.mock('../src/react-components/medcalc.js', () => {return () => { return <div>MEDCALC</div>; };});
jest.mock('../src/react-components/nutricalc.js', () => {return () => { return <div>NUTRICALC</div>; };});
jest.mock('../src/react-components/tools.js', () => {return () => { return <div>TOOLS</div>; };});
jest.mock('../src/react-components/about.js', () => {return () => { return <div>ABOUT</div>; };});
jest.mock('../src/react-components/covid19.js', () => {return () => { return <div>COVID19</div>; };});
jest.mock('../src/react-components/external-resources.js', () => {return () => { return <div>ANDRE SIDER</div>; };});
jest.mock('../src/react-components/global-search.js', () => {return () => { return <div>SOK</div>; };});
jest.mock('../src/react-components/handbook.js', () => {return () => { return <div>HANDBOK</div>; };});
jest.mock('../src/react-components/quickref.js', () => {return () => { return <div>OPPSLAG</div>; };});
jest.mock('../src/react-components/elearn.js', () => {return () => { return <div>ELAERING</div>; };});
jest.mock('../src/react-components/auth.js', () => {return { Authenticator () { return <div>AUTHENTICATOR</div>; }};});
jest.mock('../src/react-components/shared.js', () => { return { Handle404 () { return <div>404</div>; }, ScrollToTopHelper () { return <div>SCROLLTOTOPHELPER</div>; }, };});
jest.mock('../src/workarounds.js', () => {
    return {
        applyBrowserWorkaroundsIfNeeded ()
        {
            return;
        }
    };
});

describe('Pleiar component', () => {
    test('production', () => {
        jsdom.reconfigure({
            url: 'https://www.pleiar.no/'
        });

        global.PLEIAR_ENV = 'production';
        checkSnapshot(<Pleiar store={getRealRedux()} />);
    });
    test('staging', () => {
        jsdom.reconfigure({
            url: 'https://www.pleiar.no/'
        });
        global.PLEIAR_ENV = 'staging';
        checkSnapshot(<Pleiar store={getRealRedux()} />);
    });
});


const routesToCheck = {
    '/':/FRONT PAGE/,
    '/verktoy':/TOOLS/,
    '/om':/ABOUT/,
    '/auth':/AUTHENTICATOR/,
    '/covid19':/COVID19/,
    '/andresider':/ANDRE SIDER/,
    '/sok':/SOK/,
    '/handbok':/HANDBOK/,
    '/oppslag':/OPPSLAG/,
    '/elaering':/ELAERING/,
    '/does-not-exist':/404/,
};

describe('Routing', () => {
    test.each(Object.keys(routesToCheck))("path %p", (path) => {
    global.PLEIAR_ENV = 'production';
        const regex = routesToCheck[path];
        jsdom.reconfigure({
            "url": 'https://www.pleiar.no'+path
        });
        expect(
            JSON.stringify(
                getComponentWith(<Pleiar store={getRealRedux()} />).toJSON()
            )
        ).toMatch(regex);
    });
});

describe('Updates', () => {
    test("Base", () => {
        // We need fake timers to test the "updated message" fallback
        jest.useFakeTimers();

        jsdom.reconfigure({
            url: 'https://www.pleiar.no/'
        });

        const comp = getComponentWith(<Pleiar store={getRealRedux()} />);
        const location = mockLocation();

        // First, when an update is ready (ie. when the OfflinePluginRuntime
        // calls the onUpdateReady callback), we should immediately respond
        // with a call to applyUpdate()
        expect(OfflinePluginRuntime.data.appliedUpdate).toBeFalsy();
        OfflinePluginRuntime.emit('onUpdateReady');
        expect(OfflinePluginRuntime.data.appliedUpdate).toBeTruthy();

        // The update message should not have been displayed (yet)
        expect( JSON.stringify(comp.toJSON()) ).not.toMatch(/Pleiar.no har verta oppdatert./);

        // When path is / we should auto-reload on onUpdated
        OfflinePluginRuntime.emit('onUpdated');
        expect(location.reload).toHaveBeenCalled();

        // We should also have displayed the update message once some time passes
        jest.runAllTimers();
        expect( JSON.stringify(comp.toJSON()) ).toMatch(/Pleiar.no har verta oppdatert./);
    });
    test("Autoreload", () => {

        // Reset the OfflinePluginRuntime installTimes tracker
        OfflinePluginRuntime.data.installTimes = 0;

        // When path is NOT /, we should NOT auto-reload on onUpdated
        const comp = getComponentWith(<Pleiar store={getRealRedux()} />);
        window.location.pathname = '/nutriCalc';

        // Offline plugin installation should have been called once
        expect(OfflinePluginRuntime.data.installTimes).toBe(1);

        location.reload.mockClear();
        OfflinePluginRuntime.emit('onUpdated');
        expect(location.reload).not.toHaveBeenCalled();

        // The update message should have been displayed
        expect( JSON.stringify(comp.toJSON()) ).toMatch(/Pleiar.no har verta oppdatert./);

        // Offline plugin installation should *still* have been called only once
        // (even though displaying the "update" message has forced a re-render)
        expect(OfflinePluginRuntime.data.installTimes).toBe(1);
    });
});

test('UI interaction', () => {
    const location = mockLocation();
    jsdom.reconfigure({
        url: 'https://www.pleiar.no/medCalc'
    });
    global.PLEIAR_ENV = 'production';
    const comp = getComponentWith(<Pleiar store={getRealRedux()} />,{
        enzyme: true
    });
    OfflinePluginRuntime.emit('onUpdated');
    comp.update();
    expect(comp.find(Alert)).toHaveLength(1);
    comp.find('.alert-success').find('.likeLink').simulate('click');
    expect(location.reload).toHaveBeenCalled();
});
