/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { HandbookIndex, HandbookEntry } from '../src/data.handbook';

window.console.log = jest.fn();

test('HandbookIndex base', () => {
    let idx;
    expect( () => {
       idx = new HandbookIndex('Readme');
    }).not.toThrow();
    expect(idx.exists).toBe(true);
});
test('HandbookIndex .value', () => {
    const idx = new HandbookIndex('Readme');
    expect(idx.value).toMatchObject({
        title: expect.stringMatching(/\S/),
        tree: expect.any(Object)
    });
});
test('HandbookIndex .title', () => {
    const idx = new HandbookIndex('Readme');
    expect(idx.title).toBe('README');
});
test('HandbookIndex .url', () => {
    const idx = new HandbookIndex('Readme');
    expect(idx.url).toBe('/handbok/Readme');
});
test('HandbookIndex .tree', () => {
    const idx = new HandbookIndex('Readme');
    expect(idx.tree).toHaveLength(2);
    for(const entry of idx.tree)
    {
        expect(entry).toBeInstanceOf(HandbookEntry);
    }
});
test('HandbookIndex .firstChild', () => {
    const idx = new HandbookIndex('Readme');
    expect(idx.firstChild).toBeInstanceOf(HandbookEntry);
    expect(idx.firstChild.exists).toBe(true);
    const idx2 = new HandbookIndex();
    expect(idx2.firstChild).toBeInstanceOf(HandbookIndex);
    expect(idx2.firstChild.exists).toBe(true);
});
test('HandbookIndex .article', () => {
    const entry = new HandbookIndex('Test_data/Test_section');
    expect(entry.exists).toBe(true);
    expect(entry.article).toBeInstanceOf(HandbookEntry);
});
test('HandbookIndex .article on entry without one', () => {
    const entry = new HandbookIndex('Readme');
    expect(entry.exists).toBe(true);
    expect(entry.article).toBe(null);
});
test('HandbookIndex with invalid entry', () => {
    let idx;
    expect( () => {
        idx = new HandbookIndex('Readme/README');
    }).not.toThrow();
    expect(idx.exists).toBe(false);

    expect( () => {
        idx.title;
    }).toThrow();
    expect( () => {
        idx.url;
    }).toThrow();
    expect( () => {
        idx.tree;
    }).toThrow();
    expect( () => {
        idx.firstChild;
    }).toThrow();
});
test('HandbookIndex with non-existing entry', () => {
    let idx;
    expect( () => {
        idx = new HandbookIndex('path-does-not-exist');
    }).not.toThrow();
    expect(idx.exists).toBe(false);

    expect( () => {
        idx.title;
    }).toThrow();
    expect( () => {
        idx.url;
    }).toThrow();
    expect( () => {
        idx.tree;
    }).toThrow();
    expect( () => {
        idx.firstChild;
    }).toThrow();
    expect( () => {
        idx.article;
    }).toThrow();
});

test('HandbookEntry base', () => {
    let entry;
    expect( () => {
        entry = new HandbookEntry('Readme/README');
    }).not.toThrow();
    expect(entry.exists).toBe(true);
});
test('HandbookEntry .value', () => {
    const entry = new HandbookEntry('Readme/README');
    expect(entry.value).toMatchObject({
        title: expect.stringMatching(/\S/),
        content: expect.stringMatching(/\S/),
        path: expect.arrayContaining( [ expect.stringMatching(/\S/) ]),
    });
});
test('HandbookEntry .title', () => {
    const entry = new HandbookEntry('Readme/README');
    expect(entry.title).toBe('Pleiar.no README');
});
test('HandbookEntry .url', () => {
    const entry = new HandbookEntry('Readme/README');
    expect(entry.url).toBe('/handbok/Readme/README');
});
test('HandbookEntry .url with custom URL', () => {
    const entry = new HandbookEntry('License/COPYING');
    expect(entry.url).toBe('/copying');
});
test('HandbookEntry .toc', () => {
    const entry = new HandbookEntry('Readme/README');
    expect(entry.toc).toMatchSnapshot();
});
test('HandbookEntry .path', () => {
    const entry = new HandbookEntry('Readme/README');
    expect(entry.path).toBeInstanceOf(Array);
    expect(entry.path.join('/')).toBe('Readme/README');
});
test('HandbookEntry .next', () => {
    const entry = new HandbookEntry('Readme/README');
    expect(entry.next).toBeInstanceOf(HandbookEntry);
    const entry2 = new HandbookEntry('License/COPYING');
    expect(entry2.next).toBe(null);
});
test('HandbookEntry .previous', () => {
    const entry = new HandbookEntry('Test_data/Test_section/Subsection');
    expect(entry.previous).toBeInstanceOf(HandbookEntry);
});
test('HandbookEntry with invalid entry', () => {
    let entry;
    expect( () => {
        entry = new HandbookEntry('does-not-exist');
    }).not.toThrow();
    expect(entry.exists).toBe(false);

    expect( () => {
        entry.value;
    }).toThrow();
    expect( () => {
        entry.title;
    }).toThrow();
    expect( () => {
        entry.url;
    }).toThrow();
    expect( () => {
        entry.toc;
    }).toThrow();
    expect( () => {
        entry.path;
    }).toThrow();
    expect( () => {
        entry.next;
    }).toThrow();
    expect( () => {
        entry.previous;
    }).toThrow();
});
test('HandbookEntry with non-existing entry', () => {
    let entry;
    expect( () => {
        entry = new HandbookEntry('path-does-not-exist');
    }).not.toThrow();
    expect(entry.exists).toBe(false);

    expect( () => {
        entry.value;
    }).toThrow();
    expect( () => {
        entry.title;
    }).toThrow();
    expect( () => {
        entry.url;
    }).toThrow();
    expect( () => {
        entry.toc;
    }).toThrow();
    expect( () => {
        entry.path;
    }).toThrow();
    expect( () => {
        entry.next;
    }).toThrow();
    expect( () => {
        entry.previous;
    }).toThrow();
});
