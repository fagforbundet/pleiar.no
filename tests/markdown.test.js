/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import { prepareEnv, checkSnapshot, getComponentWith } from './test-helpers';

import { Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import { MarkdownHeader, MarkdownBasicLink, MarkdownExtendedLinkError, MarkdownExtendedLink, MarkdownExtendedParagraph, MarkdownImg, MarkdownTable, Markdown } from '../src/react-components/markdown';

function Dummy (props)
{
    return props.children;
}

prepareEnv();
const withRouterProps = {
    router: true,
    route: '/',
    previousPath: '/'
};

test('MarkdownHeader', () => {
    // The MarkdownHeader is a bit fragile, it expects its children to always
    // be an array. Thus we create one manually.
    const child = <Dummy key="dummy">Test</Dummy>;
    // Eslint, rightfully, doesn't like this.
    // eslint-disable-next-line react/no-children-prop
    checkSnapshot(<MarkdownHeader level="2" children={[child]} />);
});

describe('MarkdownBasicLink', () => {
    test('external', () => {
        checkSnapshot(<MarkdownBasicLink href="https://www.fagforbundet.no">Test</MarkdownBasicLink>);
    });
    test('mailto', () => {
        checkSnapshot(<MarkdownBasicLink href="mailto:test@example.com">Test</MarkdownBasicLink>);
    });
    test('internal', () => {
        checkSnapshot(<MarkdownBasicLink href="/ordliste">Test</MarkdownBasicLink>,{
            ...withRouterProps,
            enzymeSub: MarkdownBasicLink,
            enzyme: true,
        });
    });
});

test('MarkdownExtendedLinkError', () => {
    checkSnapshot(<MarkdownExtendedLinkError nodeType="tests" nodeParams={["one","two","three"]} message="Hello Error" additionalValues={{value:"one", anotherValue:"two"}} />);
});

test('MarkdownTable', () => {
    const comp = getComponentWith(<MarkdownTable><tbody></tbody></MarkdownTable>, {
        ...withRouterProps,
        enzyme: true,
    });
    expect(comp.find(Table)).toHaveLength(1);
    expect(comp.find("tbody")).toHaveLength(1);
});

describe('MarkdownExtendedLink', () => {
    describe('external object rendering (!)', () => {
        test('!dict', () => {
            checkSnapshot(<MarkdownExtendedLink href="!dict/Afasi"></MarkdownExtendedLink>, {
                ...withRouterProps,
                enzyme: true,
                enzymeSub: MarkdownExtendedLink,
            });
            checkSnapshot(<MarkdownExtendedLink href="!dict/Akuttfaseprotein"></MarkdownExtendedLink>,{
                ...withRouterProps,
                enzyme: true,
                enzymeSub: MarkdownExtendedLink,
            });
            checkSnapshot(<MarkdownExtendedLink href="!dict/C-reaktivt%20protein%20-%20CRP"></MarkdownExtendedLink>,{
                ...withRouterProps,
                enzyme: true,
                enzymeSub: MarkdownExtendedLink,
            });

            const invalid = getComponentWith(<MarkdownExtendedLink href="!dict/does-not-exist"></MarkdownExtendedLink>,{
                enzyme: true,
            });
            expect(invalid.find(MarkdownExtendedLinkError)).toHaveLength(1);
        });
        test('!labValues', () => {
            checkSnapshot(<MarkdownExtendedLink href="!labValues"></MarkdownExtendedLink>);
            checkSnapshot(<MarkdownExtendedLink href="!labValues/Hematologi,%20koagulasjon%20mm."></MarkdownExtendedLink>);

            const invalid = getComponentWith(<MarkdownExtendedLink href="!labValues/does-not-exist"></MarkdownExtendedLink>,{
                enzyme: true,
            });
            expect(invalid.find(MarkdownExtendedLinkError)).toHaveLength(1);
        });
        describe('!handbookIndex*', () =>
        {
            describe('handbookIndexOpen', () => {
                test('valid', () => {
                    checkSnapshot(<MarkdownExtendedLink href="!handbookIndexOpen/Readme"></MarkdownExtendedLink>);
                });
                test('invalid', () => {
                    const invalid = getComponentWith(<MarkdownExtendedLink href="!handbookIndexOpen/does-not-exist"></MarkdownExtendedLink>,{
                        enzyme: true,
                    });
                    expect(invalid.find(MarkdownExtendedLinkError)).toHaveLength(1);
                });
            });
            describe('handbookIndexPlain', () =>
            {
                test('valid', () => {
                    checkSnapshot(<MarkdownExtendedLink href="!handbookIndexPlain/Readme"></MarkdownExtendedLink>, {
                        ...withRouterProps
                    });
                });
                test('invalid', () => {
                    const invalid = getComponentWith(<MarkdownExtendedLink href="!handbookIndexPlain/does-not-exist"></MarkdownExtendedLink>,{
                        enzyme: true,
                    });
                    expect(invalid.find(MarkdownExtendedLinkError)).toHaveLength(1);
                });
            });
        });
        test('invalid', () => {
            const invalid = getComponentWith(<MarkdownExtendedLink href="!invalid/does-not-exist"></MarkdownExtendedLink>,{
                enzyme: true,
            });
            expect(invalid.find(MarkdownExtendedLinkError)).toHaveLength(1);
        });
    });
    describe('!vimeo', () => {
        test('valid', () => {
            const valid = getComponentWith(<MarkdownExtendedLink href="!vimeo/232524527"></MarkdownExtendedLink>,{
                enzyme: true,
            });
            expect(valid.find(MarkdownExtendedLinkError)).toHaveLength(0);
            expect(valid.find('.videoWrapper')).toHaveLength(1);
            expect(valid.find('.videoWrapper > iframe')).toHaveLength(1);
            expect(valid.find('iframe').at(0).prop('src')).toBe("https://player.vimeo.com/video/232524527?title=0&quality=&byline=0&portrait=0&dnt=1");
        });
    });
    describe('!youtube', () => {
        test('valid', () => {
            const valid = getComponentWith(<MarkdownExtendedLink href="!youtube/ZmxLja-DRIw"></MarkdownExtendedLink>,{
                enzyme: true,
            });
            expect(valid.find(MarkdownExtendedLinkError)).toHaveLength(0);
            expect(valid.find('.videoWrapper')).toHaveLength(1);
            expect(valid.find('.videoWrapper > iframe')).toHaveLength(1);
            expect(valid.find('iframe').at(0).prop('src')).toBe("https://www.youtube.com/embed/ZmxLja-DRIw");
        });
    });
    describe('internal link generation and rendering', () => {
        test('Valid internal link, :handbook', () => {
            checkSnapshot(<MarkdownExtendedLink href=":handbook/Readme">Hello world</MarkdownExtendedLink>, {
                ...withRouterProps
            });
        });
        test('Valid internal link, :dict', () => {
            checkSnapshot(<MarkdownExtendedLink href=":dict/Dictionary">Hello world</MarkdownExtendedLink>, {
                ...withRouterProps
            });
        });
        test('Valid internal link, :quickref', () => {
            checkSnapshot(<MarkdownExtendedLink href=":quickref/Sepsis">Hello world</MarkdownExtendedLink>, {
                ...withRouterProps
            });
        });
        test('Valid internal link, :elearn', () => {
            checkSnapshot(<MarkdownExtendedLink href=":elearn/testQuiz">Hello world</MarkdownExtendedLink>, {
                ...withRouterProps
            });
        });
        test('Invalid internal link', () => {
            checkSnapshot(<MarkdownExtendedLink href=":invalid/invalid">Hello world</MarkdownExtendedLink>);
        });
    });
    test('Force external links', () => {
            checkSnapshot(<MarkdownExtendedLink href="+/some-path/file.pdf">Hello world</MarkdownExtendedLink>, {
                ...withRouterProps
            });
    });
});

describe.each([MarkdownBasicLink, MarkdownExtendedLink])('%p with normal links', (Renderer) => {
    const testSettings = {
            enzyme: true,
            router: true,
            route: '/handbok',
            previousPath: '/'
    };
    const testURIs = [ 'https://www.fagforbundet.no','http://www.fagforbundet.no','mailto:test@example.com','tel:0000', '//fagforbundet.no'];
    test.each(testURIs)('%p', (URI) => {
        const comp = getComponentWith(<Renderer href={URI}></Renderer>,testSettings);
        expect(comp.find(Link)).toHaveLength(0);
        expect(comp.find('a').prop('href')).toBe(URI);
    });
    test('internal', () => {
        const comp = getComponentWith(<Renderer href='/verktoy'></Renderer>,testSettings);
        expect(comp.find(Link)).toHaveLength(1);
        expect(comp.find(Link).prop('to')).toBe('/verktoy');
    });
});

test('MarkdownExtendedParagraph', () => {
    checkSnapshot(<MarkdownExtendedParagraph>Test</MarkdownExtendedParagraph>);
});

describe('MarkdownImg', () => {
    test('with alt text', () => {
        checkSnapshot(<MarkdownImg src="/image.png" alt="alt-text" />);
    });
    test('with alt-only text', () => {
        checkSnapshot(<MarkdownImg src="/image.png" alt="#alt-only-text" />);
    });
    test('without alt text', () => {
        checkSnapshot(<MarkdownImg src="/image.png" alt="" />);
    });
});

const markdownTest = "# Hello markdown\n\n[External](https://www.fagforbundet.no)\n[Internal](/ordliste)\n\n[!dict/Afasi](!dict/Afasi)\n\nParagraph\n\n![Image](/image.png)\n\n[Force external link](+/somewhere/file.pdf)";
const markdownTableTest = "| Test | Column |\n|------|--------|\n|One   | Two    |";

test('Markdown basic rendering', () => {
    checkSnapshot(<Markdown content={markdownTest} />, {
        ...withRouterProps,
        enzyme: true,
        enzymeSub: MarkdownExtendedLink,
    });
});

test('Markdown with extended', () => {
    const comp = getComponentWith(<Markdown permitSpecialNodes={true} content={markdownTest} />, {
        ...withRouterProps,
        enzyme: true,
    });
    expect(comp.find(MarkdownExtendedLink)).toHaveLength(4);
});

test('Markdown without extended', () => {
    const comp = getComponentWith(<Markdown permitSpecialNodes={false} content={markdownTest} />, {
        ...withRouterProps,
        enzyme: true,
    });
    expect(comp.find(MarkdownExtendedLink)).toHaveLength(0);
});

test('Markdown with table', () => {
    const comp = getComponentWith(<Markdown permitSpecialNodes={false} content={markdownTableTest} />, {
        ...withRouterProps,
        enzyme: true,
    });
    expect(comp.find(MarkdownTable)).toHaveLength(1);
});
