/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { HandbookEntry } from '../src/data.handbook';
import PleiarSearcher from '../src/searcher.js';
import lunr from 'lunr';
import lunrNO from 'lunr-languages/lunr.no';
import { silentFunc } from './test-helpers.js';
import { testInitPleiarSearcher } from './search-helper.js';

testInitPleiarSearcher();

// Explicitly enable strict, since lunr needs workarounds for strict mode
"use strict;";

test("Basic searching", () =>
{
    const afasiSearch = PleiarSearcher.search("afasi");
    expect( afasiSearch ).toBeInstanceOf( Array );
    expect( afasiSearch ).toHaveLength(2);
    for(const sub of afasiSearch)
    {
        expect( sub ).toMatchObject({
            ref: expect.stringMatching(/^dictionary/),
            score: expect.any(Number)
        });
    }

    const nonExisting = PleiarSearcher.search("this-does-not-exist");
    expect(nonExisting).toBeInstanceOf(Array);
    expect(nonExisting).toHaveLength(0);
});

test("Searching with reducer", () =>
{
    const reducer = jest.fn( (accumulator, result) => {
        if(result.ref.indexOf("dictionary") === 0)
        {
            accumulator.push(result);
        }
        return accumulator;
    });
    let search = PleiarSearcher.search("jest-test-ord");
    expect( search ).toBeInstanceOf( Array );
    expect( search ).toHaveLength(4);

    search = PleiarSearcher.search("jest-test-ord",reducer);
    expect(reducer).toHaveBeenCalled();
    expect( search ).toHaveLength(1);
});

test("Fuzzy searching", () =>
{
    let partialMatch = PleiarSearcher.search("afas");
    expect(partialMatch).toBeInstanceOf(Array);
    expect(partialMatch).toHaveLength(0);

    partialMatch = PleiarSearcher.safePartialSearch("afas");
    expect( partialMatch ).toBeInstanceOf( Array );
    expect( partialMatch ).toHaveLength(2);

    partialMatch = PleiarSearcher.safePartialSearch("Serkom");
    expect( partialMatch ).toBeInstanceOf( Array );
    expect( partialMatch ).toHaveLength(1);
    expect( partialMatch[0] ).toMatchObject({
        ref: "dictionary/3"
    });
});

test("Invalid syntax",() =>
{
    expect( () => {
        PleiarSearcher.search("this -");
    }).toThrow();

    expect( () => {
        PleiarSearcher._safeSearch("this -");
    }).not.toThrow();

    expect( () => {
        PleiarSearcher.safePartialSearch("this -");
    }).not.toThrow();
});

test("Fuzzy generator", () =>
{
    expect( PleiarSearcher._mkFuzzySearch("test",0) ).toBe("*test*");
    expect( PleiarSearcher._mkFuzzySearch("test",1) ).toBe("test~1");
    expect( PleiarSearcher._mkFuzzySearch("test",2) ).toBe("test~2");
    expect( PleiarSearcher._mkFuzzySearch("-test",0) ).toBe("-test");
    expect( PleiarSearcher._mkFuzzySearch("-test",1) ).toBe("-test");
    expect( PleiarSearcher._mkFuzzySearch("-test",2) ).toBe("-test");
    expect( PleiarSearcher._mkFuzzySearch("test me",0) ).toBe("*test* *me*");
    expect( PleiarSearcher._mkFuzzySearch("test me",1) ).toBe("test~1 me~1");
    expect( PleiarSearcher._mkFuzzySearch("test me",2) ).toBe("test~2 me~2");
    expect( PleiarSearcher._mkFuzzySearch("test -me",0) ).toBe("*test* -me");
    expect( PleiarSearcher._mkFuzzySearch("test -me",1) ).toBe("test~1 -me");
    expect( PleiarSearcher._mkFuzzySearch("test -me",2) ).toBe("test~2 -me");
    expect( PleiarSearcher._mkFuzzySearch("test -me",99) ).toBe("test~99 -me");
});

test('Fuzzy search with no results', () => {
    const result = PleiarSearcher.safePartialSearch('thisdoesnotexist');
    expect(result).toHaveLength(0);
});

describe("getResultFromRef", () =>
{
    test("Dictionary", () => {
        expect( PleiarSearcher.getResultFromRef("dictionary/0").dictionary.expression ).toBe("Afasi");
        expect( PleiarSearcher.getResultFromRef("dictionary/3").dictionary.expression ).toBe("Sarkom");
    });
    test("externalResources", () => {
        expect( PleiarSearcher.getResultFromRef("externalResources/0").externalResources.title ).toBe("Felleskatalogen");
        expect( PleiarSearcher.getResultFromRef("externalResources/5").externalResources.title ).toBe("Fagforbundet");
    });
    test("labValue", () => {
        expect( PleiarSearcher.getResultFromRef("labValue/0").labValues.navn).toBe("B12");
    });
    test("tool", () => {
        expect( PleiarSearcher.getResultFromRef("tool/0/0").tool.path).toBe("/medikamentregning/infusjon");
    });
    test("nutrition", () => {
        expect( PleiarSearcher.getResultFromRef("nutrition/Kaffe").nutrition.name).toBe("Kaffi (svart, 1 kopp)");
    });
    describe("handbook", () => {
        test("valid (with metadata)", () => {
            // Note: this is very fragile, we're not actually suppose to be able to
            // supply an empty object as the metadata parameter (normally, flow
            // would catch this). But for the purposes of this test, it is ok.
            const handbook = PleiarSearcher.getResultFromRef("handbook/Readme/README",{}).handbook;
            expect(handbook).toBeInstanceOf(HandbookEntry);
            expect(handbook).toHaveProperty('title','Pleiar.no README');
        });
        test("invalid (without metadata)", () => {
            expect( () => {
                PleiarSearcher.getResultFromRef("handbook/Readme/README");
            }).toThrow(/fetching handbook entries requires metadata/);
        });
    });

    test("Invalid type", () => {
        expect( () => { PleiarSearcher.getResultFromRef("does-not-exist/0"); } ).toThrow();
    });
    test("Invalid handbook entry", () => {
        expect( silentFunc(() => { PleiarSearcher.getResultFromRef("handbook/does-not-exist"); }) ).toThrow('Wanted to look up handbook entry that does not exist');
    });

    test("Invalid tool entry", () => {
        expect( () => { PleiarSearcher.getResultFromRef("tool/999/999"); } ).toThrow(/Wanted to look up tools entry where the category does not exist/);
        expect( () => { PleiarSearcher.getResultFromRef("tool/0/999"); } ).toThrow(/Wanted to look up tools entry where the tools entry does not exist/);
    });
});

describe('Handbook result with metadata', () => {
    const result = PleiarSearcher.search('Pleiar.no', (accumulator, result) => {
        if(result.ref.indexOf("handbook") === 0)
        {
            accumulator.push(result);
        }
        return accumulator;
    });
    test.each(result)('result no %# works', (entry) => {
        let result;
        expect( () => {
            result = PleiarSearcher.getResultFromRef(entry.ref,entry.matchData);
        }).not.toThrow();
        expect(result.handbook.searchSnippet).toBeInstanceOf(Array);
        expect(result.handbook.searchSnippet.length).toBeGreaterThan(0);
    });
});

/*
 * lunr doesn't handle warnings when in strict mode, so we override its function.
 * Running lunrNO() again (will already have been run by PleiarSearcher) will trigger
 * a warning from lunr, which should successfully complete.
 */
test("lunr strict mode warnings", () =>
{
    /* eslint-disable no-console */
    console.log = jest.fn();
    expect( () =>
    {
        lunrNO(lunr);
    }).not.toThrow();
    expect(console.log).toHaveBeenCalled();
});

test('Attempt to call initialize() when already initialized', () => {return new Promise((done) => {
    expect( () => {
        PleiarSearcher.initialize().then(done).catch( (err) => {
            throw('Second initialize() call resulted in error: '+err);
        });
    }).not.toThrow();
});});

test('Uninitialized usage', () => {
    // Manually un-initialize it
    PleiarSearcher.__index = null;
    PleiarSearcher._ALR_initializing = null;
    PleiarSearcher._ALR_onInitialize = [];
    PleiarSearcher._ALR_hasInitialized = null;

    expect( () => {
        PleiarSearcher.search("afasi");
    }).toThrow('PleiarSearcher called without first performing initialization');
});

test('Initialization', () => {return new Promise((done) => {
    // Manually un-initialize it
    PleiarSearcher.__index = null;
    PleiarSearcher._ALR_initializing = null;
    PleiarSearcher._ALR_onInitialize = [];
    PleiarSearcher._ALR_hasInitialized = null;

    expect( () => {
        PleiarSearcher.initialize().then(done);
    }).not.toThrow();
});});
test('Initialization with onInitialize', () => {return new Promise((done) => {
    // Manually un-initialize it
    PleiarSearcher.__index = null;
    PleiarSearcher._ALR_initializing = null;
    PleiarSearcher._ALR_onInitialize = [];
    PleiarSearcher._ALR_hasInitialized = null;

    PleiarSearcher.onInitialize(() => {
        const init = jest.fn();
        PleiarSearcher.onInitialize(init);
        // Should call it immediately since we've already initialized
        expect(init).toHaveBeenCalled();
        done();
    });
    expect( () => {
        PleiarSearcher.initialize();
    }).not.toThrow();
});});

test('Initialization with errors', () => {return new Promise((done) => {
    // Manually un-initialize it
    PleiarSearcher.__index = null;
    PleiarSearcher._ALR_initializing = null;
    PleiarSearcher._ALR_onInitialize = [];
    PleiarSearcher._ALR_hasInitialized = null;
    jest.mock('../data/search-index.json', () => { throw('Error'); });

    expect( () => {
        PleiarSearcher.initialize().then(() => {
            throw('Should not have called then()');
        }).catch((err) => {
            expect(err).toMatch(/search-index.json failed to load/);
            done();
        });
    }).not.toThrow();
});});
