/*
 * @flow
 * @prettier
 *
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import * as React from "react";
import quizManagerHandler from "../src/quiz-manager";
import quizData from "../data/quiz/quiz.json";
import ELearn, {
    ELearnRaw,
    ELearnList,
    Question,
    CourseCompleted,
    Quiz,
} from "../src/react-components/elearn";
import { prepareEnv, checkSnapshot, getComponentWith } from "./test-helpers";
// Used to tell Flow that a value is not null (as an alternative to dozens of
// FlowIgnore statements, longer checks or just disabling flow).
import invariant from "assert";
import type { quizManagerQuestion } from "../src/quiz-manager";
import { quizDataManager } from "../src/quiz-manager";
import { ListGroupItem } from "reactstrap";
import { NotFound } from "../src/react-components/shared";

beforeEach(() => {
    global.PLEIAR_ENV = "production";
    window.scrollTo = jest.fn();
    // $FlowExpectedError[prop-missing]
    quizDataManager._loadedData(quizData);
    // $FlowExpectedError[prop-missing]
    quizDataManager._ALR_hasInitialized = true;
});
prepareEnv();

function getQuizManager() {
    return new quizManagerHandler("testQuiz");
}

function getTestQuizQuestion(question: string, stableSort: boolean = true) {
    const m = getQuizManager();
    let q = m.current;
    while (q === null || q.question !== question) {
        q = m.next();
        if (q === null) {
            throw "Unable to find question: " + question;
        }
    }

    invariant(q);

    if (stableSort) {
        q.testUseStableQuestionSorting();
    }

    return { q, m };
}

function locateCorrectAnswer(
    question: quizManagerQuestion,
    component,
    wants: "component" | "answer" = "component",
    correctID: number = 1,
) {
    let currentCorrectID = 0;
    const entries = component.find(ListGroupItem);
    for (let entryID = 0; entryID < entries.length; entryID++) {
        const entry = entries.at(entryID);
        const questionID = Number.parseInt(
            entry.props()["data-question-id"],
            10,
        );
        const answer = question.possibleAnswers[questionID];
        if (answer.correct === true) {
            currentCorrectID++;
            if (currentCorrectID === correctID) {
                if (wants === "component") {
                    return entry;
                } else {
                    return answer;
                }
            }
        }
    }
    throw (
        "Unable to locate a correct answer that matches correct ID " +
        correctID +
        " (found " +
        currentCorrectID +
        " correct answers)"
    );
}

const componentRouterPath = {
    router: true,
    route: "/elaering/testQuiz",
    previousPath: "/elaering/:learningSet?",
};

describe("CourseCompleted", () => {
    test("No endSlideContent", () => {
        checkSnapshot(<CourseCompleted handler={getQuizManager()} />);
    });
    test("With endSlideContent", () => {
        const handler = getQuizManager();
        // $FlowExpectedError[incompatible-use]
        handler._quiz.endSlideContent = {
            header: "Test end slide",
            markdown: "**Markdown test**",
        };
        checkSnapshot(<CourseCompleted handler={handler} />);
    });
});

describe("Question", () => {
    test("null question", () => {
        checkSnapshot(<Question question={null} />);
    });
    describe("splash screen", () => {
        test("snapshot", () => {
            const { q } = getTestQuizQuestion("1_SPLASH");
            checkSnapshot(<Question question={q} />, {
                ...componentRouterPath,
            });
        });
        test("functionality", () => {
            const { q, m } = getTestQuizQuestion("1_SPLASH");
            const listener = jest.fn();
            m.listen(listener);
            const comp = getComponentWith(<Question question={q} />, {
                enzyme: true,
                ...componentRouterPath,
            });
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
            comp.find("button").simulate("click");
            expect(listener).toHaveBeenCalled();
            expect(m.currentID).not.toBe(q.questionNumber);
        });
    });
    describe("Non-splash, single-answer question", () => {
        test("snapshot", () => {
            const { q } = getTestQuizQuestion("2_QUESTION");
            checkSnapshot(<Question question={q} />, {
                ...componentRouterPath,
            });
        });
        test("functionality", () => {
            const { q, m } = getTestQuizQuestion("2_QUESTION");
            const listener = jest.fn();
            m.listen(listener);
            const comp = getComponentWith(<Question question={q} />, {
                enzyme: true,
                ...componentRouterPath,
            });
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
            comp.find("button").simulate("click");
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
        });
    });
    describe("Non-splash, multi-answer question", () => {
        test("snapshot", () => {
            const { q } = getTestQuizQuestion("5_MULTI_ANSWER_OPTIONAL");
            checkSnapshot(<Question question={q} />, {
                ...componentRouterPath,
            });
        });
        test("functionality", () => {
            const { q, m } = getTestQuizQuestion("5_MULTI_ANSWER_OPTIONAL");
            const listener = jest.fn();
            m.listen(listener);
            const comp = getComponentWith(<Question question={q} />, {
                enzyme: true,
                ...componentRouterPath,
            });
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
            comp.find("button").simulate("click");
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
        });
    });
    describe("Non-splash, multi-answer question with multiple required answers", () => {
        test("snapshot", () => {
            const { q } = getTestQuizQuestion("4_MULTI_ANSWER_REQUIRED");
            checkSnapshot(<Question question={q} />, {
                ...componentRouterPath,
            });
        });
        test("functionality", () => {
            const { q, m } = getTestQuizQuestion("4_MULTI_ANSWER_REQUIRED");
            const listener = jest.fn();
            m.listen(listener);
            const comp = getComponentWith(<Question question={q} />, {
                enzyme: true,
                ...componentRouterPath,
            });
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
            comp.find("button").simulate("click");
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
        });
    });
});

describe("Quiz", () => {
    test("base should pass to Question", () => {
        const { m } = getTestQuizQuestion("2_QUESTION");
        const comp = getComponentWith(<Quiz handler={m} />, {
            enzyme: true,
            ...componentRouterPath,
        });
        expect(comp.find(Question)).toHaveLength(1);
        expect(comp.find(CourseCompleted)).toHaveLength(0);
    });
    test("done should pass to CourseCompleted", () => {
        const { m } = getTestQuizQuestion("5_MULTI_ANSWER_OPTIONAL");
        while (m.isDone() !== true) {
            m.next();
        }
        const comp = getComponentWith(<Quiz handler={m} />, {
            enzyme: true,
            ...componentRouterPath,
        });
        expect(comp.find(Question)).toHaveLength(0);
        expect(comp.find(CourseCompleted)).toHaveLength(1);
    });
    test("updates when changing", () => {
        const { m } = getTestQuizQuestion("2_QUESTION");
        const comp = getComponentWith(<Quiz handler={m} />, {
            enzyme: true,
            ...componentRouterPath,
        });
        expect(comp.find(Question).at(0).props().question.question).toBe(
            "2_QUESTION",
        );
        comp.update();
        expect(comp.find(Question).at(0).props().question.question).toBe(
            "2_QUESTION",
        );
        m.next();
        // Needed to get enzyme to refresh its tree, since it doesn't react
        // (pun not intended) to .forceUpdate(), which is what Quiz does when
        // the quizManager tells it that something has changed.
        comp.update();
        expect(comp.find(Question).at(0).props().question.question).toBe(
            "3_QUESTION",
        );
    });
    describe("reacts to changes in quizManager and updates the Question", () => {
        test("splash screen", () => {
            const { q, m } = getTestQuizQuestion("1_SPLASH");
            const listener = jest.fn();
            m.listen(listener);
            const comp = getComponentWith(<Quiz handler={m} />, {
                enzyme: true,
                ...componentRouterPath,
            });
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
            comp.find("button").simulate("click");
            expect(listener).toHaveBeenCalled();
            expect(m.currentID).not.toBe(q.questionNumber);
        });
        test("Non-splash, single-answer question", () => {
            const { q, m } = getTestQuizQuestion("2_QUESTION");
            const listener = jest.fn();
            m.listen(listener);
            const comp = getComponentWith(<Quiz handler={m} />, {
                enzyme: true,
                fullDOM: true,
                ...componentRouterPath,
            });
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
            comp.find("button").simulate("click");
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();

            expect(q.correct).toBe(false);
            const answer = locateCorrectAnswer(q, comp, "answer");
            expect(q.hasAnswered(answer)).toBe(false);
            // $FlowExpectedError[prop-missing]
            locateCorrectAnswer(q, comp).props().onClick();
            expect(listener).toHaveBeenCalled();
            expect(m.currentID).toBe(q.questionNumber);
            expect(q.correct).toBe(true);
            expect(q.hasAnswered(answer)).toBe(true);

            expect(comp.find(Question).at(0).props().question.question).toBe(
                "2_QUESTION",
            );

            comp.find("button").simulate("click");

            expect(m.currentID).toBe(q.questionNumber + 1);
            expect(listener).toHaveBeenCalledTimes(2);
            expect(comp.find(Question).at(0).props().question.question).toBe(
                "3_QUESTION",
            );
        });
        test("Non-splash, multi-answer question", () => {
            const { q, m } = getTestQuizQuestion("5_MULTI_ANSWER_OPTIONAL");
            const listener = jest.fn();
            m.listen(listener);
            const comp = getComponentWith(<Quiz handler={m} />, {
                enzyme: true,
                ...componentRouterPath,
            });
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
            comp.find("button").simulate("click");
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();

            expect(q.correct).toBe(false);
            const answer = locateCorrectAnswer(q, comp, "answer");
            expect(q.hasAnswered(answer)).toBe(false);
            // $FlowExpectedError[prop-missing]
            locateCorrectAnswer(q, comp).props().onClick();
            expect(listener).toHaveBeenCalled();
            expect(m.currentID).toBe(q.questionNumber);
            expect(q.correct).toBe(true);
            expect(q.hasAnswered(answer)).toBe(true);

            expect(comp.find(Question)).toHaveLength(1);

            comp.find("button").simulate("click");

            expect(m.currentID).toBe(q.questionNumber + 1);
            expect(listener).toHaveBeenCalledTimes(2);
            expect(comp.find(Question)).toHaveLength(0);
            expect(comp.find(CourseCompleted)).toHaveLength(1);
        });
        test("Non-splash, multi-answer question with multiple required answers", () => {
            const { q, m } = getTestQuizQuestion("4_MULTI_ANSWER_REQUIRED");
            const listener = jest.fn();
            m.listen(listener);
            const comp = getComponentWith(<Quiz handler={m} />, {
                enzyme: true,
                ...componentRouterPath,
            });
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();
            comp.find("button").simulate("click");
            expect(m.currentID).toBe(q.questionNumber);
            expect(listener).not.toHaveBeenCalled();

            const answer1 = locateCorrectAnswer(q, comp, "answer", 1);
            const answer2 = locateCorrectAnswer(q, comp, "answer", 2);
            expect(q.hasAnswered(answer1)).toBe(false);
            expect(q.hasAnswered(answer2)).toBe(false);

            // $FlowExpectedError[prop-missing]
            locateCorrectAnswer(q, comp, "component", 1).props().onClick();
            expect(listener).toHaveBeenCalledTimes(1);
            expect(m.currentID).toBe(q.questionNumber);
            expect(q.correct).toBe(false);
            expect(q.hasAnswered(answer1)).toBe(true);

            expect(comp.find(Question).at(0).props().question.question).toBe(
                "4_MULTI_ANSWER_REQUIRED",
            );

            comp.find("button").simulate("click");

            expect(listener).toHaveBeenCalledTimes(1);
            expect(comp.find(Question).at(0).props().question.question).toBe(
                "4_MULTI_ANSWER_REQUIRED",
            );

            // $FlowExpectedError[prop-missing]
            locateCorrectAnswer(q, comp, "component", 2).props().onClick();
            expect(listener).toHaveBeenCalledTimes(2);
            expect(m.currentID).toBe(q.questionNumber);
            expect(q.correct).toBe(true);
            expect(q.hasAnswered(answer2)).toBe(true);

            comp.find("button").simulate("click");

            expect(m.currentID).toBe(q.questionNumber + 1);
            expect(listener).toHaveBeenCalledTimes(3);
            expect(comp.find(Question).at(0).props().question.question).toBe(
                "5_MULTI_ANSWER_OPTIONAL",
            );
        });
    });
});

describe("ELearnRaw", () => {
    test("404 with subcomponent", () => {
        const comp = getComponentWith(ELearnRaw, {
            router: true,
            route: "/elaering/non-existant",
            previousPath: "/elaering/:learningSet?",
            ignore404: true,
            enzyme: true,
        });
        expect(comp.find(NotFound)).toHaveLength(1);
    });
    test("root routing", () => {
        const comp = getComponentWith(ELearnRaw, {
            router: true,
            route: "/elaering/",
            previousPath: "/elaering/:learningSet?",
            ignore404: true,
            enzyme: true,
        });
        expect(comp.find(NotFound)).toHaveLength(0);
        expect(comp.find(ELearnList)).toHaveLength(1);
    });
    test("Exists", () => {
        const comp = getComponentWith(ELearnRaw, {
            ...componentRouterPath,
            enzyme: true,
        });
        expect(comp.find(Quiz)).toHaveLength(1);
    });
});
/**
 * This *tests* ELearn, but we use ELearnRaw in most of these tests because
 * ELearnRaw is really just ELearn that doesn't try to load quiz data
 */
describe("ELearn", () => {
    let doResolve;
    const initMock = jest.fn(() => {
        return new Promise((resolve) => {
            doResolve = resolve;
        });
    });
    test("Should just render since quizDataManager is initialized", () => {
        const component = getComponentWith(ELearn, {
            router: true,
            route: "/elaering/non-existant",
            previousPath: "/elaering/:learningSet?",
            ignore404: true,
            enzyme: true,
        });
        expect(component.find(ELearnRaw)).toHaveLength(1);
    });
    test("Should initialize if quizDataManager is not initialized", () => {
        return new Promise((done) => {
            initMock.mockClear();
            // $FlowExpectedError[prop-missing]
            quizDataManager.__index = null;
            // $FlowExpectedError[prop-missing]
            quizDataManager._ALR_initializing = null;
            // $FlowExpectedError[prop-missing]
            quizDataManager._ALR_onInitialize = [];
            // $FlowExpectedError[prop-missing]
            quizDataManager._ALR_hasInitialized = false;
            quizDataManager.initialize = initMock;
            expect(initMock).not.toHaveBeenCalled();
            const component = getComponentWith(ELearn, {
                router: true,
                route: "/elaering/non-existant",
                previousPath: "/elaering/:learningSet?",
                ignore404: true,
                enzyme: true,
            });
            expect(initMock).toHaveBeenCalledTimes(1);
            doResolve(null);
            setTimeout(() => {
                component.update();
                expect(component.find(ELearnRaw)).toHaveLength(1);
                done();
            }, 1);
        });
    });
});

describe("ELearnList", () => {
    test("Basic rendering", () => {
        checkSnapshot(<ELearnList />, {
            router: true,
            route: "/elaering/non-existant",
            previousPath: "/elaering/:learningSet?",
            enzyme: true,
        });
    });
});
