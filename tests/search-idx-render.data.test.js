/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

// This test unmocks all data that is to be indexed and makes sure it all
// actually indexes
jest.unmock('../data/datasett/dictionary.json');
jest.unmock('../data/datasett/external-resources.json');
jest.unmock('../data/datasett/website-identification.json');
jest.unmock('../data/handbok/handbok.json');
jest.unmock('../data/datasett/lab.json');

import React from 'react';
import PleiarSearcher from '../src/searcher.js';
import { WhateverRenderer } from '../src/react-components/global-search.js';

import { testInitPleiarSearcher } from './search-helper.js';
import { getComponentWith } from './test-helpers';

test("Indexing of real data",() =>
{
    expect( () =>
    {
        testInitPleiarSearcher();
    }).not.toThrow();
});

describe("Searcher results", () => {
    testInitPleiarSearcher();
    const results = PleiarSearcher.search('');
    test.each(results)('Data retrieval works %#', (entry) => {
        expect( () => {
            PleiarSearcher.getResultFromRef( entry.ref, entry.matchData );
        }).not.toThrow();
    });

    const whateverSettings = {
        enzyme: true,
        redux: true,
        router: true,
        route: "/sok",
        previousPath: "/"
    };
    test.each(results)('WhateverRenderer works %p', (entry) => {
        let renderer;
        expect( () => {
            renderer = getComponentWith(<WhateverRenderer result={entry.ref} metadata={entry.matchData} />, whateverSettings);
        }).not.toThrow();

        // $FlowIgnore[incompatible-use] - doesn't realize that renderer is defined at this point in the test
        expect( renderer.find('#whateverRendererError') ).toHaveLength(0);

        expect( () => {
            PleiarSearcher.getResultFromRef( entry.ref, entry.matchData );
        }).not.toThrow();
    });
});
