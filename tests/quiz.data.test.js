/*
 * @prettier
 *
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import remark from "remark";
import html from "remark-html";
import data from "../data/quiz/quiz.json";
import { getComponentWith, linkValidator } from "./test-helpers";
import React from "react";
import process from "process";
import {
    MarkdownExtendedLinkError,
    Markdown,
    MarkdownExtendedLink,
} from "../src/react-components/markdown";

jest.unmock("../data/quiz/quiz.json");
jest.unmock("../data/handbok/handbok.json");

function testMarkdown(markdown) {
    return remark().use(html).processSync(markdown);
}

function markdownValidator(markdown) {
    expect(markdown).toMatch(/\S+/);
    expect(testMarkdown(markdown)).not.toBeFalsy();

    const comp = getComponentWith(
        <Markdown permitSpecialNodes={true} content={markdown} />,
        {
            enzyme: true,
            router: true,
            route: "/",
            previousPath: "/",
        },
    );
    expect(comp.find(MarkdownExtendedLinkError)).toHaveLength(0);

    const links = comp.find(MarkdownExtendedLink);

    // Validate links
    for (const link of links) {
        const to = link.props.href;
        expect(linkValidator(to)).not.toThrow();
    }
}

describe.each(Object.keys(data))("Quiz %p", (quizName) => {
    test("Base data", () => {
        const entry = data[quizName];
        const validKeys = {
            title: true,
            quiz: true,
            endSlideContent: true,
        };
        for (const key of Object.keys(entry)) {
            expect(validKeys[key] === true).toBe(true);
        }
        expect(entry).toMatchObject({
            title: expect.stringMatching(/\S+/),
        });
        expect(entry.quiz).toBeInstanceOf(Array);
        if (
            entry.endSlideContent !== null &&
            entry.endSlideContent !== undefined
        ) {
            expect(entry.endSlideContent).toMatchObject({
                header: expect.stringMatching(/\S+/),
                markdown: expect.stringMatching(/\S+/),
            });
            markdownValidator(entry.endSlideContent.markdown);
            const endSlideValidKeys = {
                header: true,
                markdown: true,
            };
            for (const key of Object.keys(entry.endSlideContent)) {
                expect(endSlideValidKeys[key] === true).toBe(true);
            }
        }
    });
    test.each(data[quizName].quiz)("Entry %p", (entry) => {
        expect(entry).toMatchObject({
            question: expect.stringMatching(/\S+/),
        });
        if (process.env.PLEIAR_QUIZ_SKIP_PLACEHOLDER_CHECKS !== "1") {
            expect(entry.question).not.toMatch(/(Placeholder)/i);
            expect(entry.question).not.toMatch(/(STUB)/);
        }
        if (entry.markdown !== undefined) {
            markdownValidator(entry.markdown);
        }
        if (entry.answers === undefined || entry.answers.length === 0) {
            expect(entry.splashSlide).toBe(true);
        } else {
            expect(entry.splashSlide).not.toBe(true);
            let correct = 0;
            for (const answer of entry.answers) {
                expect(answer).toMatchObject({
                    entry: expect.stringMatching(/\S+/),
                });
                if (answer.correct !== undefined && answer.correct !== null) {
                    expect(typeof answer.correct).toBe("boolean");
                    if (answer.correct === true) {
                        correct++;
                    }
                }
                // Verify that only known keys are used, to defend against typos
                const validKeys = {
                    entry: true,
                    correct: true,
                };
                for (const key of Object.keys(answer)) {
                    expect(validKeys[key] === true).toBe(true);
                }
            }
            expect(correct).toBeGreaterThan(0);
            if (entry.numberOfCorrectAnswersRequired !== undefined) {
                // The entry needs to have at least numberOfCorrectAnswersRequired correct answers
                expect(correct).toBeGreaterThanOrEqual(
                    entry.numberOfCorrectAnswersRequired,
                );
            }
        }

        // Validate that all keys in the object are known, to defend against typos
        const validKeys = {
            question: true,
            answers: true,
            markdown: true,
            splashSlide: true,
            numberOfCorrectAnswersRequired: true,
        };
        for (const key of Object.keys(entry)) {
            expect(validKeys[key] === true).toBe(true);
        }
    });
});
