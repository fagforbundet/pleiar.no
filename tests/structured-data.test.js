/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import * as React from 'react';
import { getComponentWith } from './test-helpers';

jest.unmock('../src/react-components/shared/structured-data');
import { StructuredData } from '../src/react-components/shared/structured-data';

function getLDJSON (from)
{
    const comp = getComponentWith(from, { enzyme: true });
    const script = comp.find('script');
    expect(script).toHaveLength(1);
    const JSONStr = script.text();
    expect(script.props().type).toBe('application/ld+json');
    let result;
    expect(() => {
        result = JSON.parse(JSONStr);
    }).not.toThrow();
    return result;
}

const hasPartWalled = {
    "@type":"WebPageElement",
    "isAccessibleForFree":"False",
    "cssSelector":".auth-content"
};

test('StructuredData base', () => {
    const data = getLDJSON(<StructuredData walled={true} type="auto" />);
    expect(data).toMatchObject({
        "@context": "https://schema.org",
        "publisher": {
            "@type": "Organization",
            "name": "Fagforbundet",
            "logo": {
                "@type": "ImageObject",
                "url": /brand.jpg/,
            }
        },
        "author": {
            "@type": "Organization",
            "name": /Fagforbundet/
        },
    });
});

describe('StructuredData type=auto', () => {
    test('walled=true', () => {
        const data = getLDJSON(<StructuredData walled={true} type="auto" />);
        expect(data['@type']).toBe('WebPage');
        expect(data.isAccessibleForFree).toBe("False");
        expect(data.hasPart).toMatchObject(hasPartWalled);
    });
    test('walled=false', () => {
        const data = getLDJSON(<StructuredData walled={false} type="auto" />);
        expect(data['@type']).toBe('WebPage');
        expect(data.isAccessibleForFree).toBe(undefined);
        expect(data.hasPart).toBe(undefined);
    });
});
describe('StructuredData type=article', () => {
    test('walled=true, title only', () => {
        const data = getLDJSON(<StructuredData walled={true} type="article" title="test" />);
        expect(data['@type']).toBe('Article');
        expect(data.isAccessibleForFree).toBe("False");
        expect(data.hasPart).toMatchObject(hasPartWalled);
        expect(data.headline).toBe("test");
        expect(data.image).toBe('/fbed.jpg');
    });
    test('walled=false, title only', () => {
        const data = getLDJSON(<StructuredData walled={false} type="article" title="test" />);
        expect(data.isAccessibleForFree).toBe(undefined);
        expect(data.hasPart).toBe(undefined);
        expect(data['@type']).toBe('Article');
        expect(data.headline).toBe("test");
        expect(data.image).toBe('/fbed.jpg');
    });
    test('walled=true, author and image', () => {
        const data = getLDJSON(<StructuredData walled={true} type="article" title="test" author="Test author" image="/test.jpg" />);
        expect(data['@type']).toBe('Article');
        expect(data.isAccessibleForFree).toBe("False");
        expect(data.hasPart).toMatchObject(hasPartWalled);
        expect(data.headline).toBe("test");
        expect(data.image).toBe('/test.jpg');
        expect(data.author).toMatchObject({
            "@type":"Person",
            "name":"Test author"
        });
    });
});
