/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019-2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import glob from 'glob';
import yaml from 'js-yaml';
import fs from 'fs';
import path from 'path';
/*
 * NOTE: Most of the handbook tests are actually done by markdown.data.test.js.
 * This file does some syntax validation of the metadata.yml files themselves
 */

const allFiles = glob.sync('data/**/Metadata.yml');
test.each(allFiles)('Syntax of %s',(file) => {
    const parent = path.dirname(file);
    let content;
    expect( () => {
        content = yaml.safeLoad(fs.readFileSync(file, 'utf8'));
    }).not.toThrow();
    expect(content.title).not.toBeFalsy();
    // Check for bad titles
    expect(content.title).not.toMatch(/Generel[tl]/);
    // Validate other settings
    expect(content.sections).toBeInstanceOf(Object);

    for(const subFile in content.sections)
    {
        const absPath = parent+'/'+subFile;
        expect(fs.existsSync(absPath)).toBeTruthy();
        if(fs.statSync(absPath).isDirectory())
        {
            // Directory entries should *not* have any additional keys. Everything for
            // these are defined in their own Metadata.yml
            expect(content.sections[subFile]).toBeFalsy();
        }
        else
        {
            expect(content.sections[subFile]).toBeInstanceOf(Object);
            expect(content.sections[subFile].title).toMatch(/\S/);
            if(content.sections[subFile].public !== undefined)
            {
                expect(typeof(content.sections[subFile].public)).toBe('boolean');
            }
            // Validate keys
            const knownKeys = {
                title: /\S/,
                customURL: /\S/,
                hideTOC: "boolean",
                public: "boolean",
            };
            for(const key in content.sections[subFile])
            {
                expect( () => {
                    if (knownKeys[key] === undefined)
                    {
                        throw('Unknown metadata setting: '+key);
                    }
                }).not.toThrow();
                if (typeof(knownKeys[key]) == "object")
                {
                    expect(content.sections[subFile][key]).toMatch(knownKeys[key]);
                }
                else
                {
                    expect(typeof(content.sections[subFile][key])).toBe(knownKeys[key]);
                }
            }
        }
    }
});
