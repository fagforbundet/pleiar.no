/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import { prepareEnv, checkSnapshot, getComponentWith, setConsole } from './test-helpers';
import Select from 'react-select';

import Handbook, { HandbookRender, HandbookTocRender, HandbookSearchResult, HandbookIndexRenderer, HandbookNavLinkRaw } from '../src/react-components/handbook';
import { HandbookEntry, HandbookIndex } from '../src/data.handbook';
import { NotFound } from '../src/react-components/shared';
import { RequiresAuth } from '../src/react-components/auth';
import SimpleAccordion from '../src/react-components/shared/simple-accordion';

window.console.log = jest.fn();

prepareEnv();

const globalDefaultCompSettings = {
    redux: false,
    includeReduxStore: false,
    router: true,
    route: "/handbok",
    routingAssistant: false,
    enzyme: true,
    previousPath: "/"
};

const genericSearchData = new HandbookEntry(['Readme','README']);
const genericSearchSnippet = [
    {
        type: 'normal',
        str: 'normal text',
    },
    {
        type: 'highlight',
        str: 'highlighted text',
    },
    {
        type: 'delimiter',
    },
];
genericSearchData.searchSnippet = genericSearchSnippet;

describe('Basic rendering', () => {
    test('of the handbook index page', () => {
        checkSnapshot(Handbook,{
            ...globalDefaultCompSettings,
            enzymeSub: Handbook
        });
        expect(global.__title).toMatch('Handbok');
    });

    test('of a handbook subpage', () => {
        const compSettings = Object.assign({},globalDefaultCompSettings,{
            route: '/handbok/Readme/README',
            previousPath: "/handbok/:handbookPath*",
            enzymeSub: Handbook
        });
        checkSnapshot(Handbook,compSettings);
        expect(global.__title).toMatch('Handbok');
    });

    test('of a second handbook subpage', () => {
        const compSettings = Object.assign({},globalDefaultCompSettings,{
            route: '/handbok/Readme/REPO-README',
            previousPath: "/handbok/:handbookPath*",
            enzymeSub: Handbook,
        });
        checkSnapshot(Handbook,compSettings);
        expect(global.__title).toMatch('Handbok');
    });
});

describe('Contentless handbook chapters', () => {
    test('redirecting to first available content', () => {
        const compSettings = Object.assign({},globalDefaultCompSettings,{
            route: '/handbok/Readme',
            previousPath: "/handbok/:handbookPath*"
        });
        const comp = getComponentWith(Handbook,compSettings);
        // Should have redirected
        expect( comp.find( HandbookRender ).at(0).props().entry.path ).toEqual([ 'Readme','README']);
        expect( comp.find( HandbookTocRender ) ).toHaveLength(1);
        expect(comp.find(HandbookTocRender).at(0).props('toc')).toMatchSnapshot();
    });
});

test('Rendering an entry with hidden TOC', () => {
    const compSettings = Object.assign({},globalDefaultCompSettings,{
        route: '/handbok/Test_data/Test_section',
        previousPath: "/handbok/:handbookPath*"
    });
    const comp = getComponentWith(Handbook,compSettings);
    expect( comp.find( HandbookRender ) ).toHaveLength(1);
    expect( comp.find( HandbookTocRender ) ).toHaveLength(0);
});

test('HandbookTocRender .onChange with invalid params', () => {
    const compSettings = Object.assign({},globalDefaultCompSettings,{
        route: '/handbok/Readme',
        previousPath: "/handbok/:handbookPath*"
    });
    const comp = getComponentWith(Handbook,compSettings);
    const instance = comp.find(HandbookTocRender).at(0).instance();
    expect(() => {
        instance.onChange(null);
    }).not.toThrow();
});

describe('Authentication', () => {
    test('required', () => {
        const fakeEntry = {
            content: '',
            toc: [],
            public: false,
            toplevelParent: {},
        };
        const render = getComponentWith(<HandbookRender entry={fakeEntry} />,
            {
                enzyme: true
            });
        expect(render.find(RequiresAuth)).toHaveLength(1);
    });

    test('not required', () => {
        const fakeEntry = {
            content: '',
            toc: [],
            public: true,
            toplevelParent: {},
        };
        const render = getComponentWith(<HandbookRender entry={fakeEntry} />,
            {
                enzyme: true
            });
        expect(render.find(RequiresAuth)).toHaveLength(0);
    });
});

describe('HandbookSearchResult', () => {
    test('base snapshot', () => {
        const compSettings = Object.assign({},globalDefaultCompSettings,{
            enzyme: false
        });
        checkSnapshot(<HandbookSearchResult entry={genericSearchData} />, compSettings);
    });

    test('with invalid content type', () => {
        setConsole("disabled");
        const compSettings = Object.assign({},globalDefaultCompSettings,{
            enzyme: false
        });

        const brokenSearch = new HandbookEntry(['Readme','README']);
        brokenSearch.searchSnippet = [].concat(genericSearchSnippet);
        brokenSearch.searchSnippet.push({
            type: "does-not-exist"
        });
        expect( () => {
            getComponentWith(<HandbookSearchResult entry={brokenSearch} />,compSettings);
        }).toThrow("Unknown string type: does-not-exist");

        setConsole("enabled");
    });
});

test('404 handling', () => {
    const compSettings = Object.assign({},globalDefaultCompSettings,{
        route: '/handbok/non-existant-chapter',
        previousPath: "/handbok/:handbookPath*",
        ignore404: true
    });
    let comp = getComponentWith(Handbook,compSettings);

    expect(comp.find(NotFound)).toHaveLength(1);

    compSettings.route = '/handbok/Readme/non-existant-section';
    comp = getComponentWith(Handbook,compSettings);

    expect(comp.find(NotFound)).toHaveLength(1);

    compSettings.route = '/handbok/Readme/README';
    comp = getComponentWith(Handbook,compSettings);
    expect(comp.find(NotFound)).toHaveLength(0);
});

test('Scrolling to correct location in HandbookTocRender', () => {
    const toc = [{
        content: "test",
        slug: "test",
        lvl: 1
    }];
    window.scrollTo = jest.fn();
    const comp = getComponentWith(<div>
        <div>
            <HandbookTocRender toc={toc} />
        </div>
        <div id="test">Hello</div>
    </div>, { enzyme: {fullDOM: true } });

    // Calling it with a valid id should result in scrollTo being called
    comp.find(Select).props().onChange({ value: "test", name: "test" });
    expect(window.scrollTo).toHaveBeenCalledTimes(1);

    // Should not call scrollTo with an invalid id
    comp.find(Select).props().onChange("");
    expect(window.scrollTo).toHaveBeenCalledTimes(1);
});

describe('HandbookIndexRenderer', () => {
    test('.recursiveRender with suppressTitle=false', () => {
        const comp = getComponentWith(<HandbookIndexRenderer />,globalDefaultCompSettings);
        const instance = comp.find(HandbookIndexRenderer).at(0).instance();

        const entry = new HandbookIndex('Readme');
        checkSnapshot(<div>
            {instance.recursiveRender(entry,false)}
            </div>, globalDefaultCompSettings);
    });
    test('plainList', () => {
        const comp = getComponentWith(<HandbookIndexRenderer plainList />,globalDefaultCompSettings);
        expect(comp.find(SimpleAccordion)).toHaveLength(0);

        const instance = comp.find(HandbookIndexRenderer).at(0).instance();

        const entry = new HandbookIndex('Readme');
        checkSnapshot(<div>
            {instance.renderAsUlList(entry)}
            </div>, globalDefaultCompSettings);
    });
    test('Default title', () => {
        const comp = getComponentWith(<HandbookIndexRenderer />,globalDefaultCompSettings);
        expect(comp.find('h1').at(0).text()).toBe('Håndbok');
    });
    test('Sub-title', () => {
        const comp = getComponentWith(<HandbookIndexRenderer title="Test" />,globalDefaultCompSettings);
        expect(comp.find('h1').at(0).text()).toBe('Håndbok: Test');
    });
});

describe('HandbookNavLinkRaw', () => {
    test.each(['next','previous'], 'Basic rendering with defaults', (type) => {
        checkSnapshot(<HandbookNavLinkRaw url="/test" title="Hello world" type={type}  />,globalDefaultCompSettings);
    });
    test.each(['next','previous'], 'Basic rendering with custom text', (type) => {
        checkSnapshot(<HandbookNavLinkRaw url="/test" title="Hello world" type={type}  />,globalDefaultCompSettings);
    });
});

test('Handbook rendering with custom back button', () => {
    const compSettings = Object.assign({},globalDefaultCompSettings,{
        route: '/handbok/Readme',
        previousPath: "/handbok/:handbookPath*"
    });
    const fakeEntry = {
        content: 'Test',
        toc: [],
        public: true
    };
    const comp = getComponentWith(<HandbookRender entry={fakeEntry} navMode="back" navBackLink="/home" /> ,compSettings);
    expect( comp.find( HandbookNavLinkRaw ).at(0).props().url).toEqual("/home");
    expect( comp.find( HandbookNavLinkRaw ).at(0).props().title).toEqual("Tilbake");
    expect( comp.find( HandbookNavLinkRaw ).at(0).props().type).toEqual("previous");
});
