/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { Covid19 } from '../src/react-components/covid19.js';
import { prepareEnv, getComponentWith} from './test-helpers';

prepareEnv();
jest.unmock('../data/handbok/handbok.json');

test('The Covid19 page', () => {
    expect( () => {
        getComponentWith(Covid19, {
            router: true,
            route: '/covid19',
            previousPath: '/'
        });
    }).not.toThrow();
    expect(global.__title).toMatch('Covid-19');
});
