/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { numbers } from '../src/helper.numbers.js';

test('numbers.parseFloatish', () => {
    expect( numbers.parseFloatish("") ).toBe("");
    expect( numbers.parseFloatish("abc") ).toBe(NaN);
    expect( numbers.parseFloatish("1,1") ).toBe(1.1);
    expect( numbers.parseFloatish("1.1") ).toBe(1.1);
    expect( numbers.parseFloatish(1.1) ).toBe(1.1);
});

// Note about *FromThing methods. The actual "thing" conversion is handled by
// getValueFromLiteralOrEvent from helper.general, which has its own test.
test('numbers.parseFloatishFromThing', () => {
    expect( numbers.parseFloatishFromThing("") ).toBe("");
    expect( numbers.parseFloatishFromThing("abc") ).toBe(NaN);
    expect( numbers.parseFloatishFromThing("1,1") ).toBe(1.1);
    expect( numbers.parseFloatishFromThing("1.1") ).toBe(1.1);
});

test('numbers.floatishInRangeFromThing', () => {
    expect( numbers.floatishInRangeFromThing("",0,100) ).toBe("");
    expect( numbers.floatishInRangeFromThing("abc",0,100) ).toBe(NaN);
    expect( numbers.floatishInRangeFromThing("1,1",0,100) ).toBe(1.1);
    expect( numbers.floatishInRangeFromThing("1.1",0,100) ).toBe(1.1);
    expect( numbers.floatishInRangeFromThing(101,0,100) ).toBe(100);
    expect( numbers.floatishInRangeFromThing(99999999,0,100) ).toBe(100);
    expect( numbers.floatishInRangeFromThing(-1,0,100) ).toBe(0);
    expect( numbers.floatishInRangeFromThing("-1",0,100) ).toBe(0);
    expect( numbers.floatishInRangeFromThing(0.1,0,100) ).toBe(0.1);
});

test('numbers.getFloatishFromThing', () => {
    expect( () => {
        numbers.getFloatishFromThing("");
    }).toThrow(/got empty string/);
    expect( numbers.getFloatishFromThing(1.1) ).toBe(1.1);
    expect( numbers.getFloatishFromThing("1,1") ).toBe(1.1);
    expect( numbers.getFloatishFromThing("1.1") ).toBe(1.1);
});

test('numbers.getFloatishOrZeroFromThing', () => {
    expect(numbers.getFloatishOrZeroFromThing("")).toBe(0);
    expect(numbers.getFloatishOrZeroFromThing("abc")).toBe(0);
    expect( numbers.getFloatishOrZeroFromThing(1.1) ).toBe(1.1);
    expect( numbers.getFloatishOrZeroFromThing("1,1") ).toBe(1.1);
    expect( numbers.getFloatishOrZeroFromThing("1.1") ).toBe(1.1);
});

test('numbers.parseFloat', () => {
    expect( numbers.parseFloat("") ).toBe(NaN);
    expect( numbers.parseFloat("abc") ).toBe(NaN);
    expect( numbers.parseFloat("1,1") ).toBe(1.1);
    expect( numbers.parseFloat("1.1") ).toBe(1.1);
    expect( numbers.parseFloat(1.1) ).toBe(1.1);
});

test('numbers.roundNearestTen', () => {
    expect( numbers.roundNearestTen(15) ).toBe(20);
    expect( numbers.roundNearestTen(14) ).toBe(10);
    expect( numbers.roundNearestTen(2432) ).toBe(2430);
    expect( numbers.roundNearestTen(2437) ).toBe(2440);
    expect( numbers.roundNearestTen(2444.5) ).toBe(2440);
    expect( numbers.roundNearestTen(2444.9) ).toBe(2440);
});

test('numbers.round', () => {
    expect( numbers.round(1.00000) ).toBe(1);
    expect( numbers.round(1.10000) ).toBe(1.1);
    expect( numbers.round(1.00001) ).toBe(1);
    expect( numbers.round(2.22222,2) ).toBe(2.22);
    expect( numbers.round(2.22222,4) ).toBe(2.2222);
    expect( numbers.round(2.22222,0) ).toBe(2);
    expect( numbers.round(1.5,0) ).toBe(2);
});

test('numbers.conditionalRound', () => {
    expect( numbers.conditionalRound(1.00000,true) ).toBe(1);
    expect( numbers.conditionalRound(1.10000,true) ).toBe(1.1);
    expect( numbers.conditionalRound(1.00001,true) ).toBe(1);
    expect( numbers.conditionalRound(2.22222,true,2) ).toBe(2.22);
    expect( numbers.conditionalRound(2.22222,true,4) ).toBe(2.2222);
    expect( numbers.conditionalRound(2.22222,true,0) ).toBe(2);
    // Test with rounding "disabled" (should just return the raw number)
    expect( numbers.conditionalRound(2.22222,false,0) ).toBe(2.22222);
    expect( numbers.conditionalRound(1.5,true,0) ).toBe(2);
});

test('numbers.roundToSimplestSignificant', () => {
    expect( numbers.roundToSimplestSignificant(0.05) ).toBe(0.05);
    expect( numbers.roundToSimplestSignificant(1.00000) ).toBe(1);
    expect( numbers.roundToSimplestSignificant(1.25000) ).toBe(1);
    expect( numbers.roundToSimplestSignificant(1.4) ).toBe(1);
    expect( numbers.roundToSimplestSignificant(1.5) ).toBe(2);
    expect( numbers.roundToSimplestSignificant(0.0000001) ).toBe(0.0000001);
});

test('numbers.conditionalRoundToSimplestSignificant', () => {
    expect( numbers.conditionalRoundToSimplestSignificant(0.05,true) ).toBe(0.05);
    expect( numbers.conditionalRoundToSimplestSignificant(1.00000,true) ).toBe(1);
    expect( numbers.conditionalRoundToSimplestSignificant(1.25000,true) ).toBe(1);
    // Test with rounding "disabled" (should just return the raw number)
    expect( numbers.conditionalRoundToSimplestSignificant(1.25000,false) ).toBe(1.25000);
    expect( numbers.conditionalRoundToSimplestSignificant(1.4,true) ).toBe(1);
    expect( numbers.conditionalRoundToSimplestSignificant(1.5,true) ).toBe(2);
});

test('numbers.format', () => {
    expect( numbers.format(1.1) ).toBe("1,1");
    expect( numbers.format(1000.1) ).toMatch(/^1\s+000,1$/);
});
