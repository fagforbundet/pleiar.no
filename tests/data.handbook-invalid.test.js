/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.mock("../data/handbok/handbok.json", () => {
    return {
        "title":"Root",
        "order":["Reaxdme","License"],
        "tree":{
            "Readme":{
                "title":"README",
                "order":["README","REPO-README"],
                "tree":{
                    "README":{
                        "content":"CONTENT",
                        "title":"Pleiar.no README",
                        "toc":["Pleiar.no","Using this repository","Dependencies","Data set","git branches","License","Permission for Fagforbundet to combine AGPL code with non-free data"],
                        "path":["Readme","README"],
                        "next":["Readme","REPO-README"]
                    },
                    "REPO-README":{
                        "content":"CONTENT",
                        "title":"Pleiar.no public dataset README",
                        "toc":["Pleiar.no public dataset"],
                        "path":["Readme","REPO-README"],
                        "previous":["Rxeadme","README"],
                        "next":["License","COxPYING"]
                    },
                }
            },
            "License":{
                "title":"COPYING",
                "order":["COPYING"],
                "tree":{
                    "COPYING":{
                        "content":"CONTENT",
                        "title":"Pleiar.no license",
                        "toc":[],
                        "path":["License","COPYING"],
                        "previous":["Readme","REPO-README"]
                    }
                }
            }
        }
    };
});

import { HandbookIndex, HandbookEntry } from '../src/data.handbook';

window.console.log = jest.fn();

test('HandbookIndex invalid firstChild', () => {
    const idx = new HandbookIndex();
    expect( () => {
        idx.firstChild;
    }).toThrow('undefined entry listed in order');
});
test('HandbookIndex invalid tree', () => {
    const idx = new HandbookIndex();
    expect( () => {
        idx.tree;
    }).toThrow('undefined entry listed in order');
});
test('HandbookIndex invalid previous/next', () => {
    const idx = new HandbookEntry('Readme/REPO-README');
    expect( () => {
        idx.previous;
    }).toThrow('Failed to look up');
    expect( () => {
        idx.next;
    }).toThrow('Failed to look up');
});
