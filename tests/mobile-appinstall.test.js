/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { appInstall } from '../src/mobile-appinstall';
import device from '../src/helper.device';

function setUA (UA)
{
    Object.defineProperty(navigator,'userAgent', { value: 'Mozilla/5.0 '+UA, configurable: true });
}

beforeEach( () => {
    // Reset onbeforeinstallprompt
    window.onbeforeinstallprompt = undefined;
    // Reset appInstall
    appInstall.mobileAppInstallEvent = null;
    appInstall.listeners = [];
    // Reset mocks
    window.addEventListener.mockClear();
    // Mock device
    device.isAppMode = jest.fn( () => false );
    device.isMobileDevice = jest.fn( () => true );
    setUA('jest');
});
window.addEventListener = jest.fn(window.addEventListener);

describe('init', () => {
    test('without onbeforeinstallprompt', () => {
        expect( () => {
            appInstall.init();
        }).not.toThrow();
        expect(window.addEventListener).not.toHaveBeenCalled();
    });
    test('with onbeforeinstallprompt', () => {
        window.onbeforeinstallprompt = true;
        expect( () => {
            appInstall.init();
        }).not.toThrow();
        expect(window.addEventListener).toHaveBeenCalledWith('beforeinstallprompt',expect.any(Function));
    });
});

describe('mode', () => {
    test('nothing', () => {
        expect( appInstall.mode() ).toBe(null);
    });
    test('chrome', () => {
        window.onbeforeinstallprompt = true;
        expect( appInstall.mode() ).toBe('chrome');
    });
    test('Android WebView', () => {
        setUA('(Linux; Android 9) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Focus/8.0.8 Chrome/73.0.3683.90 Mobile Safari/537.36');
        window.onbeforeinstallprompt = true;
        expect( appInstall.mode() ).toBe('android-webview');
    });
    test('Android WebView without onbeforeinstallprompt', () => {
        setUA('(Linux; Android 9) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Focus/8.0.8 Chrome/73.0.3683.90 Mobile Safari/537.36');
        expect( appInstall.mode() ).toBe(null);
    });
    test('firefox-android', () => {
        setUA('(Android 9; Mobile; rv:67.0) Gecko/67.0 Firefox/67.0');
        expect( appInstall.mode() ).toBe('firefox-android');
    });
    test('iPhone Safari', () => {
        setUA('(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Mobile/15B202 Safari/604.1');
        expect( appInstall.mode() ).toBe('safari-ios');
    });
    test('iPad Safari', () => {
        setUA('(iPad; CPU OS 9_3_5 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13G36 Safari/601.1');
        expect( appInstall.mode() ).toBe('safari-ios');
    });
    test('Samsung browser', () => {
        window.onbeforeinstallprompt = true;
        setUA('Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G950F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.2 Chrome/67.0.3396.87 Mobile Safari/537.36');
        expect( appInstall.mode() ).toBe('samsung');
    });
    test('Samsung browser with functional onbeforeinstallprompt', () => {
        window.onbeforeinstallprompt = true;
        setUA('Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G950F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.2 Chrome/67.0.3396.87 Mobile Safari/537.36');
        appInstall.mobileAppInstallEvent = true;
        expect( appInstall.mode() ).toBe('chrome');
    });
    test('iPhone Chrome', () => {
        setUA('(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) CriOS/63.0.3239.73 Mobile/15B202 Safari/604.1');
        expect( appInstall.mode() ).toBe('ios-other');
    });
    test('iPhone Firefox', () => {
        setUA('(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) FxiOS/63.0.3239.73 Mobile/15B202 Safari/604.1');
        expect( appInstall.mode() ).toBe('ios-other');
    });
    test('iPhone Google Search', () => {
        setUA('(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) GSA/63.0.3239.73 Mobile/15B202 Safari/604.1');
        expect( appInstall.mode() ).toBe('ios-other');
    });
    test('iPad Facebook', () => {
        setUA('Mozilla/5.0 (iPad; CPU OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 [FBAN/FBIOS;FBDV/iPad6,11;FBMD/iPad;FBSN/iOS;FBSV/12.4;FBSS/2;FBID/tablet;FBLC/nb_NO;FBOP/5;FBCR/]');
        expect( appInstall.mode() ).toBe('facebook-ios');
    });
    test('Android Facebook', () => {
        window.onbeforeinstallprompt = true;
        setUA('Mozilla/5.0 (Linux; Android 9; SM-G950F Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/76.0.3809.89 Mobile Safari/537.36 [FB_IAB/FB4A;FBAV/231.0.0.39.113;]');
        expect( appInstall.mode() ).toBe('facebook-android');
    });
    test('Android Edge', () => { // Is basically chrome, so we treat it as chrome
        window.onbeforeinstallprompt = true;
        setUA('Mozilla/5.0 (Linux; Android 9; SM-G950F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.90 Mobile Safari/537.36 EdgA/42.0.2.3819');
        expect( appInstall.mode() ).toBe('chrome');
    });
    test('Android Opera', () => { // Is similarly to the samsung browser chrome-ish but without onbeforeinstallprompt working
        window.onbeforeinstallprompt = true;
        setUA('Mozilla/5.0 (Linux; Android 9; SM-G950F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Mobile Safari/537.36 OPR/53.0.2569.141117');
        expect( appInstall.mode() ).toBe('opera');
    });
});

describe('canInstall', () => {
    test('in app mode', () => {
        device.isAppMode = jest.fn( () => true );
        expect(appInstall.canInstall()).toBe(false);
        expect(appInstall.canInstallWithAppInstallEvent()).toBe(false);
    });
    test('on a non-mobile device', () => {
        device.isAppMode = jest.fn( () => false );
        device.isMobileDevice = jest.fn( () => false );
        expect(appInstall.canInstall()).toBe(false);
        expect(appInstall.canInstallWithAppInstallEvent()).toBe(false);
    });
    test('un-initialized chrome', () => {
        window.onbeforeinstallprompt = true;
        expect(appInstall.mode()).toBe('chrome');
        expect(appInstall.canInstall()).toBe(true);
        expect(appInstall.canInstallWithAppInstallEvent()).toBe(false);
    });
    test('initialized chrome', () => {
        window.onbeforeinstallprompt = true;
        appInstall.mobileAppInstallEvent = true;
        expect(appInstall.mode()).toBe('chrome');
        expect(appInstall.canInstall()).toBe(true);
        expect(appInstall.canInstallWithAppInstallEvent()).toBe(true);
    });
    test.each([
        '(Android 9; Mobile; rv:67.0) Gecko/67.0 Firefox/67.0',
        '(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Mobile/15B202 Safari/604.1',
        '(iPad; CPU OS 9_3_5 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13G36 Safari/601.1',
        '(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) CriOS/63.0.3239.73 Mobile/15B202 Safari/604.1',
        '(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) FxiOS/63.0.3239.73 Mobile/15B202 Safari/604.1',
        'Mozilla/5.0 (iPad; CPU OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 [FBAN/FBIOS;FBDV/iPad6,11;FBMD/iPad;FBSN/iOS;FBSV/12.4;FBSS/2;FBID/tablet;FBLC/nb_NO;FBOP/5;FBCR/]',
    ])('valid UA %p', (UA) => {
        setUA(UA);
        expect( appInstall.mode() ).not.toBe(null);
        expect(appInstall.canInstall()).toBe(true);
        expect(appInstall.canInstallWithAppInstallEvent()).toBe(false);
    });
    test('other functional mode', () => {
        setUA('(Android 9; Mobile; rv:67.0) Gecko/67.0 Firefox/67.0');
        expect( appInstall.mode() ).toBe('firefox-android');
        expect(appInstall.canInstall()).toBe(true);
        expect(appInstall.canInstallWithAppInstallEvent()).toBe(false);
    });
    test('other non-functional mode', () => {
        setUA('(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko)');
        expect( appInstall.mode() ).toBe(null);
        expect(appInstall.canInstall()).toBe(false);
        expect(appInstall.canInstallWithAppInstallEvent()).toBe(false);
    });
});

describe('listen', () => {
    test('without onbeforeinstallprompt', () => {
        appInstall.listen(jest.fn());
        expect(appInstall.listeners).toEqual([]);
    });
    test('with onbeforeinstallprompt', () => {
        const testCB = jest.fn();
        window.onbeforeinstallprompt = true;
        appInstall.listen(testCB);
        expect(appInstall.listeners).toEqual([testCB]);
    });
});

describe('trigger', () => {
    test('with onbeforeinstallprompt', () => {
        appInstall.mobileAppInstallEvent = {
            prompt: jest.fn(),
            userChoice: {
                then: jest.fn()
            }
        };
        expect(appInstall.trigger()).toBe(true);
        expect(appInstall.mobileAppInstallEvent.prompt).toHaveBeenCalled();
        expect(appInstall.mobileAppInstallEvent.userChoice.then).toHaveBeenCalled();
        const callback = appInstall.mobileAppInstallEvent.userChoice.then.mock.calls[0][0];
        callback('rejected');
        expect(appInstall.mobileAppInstallEvent).not.toBe(null);
        callback('accepted');
        expect(appInstall.mobileAppInstallEvent).toBe(null);
    });
    test('without onbeforeinstallprompt', () => {
        expect(appInstall.trigger()).toBe(false);
    });
});

test('init onbeforeinstallprompt capture and listeners callback', () => {
    window.onbeforeinstallprompt = true;
    expect( () => {
        appInstall.init();
    }).not.toThrow();
    expect(window.addEventListener).toHaveBeenCalledWith('beforeinstallprompt',expect.any(Function));
    const listener = jest.fn();
    const fakeEvent = {
        preventDefault: jest.fn()
    };
    appInstall.listen(listener);
    const callback = window.addEventListener.mock.calls[0][1];
    callback(fakeEvent);
    expect(fakeEvent.preventDefault).toHaveBeenCalled();
    expect(listener).toHaveBeenCalled();
    expect(appInstall.listeners).toEqual([]);
});
