/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { getPleiarInstance } from './integration-helpers';
import { apps } from '../../src/react-components/menu';
import { cleanup } from '@testing-library/react';

jest.unmock('../../data/handbok/handbok.json');

afterEach(cleanup);

test.each(apps)('Navigating to %p', (app) =>
{
    const { component } = getPleiarInstance('/');
    expect( () => {
        component.getAllByText(app.name).shift().click();
    }).not.toThrow();
    let route = app.route || app.id;
    if(route.substr(0,1) !== '/')
    {
        route = '/'+route;
    }
    expect(window.location.pathname).toBe(route);
    expect( () => {
        component.getByText('404 feil: fil ikke funnet');
    }).toThrow();
});
