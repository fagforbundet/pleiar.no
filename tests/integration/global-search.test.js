/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { getPleiarInstance } from './integration-helpers';
import { cleanup } from '@testing-library/react';
import userEvent from 'user-event';
import { testInitPleiarSearcher } from '../search-helper';

testInitPleiarSearcher();

afterEach(cleanup);

jest.mock('lodash/debounce', () => jest.fn(fn => fn));

test('Searching', () => {
    const { component } = getPleiarInstance('/');
    const searchField = component.queryByPlaceholderText('Søk');
    expect(searchField).not.toBe(null);
    const searchString = 'GNU Affero General Public License';
    userEvent.type(searchField,searchString);
    expect(searchField).toHaveAttribute('value',searchString);
    const license = component.queryByText('Håndbok: Pleiar.no license');
    expect(license).not.toBe(null);
    expect(window.location.pathname).toBe('/sok/'+encodeURIComponent(searchString));
});
