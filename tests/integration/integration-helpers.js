/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('reactstrap');
import type jsdom from 'jsdom';
import { Pleiar } from '../../src/react-components/main';
import { render } from '@testing-library/react';
import { getRealRedux } from '../test-helpers';
import * as React from 'react';

// @flow

function getPleiarInstance (path: string)
{
    global.PLEIAR_ENV = 'production';
    window.scrollTo = jest.fn();
    const redux = getRealRedux();
    jsdom.reconfigure({
        url: 'https://www.pleiar.no'+path
    });
    const component = render(<Pleiar store={redux} />);
    return { redux, component };
}

export { getPleiarInstance };
