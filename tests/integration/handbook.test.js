/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { getPleiarInstance } from './integration-helpers';

test('Handbook navigation to first subpage', () => {
    expect( () => {
        const { component } = getPleiarInstance('/handbok');
        const readmeLink = component.getByText('Pleiar.no README');
        readmeLink.click();
    }).not.toThrow();
    expect(window.location.pathname).toBe('/handbok/Readme/README');
});

test('Next link navigation', () => {
    expect( () => {
        const { component } = getPleiarInstance('/handbok/Readme/README');
        const readmeLink = component.getAllByText('Pleiar.no public dataset README');
        readmeLink.shift().click();
    }).not.toThrow();
    expect(window.location.pathname).toBe('/handbok/Readme/REPO-README');
});
