/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import * as React from 'react';
import { prepareEnv, getComponentWith, getRealRedux, checkSnapshot } from './test-helpers';
import NutriCalc, { NutritionSearchRenderer } from '../src/react-components/nutricalc';
import NutriCalcDay from '../src/react-components/nutricalc-day';
import { setWeight } from '../src/actions/nutricalc-shared.js';
import { NotFound } from '../src/react-components/shared.js';

prepareEnv();

test.each(['dagsinntak','energibehov','bmi'])("Routing of %s", (entry) => {
    const component = getComponentWith(NutriCalc,{
        redux: true,
        router: true,
        route: '/verktoy/ernaering/'+entry,
        enzyme: true,
        previousPath: '/verktoy/ernaering'
    });
    expect( component.find(NotFound) ).toHaveLength(0);
    expect( component ) .toMatchSnapshot();
});
test('Redirection', () => {
    const comp = getComponentWith(NutriCalc,{
        redux: true,
        router: true,
        // This is a bit counter-intuitive. You'd expect this to be
        // /verktoy/ernaering.  However, the routing depends upon its parents
        // match, which usually WILL be /verktoy/ernaering since the toplevel
        // router will have used that to pass the route on to verktoy/ernaering.
        // In the test, however, there IS no toplevel router, and we are
        // calling it directly, thus the parent match will simply be /. The
        // result is that if route is / it is the same as route being
        // /verktoy/ernaering in a browser environment, and this should trigger
        // the redirect.
        route: '/',
        enzyme: true,
        previousPath: '/'
    });
    // Should have redirected to NutriCalcDay
    expect(comp.find(NutriCalcDay)).toHaveLength(1);
});

test('Shared redux', () => {
    const store = getRealRedux();
    // Default state
    expect( store.getState().nutriCalc.shared ).toMatchObject({
        weight: 75
    });
    // Set the minimum to 1000
    store.dispatch(setWeight(95));
    // Update should have hit the store
    expect( store.getState().nutriCalc.shared.weight).toBe(95);
});

test('NutritionSearchRenderer', () => {
    const fakeEntry = {
        "kcal":0,
        "fluidML":150,
        "name":"Kaffi (svart, 1 kopp)",
        protein:1
    };
    checkSnapshot(<NutritionSearchRenderer entry={fakeEntry} />);
    delete fakeEntry.fluidML;
    delete fakeEntry.protein;
    checkSnapshot(<NutritionSearchRenderer entry={fakeEntry} />);
});
