/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('reactstrap');
import React from 'react';
import { getComponentWith, checkSnapshot, setUA } from './test-helpers';
import { Collapse } from 'reactstrap';
import { Link } from 'react-router-dom';
import auth from '../src/auth.js';
import Menu from '../src/react-components/menu';

test('Menu', () => {
    checkSnapshot(<Menu />,
        {
            redux: true,
            router: true,
            route: '/',
            previousPath: '/'
        });
});

describe('Menu without auth', () => {
    auth.isAuthenticated = jest.fn(() => false);
    test('Snapshot', () => {
        checkSnapshot(<Menu />,
            {
                redux: true,
                router: true,
                route: '/',
                previousPath: '/'
            });
    });
    test('onClick', () => {
        const component = getComponentWith(Menu,{
            redux: true,
            enzyme: {fullDOM: true},
            router: true,
            route: '/',
            previousPath: '/'
        });
        window.localStorage.pleiarAuthRedirect = null;
        component.find('a').last().props().onClick();
        expect(window.localStorage.pleiarAuthRedirect).toBe(location.pathname);
    });
});

test('Menu UI interaction', () => {
    const component = getComponentWith(Menu,{
        redux: true,
        enzyme: {fullDOM: true},
        router: true,
        route: '/',
        previousPath: '/'
    });

    // Test opening the collapse
    expect(component.find(Collapse).at(0).props().isOpen).toBeFalsy();
    component.find('.likeLink').at(0).simulate('click');
    expect(component.find(Collapse).at(0).props().isOpen).toBeTruthy();
    // Manually trigger onEntered (since this is on a timer that isn't
    // triggered during testing)
    component.find(Collapse).at(0).props().onEntered();

    // Closing it
    component.find(Link).at(1).props().onClick();
    component.update();
    expect(component.find(Collapse).at(0).props().isOpen).toBeFalsy();

    component.find('.likeLink').at(0).simulate('click');
    component.find(Link).at(2).props().onClick();
    component.update();
    expect(component.find(Collapse).at(0).props().isOpen).toBeFalsy();
});

function beIOS ()
{
    setUA('(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Mobile/15B202 Safari/604.1');
}

function beAndroid ()
{
    setUA('(Linux; Android 9) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.90 Mobile Safari/537.36');
}

describe("iOS app menu", () => {
    const iosSnapshotTestSettings = {
        redux: true,
        router: true,
        route: '/',
        previousPath: '/'
    };
    const iosComponentTestSettings = {
        ...iosSnapshotTestSettings,
        enzyme: true
    };
    test("Snapshot", () => {
        beIOS();
        window.navigator.standalone = true;
        checkSnapshot(<Menu />, iosSnapshotTestSettings);
    });
    test("Should not use app menu when not in standalone mode", () => {
        beIOS();
        delete window.navigator.standalone;
        const component = getComponentWith(Menu,iosComponentTestSettings);
        expect(component.find('.ios-menu-back')).toHaveLength(0);
    });
    test("Should use app menu when in standalone mode", () => {
        beIOS();
        window.navigator.standalone = true;
        const component = getComponentWith(Menu,iosComponentTestSettings);
        expect(component.find('.ios-menu-back')).toHaveLength(1);
    });
    test("Should not use app menu on Android", () => {
        beAndroid();
        window.navigator.standalone = true;
        const component = getComponentWith(Menu,iosComponentTestSettings);
        expect(component.find('.ios-menu-back')).toHaveLength(0);
    });
    test("Back button should be disabled on front page", () => {
        beIOS();
        window.navigator.standalone = true;
        const component = getComponentWith(Menu,iosComponentTestSettings);
        expect(component.find(".ios-menu-back-inactive")).toHaveLength(1);
        expect(component.find(".ios-brand").find(Link)).toHaveLength(0);
    });
    test("Back button should be a link when there's no history", () => {
        beIOS();
        window.navigator.standalone = true;
        const component = getComponentWith(Menu,{
            ...iosComponentTestSettings,
            route:'/handbook/'
        });
        expect(component.find(".ios-menu-back-inactive")).toHaveLength(0);
        expect(component.find(".ios-brand").find(Link)).toHaveLength(1);
    });
    test("Back button should be a callback when there's some history", () => {
        beIOS();
        window.navigator.standalone = true;
        Object.defineProperty(window.history,'state', { value:true, configurable: true });
        const component = getComponentWith(Menu,{
            ...iosComponentTestSettings,
            route:'/handbook/'
        });
        expect(component.find(".ios-menu-back-inactive")).toHaveLength(0);
        expect(component.find(".ios-brand").find(Link)).toHaveLength(0);
        expect(component.find(".ios-menu-back")).toHaveLength(1);
        expect(component.find(".ios-menu-back").prop('onClick')).toBeInstanceOf(Function);
    });
    test("Back button callback should work", () => {
        beIOS();
        window.navigator.standalone = true;
        Object.defineProperty(window.history,'state', { value:true, configurable: true });
        const backStub = jest.fn();
        Object.defineProperty(window.history,'back', { value: backStub, configurable: true });
        const component = getComponentWith(Menu,{
            ...iosComponentTestSettings,
            route:'/handbook/'
        });
        expect(component.find(".ios-menu-back-inactive")).toHaveLength(0);
        expect(component.find(".ios-brand").find(Link)).toHaveLength(0);
        expect(component.find(".ios-menu-back")).toHaveLength(1);
        expect(backStub).not.toHaveBeenCalled();
        expect( () => {
            component.find(".ios-menu-back").simulate('click');
        }).not.toThrow();
        expect(backStub).toHaveBeenCalled();
    });
});
