/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import { NotFound, Handle404, ScrollToTopHelperRaw, ConditionalRender, ConditionalRenderLoading, ConditionalRenderRejected } from '../src/react-components/shared.js';
import { checkSnapshot, prepareEnv, getComponentWith } from './test-helpers';

prepareEnv();

describe('NotFound', () => {

    const NotFoundSettings = {
        router: true,
        route: '/test',
        previousPath:'/',
        ignore404: true
    };
    describe('snapshots', () =>
    {
        test('root=false', () => {
            checkSnapshot(
                <NotFound root={false} />, NotFoundSettings
            );
        });
        test('root=true', () => {
            checkSnapshot(
                <NotFound root={true} />, NotFoundSettings
            );
        });
    });

    test('Timeout', () => {
        // Needed to test the timeout
        jest.useFakeTimers();

        const comp = getComponentWith(<NotFound root={false} />, NotFoundSettings);
        expect(JSON.stringify(comp.toJSON())).toMatch(/Ser etter oppdateringer/);
        jest.advanceTimersByTime(20000);
        expect(JSON.stringify(comp.toJSON())).toMatch(/Siden du prøvde å besøke finnes ikke/);
    });
});

test('Handle404', () =>
{
    checkSnapshot(
        <Handle404 />,
        {
            router: true,
            route: '/test',
            previousPath:'/',
            ignore404: true
        }
    );
});

test('ScrollToTopHelper', () =>
{
    window.scrollTo = jest.fn();

    const fakeLocation = {
        pathname: '/test-scroll'
    };

    const comp = getComponentWith(<ScrollToTopHelperRaw location={fakeLocation} />);
    // Should not call scrollTo on construction
    expect(window.scrollTo).not.toHaveBeenCalled();

    // Should not call scrollTo if it gets an identical path as last time
    comp.getInstance().componentDidUpdate({
        location: {
            pathname: '/test-scroll'
        }
    });
    expect(window.scrollTo).not.toHaveBeenCalled();

    // Simulate a change in path
    fakeLocation.pathname = '/new-location';
    // And then tell ScrollToTopHelper about the event
    comp.getInstance().componentDidUpdate({
        location: {
            pathname: '/test-scroll'
        }
    });
    // ScrollToTopHelper should now call window.scrollTo() once.
    expect(window.scrollTo).toHaveBeenCalledTimes(1);
    // And it should have been called with 0,0
    expect(window.scrollTo).toHaveBeenLastCalledWith(0,0);
});

describe('ConditionalRender', () => {
    const dummyComponent =  () => {
        return <div id="conditionalRenderSuccess">FINAL_RENDERED</div>;
    };
    let doResolve;
    let doReject; // eslint-disable-line
    const promiseGenerator = () => {
        return new Promise( (resolve, reject) => {
            doResolve = resolve;
            doReject = reject;
        });
    };
    test('should show loading screen', () => {
        const comp = getComponentWith(<ConditionalRender component={dummyComponent} test={() => false} resolve={promiseGenerator} />, { enzyme: true });
        expect(comp.find(ConditionalRenderLoading)).toHaveLength(1);
        doResolve('Making sure the promise is dealt with');
    });
    test('should not show loading screen when already resolved', () => {
        const alreadyResolvedGenerator = jest.fn(promiseGenerator);
        const comp = getComponentWith(<ConditionalRender component={dummyComponent} test={() => true} resolve={alreadyResolvedGenerator} />, { enzyme: true });
        expect(comp.find(ConditionalRenderLoading)).toHaveLength(0);
        expect(comp.find('#conditionalRenderSuccess')).toHaveLength(1);
        expect(alreadyResolvedGenerator).not.toHaveBeenCalled();
    });
    test('should show final component when resolved', () => {return new Promise((done) => {
        jest.useRealTimers();
        const comp = getComponentWith(<ConditionalRender component={dummyComponent} test={() => false} resolve={promiseGenerator} />, { enzyme: true });
        setTimeout( () => {
            expect(comp.find('#conditionalRenderSuccess')).toHaveLength(0);
            doResolve('Done');
            setTimeout( () => {
                comp.update();
                expect(comp.find('#conditionalRenderSuccess')).toHaveLength(1);
                done();
            },1);
        },1);
    });});
    test('should show ConditionalRenderRejected on rejection', () => {return new Promise((done) => {
        jest.useRealTimers();
        const comp = getComponentWith(<ConditionalRender component={dummyComponent} test={() => false} resolve={promiseGenerator} />, { enzyme: true });
        setTimeout( () => {
            expect(comp.find('#conditionalRenderSuccess')).toHaveLength(0);
            expect(comp.find(ConditionalRenderRejected)).toHaveLength(0);
            doReject('Failed to load');
            setTimeout( () => {
                comp.update();
                expect(comp.find(ConditionalRenderRejected)).toHaveLength(1);
                done();
            },1);
        },1);
    });});
});
