/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { asyncLoaderRole } from '../src/helper.asyncLoader';

function reset ()
{
    dummyObject.DATA = null;
    dummyObject._ALR_initializing = null;
    dummyObject._ALR_onInitialize = [];
    dummyObject._ALR_hasInitialized = false;
}

const dummyObject = {
    ...asyncLoaderRole,

    DATA: null,

    _performAsyncImport()
    {
        return new Promise( (resolve) =>
        {
            resolve({ module: { data: "dummy" }});
        });
    },

    _loadedData(data)
    {
        this.DATA = data;
    },
};

beforeEach( reset );

describe('Raw object', () => {
    test('_performAsyncImport', () => {
        expect( () => {
            asyncLoaderRole._performAsyncImport();
        }).toThrow();
    });
    test('_loadedData', () => {
        expect( () => {
            asyncLoaderRole._loadedData();
        }).toThrow();
    });
});

describe('With a dummy object', () => {
    test('Initialization', () => {return new Promise((done) => {

        expect( () => {
            dummyObject.initialize().then(done);
        }).not.toThrow();
    });});
    test('Initialization with onInitialize', () => {return new Promise((done) => {
        dummyObject.onInitialize(() => {
            const init = jest.fn();
            dummyObject.onInitialize(init);
            // Should call it immediately since we've already initialized
            expect(init).toHaveBeenCalled();
            done();
        });
        expect( () => {
            dummyObject.initialize();
        }).not.toThrow();
    });});
    test('Double-initialization protection', () => {
        dummyObject._ALR_initializing = true;
        expect(dummyObject.initialize()).toBe(true);
    });
});
