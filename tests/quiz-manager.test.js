/*
 * @flow
 * @prettier
 *
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import quizManagerHandler from "../src/quiz-manager";
import { quizDataManager } from "../src/quiz-manager";
import quizData from "../data/quiz/quiz.json";
// Used to tell Flow that a value is not null (as an alternative to dozens of
// FlowIgnore statements, longer checks or just disabling flow).
import invariant from "assert";

beforeEach(() => {
    // $FlowExpectedError[prop-missing]
    quizDataManager._loadedData(quizData);
    // $FlowExpectedError[prop-missing]
    quizDataManager._ALR_hasInitialized = true;
});

function getQuizManager() {
    return new quizManagerHandler("testQuiz");
}

describe("quizManagerHandler", () => {
    test("Non-existing quiz", () => {
        const m = new quizManagerHandler("does-not-exist");
        expect(m.exists).toBe(false);
        expect(m.totalQuestions).toBe(0);
        expect(m.currentID).toBe(1);
        expect(m.current).toBe(null);
        expect(m.points).toBe(0);
        expect(m.possiblePoints()).toBe(0);
        expect(m.isDone()).toBe(true);
        expect(JSON.stringify(m.questionList)).toBe("[]");

        expect(() => m.next()).not.toThrow();
    });

    test("Basic object", () => {
        const m = getQuizManager();
        expect(m.exists).toBe(true);
        expect(m.totalQuestions).toBe(5);
        expect(m.possiblePoints()).toBe(m.totalQuestions);
        expect(m.currentID).toBe(1);
        expect(m.current).not.toBe(null);
        expect(m.points).toBe(0);
        expect(m.isDone()).toBe(false);
    });

    test("next()", () => {
        const m = getQuizManager();
        expect(m.exists).toBe(true);
        expect(m.currentID).toBe(1);
        expect(() => m.next()).not.toThrow();
        expect(m.currentID).toBe(2);

        let maxLoop = 0;
        while (m.current !== null) {
            expect(() => m.next()).not.toThrow();
            if (maxLoop > 1000) {
                throw "next() is looping too long, aborting";
            }
            maxLoop++;
        }

        expect(m.next()).toBe(null);
    });

    test("listen/notifyListeners", () => {
        const m = getQuizManager();
        const listener = jest.fn();
        m.listen(listener);
        expect(listener).not.toHaveBeenCalled();
        m.notifyListeners();
        expect(listener).toHaveBeenCalledTimes(1);

        m.next();
        expect(listener).toHaveBeenCalledTimes(2);

        m.registerCorrect();
        expect(listener).toHaveBeenCalledTimes(3);

        // $FlowIgnore[incompatible-call]
        m.registerIncorrect(m.current);
        expect(listener).toHaveBeenCalledTimes(4);

        // Should survive a reset
        m.reset();
        expect(listener).toHaveBeenCalledTimes(5);
    });

    test("reset", () => {
        const m = getQuizManager();
        expect(m.exists).toBe(true);
        expect(m.totalQuestions).toBe(5);
        expect(m.possiblePoints()).toBe(m.totalQuestions);
        expect(m.currentID).toBe(1);
        expect(m.current).not.toBe(null);
        expect(m.points).toBe(0);
        expect(m.isDone()).toBe(false);

        m.registerCorrect();
        m.next();
        expect(m.points).toBe(1);
        expect(m.currentID).toBe(2);

        m.reset();
        expect(m.points).toBe(0);
        expect(m.currentID).toBe(1);

        let maxLoop = 0;
        while (m.current !== null) {
            expect(() => m.registerCorrect()).not.toThrow();
            expect(() => m.next()).not.toThrow();
            if (maxLoop > 1000) {
                throw "next() is looping too long, aborting";
            }
            maxLoop++;
        }
        expect(m.points).not.toBe(0);
        expect(m.currentID).not.toBe(1);

        m.reset();
        expect(m.points).toBe(0);
        expect(m.currentID).toBe(1);
    });

    test("questionList should be immutable", () => {
        const m = getQuizManager();
        const questions = m.totalQuestions;
        expect(questions).not.toBe(0);

        let maxLoop = 0;
        while (m.current !== null) {
            expect(() => m.next()).not.toThrow();
            if (maxLoop > 1000) {
                throw "next() is looping too long, aborting";
            }
            maxLoop++;
            expect(m.questionList).toHaveLength(questions);
        }
    });
});

describe("quizManagerQuestion", () => {
    describe("Splash question", () => {
        test("Basic object", () => {
            const m = getQuizManager();
            const q = m.current;
            invariant(q);

            expect(q.markdown).toBe(
                "Splash screen with markdown [internal link](:handbook/Legemiddelhandtering).",
            );
            expect(q.correct).toBe(false);
            expect(q.question).toBe("1_SPLASH");
            expect(q.splashSlide).toBe(true);
            expect(q.questionNumber).toBe(1);
            expect(q.totalQuestions).toBe(m.totalQuestions);
            expect(Array.isArray(q.possibleAnswers)).toBe(true);
            expect(q.handler).toBeInstanceOf(quizManagerHandler);
            expect(q.numberOfCorrectAnswersRequired).toBe(1);
        });
    });
    describe("Non-splash, single-answer question", () => {
        test("Basic object", () => {
            const m = getQuizManager();
            m.next();
            const q = m.current;
            invariant(q);

            expect(q.correct).toBe(false);
            expect(q.markdown).toBe(undefined);
            expect(q.question).toBe("2_QUESTION");
            expect(q.splashSlide).toBe(false);
            expect(q.questionNumber).toBe(2);
            expect(q.totalQuestions).toBe(m.totalQuestions);
            expect(Array.isArray(q.possibleAnswers)).toBe(true);
            expect(q.handler).toBeInstanceOf(quizManagerHandler);
            expect(q.possibleAnswers.length).toBe(3);
            expect(q.numberOfCorrectAnswersRequired).toBe(1);
        });
    });
    describe("Non-splash, multi-answer question", () => {
        test("Basic object", () => {
            const m = getQuizManager();
            m.next();
            m.next();
            m.next();
            m.next();
            const q = m.current;
            invariant(q);

            expect(q.correct).toBe(false);
            expect(q.markdown).toBe("MARKDOWN");
            expect(q.question).toBe("5_MULTI_ANSWER_OPTIONAL");
            expect(q.splashSlide).toBe(false);
            expect(q.questionNumber).toBe(5);
            expect(q.totalQuestions).toBe(m.totalQuestions);
            expect(Array.isArray(q.possibleAnswers)).toBe(true);
            expect(q.handler).toBeInstanceOf(quizManagerHandler);
            expect(q.possibleAnswers.length).toBe(5);
            expect(q.numberOfCorrectAnswersRequired).toBe(1);
        });
        test("multi correct with only one answer required", () => {
            const m = getQuizManager();
            m.next();
            m.next();
            m.next();
            m.next();
            const q = m.current;
            invariant(q);
            expect(q.correct).toBe(false);

            expect(q.numberOfCorrectAnswersRequired).toBe(1);

            const listener = jest.fn();
            m.listen(listener);
            // State should already be updated when we notify listeners, so we
            // validate state within a listener
            m.listen(() => {
                expect(q.correct).toBe(true);
            });

            q.answer({
                correct: true,
                entry: "Correct",
            });
            expect(listener).toHaveBeenCalledTimes(1);
            expect(q.correct).toBe(true);
        });
    });
    describe("Non-splash, multi-answer question with multiple required answers", () => {
        test("Basic object", () => {
            const m = getQuizManager();
            m.next();
            m.next();
            m.next();
            const q = m.current;
            invariant(q);

            expect(q.correct).toBe(false);
            expect(q.markdown).toBe("MARKDOWN");
            expect(q.question).toBe("4_MULTI_ANSWER_REQUIRED");
            expect(q.splashSlide).toBe(false);
            expect(q.questionNumber).toBe(4);
            expect(q.totalQuestions).toBe(m.totalQuestions);
            expect(Array.isArray(q.possibleAnswers)).toBe(true);
            expect(q.handler).toBeInstanceOf(quizManagerHandler);
            expect(q.possibleAnswers.length).toBe(5);
            expect(q.numberOfCorrectAnswersRequired).toBe(2);
        });
        test("multi correct", () => {
            const m = getQuizManager();
            m.next();
            m.next();
            m.next();
            const q = m.current;
            invariant(q);
            expect(q.correct).toBe(false);

            const listener = jest.fn();
            m.listen(listener);

            q.answer({
                correct: true,
                entry: "Correct",
            });
            expect(listener).toHaveBeenCalledTimes(1);
            expect(q.correct).toBe(false);

            q.answer({
                correct: true,
                entry: "Correct2",
            });
            expect(listener).toHaveBeenCalledTimes(2);
            expect(q.correct).toBe(true);
        });
        test("multi correct with same answer twice", () => {
            const m = getQuizManager();
            m.next();
            m.next();
            m.next();
            const q = m.current;
            invariant(q);
            expect(q.correct).toBe(false);

            const listener = jest.fn();
            m.listen(listener);

            q.answer({
                correct: true,
                entry: "Correct",
            });
            expect(listener).toHaveBeenCalledTimes(1);
            expect(q.correct).toBe(false);

            q.answer({
                correct: true,
                entry: "Correct",
            });
            expect(listener).toHaveBeenCalledTimes(1);
            expect(q.correct).toBe(false);
        });
    });
    describe("Shared logic", () => {
        test("answerCorrect", () => {
            const m = getQuizManager();
            const q = m.current;
            invariant(q);
            expect(q.correct).toBe(false);

            const listener = jest.fn();
            m.listen(listener);

            q.answerCorrect();
            expect(listener).toHaveBeenCalledTimes(1);
            expect(q.correct).toBe(true);
        });
        describe("answer", () => {
            test("correct", () => {
                const m = getQuizManager();
                m.next();
                const q = m.current;
                invariant(q);
                expect(q.correct).toBe(false);

                const listener = jest.fn();
                m.listen(listener);

                q.answer({
                    correct: true,
                    entry: "Correct",
                });
                expect(listener).toHaveBeenCalledTimes(1);
                expect(q.correct).toBe(true);
            });
            test("incorrect", () => {
                const m = getQuizManager();
                m.next();
                const q = m.current;
                invariant(q);
                expect(q.correct).toBe(false);

                const listener = jest.fn();
                m.listen(listener);

                q.answer({
                    correct: false,
                    entry: "Wrong1",
                });
                expect(listener).toHaveBeenCalledTimes(1);
                expect(q.correct).toBe(false);
            });
        });
        test("hasAnswered", () => {
            const m = getQuizManager();
            m.next();
            const q = m.current;
            invariant(q);

            const answer = {
                correct: false,
                entry: "Wrong1",
            };
            const secondAnswer = {
                correct: true,
                entry: "Correct",
            };

            expect(q.hasAnswered(answer)).toBe(false);
            expect(q.hasAnswered(secondAnswer)).toBe(false);

            q.answer(answer);
            expect(q.hasAnswered(answer)).toBe(true);
            expect(q.hasAnswered(secondAnswer)).toBe(false);

            q.answer(secondAnswer);
            expect(q.hasAnswered(answer)).toBe(true);
            expect(q.hasAnswered(secondAnswer)).toBe(true);
        });
    });
});

describe("quizDataManager", () => {
    test("Initialization", () => {
        // $FlowExpectedError[prop-missing]
        quizDataManager.__index = null;
        // $FlowExpectedError[prop-missing]
        quizDataManager._ALR_initializing = null;
        // $FlowExpectedError[prop-missing]
        quizDataManager._ALR_onInitialize = [];
        // $FlowExpectedError[prop-missing]
        quizDataManager._ALR_hasInitialized = false;
        // $FlowExpectedError[prop-missing]
        quizDataManager._data = null;

        expect(quizDataManager._data).toBe(null);
        expect(() => {
            // $FlowExpectedError[prop-missing]
            quizDataManager._loadedData({});
        }).not.toThrow();
        expect(quizDataManager._data).not.toBe(null);
    });
    test("data()", () => {
        // $FlowExpectedError[prop-missing]
        quizDataManager.__index = null;
        // $FlowExpectedError[prop-missing]
        quizDataManager._ALR_initializing = null;
        // $FlowExpectedError[prop-missing]
        quizDataManager._ALR_onInitialize = [];
        // $FlowExpectedError[prop-missing]
        quizDataManager._ALR_hasInitialized = false;
        // $FlowExpectedError[prop-missing]
        quizDataManager._data = null;

        expect(() => quizDataManager.data()).toThrow();
        expect(() => {
            // $FlowExpectedError[prop-missing]
            quizDataManager._loadedData({});
        }).not.toThrow();
        expect(() => quizDataManager.data()).not.toThrow();
    });
});
