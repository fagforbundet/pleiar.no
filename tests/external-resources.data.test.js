/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('../data/datasett/external-resources.json');
import data from '../data/datasett/external-resources.json';

import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { getComponentWith } from './test-helpers';
import ExternalResources from '../src/react-components/external-resources';
import { testInitPleiarSearcher } from './search-helper.js';
Enzyme.configure({ adapter: new Adapter() });

test.each(data)('External resources data syntax: %p', (entry) =>
{
    expect(entry).toMatchObject({
        // Must be non-empty string
        title: expect.stringMatching(/.+/),
        // Must be URL-like
        url: expect.stringMatching(/^https?:\/\//),
        // Can be empty, but must be present
        description: expect.any(String),
        // Must be one of the known languages
        language: expect.stringMatching(/^(Norsk|Engelsk)$/)
    });
    if(entry.description !== "")
    {
        expect(entry).toMatchObject({
            description: expect.stringMatching(/\.$/)
        });
    }
    // keywords is optional, but must be non-zero-length string if present
    if(entry.keywords !== undefined)
    {
        expect(entry).toMatchObject({
            keywords: expect.stringMatching(/.+/)
        });
    }

    // There's probably a better way to do this, the point is to check if
    // there are any unknown keys
    const permittedEntries = [ 'title','url','description','language','keywords' ];

    for(const key in entry)
    {
        expect(permittedEntries).toContain(key);
    }
});

test('External resources data rendering', () =>
{
    expect( () =>
    {
        getComponentWith(ExternalResources,{
            routingAssistant: true,
            router: true,
            route: "/andresider",
            previousPath: "/",
            enzyme: true,
            redux: true
        });
    }).not.toThrow();
});

test('External resources data indexing', () =>
{
    expect( () =>
    {
        testInitPleiarSearcher();
    }).not.toThrow();
});
