/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import { prepareEnv } from "./test-helpers";
jest.mock("../src/searcher", () => {
  return {
    hasInitialized: jest.fn(() => false),
    onInitialize: jest.fn(),
    initialize: jest.fn(),
  };
});
jest.mock("../src/med-synonym-searcher", () => {
  return {
    hasInitialized: jest.fn(() => false),
    onInitialize: jest.fn(),
    initialize: jest.fn(),
  };
});
jest.mock("../src/helper.feature-detection", () => {
  return {
    aSupportsRel: jest.fn(() => false),
  };
});
import featureDetection from "../src/helper.feature-detection";
import PleiarSearcher from "../src/searcher";
import MedSearcher from "../src/med-synonym-searcher";
import SysInfo from "../src/helper.sysinfo";

prepareEnv();

function redefineProp(on, name, value) {
  Object.defineProperty(on, name, { value: value, configurable: true });
}

// $FlowIgnore[method-unbinding]
const getElementsByTagName = document.getElementsByTagName;
beforeEach(() => {
  // $FlowIgnore[prop-missing]
  PleiarSearcher.hasInitialized.mockClear();
  // $FlowIgnore[prop-missing]
  PleiarSearcher.initialize.mockClear();
  // $FlowIgnore[prop-missing]
  PleiarSearcher.onInitialize.mockClear();
  // $FlowIgnore[prop-missing]
  MedSearcher.hasInitialized.mockClear();
  // $FlowIgnore[prop-missing]
  MedSearcher.initialize.mockClear();
  // $FlowIgnore[prop-missing]
  MedSearcher.onInitialize.mockClear();
  // $FlowIgnore[cannot-write]
  document.getElementsByTagName = getElementsByTagName;
});

test("Snapshot", () => {
    redefineProp(
        navigator,
        "userAgent",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
    );
    expect(JSON.stringify(SysInfo.getSystemInfo())).toBe("{\"UA\":\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36\",\"sources\":[],\"dimensions\":{\"w\":0,\"h\":0},\"bodyClass\":null,\"auth\":true,\"platform\":\"\",\"quizDataManagerStatus\":\"ikke lastet\",\"medSearcherStatus\":\"ikke lastet\",\"pleiarSearcherStatus\":\"ikke lastet\",\"serviceWorkerStatus\":\"ikke tilgjengelig\",\"appVersion\":\"(testing)\",\"dataVersion\":\"(testing)\",\"featuresDetected\":[],\"device\":{\"isAppMode\":false,\"isTouchScreen\":false},\"appInstall\":{\"mode\":null,\"canInstall\":false}}");
});
test("With fake service worker, not enabled", () => {
    redefineProp(navigator, "serviceWorker", { controller: null });
    expect(SysInfo.getSystemInfo()).toMatchObject({
        serviceWorkerStatus: "tilgjengelig, ikke i bruk",
    });
});
test("With fake service worker, enabled", () => {
    redefineProp(navigator, "serviceWorker", { controller: true });
    expect(SysInfo.getSystemInfo()).toMatchObject({
        serviceWorkerStatus: "tilgjengelig, i bruk",
    });
});
test("Non-android platform", () => {
    redefineProp(
        navigator,
        "userAgent",
        "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
    );
    redefineProp(navigator, "platform", "Linux x86_64");
    expect(SysInfo.getSystemInfo()).toMatchObject({
        platform: "Linux x86_64",
    });
});
test("Android platform", () => {
    redefineProp(
        navigator,
        "userAgent",
        "Mozilla/5.0 (Linux; Android 9; SM-G950F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.90 Mobile Safari/537.36"
    );
    redefineProp(navigator, "platform", "Linux armv8l");
    expect(SysInfo.getSystemInfo()).toMatchObject({
        platform: "Android (Linux) armv8l",
    });
});
test("Crashing source list", () => {
    // $FlowIgnore[cannot-write]
    document.getElementsByTagName = jest.fn(() => {
        throw "OH NOES";
    });
    expect(() => {
        SysInfo.getSystemInfo();
    }).not.toThrow();
});
describe("featureDetection", () => {
    test("With fake appcache, not enabled", () => {
        redefineProp(window, "applicationCache", { UNCACHED: 0, status: 0 });
        expect(SysInfo.featureDetection()).toEqual(
            expect.arrayContaining(["appCache(unused)"])
        );
    });

    test("With fake appcache, enabled", () => {
        redefineProp(window, "applicationCache", { UNCACHED: 0, status: 99 });
        // For some reason the other tests are affecting this one, so we have
        // to reset here
        window.applicationCache.status = 99;
        expect(SysInfo.featureDetection()).toEqual(
            expect.arrayContaining(["appCache(inUse)"])
        );
    });

    test("With no appcache", () => {
        redefineProp(window, "applicationCache", { UNCACHED: 0, status: 99 });
        delete window.applicationCache;
        expect(SysInfo.featureDetection()).not.toEqual(
            expect.arrayContaining(["appCache(inUse)"])
        );
        expect(SysInfo.featureDetection()).not.toEqual(
            expect.arrayContaining(["appCache(unused)"])
        );
    });

    test("Supports rel=noopener", () => {
        // $FlowIgnore[cannot-write]
        featureDetection.aSupportsRel = jest.fn(() => true);
        expect(SysInfo.featureDetection()).toEqual(
            expect.arrayContaining(["a-rel-noopener"])
        );
    });

    test("Does not support rel=noopener", () => {
        // $FlowIgnore[cannot-write]
        featureDetection.aSupportsRel = jest.fn(() => false);
        expect(SysInfo.featureDetection()).not.toEqual(
            expect.arrayContaining(["a-rel-noopener"])
        );
    });

    test("Has storage.persist", () => {
        redefineProp(window.navigator, "storage", { persist: jest.fn() });
        expect(SysInfo.featureDetection()).toEqual(
            expect.arrayContaining(["storage.persist"])
        );
    });

    test("No storage.persist", () => {
        redefineProp(window.navigator, "storage", { persist: true });
        expect(SysInfo.featureDetection()).not.toEqual(
            expect.arrayContaining(["storage.persist"])
        );
    });

    test.each(["connection", "mozConnection", "webkitConnection"])(
        "Has navigator.connection: %s",
        (variant) => {
            redefineProp(window.navigator, "connection", false);
            redefineProp(window.navigator, "mozConnection", false);
            redefineProp(window.navigator, "webkitConnection", false);
            redefineProp(window.navigator, variant, true);
            expect(SysInfo.featureDetection()).toEqual(
                expect.arrayContaining(["navigator.connection"])
            );
        }
    );

    test("No navigator.connection", () => {
        redefineProp(window.navigator, "connection", false);
        redefineProp(window.navigator, "mozConnection", false);
        redefineProp(window.navigator, "webkitConnection", false);
        expect(SysInfo.featureDetection()).not.toEqual(
            expect.arrayContaining(["navigator.connection"])
        );
    });
});
