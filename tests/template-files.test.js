/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import _ from 'lodash';
import fs from 'fs';

test('index.html syntax', () => {
    const indexHtml = fs.readFileSync('./index.html').toString();
    let template;
    let rendered;
    expect( () => {
        template = _.template(indexHtml);
    }).not.toThrow();
    expect( () => {
        rendered = template({
            htmlWebpackPlugin: {
                options: { dataConfig: { themeColor: '#343a40' } },
                files: {
                    chunks: [
                        { entry: 'test.js' },
                    ],
                    css: [
                        'test.css',
                    ]
                },
            },
            webpack: { assetsByChunkName: { 'search-index': 'search-index' } }
        });
    }).not.toThrow();
    expect(rendered).toMatch('<meta name="theme-color" content="#343a40"/>');
    expect(rendered).toMatch('src="test.js"');
    expect(rendered).toMatch('href="test.css"');
});
test('appmanifest.json syntax', () => {
    const appmanifestJSON = fs.readFileSync('./src/appmanifest.json').toString();
    let template;
    let rendered;
    expect( () => {
        template = _.template(appmanifestJSON);
    }).not.toThrow();
    expect( () => {
        rendered = template({
            dataConfig: { themeColor: '#343a40' }
        });
    }).not.toThrow();
    expect(rendered).toMatch('"theme_color":"#343a40"');
    expect(rendered).toMatch('"background_color":"#343a40"');
});
