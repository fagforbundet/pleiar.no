/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('reactstrap');
import ExternalResources from '../src/react-components/external-resources';
import data from '../data/datasett/external-resources.json';
import { setExtRSearchString, toggleOnlyNorwegian } from '../src/actions/external-resources';
import { prepareEnv, checkSnapshot, getRealRedux, changeFieldValue, getComponentWith } from './test-helpers';
import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import RoutingAssistant from '../src/routing';
import { RequiresPleiarSearcher } from '../src/react-components/global-search.js';
import { testInitPleiarSearcher } from './search-helper';
import device from '../src/helper.device';
import { Input } from 'reactstrap';

Enzyme.configure({ adapter: new Adapter() });

jest.useFakeTimers();
RoutingAssistant._debounceTimeout = 1;
prepareEnv();
testInitPleiarSearcher();

const compSettings = {
    redux: true,
    includeReduxStore: true,
    router: true,
    route: "/andresider",
    routingAssistant: true,
    enzyme: true,
    previousPath: "/"
};
const getExternalResourcesWithRedux = () =>
{
    return getComponentWith(ExternalResources,compSettings);
};

test('Basic rendering of the external resources page', () => {
    checkSnapshot(ExternalResources,compSettings);
    expect(global.__title).toMatch('Andre sider');
});

test("redux store", () => {
    const store = getRealRedux();
    // Default state
    expect( store.getState().externalResources).toMatchObject({
        onlyNorwegian: false,
        search: ""
    });

    store.dispatch(toggleOnlyNorwegian());
    expect( store.getState().externalResources.onlyNorwegian ).toBe(true);
    store.dispatch(toggleOnlyNorwegian());
    expect( store.getState().externalResources.onlyNorwegian ).toBe(false);

    store.dispatch(setExtRSearchString("test"));
    expect( store.getState().externalResources.search).toBe("test");
});

test('UI interaction', () => {
    const {store,component} = getExternalResourcesWithRedux();
    // Default should be an empty search
    expect(store.getState().externalResources.search).toBe('');

    // And the first item should match the first data source
    expect(component.find('.externalEntry').at(0).find('.linkWrapper').at(0).props().href).toBe(data[0].url);
    expect(component.find('.externalEntry')).toHaveLength(12);

    // Try toggling only Norwegian entries
    expect(store.getState().externalResources.onlyNorwegian).toBeFalsy();
    expect(component.find('[name="onlyNorwegian"]').at(0).props().checked).toBeFalsy();
    component.find('[name="onlyNorwegian"]').at(0).simulate('change');
    expect(store.getState().externalResources.onlyNorwegian).toBeTruthy();
    expect(component.find('[name="onlyNorwegian"]').at(0).props().checked).toBeTruthy();
    expect(component.find('.externalEntry')).toHaveLength(8);
    component.find('[name="onlyNorwegian"]').at(0).simulate('change');
    expect(store.getState().externalResources.onlyNorwegian).toBeFalsy();


    // Try changing the search value
    changeFieldValue(component.find('[name="search"]').at(0),'Legemiddel');
    expect(store.getState().externalResources.search).toBe('Legemiddel');
    expect(component.find('[name="search"]').at(0).props().value).toBe('Legemiddel');
    expect(component.find('.externalEntry')).toHaveLength(8);

    // Try toggling only Norwegian entries
    expect(store.getState().externalResources.onlyNorwegian).toBeFalsy();
    expect(component.find('[name="onlyNorwegian"]').at(0).props().checked).toBeFalsy();
    component.find('[name="onlyNorwegian"]').at(0).simulate('change');
    expect(store.getState().externalResources.onlyNorwegian).toBeTruthy();
    expect(component.find('[name="onlyNorwegian"]').at(0).props().checked).toBeTruthy();
    expect(component.find('.externalEntry')).toHaveLength(6);
    component.find('[name="onlyNorwegian"]').at(0).simulate('change');
    expect(store.getState().externalResources.onlyNorwegian).toBeFalsy();

    // Try removing the search value
    component.find('.clearSearchField').at(0).simulate('click');
    expect(store.getState().externalResources.search).toBe('');
    expect(component.find('[name="search"]').at(0).props().value).toBe('');
    expect(component.find('.externalEntry')).toHaveLength(12);

    // Try one of the "suggestion" links
    component.find('.externalKeyword').at(0).simulate('click');
    expect(store.getState().externalResources.search).toBe('Legemiddel');
    expect(component.find('[name="search"]').at(0).props().value).toBe('Legemiddel');
    expect(component.find('.externalEntry')).toHaveLength(8);

    // Try searching for a string that has no hits
    changeFieldValue(component.find('[name="search"]').at(0),'thishasnoresults');
    expect(component.find('.externalEntry')).toHaveLength(0);
    expect(component.find('#extNoResults').at(0).text()).toMatch(/ingen treff/);
    expect(component.find('#extNoResults img').at(0).prop('src')).toMatch(/beaver/);

    // Try removing the search value by emptying the field
    changeFieldValue(component.find('[name="search"]').at(0),'');
    expect(store.getState().externalResources.search).toBe('');
    expect(component.find('[name="search"]').at(0).props().value).toBe('');
    expect(component.find('.externalEntry')).toHaveLength(data.length*2);
    jest.runAllTimers();
    expect(RoutingAssistant.history.action).toBe('REPLACE');
});

describe('Field focus', () => {
    test('Should focus on desktop', () => {
        device.isTouchScreen = jest.fn( () => { return false; });
        const {component} = getExternalResourcesWithRedux();
        expect(component.find(Input).at(0).props().autoFocus).toBe(true);
    });
    test('Should not focus on touch screens', () => {
        device.isTouchScreen = jest.fn( () => { return true; });
        const {component} = getExternalResourcesWithRedux();
        expect(component.find(Input).at(0).props().autoFocus).toBe(false);
    });
});

test('Routing callbacks', () => {
    const {store,component} = getExternalResourcesWithRedux();
    store.dispatch(setExtRSearchString("test"));
    expect( store.getState().externalResources.search).toBe("test");
    component.find(RequiresPleiarSearcher).at(0).instance().props.onRouteSync("hello",2);
    expect( store.getState().externalResources.search).toBe("hello");
});
