/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import ToolsSection, { ToolsSearchRenderer, ToolContainer } from '../src/react-components/tools';
import { checkSnapshot, getComponentWith } from './test-helpers';
import { RequiresAuth } from '../src/react-components/auth';
import toolsList from '../src/tools.list.js';
import NutriCalc from '../src/react-components/nutricalc';
import MedCalc from '../src/react-components/medcalc';
import NEWS from '../src/react-components/news';

const globalDefaultCompSettings = {
    redux: true,
    includeReduxStore: false,
    router: true,
    route: "/verktoy",
    routingAssistant: false,
    enzyme: true,
    previousPath: "/verktoy"
};

test('Basic rendering of the tools index page', () => {
    checkSnapshot(ToolsSection,{
        ...globalDefaultCompSettings,
        enzymeSub: ToolsSection
    });
    expect(global.__title).toMatch('Verktøy');
});

test('Rendering of a tools search result', () => {
    checkSnapshot(<ToolsSearchRenderer entry={ toolsList[0].tools[0] } />,{
        ...globalDefaultCompSettings,
        enzymeSub: ToolsSearchRenderer
    });
});

describe('ToolContainer', () => {
    test('Basic', () => {
        const comp = getComponentWith(<ToolContainer tool="test">
            Something
        </ToolContainer>, globalDefaultCompSettings);
        expect(comp.find(RequiresAuth)).toHaveLength(1);
    });
    test('requiresAuth=false', () => {
        const comp = getComponentWith(<ToolContainer tool="test" requiresAuth={false}>
            Something
        </ToolContainer>, globalDefaultCompSettings);
        expect(comp.find(RequiresAuth)).toHaveLength(0);
    });
});

describe('ToolsSearchRenderer functionality for toolsList entries', () => {
    describe.each(toolsList)("subentry", (entry) => {
        test.each(entry.tools)("%j", (tool) => {
            checkSnapshot(<ToolsSearchRenderer entry={ tool } />,{
                ...globalDefaultCompSettings,
                enzymeSub: ToolsSearchRenderer
            });
        });
    });
});

describe('Routing', () => {
    const routes = [
        {
            path: 'ernaering',
            comp: NutriCalc,
        },
        {
            path: 'medikamentregning',
            comp: MedCalc,
        },
        {
            path: 'news',
            comp: NEWS
        }
    ];
    test.each(routes)('%p', (entry) => {
        const comp = getComponentWith(ToolsSection,{
            ...globalDefaultCompSettings,
            route: '/verktoy/'+entry.path
        });
        expect(comp.find(entry.comp)).toHaveLength(1);
    });
});
