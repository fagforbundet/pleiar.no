/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { applyBrowserWorkaroundsIfNeeded } from '../src/workarounds';

beforeEach(() => {
    // Ensure the class list is reset
    document.body.className = '';
});

const noWorkaroundsBrowsers = [
    null,
    // Firefox
    '(X11; Linux x86_64; rv:57.0) Gecko/20100101 Firefox/57.0',
    // Chrome
    '(X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
    // Chrome on Android
    '(Linux; Android 9; SM-G950F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.90 Mobile Safari/537.36',
    // Firefox focus with Android web engine
    '(Linux; Android 9) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Focus/8.0.8 Chrome/73.0.3683.90 Mobile Safari/537.36',
    // Firefox android
    '(Android 9; Mobile; rv:67.0) Gecko/67.0 Firefox/67.0',
    // The new chromium-based Edge
    "(Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.74 Safari/537.36 Edg/79.0.309.43",
    // Safari on iOS 11
    '(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Mobile/15B202 Safari/604.1',
    // Safari on iOS 9
    '(iPad; CPU OS 9_3_5 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13G36 Safari/601.1',
    // Chrome on iOS
    '(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) CriOS/63.0.3239.73 Mobile/15B202 Safari/604.1',
    // Firefox on iOS
    '(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) FxiOS/63.0.3239.73 Mobile/15B202 Safari/604.1',
    // Facebook browser on iOS
    '(iPad; CPU OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 [FBAN/FBIOS;FBDV/iPad6,11;FBMD/iPad;FBSN/iOS;FBSV/12.4;FBSS/2;FBID/tablet;FBLC/nb_NO;FBOP/5;FBCR/]',
];
test.each(noWorkaroundsBrowsers)('Browser with no workarounds: %s',(browser) =>
{
    if(browser !== null)
    {
        navigator.__defineGetter__('userAgent', function(){
            return "Mozilla/5.0 "+browser;
        });
    }
    applyBrowserWorkaroundsIfNeeded();
    // Nothing at all should be applied
    expect(document.body.classList.contains('ms-workarounds')).toBeFalsy();
    expect(document.body.classList.contains('ms-ie')).toBeFalsy();
    expect(document.body.classList.contains('ms-edge')).toBeFalsy();
    expect(document.body.classList.contains('ms-edge-legacy')).toBeFalsy();
    expect(document.body.classList.contains('ios-workarounds')).toBeFalsy();
});

test('Edge (legacy EdgeHTML)', () => {
    // Changes the userAgent
    navigator.__defineGetter__('userAgent', function(){
        return 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.10136';
    });

    // Should have successfully identified Edge and applied workarounds
    applyBrowserWorkaroundsIfNeeded();
    expect(document.body.classList.contains('ms-workarounds')).toBeTruthy();
    expect(document.body.classList.contains('ms-ie')).toBeFalsy();
    expect(document.body.classList.contains('ms-edge')).toBeTruthy();
    expect(document.body.classList.contains('ms-edge-legacy')).toBeTruthy();
    expect(document.body.classList.contains('ios-workarounds')).toBeFalsy();
});

test('IE 11', () => {
    // IE 11 doesn't have a parseFloat method on Number
    Number.parseFloat = undefined;

    // Changes the userAgent
    navigator.__defineGetter__('userAgent', function(){
        return 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';
    });

    // Should have successfully identified IE11 and applied workarounds
    applyBrowserWorkaroundsIfNeeded();
    expect(document.body.classList.contains('ms-workarounds')).toBeTruthy();
    expect(document.body.classList.contains('ms-ie')).toBeTruthy();
    expect(document.body.classList.contains('ms-edge')).toBeFalsy();
    expect(document.body.classList.contains('ms-edge-legacy')).toBeFalsy();
    expect(document.body.classList.contains('ios-workarounds')).toBeFalsy();
    // Should have polyfilled parseFloat
    expect(Number.parseFloat).toBe(global.parseFloat);
});

// We don't officially support IE10. Workarounds should still be applied, though,
// in a best-effort approach to it working without modifying the source.
test('IE 10', () => {
    // IE 11 doesn't have a parseFloat method on Number
    Number.parseFloat = undefined;

    // Changes the userAgent
    navigator.__defineGetter__('userAgent', function(){
        return 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; Trident/6.0)';
    });

    // Should have successfully identified IE and applied workarounds
    applyBrowserWorkaroundsIfNeeded();
    expect(document.body.classList.contains('ms-workarounds')).toBeTruthy();
    expect(document.body.classList.contains('ms-ie')).toBeTruthy();
    expect(document.body.classList.contains('ms-edge')).toBeFalsy();
    expect(document.body.classList.contains('ms-edge-legacy')).toBeFalsy();
    expect(document.body.classList.contains('ios-workarounds')).toBeFalsy();
    // Should have polyfilled parseFloat
    expect(Number.parseFloat).toBe(global.parseFloat);
});

test('iPhone', () => {
    // Changes the userAgent
    navigator.__defineGetter__('userAgent', function(){
        return 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.3.5 (KHTML, like Gecko) Mobile/15B202';
    });

    // Should have successfully identified iOS and applied workarounds
    applyBrowserWorkaroundsIfNeeded();
    expect(document.body.classList.contains('ms-workarounds')).toBeFalsy();
    expect(document.body.classList.contains('ms-ie')).toBeFalsy();
    expect(document.body.classList.contains('ms-edge')).toBeFalsy();
    expect(document.body.classList.contains('ms-edge-legacy')).toBeFalsy();
    expect(document.body.classList.contains('ios-workarounds')).toBeFalsy();
});

test('iPad', () => {
    // Changes the userAgent
    navigator.__defineGetter__('userAgent', function(){
        return 'Mozilla/5.0 (iPad; CPU OS 12_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1';
    });

    // Should have successfully identified iOS and applied workarounds
    applyBrowserWorkaroundsIfNeeded();
    expect(document.body.classList.contains('ms-workarounds')).toBeFalsy();
    expect(document.body.classList.contains('ms-ie')).toBeFalsy();
    expect(document.body.classList.contains('ms-edge')).toBeFalsy();
    expect(document.body.classList.contains('ms-edge-legacy')).toBeFalsy();
    expect(document.body.classList.contains('ios-workarounds')).toBeFalsy();
});

test('Weirdo browser where document.body is null', () => {
    document.__defineGetter__('body', () => null);
    expect( () => {
        applyBrowserWorkaroundsIfNeeded();
    }).not.toThrow();
});
