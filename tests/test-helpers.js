/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import React from 'react';
import { MemoryRouter, Route } from 'react-router-dom';
import { NotFound } from '../src/react-components/shared.js';
import renderer from 'react-test-renderer';
import { act } from 'react-test-renderer';

import { HandbookEntry, HandbookIndex } from '../src/data.handbook';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import pleiarStore from '../src/reducers/root';
import { RoutingAssistantInit } from '../src/routing';

import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import dictionary from '../data/datasett/dictionary.json';
import quiz from "../data/quiz/quiz.json";

Enzyme.configure({ adapter: new Adapter() });

function getRealRedux ()
{
    return createStore(pleiarStore);
}

function prepareEnv ()
{
    global.GIT_REVISION_FULL = "(testing)";
    global.GIT_REVISION = "(testing)";
    global.GIT_REVISION_DATA = "(testing)";
}

function routedComponentWithRealRedux (route,previousPath,subcomponent, settings = {})
{
    const options = {
        enzyme: settings.enzyme,
        route, previousPath,
        router: true
    };
    return getComponentWith(subcomponent,options);
}

function _getRawReactComponentWith(subcomponent,options)
{
    const result = { };
    let component;
    const getSubComponentInstance = (props = {}) => {
        if(subcomponent.constructor.name == 'ReactWrapper' || subcomponent.$$typeof === Symbol.for('react.element'))
        {
            return subcomponent;
        }
        else
        {
            return React.createElement(subcomponent,props,null);
        }
    };
    if(typeof(options.redux) === "object" && options.redux.wrap === true && options.enzyme)
    {
        console.log(new Error("redux: { wrap: true } used with enzyme: they are incompatible and this isn't needed").stack);
        options.redux.wrap = false;
    }
    if(options.router)
    {
        component = props => getSubComponentInstance(props);
    }
    else
    {
        component = getSubComponentInstance();
    }
    if(options.router)
    {
        // Verify required parameters
        if (!options.route || !options.previousPath)
        {
            throw('To use a routed component the route and previousPath options must be provided');
        }
        jsdom.reconfigure({
            url: 'https://www.pleiar.no'+options.route
        });
        let routeSubObject;
        if(options.routingAssistant)
        {
            routeSubObject = React.createElement('div',{},[
                React.createElement(RoutingAssistantInit,{key:'routing-assistant'},null),
                React.createElement(Route, {
                    key: 'route',
                    component: component,
                    path: options.previousPath
                })
            ]);
        }
        else
        {
            routeSubObject = React.createElement(Route, {
                    component: component,
                    path: options.previousPath
                });
        }
        component = React.createElement(MemoryRouter, { initialEntries: [ { pathname: options.route, key: 'testing'} ]}, routeSubObject,);
    }
    if(options.redux)
    {
        const store = getRealRedux();
        if(typeof(options.redux) === 'object' && options.redux.wrap === true)
        {
            result.store = wrapRedux(store);
        }
        else
        {
            result.store = store;
        }
        component = React.createElement(Provider,{store},component);
    }
    result.component = component;
    return result;
}

function getComponentWith(subcomponent, options = {})
{
    let result;
    const {component,store} = _getRawReactComponentWith(subcomponent,options);
    if(options.enzyme)
    {
        const enzymeOptions = {};
        if(typeof(options.enzyme) === 'object' && options.enzyme.fullDOM)
        {
            const div = document.createElement('div');
            document.body.appendChild(div);
            enzymeOptions.attachTo = div;
        }
        result = Enzyme.mount( component, enzymeOptions );
        if(options.router && !options.ignore404)
        {
            expect(result.find(NotFound)).toHaveLength(0);
        }
        if(options.redux)
        {
            store.subscribe( () =>
            {
                // It won't auto-update it during testing, so we manually refresh on any redux event
                result.update();
            });
        }
    }
    else
    {
        result = renderer.create( component );
        if(options.router && !options.ignore404)
        {
            expect(result.root.findAllByType(NotFound)).toEqual([]);
        }
    }
    if(options.includeReduxStore)
    {
        return { component: result, store };
    }
    return result;
}

function checkSnapshot (subcomponent, options = {})
{
    if(subcomponent == null)
    {
        throw('checkSnapshot: got null subcomponent');
    }
    let tree;
    let root;
    if(options.enzyme)
    {
        root = tree = getComponentWith(subcomponent,options);
        if(options.enzymeSub)
        {
            tree = tree.find(options.enzymeSub);
        }
    }
    else
    {
        const component = getComponentWith(subcomponent,options);
        tree = component.toJSON();
    }
    expect(tree).toMatchSnapshot();
    if(options.enzyme && tree.unmount)
    {
        root.unmount();
    }
}

function checkRoutedSnapshot (route,previousPath,subcomponent)
{
    checkSnapshot(subcomponent,{
        router: true,
        route, previousPath
    });
}

function checkRoutedSnapshotWithRealRedux (route,previousPath,subcomponent)
{
    checkSnapshot(subcomponent,{
        router: true,
        redux: true,
        route, previousPath
    });
}

function changeFieldValue (field,text)
{
    field.getDOMNode().value = text;
    // FIXME: Should not provide target.value, as components should switch to
    // .currentTarget, which enzyme already provides
    field.simulate("change", { target: { value: text } });
}

let setConsoleState;
function setConsole (action)
{
    /* eslint-disable no-console */
    if(action === "disabled")
    {
        if (setConsoleState === undefined || !setConsoleState.error)
        {
            setConsoleState = {
                error: console.error,
                info: console.info,
                warn: console.warn,
                log: console.log
            };
        }
        console.error = console.log = console.warn = console.info = () => {};
    }
    else if(action === "enabled")
    {
        for(const name in setConsoleState)
        {
            console[name] = setConsoleState[name];
        }
    }
    else
    {
        throw('Unknown setConsole() parameter: '+action);
    }
    /* eslint-enable no-console */
}

function silent (block,args)
{
    let error;
    let retVal;
    setConsole("disabled");
    try
    {
        retVal = block().apply(null,args);
    }
    catch(err)
    {
        error = err;
    }
    setConsole("enabled");
    if(error)
    {
        throw(error);
    }
    return retVal;
}

function silentFunc (block)
{
    return () => {
        return silent(block,arguments);
    };
}

// Create a mock of window.location
function mockLocation ()
{
    // First we take a copy of whatever bits of the location that we can reuse
    // by stringifying and then parsing JSON
    const newLocation = JSON.parse(JSON.stringify(window.location));
    // Then we add mocked versions of various methods
    newLocation.reload = jest.fn();
    newLocation.replace = jest.fn();
    newLocation.toString = () => { return newLocation.href; };
    // Delete jsdom's implementation
    delete window.location;
    // Finally add our variant to window
    Object.defineProperty(window, 'location', {
        value: newLocation,
        configurable: true
    });
    return newLocation;
}

// Create a just-barely-good-enough mocked version of the history API
function mockedRouterHistoryObj (location)
{
    const mockedRouter = {
        pushedValues: [],
        action: "POP",
        push: jest.fn((path) =>
        {
            mockedRouter.pushedValues.push(path);
            mockedRouter.action = "PUSH";
            if(location)
            {
                location.pathname = path;
            }
        }),
        replace: jest.fn( (path) => {
            const idx = mockedRouter.pushedValues.length-1;
            mockedRouter.pushedValues[idx] = path;
            mockedRouter.action = "REPLACE";
            if(location)
            {
                location.pathname = path;
            }
        }),
        goBack ()
        {
            mockedRouter.pushedValues.pop();
            if(location)
            {
                const idx = mockedRouter.pushedValues.length-1;
                location.pathname = mockedRouter.pushedValues[idx];
            }
        }
    };
    return mockedRouter;
}

function linkValidator (to)
{
    return () => {
        const link = to.split(/\/+/);
        if(link[0] === "")
        {
            link.shift();
        }
        if(link[0].indexOf(':') !== 0)
        {
            return;
        }
        if(link[0] === ":handbook")
        {
            link.shift();
            const entry = new HandbookEntry(link);
            const index = new HandbookIndex(link);

            if (!entry.exists && !index.exists)
            {
                throw('Invalid link: '+to);
            }
        }
        else if(link[0] === ":dict")
        {
            link.shift();
            const search = link.join('/');
            for(const entry of dictionary)
            {
                if(entry.expression.indexOf(search) !== -1)
                {
                    return;
                }
            }
            throw('Link to non-existing dictionary (expression) search term: '+search);
        }
        else if(link[0] === ":elearn")
        {
            if(quiz[link[1]] === null || quiz[link[1]] === undefined)
            {
                throw('Link to non-existing elearn entry: '+link[1]);
            }
            return;
        }
        else
        {
            throw('Unhandled link type: '+link[0]);
        }
    };
}

/*
 * This wraps a redux store in an object that that encloses any dispatch() in
 * an act() block.
 *
 * You can also enable this by providing { redux: { wrap: true } } to any of
 * the functions that retrieves components.
 */
function wrapRedux (store)
{
    return {
        getState()
        {
            return store.getState();
        },

        subscribe(...args)
        {
            return store.subscribe.apply(store,args);
        },

        dispatch(...args)
        {
            act( () => {
                store.dispatch.apply(store,args);
            });
        }
    };
}

// Used to simulate a user agent
function setUA (UA)
{
    Object.defineProperty(navigator,'userAgent', { value: 'Mozilla/5.0 '+UA, configurable: true });
}

/**
 * Retrieves fake data for the med searcher
 */
function getFakeMedSearcherData ()
{
    return {
        // Yes, these are pokemon. It's hard to distinguish pokemon from drug
        // names. Some have spelling variants to simulate generica.
        drugs: [
            // 0
            {
                name: "Urasaring 100 mg",
                exchangeGroup: 0,
            },
            // 1
            {
                name: "Ursaring 100 mg",
                exchangeGroup: 0,
            },
            // 2
            {
                name: "Nincada 100 mg",
                exchangeGroup: 0
            },
            // 3
            {
                name: "Vulpix 200 mg",
                atc: "TEST",
            },
            // 4
            {
                name: "Sentret 200 mg",
                atc: "TEST",
            },
            // 5
            {
                name: "Gliscor 10 mg",
                exchangeGroup: 1
            },
            // 6
            {
                name: "Azurill 10 mg",
                exchangeGroup: 1,
            },
            // 7
            {
                name: "Gliscor 20 mg",
                exchangeGroup: 2
            },
            // 8
            {
                name: "Azurill 20 mg",
                exchangeGroup: 2,
            },
            // 9
            {
                name: "Azurill 40 mg",
                atc: "TEST",
            },
            // 10
            {
                name: "Bibarel 0,5mg",
                atc: "PMN400",
                exchangeGroup: 3,
            },
        ],
        exchangeList: [
            // 0
            {
                atc: "TEST",
                drugIndexes: [0,1,2],
            },
            // 1
            {
                atc: "TEST-0-1",
                drugIndexes: [ 5, 6 ],
            },
            // 2
            {
                atc: "TEST-0-1",
                drugIndexes: [ 7, 8 ],
            },
            // 3
            {
                atc: "PMN400",
                drugIndexes: [ 10 ],
                merknad: "Kan ikke byttes uten videre, konsulter en lege",
            }
        ],
        fVer: 0,
        fCompV: 0,
    };
}

export { prepareEnv, checkRoutedSnapshot, routedComponentWithRealRedux, checkRoutedSnapshotWithRealRedux, getRealRedux, checkSnapshot, getComponentWith, changeFieldValue, mockLocation, mockedRouterHistoryObj, setConsole, silent, silentFunc, linkValidator, wrapRedux, setUA, getFakeMedSearcherData};
