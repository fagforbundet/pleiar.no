/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import dictionary from '../data/datasett/dictionary.json';
import externalResources from '../data/datasett/external-resources.json';
import handbook from '../data/handbok/handbok.json';
import labValues from '../data/datasett/lab.json';
import toolsList from '../src/tools.list.js';
import nutrition from '../data/datasett/nutricalc-data.json';
import externallyIndexed from '../data/externallyIndexed.json';
import quickref from '../data/hurtigoppslag/quickref.json';
import quiz from '../data/quiz/quiz.json';

import PleiarSearcher from '../src/searcher';
import PleiarIndexer from '../src/indexer';

// $FlowIgnore[signature-verification-failure]
function testInitPleiarSearcher ()
{
    PleiarSearcher.__index = PleiarIndexer.index(
        externalResources,
        dictionary,
        labValues,
        handbook,
        toolsList,
        nutrition,
        externallyIndexed,
        quickref,
        quiz,
    );
    PleiarSearcher._ALR_hasInitialized = true;
    return PleiarSearcher.__index;
}

export { testInitPleiarSearcher };
