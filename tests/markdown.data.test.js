/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('../data/datasett/dictionary.json');
jest.unmock('../data/datasett/lab.json');
jest.unmock('../data/datasett/external-resources.json');
jest.unmock("../data/quiz/quiz.json");
jest.unmock('../data/handbok/handbok.json');
jest.unmock('../data/datasett/website-identification.json');

import remark from 'remark';
import html from 'remark-html';
import fs from 'fs';
import glob from 'glob';
import React from 'react';

import { getComponentWith, linkValidator } from './test-helpers';
import { MarkdownExtendedLinkError, Markdown, MarkdownExtendedLink } from '../src/react-components/markdown';

function testMarkdown (file)
{
    const markdown = fs.readFileSync(file).toString();
    return remark()
        .use(html)
        .processSync(markdown);
}

const allFiles = glob.sync('data/**/*.md');
test.each(allFiles)('Markdown syntax of %s',(file) => {
    expect(testMarkdown(file)).not.toBeFalsy();
});

const handbookFiles = glob.sync('data/handbok/**/*.md').concat(glob.sync('data/hurtigoppslag/**/*.md'));
test.each(handbookFiles)('Extended markdown syntax of %s',(file) => {
    const content = fs.readFileSync(file).toString();
    const comp = getComponentWith(<Markdown permitSpecialNodes={true} content={content} />, {
        enzyme: true,
        router: true,
        route: '/',
        previousPath: '/'
    });
    expect(comp.find(MarkdownExtendedLinkError)).toHaveLength(0);

    const links = comp.find(MarkdownExtendedLink);

    // Validate links
    for(const link of links)
    {
        const to = link.props.href;
        expect(linkValidator(to)).not.toThrow();
    }
});

const nonHandbookFiles = glob.sync('data/{!(handbok|hurtigoppslag)**/,}*.md');
test.each(nonHandbookFiles)('No extended markdown syntax in %s',(file) => {
    const content = fs.readFileSync(file).toString();
    expect(/\]\(!/.test(content)).toBe(false);
});
