/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';

import { MobileAbbreviation, WordDefinition, MobileAbbrWordDefinition, WordDefinitionFromDictionary } from '../src/react-components/shared/abbrev';
import { getComponentWith, checkSnapshot, silentFunc } from './test-helpers';

describe('WordDefinition', () => {
    test('Basic rendering', () => {
        checkSnapshot(<WordDefinition content="Test-content" definition="Test-definition" id="test" />);
    });
    test('Rendering of react element', () => {
        const subElement = <div id="subElement">Test</div>;
        checkSnapshot(<WordDefinition content="Test-content" definition={subElement} id="test" />);
    });
    test('onClick', () => {
        const comp = getComponentWith(<WordDefinition content="Test-content" definition="Test-definition" id="test" />,{
            enzyme: true
        });
        const instance = comp.instance();
        expect(instance.state.tooltipVisible).toBe(false);
        comp.find('.abbreviation').simulate('click');
        expect(instance.state.tooltipVisible).toBe(true);
    });
});

describe('MobileAbbrWordDefinition', () => {
    test('Basic rendering', () => {
        checkSnapshot(<MobileAbbrWordDefinition mobileText="TC" fullText="Test-content" definition="Test-definition" id="test" />);
    });
    test('Rendering of react element', () => {
        const subElement = <div id="subElement">Test</div>;
        checkSnapshot(<MobileAbbrWordDefinition mobileText="TC" fullText="Test-content" definition={subElement} id="test" />);
    });
});

describe('WordDefinitionFromDictionary', () => {
    test('Successful lookup', () => {
        checkSnapshot(<WordDefinitionFromDictionary content="Test-content" lookupKey="respirasjonsfrekvens" id="test" />);
    });
    test('Invalid lookup', () => {
        expect(silentFunc( () => {
            getComponentWith(<WordDefinitionFromDictionary content="Test-content" lookupKey="this-does-not-exist" id="test" />);
        })).toThrow('Unable to find dictionary entry:');
    });
});

test('MobileAbbreviation tooltip functionality', () => {
    window.console.error = jest.fn();
    const comp = getComponentWith(<MobileAbbreviation abbrev="test" full="testing it" />, { enzyme: true });
    const instance = comp.instance();
    expect(instance.state).toMatchObject({
        tooltipVisible: false
    });
    instance.onToggleTooltip();
    expect(instance.state).toMatchObject({
        tooltipVisible: true
    });
});

test('MobileAbbreviation snapshot', () => {
    checkSnapshot(<MobileAbbreviation abbrev="test" full="testing it" />, { enzyme: true });
});
