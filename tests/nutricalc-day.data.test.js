/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('../data/datasett/nutricalc-data.json');
import data from '../data/datasett/nutricalc-data.json';

test('Nutricalc data syntax', () =>
{
    // These are entries that MAY exist, and IF they exist then
    // they must have the type specified
    const possibleEntries = {
        protein: expect.any(Number),
        fluidML: expect.any(Number)
    };
    // These are entries that MUST exist, and if they don't exist then
    // that is a test error
    const requiredEntries = {
        kcal: expect.any(Number),
        name: expect.any(String)
    };
    for(const entry in data.basicGroups)
    {
        const dataEntry = data.basicGroups[entry];
        // The entry key has to be a string
        expect(entry).toEqual( expect.any(String) );

        // This is just to make it easier to find out which entry is being processed.
        dataEntry.entryKeyName = entry;

        // Required keys
        expect(dataEntry).toMatchObject(requiredEntries);

        // Process all "possible" keys
        for(const key in possibleEntries)
        {
            // If it exists in the data entry, we test it
            if (data[key] !== undefined)
            {
                // This is a version of possibleEntries with only one entry
                const matchObj = {};
                matchObj[key] = possibleEntries[key];

                expect(dataEntry).toMatchObject(matchObj);
            }
        }
    }
});
