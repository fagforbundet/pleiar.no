/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { OptionSelector, OptionSelectorSelectElement, OptionSelectorRadioButtons } from '../src/react-components/shared/option-selector';
import React from 'react';
import { getComponentWith, checkSnapshot, prepareEnv } from './test-helpers';

prepareEnv();

const dummyOptionSelectors = [
    {
        key: "test-key"
    },
    {
        key: "with-label",
        name: "This is a label",
    }
];

describe('OptionSelectorSelectElement', () => {
    test('Basic rendering', () => {
        checkSnapshot(<OptionSelectorSelectElement options={dummyOptionSelectors} label="option-selector-select-element test" onChange={jest.fn()} />);
    });

    test('onChange', () => {
        const onChangeFN = jest.fn();
        const component = getComponentWith(<OptionSelectorSelectElement options={dummyOptionSelectors} label="onchange-test" onChange={onChangeFN} />, {
            enzyme: true
        });
        component.find('select').at(0).simulate('change');
        expect(onChangeFN).toHaveBeenCalled();
    });
    test('without a selected element', () => {
        const component = getComponentWith(<OptionSelectorSelectElement options={dummyOptionSelectors} label="onchange-test" />, {
            enzyme: true
        });
        expect(component.find('option')).toHaveLength(3);
    });
    test('with a selected element', () => {
        const component = getComponentWith(<OptionSelectorSelectElement options={dummyOptionSelectors} selected="test-key" />, {
            enzyme: true
        });
        expect(component.find('option')).toHaveLength(2);
    });
    test('with an invalid selected element', () => {
        const component = getComponentWith(<OptionSelectorSelectElement options={dummyOptionSelectors} selected="invalid" />, {
            enzyme: true
        });
        expect(component.find('option')).toHaveLength(3);
    });
    test('with an null selected element', () => {
        const component = getComponentWith(<OptionSelectorSelectElement options={dummyOptionSelectors} selected={null} />, {
            enzyme: true
        });
        expect(component.find('option')).toHaveLength(3);
    });
    test('with the default placeholder', () => {
        const component = getComponentWith(<OptionSelectorSelectElement options={dummyOptionSelectors} />, {
            enzyme: true
        });
        expect(component.find('option[value=""]')).toHaveLength(1);
        expect(component.find('option[value=""]').at(0).text()).toBe("- Velg -");
    });
    test('with a custom placeholder', () => {
        const component = getComponentWith(<OptionSelectorSelectElement options={dummyOptionSelectors} placeholder="Hello world" />, {
            enzyme: true
        });
        expect(component.find('option[value=""]')).toHaveLength(1);
        expect(component.find('option[value=""]').at(0).text()).toBe("Hello world");
    });
});

describe('OptionSelectorRadioButtons', () => {
    test('Basic rendering', () => {
        checkSnapshot(<OptionSelectorRadioButtons options={dummyOptionSelectors} label="option-selector-select-element test" onChange={jest.fn()} />);
    });

    test('onChange', () => {
        const onChangeFN = jest.fn();
        const component = getComponentWith(<OptionSelectorRadioButtons options={dummyOptionSelectors} label="onchange-test" onChange={onChangeFN} />, {
            enzyme: true
        });
        component.find('input').at(0).simulate('change');
        expect(onChangeFN).toHaveBeenCalled();
    });
    test('without a selected element', () => {
        const component = getComponentWith(<OptionSelectorRadioButtons options={dummyOptionSelectors} label="onchange-test" />, {
            enzyme: true
        });
        component.find('input').forEach((input) =>
        {
            expect(input.prop('checked')).toBe(false);
        });
    });
    test('with a selected element', () => {
        const component = getComponentWith(<OptionSelectorRadioButtons options={dummyOptionSelectors} selected="test-key" />, {
            enzyme: true
        });
        let foundChecked = 0;
        component.find('input').forEach((input) =>
        {
            if(input.prop('checked'))
            {
                foundChecked++;
            }
        });
        expect(foundChecked).toBe(1);
    });
    test('with an invalid selected element', () => {
        const component = getComponentWith(<OptionSelectorRadioButtons options={dummyOptionSelectors} selected="invalid" />, {
            enzyme: true
        });
        component.find('input').forEach((input) =>
        {
            expect(input.prop('checked')).toBe(false);
        });
    });
    test('with an null selected element', () => {
        const component = getComponentWith(<OptionSelectorRadioButtons options={dummyOptionSelectors} selected={null} />, {
            enzyme: true
        });
        component.find('input').forEach((input) =>
        {
            expect(input.prop('checked')).toBe(false);
        });
    });
});

describe('OptionSelector', () => {
    test('Default mode', () => {
        const component = getComponentWith(<OptionSelector options={dummyOptionSelectors} selected="invalid" />, {
            enzyme: true
        });
        expect(component.find(OptionSelectorSelectElement)).toHaveLength(1);
        expect(component.find(OptionSelectorRadioButtons)).toHaveLength(0);
        expect(component.find('.option-selector')).toHaveLength(1);
    });

    test('Explicit mode: radio', () => {
        const component = getComponentWith(<OptionSelector options={dummyOptionSelectors} selected="invalid" mode="radio" />, {
            enzyme: true
        });
        expect(component.find(OptionSelectorRadioButtons)).toHaveLength(1);
        expect(component.find(OptionSelectorSelectElement)).toHaveLength(0);
        expect(component.find('.option-selector')).toHaveLength(1);
    });

    test('Explicit mode: select', () => {
        const component = getComponentWith(<OptionSelector options={dummyOptionSelectors} selected="invalid" mode="select" />, {
            enzyme: true
        });
        expect(component.find(OptionSelectorSelectElement)).toHaveLength(1);
        expect(component.find(OptionSelectorRadioButtons)).toHaveLength(0);
        expect(component.find('.option-selector')).toHaveLength(1);
    });

    describe('Prop passing', () => {
        test.each(['radio','select'])('%s', (mode) => {
            expect(mode).toMatch(/^(radio|select)$/);
            const testObject = mode === 'radio' ? OptionSelectorRadioButtons : OptionSelectorSelectElement;
            const onChangeFN = jest.fn();
            const component = getComponentWith(<OptionSelector options={dummyOptionSelectors} selected="invalid" label="test-label" placeholder="test-placeholder" onChange={onChangeFN} mode={mode} />, {
                enzyme: true
            });
            expect(component.find(testObject)).toHaveLength(1);
            expect(component.find(testObject).props()).toMatchObject({
                selected: "invalid",
                label:"test-label",
                placeholder:"test-placeholder",
                onChange: onChangeFN
            });
            expect(component.find(testObject).props().mode).toBe(undefined);
        });
    });
});
