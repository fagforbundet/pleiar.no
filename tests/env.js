/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import IntlPolyfill from 'intl';
import areIntlLocalesSupported from 'intl-locales-supported';

if (!areIntlLocalesSupported("nn-NO"))
{
    const formatter = (new IntlPolyfill.NumberFormat("nn-NO"));
    global.Number.prototype.toLocaleString = function () {
        return formatter.format(this);
    };
}
global.AUTH_URL = 'http://example.com/';
global.AUTH_APPID = 'no.pleiar';
global.AUTH_DEFAULT_STATE = true; // authenticated by default
global.AUTH_ALLOW_URL = /^http:\/\/example.com/;
global.GIT_REVISION_FULL = "(testing)";
global.GIT_REVISION = "(testing)";
global.GIT_REVISION_DATA = "(testing)";
