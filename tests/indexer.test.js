/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { testInitPleiarSearcher } from './search-helper.js';

import dictionary from '../data/datasett/dictionary.json';
import externalResources from '../data/datasett/external-resources.json';
import handbook from '../data/handbok/handbok.json';
import labValues from '../data/datasett/lab.json';
import toolsList from '../src/tools.list.js';
import nutrition from '../data/datasett/nutricalc-data.json';
import externallyIndexed from '../data/externallyIndexed.json';
import quickref from '../data/hurtigoppslag/quickref.json';
import PleiarIndexer from '../src/indexer';
import quiz from '../data/quiz/quiz.json';

// Explicitly enable strict, since lunr needs workarounds for strict mode
"use strict;";

test("Indexing",() =>
{
    let idx;
    expect( () =>
    {
        idx = testInitPleiarSearcher();
    }).not.toThrow();
    expect(idx).toMatchObject({
        fieldVectors: expect.any(Object),
        fields: expect.any(Array),
        invertedIndex: expect.any(Object),
        pipeline: expect.any(Object),
    });
});

describe('Parameter validation in indexer', () => {
    const parameters = [
        externalResources,
        dictionary,
        labValues,
        handbook,
        toolsList,
        nutrition,
        externallyIndexed,
        quickref,
        quiz
    ];
    test('Correct parameters should succeed', () => {
        expect( () => {
            PleiarIndexer.index.apply(PleiarIndexer,parameters);
        }).not.toThrow();
    });
    test('Invalid parameters should throw', () => {
        while(parameters.length > 0)
        {
            parameters.pop();
            expect( () => {
                PleiarIndexer.index.apply(PleiarIndexer,parameters);
            }).toThrow(/Missing parameter to PleiarIndexer.index/);
            parameters.push({});
            expect( () => {
                PleiarIndexer.index.apply(PleiarIndexer, parameters);
            }).toThrow(/Invalid parameter to PleiarIndexer.index/);
            parameters.pop();
        }
    });
});
