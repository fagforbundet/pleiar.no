/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import device from '../src/helper.device';
import { setUA } from './test-helpers';

beforeEach( () => {
    delete window.msMatchMedia;
});

const IOSBrowsers = [
    '(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Mobile/15B202 Safari/604.1',
    '(iPad; CPU OS 9_3_5 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13G36 Safari/601.1',
    '(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) CriOS/63.0.3239.73 Mobile/15B202 Safari/604.1',
];
const androidBrowsers = [
    '(Android 9; Mobile; rv:67.0) Gecko/67.0 Firefox/67.0',
    '(Linux; Android 9) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.90 Mobile Safari/537.36',
];
const desktopBrowsers = [
    '(X11; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0',
];

describe('isAppMode', () => {
    test('Without support', () => {
        window.matchMedia = undefined;
        window.navigator.standalone = undefined;
        expect(device.isAppMode()).toBe(false);
    });
    test('With matchMedia support -> matches', () => {
        window.matchMedia = jest.fn( () => {
            return { matches: true };
        });
        window.navigator.standalone = undefined;
        expect(device.isAppMode()).toBe(true);
    });
    test('With matchMedia support -> does not match', () => {
        window.matchMedia = jest.fn( () => {
            return { matches: false };
        });
        window.navigator.standalone = undefined;
        expect(device.isAppMode()).toBe(false);
    });
    test('Safari -> matches', () => {
        window.matchMedia = undefined;
        window.navigator.standalone = true;
        expect(device.isAppMode()).toBe(true);
    });
    test('Safari -> does not match', () => {
        window.matchMedia = undefined;
        window.navigator.standalone = false;
        expect(device.isAppMode()).toBe(false);
    });
});

describe('isTouchScreen', () => {
    test('Without support', () => {
        window.matchMedia = undefined;
        expect(device.isTouchScreen()).toBe(false);
    });
    /*
     * These are a bit contra-intuitive. The way we detect it is if the media query
     * does NOT match, then we assume a touch screen.
     */
    test('With matchMedia support -> matches', () => {
        window.matchMedia = jest.fn( () => {
            return { matches: true };
        });
        expect(device.isTouchScreen()).toBe(false);
    });
    test('With matchMedia support -> does not match', () => {
        window.matchMedia = jest.fn( () => {
            return { matches: false };
        });
        expect(device.isTouchScreen()).toBe(true);
    });

    // On IE it should always be false, even if a matchMedia is present that
    // does not match, since it does not support the media query.
    test('IE11: With matchMedia support -> does not match', () => {
        window.matchMedia = jest.fn( () => {
            return { matches: false };
        });
        window.msMatchMedia = jest.fn();
        expect(device.isTouchScreen()).toBe(false);
    });
});

describe('isMobileDevice', () => {
    test('Without touch screen', () => {
        window.matchMedia = jest.fn( () => {
            return { matches: true };
        });
        expect(device.isMobileDevice()).toBe(false);
    });
    test('With touch screen, without expected browser', () => {
        setUA('(X11; Linux x86_64; rv:67.0) Gecko/20100101 Firefox/67.0');
        window.matchMedia = jest.fn( () => {
            return { matches: false};
        });
        expect(device.isMobileDevice()).toBe(false);
    });
    const okBrowsers = [
        ...androidBrowsers,
        ...IOSBrowsers
    ];
    test.each(okBrowsers)('With touch screen, with browser %s', (browser) => {
        setUA(browser);
        window.matchMedia = jest.fn( () => {
            return { matches: false };
        });
        expect(device.isTouchScreen()).toBe(true);
        expect(device.isMobileDevice()).toBe(true);
    });
});

describe("isIOSApp", () => {
    const nonIOSBrowsers = [
        ...androidBrowsers,
        ...desktopBrowsers
    ];
    test.each(nonIOSBrowsers)('Non-iOS browser, non-app mode: %s', (browser) => {
        window.navigator.standalone = false;
        setUA(browser);
        expect(device.isIOSApp()).toBe(false);
    });
    test.each(nonIOSBrowsers)('Non-iOS browser, app mode: %s', (browser) => {
        window.navigator.standalone = true;
        setUA(browser);
        expect(device.isIOSApp()).toBe(false);
    });
    test.each(IOSBrowsers)('iOS browser, non-app mode: %s', (browser) => {
        window.navigator.standalone = false;
        setUA(browser);
        expect(device.isIOSApp()).toBe(false);
    });
    test.each(IOSBrowsers)('iOS browser, app mode: %s', (browser) => {
        window.navigator.standalone = true;
        setUA(browser);
        expect(device.isIOSApp()).toBe(true);
    });
});
