/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import { checkSnapshot, getComponentWith } from './test-helpers';
import FrontPage from '../src/react-components/frontpage';

jest.mock('../src/mobile-appinstall', () => {
    return {
        appInstall: {
            listen: jest.fn(),
            trigger: jest.fn(),
            canInstall: jest.fn( () => false ),
        }
    };
});

import { appInstall } from '../src/mobile-appinstall';

beforeEach( () => {
    appInstall.listen.mockClear();
});

test('Front Page', () => {
    checkSnapshot(<FrontPage />,
        {
            router: true,
            route: '/',
            previousPath: '/'
        });
    expect(global.__title).toBe("Pleiar.no");
});

describe('appInstall menu item', () => {
    test('Default, no item', () => {
        const comp = getComponentWith(<FrontPage />, {
            enzyme: true,
            router: true,
            route: '/',
            previousPath: '/'
        });
        expect(comp.find('MenuList').prop('extraApps')).toHaveLength(0);
    });
    test('With item', () => {
        appInstall.canInstall = jest.fn( () => true);
        const comp = getComponentWith(<FrontPage />, {
            enzyme: true,
            router: true,
            route: '/',
            previousPath: '/'
        });
        expect(comp.find('MenuList').prop('extraApps')).toHaveLength(1);
    });
    test('With appearing item', () => {
        appInstall.canInstall = jest.fn( () => false);
        const comp = getComponentWith(<FrontPage />, {
            enzyme: true,
            router: true,
            route: '/',
            previousPath: '/'
        });
        const FrontPageInstance = comp.find(FrontPage).instance();
        const forceUpdateMock = jest.fn(FrontPageInstance.forceUpdate);
        FrontPageInstance.forceUpdate = forceUpdateMock;
        expect(comp.find('MenuList').prop('extraApps')).toHaveLength(0);
        expect(forceUpdateMock).not.toHaveBeenCalled();
        appInstall.canInstall = jest.fn( () => true);
        appInstall.listen.mock.calls[0][0]();
        expect(forceUpdateMock).toHaveBeenCalled();
        comp.update();
        expect(comp.find('MenuList').prop('extraApps')).toHaveLength(1);
    });
    test('onClick callback with auto install support', () => {
        appInstall.canInstall = jest.fn( () => true);
        appInstall.trigger = jest.fn( () => true);
        const comp = getComponentWith(<FrontPage />, {
            enzyme: true,
            router: true,
            route: '/',
            previousPath: '/'
        });
        const menuInstance = comp.find('MenuList');
        const fakeEvent = {
            preventDefault: jest.fn()
        };
        menuInstance.prop('extraApps')[0].onClick(fakeEvent);
        expect(fakeEvent.preventDefault).toHaveBeenCalled();
    });
    test('onClick callback with manual install support', () => {
        appInstall.canInstall = jest.fn( () => true);
        appInstall.trigger = jest.fn( () => false);
        const comp = getComponentWith(<FrontPage />, {
            enzyme: true,
            router: true,
            route: '/',
            previousPath: '/'
        });
        const menuInstance = comp.find('MenuList');
        const fakeEvent = {
            preventDefault: jest.fn()
        };
        menuInstance.prop('extraApps')[0].onClick(fakeEvent);
        expect(fakeEvent.preventDefault).not.toHaveBeenCalled();
    });
});
