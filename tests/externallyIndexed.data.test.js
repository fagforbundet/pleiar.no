/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('../data/externallyIndexed.json');
import externallyIndexed from '../data/externallyIndexed.json';
import { testInitPleiarSearcher } from './search-helper.js';

test.each(externallyIndexed)('Externally indexed item syntax validation: %p', (entry) =>
{
    expect(entry).toMatchObject({
        "text": expect.stringMatching(/\S+/),
        "title": expect.stringMatching(/\S+/),
        "type": expect.stringMatching(/^(fagprat|other)$/),
        "url": expect.stringMatching(/^https?:\/\/(www\.)?(fagifokus.no|fagforbundet.no|soundcloud.com\/fagprat).*/)
    });
    if(entry.keywords !== undefined)
    {
        expect(entry.keywords).toMatch(/\S+/);
    }
});

test('Externally indexed item indexing', () =>
{
    expect( () =>
    {
        testInitPleiarSearcher();
    }).not.toThrow();
});
