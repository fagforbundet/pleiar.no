/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { lunrResultSnippet } from '../src/helper.lunrSnippet.js';

const testText = "This is a header\n\nThis is a test sentence. It is followed by this one. And then there's another one here. But it is still not done, as a final test we add this very long one here.\n\nThen there's even more, because of course there is. It is for testing, and another test. It's done now, though.";
const testHighlight = {
    test: {
        body: {
            position: [
                [28,4],
                [143,4],
                [243,7],
                [264,4]
            ]
        }
    }
};

test('Core functionality with highlights', () => {
    const snippet = new lunrResultSnippet(testText,testHighlight);
    expect(snippet.get()).toMatchSnapshot();

    expect(snippet._highlight).toMatchSnapshot();
    expect(snippet._targets).toMatchSnapshot();
});

test('Core functionality without highlights', () => {
    const snippet = new lunrResultSnippet(testText,{});
    expect(snippet.get()).toMatchSnapshot();
});

test('Core functionality without highlighting first sentence', () => {
    const ourTestHighlight = Object.assign({},testHighlight);
    delete ourTestHighlight.test;
    ourTestHighlight.test = { body: { position: [].concat(testHighlight.test.body.position) } };
    ourTestHighlight.test.body.position.shift();
    const snippet = new lunrResultSnippet(testText,ourTestHighlight);
    expect(snippet.get()).toMatchSnapshot();
});

test('Core functionality with too many targets', () => {
    const ourTestHighlight = Object.assign({},testHighlight);
    delete ourTestHighlight.test;
    const positions = [].concat(testHighlight.test.body.position);
    for(let no = 0; no < positions.length; no++)
    {
        ourTestHighlight['test'+no] = {
            body: { position: [ positions[no] ]}
        };
    }
    const snippet = new lunrResultSnippet(testText,ourTestHighlight);
    expect(snippet.get()).toMatchSnapshot();
});

test('_strIsBoundaryCharacter', () => {
    const snippet = new lunrResultSnippet(testText,testHighlight);
    const boundaryCharacters = [ '.','…','!','?'];
    const notBoundaryCharacters = [ "\n",',',':',';','-','_',' ','+','='];

    for(const boundary of boundaryCharacters)
    {
        expect( snippet._strIsBoundaryCharacter(boundary) ).toBe(true);
    }

    for(const notBoundary of notBoundaryCharacters)
    {
        expect( snippet._strIsBoundaryCharacter(notBoundary) ).toBe(false);
    }
});

test('_idxIsStartOfSentence', () => {
    const snippet = new lunrResultSnippet(testText,testHighlight);

    expect(snippet._idxIsStartOfSentence(0)).toBe(true);
    expect(snippet._idxIsStartOfSentence(1)).toBe(false);

    expect(snippet._idxIsStartOfSentence(17)).toBe(true);
    expect(snippet._idxIsStartOfSentence(18)).toBe(false);

    expect(snippet._idxIsStartOfSentence(42)).toBe(true);
    expect(snippet._idxIsStartOfSentence(43)).toBe(false);
});

test('_idxIsEndOfSentence', () => {
    const snippet = new lunrResultSnippet(testText,testHighlight);

    expect(snippet._idxIsEndOfSentence(0,0)).toBe(false);
    expect(snippet._idxIsEndOfSentence(14,2)).toBe(true);
    expect(snippet._idxIsEndOfSentence(14,3)).toBe(false);
    expect(snippet._idxIsEndOfSentence(40,1)).toBe(true);
    expect(snippet._idxIsEndOfSentence(40,4)).toBe(false);
});

test('findSingleSnippet', () => {
    const snippet = new lunrResultSnippet(testText,testHighlight);

    for(const attempt of testHighlight.test.body.position)
    {
        expect(snippet.findSingleSnippet(attempt[0],attempt[1],[])).toMatchSnapshot();
    }
});
