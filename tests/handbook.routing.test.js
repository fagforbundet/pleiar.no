/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { prepareEnv, getComponentWith, getRealRedux } from './test-helpers';

import React from 'react';
import { HandbookEntry, HandbookIndex } from '../src/data.handbook';
import { NotFound } from '../src/react-components/shared';
import { Pleiar } from '../src/react-components/main';

jest.unmock('../data/handbok/handbok.json');

prepareEnv();

function recursiveTreeRender (entry)
{
    let rendered = [];
    if(entry instanceof HandbookEntry)
    {
        rendered.push({ title: entry.title, url: entry.url});
    }
    else
    {
        for(const child of entry.tree)
        {
            rendered = rendered.concat( recursiveTreeRender(child) );
        }
    }
    return rendered;
}

function constructIndexTree ()
{
    const index = new HandbookIndex();
    const rootChildren = index.tree;
    let result = [];
    for(const child of rootChildren)
    {
        result = result.concat( recursiveTreeRender(child) );
    }
    return result;
}

const tree = constructIndexTree();
global.PLEIAR_ENV = 'production';

test.each(tree)('Routing for %s',(entry) => {
    const compSettings = {
        redux: false,
        includeReduxStore: false,
        router: true,
        routingAssistant: false,
        enzyme: true,
        route: entry.url,
        previousPath: "/",
        ignore404: true
    };
    const comp = getComponentWith(<Pleiar store={getRealRedux()} />, compSettings);

    expect(comp.find(NotFound)).toHaveLength(0);
});
