/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('reactstrap');

import NutriCalcReq from '../src/react-components/nutricalc-req';
import { changeFieldValue, prepareEnv, checkSnapshot, getComponentWith, getRealRedux } from './test-helpers';
import { setValue, toggleValue } from '../src/actions/nutricalc-req';
import { toggleRounding, toggleFormulaVisibility } from '../src/actions/global';
import { setWeight } from '../src/actions/nutricalc-shared.js';
import { CalcExplanationLine } from '../src/react-components/calculators-shared';

const getResultFromComp = (comp) =>
{
    const resultElement = comp.find('#nutriCalcReqResult');
    expect( resultElement ).toHaveLength(1);
    const result = resultElement.text().replace(/\s/g,'').replace(/,/g,'.').replace('kcal/døgn','');
    return Number.parseFloat(result);
};

prepareEnv();

test('Basic rendering of NutriCalc req calculator', () => {
    checkSnapshot(NutriCalcReq,{ redux: true });
    expect(global.__title).toMatch('Ernæringskalkulator - behov');
});

test("redux store", () => {
    const store = getRealRedux();
    // Default state
    expect( store.getState().nutriCalc.req ).toMatchObject({
        ageGroup: '31-69',
        patientState: 'up',
        febrile: false,
        skinny: false,
        temp: 38
    });
    expect( store.getState().nutriCalc.shared ).toMatchObject({
        weight: 75,
    });
    store.dispatch(setValue("patientState","building"));
    expect( store.getState().nutriCalc.req.patientState ).toBe("building");

    store.dispatch(setValue("patientState","bed"));
    expect( store.getState().nutriCalc.req.patientState ).toBe("bed");

    store.dispatch(toggleValue("febrile"));
    expect( store.getState().nutriCalc.req.febrile ).toBe(true);

    store.dispatch(toggleValue("skinny"));
    expect( store.getState().nutriCalc.req.skinny ).toBe(true);

    store.dispatch(setValue("ageGroup","18-30"));
    expect( store.getState().nutriCalc.req.ageGroup ).toBe("18-30");

    store.dispatch(setValue("ageGroup","70+"));
    expect( store.getState().nutriCalc.req.ageGroup ).toBe("70+");

    store.dispatch(setWeight("50"));
    expect( store.getState().nutriCalc.shared.weight ).toBe("50");

    expect( () => { store.dispatch(setValue("nonExisting","meh")); }).toThrow(/unknown field/);

    expect( () => { store.dispatch(toggleValue("nonExisting")); }).toThrow(/unknown field/);
});

test('NutriCalcReq calculations', () => {
    const {component, store} = getComponentWith(NutriCalcReq,{
        redux: true,
        includeReduxStore: true,
        enzyme: true
    });

    // By default we have 4 calculator description lines (two headers, two descriptions)
    expect(component.find('.calculator-descriptions small .row')).toHaveLength(4);

    expect( getResultFromComp(component) ).toBe(2480);

    store.dispatch(setValue("patientState","building"));
    expect( getResultFromComp(component) ).toBe(3000);

    store.dispatch(setValue("patientState","bed"));
    expect( getResultFromComp(component) ).toBe(2180);

    store.dispatch(toggleValue("febrile"));
    expect( getResultFromComp(component) ).toBe(2390);

    store.dispatch(toggleValue("skinny"));
    expect( getResultFromComp(component) ).toBe(2610);

    store.dispatch(setValue("ageGroup","18-30"));
    expect( getResultFromComp(component) ).toBe(2830);

    store.dispatch(setValue("ageGroup","70+"));
    expect( getResultFromComp(component) ).toBe(2390);

    store.dispatch(toggleRounding());
    expect( getResultFromComp(component) ).toBe(2392.5);
    store.dispatch(toggleRounding());

    store.dispatch(setWeight("50"));
    expect( getResultFromComp(component) ).toBe(1600);

    // Enable formulas
    store.dispatch(toggleFormulaVisibility());
    // (also toggle rounding off since the last line will be rounding if not)
    store.dispatch(toggleRounding());
    expect( component.find('.calculator-descriptions small .row').last().find('.formula').last().text() ).toMatch(/behov.*basisbehov.*grader.*over.*37/);
});

test('UI interaction', () => {
    const {component, store} = getComponentWith(NutriCalcReq,{
        redux: true,
        includeReduxStore: true,
        enzyme: true
    });
    expect(store.getState().nutriCalc.req).toMatchObject({
        ageGroup: '31-69',
        patientState: 'up',
        febrile: false,
        skinny: false,
        temp: 38
    });

    // Note: We're using Enzyme in mount() mode not shallow() mode, so find()
    // returns two elements, even for id find()s. So .at() needs to be 0,2,4
    // not 0,1,2
    component.find('[name="patientState"]').at(0).simulate('change');
    expect( store.getState().nutriCalc.req.patientState ).toBe("bed");
    component.find('[name="patientState"]').at(2).simulate('change');
    expect( store.getState().nutriCalc.req.patientState ).toBe("up");
    component.find('[name="patientState"]').at(4).simulate('change');
    expect( store.getState().nutriCalc.req.patientState ).toBe("building");

    changeFieldValue(component.find('#weight').first(),200);
    expect(store.getState().nutriCalc.shared.weight).toBe(200);

    component.find('[name="ageGroup"]').at(0).simulate('change');
    expect( store.getState().nutriCalc.req.ageGroup ).toBe("18-30");
    component.find('[name="ageGroup"]').at(2).simulate('change');
    expect( store.getState().nutriCalc.req.ageGroup ).toBe("31-69");
    component.find('[name="ageGroup"]').at(4).simulate('change');
    expect( store.getState().nutriCalc.req.ageGroup ).toBe("70+");

    expect( store.getState().nutriCalc.req.febrile ).toBeFalsy();
    component.find('[name="febrileState"]').at(0).simulate('change');
    expect( store.getState().nutriCalc.req.febrile ).toBeTruthy();
    component.find('[name="febrileState"]').at(0).simulate('change');
    expect( store.getState().nutriCalc.req.febrile ).toBeFalsy();

    component.find('[name="febrileState"]').at(0).simulate('change');
    // When not febrile, it shouldn't match × 3, because that's added
    // to explain being febrile when temp > 38 (temp=38 would be × 1,
    // which we just omit displaying)
    expect( component.find(CalcExplanationLine).at(3).html() ).not.toMatch(/× 3/);
    expect( store.getState().nutriCalc.req.febrile ).toBeTruthy();
    changeFieldValue(component.find('#temp').at(0),40);
    expect( store.getState().nutriCalc.req.temp ).toBe(40);
    // Now it should be \times 3
    expect( component.find(CalcExplanationLine).at(3).html() ).toMatch(/× 3/);
    expect(component.find('.is-invalid')).toHaveLength(0);
    changeFieldValue(component.find('#temp').at(0),"");
    expect( store.getState().nutriCalc.req.temp ).toBe("");
    expect(component.find('.is-invalid')).toHaveLength(2);

    expect( store.getState().nutriCalc.req.skinny ).toBeFalsy();
    component.find('[name="skinnyState"]').at(0).simulate('change');
    expect( store.getState().nutriCalc.req.skinny ).toBeTruthy();
    component.find('[name="skinnyState"]').at(0).simulate('change');
    expect( store.getState().nutriCalc.req.skinny ).toBeFalsy();

});
