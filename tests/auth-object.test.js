/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('../src/auth.js');

global.AUTH_URL = 'http://example.com/';
global.AUTH_APPID = 'no.pleiar';
global.AUTH_DEFAULT_STATE = false;

import auth from '../src/auth';
import { jws as _jwsroot, utf8tob64 } from 'jsrsasign';
const { JWS } = _jwsroot;

function resetState ()
{
    auth.initialized = false;
    auth.authenticated = false;
    auth._error = null;
    global.AUTH_DEFAULT_STATE = false;
}

beforeEach( () => {
    resetState();
    delete window.localStorage.pau;
});

test('Initialize without existing state', () => {
    expect(() => {
        auth.initialize();
    }).not.toThrow();
    expect(auth.authenticated).toBe(false);
    expect(auth.isAuthenticated()).toBe(false);
});

test('Initialize with valid existing state', () => {
    const now = Math.round((new Date).getTime()/1000);
    window.localStorage.pau = utf8tob64(JSON.stringify({ date: now }));
    expect(() => {
        auth.initialize();
    }).not.toThrow();
    expect(auth.authenticated).toBe(true);
    expect(auth.isAuthenticated()).toBe(true);
});

test('Initialize with expired existing state', () => {
    const longAgo = ( Math.round((new Date).getTime()/1000)) - 31536001;
    window.localStorage.pau = utf8tob64(JSON.stringify({ date: longAgo }));
    expect(() => {
        auth.initialize();
    }).not.toThrow();
    expect(auth.authenticated).toBe(false);
    expect(auth.isAuthenticated()).toBe(false);
});

test('Initialize with empty existing state', () => {
    window.localStorage.pau = utf8tob64(JSON.stringify({ }));
    expect(() => {
        auth.initialize();
    }).not.toThrow();
    expect(auth.authenticated).toBe(false);
    expect(auth.isAuthenticated()).toBe(false);
});

test('Initialize with corrupt existing state', () => {
    window.localStorage.pau = "YnJva2Vu";
    expect(() => {
        auth.initialize();
    }).not.toThrow();
    expect(auth.authenticated).toBe(false);
    expect(auth.isAuthenticated()).toBe(false);
});

describe('authenticate()', () => {
    const now = Math.round((new Date).getTime()/1000);
    const key = "35fe6e19601f6a110693c4ec8bcfdef3b278e5864de0a3b088d37da045f27442";
    const header = {
        "alg": "HS256",
        "typ": "JWT"
    };
    const valid = {
        memberId: 123,
        iat: now
    };
    const validJWT = JWS.sign(header.alg, header, valid,key);
    test('authenticate valid data', () => {
        Object.defineProperty(document,'referrer', { value: global.AUTH_URL+'/test', configurable: true });

        expect(auth.isAuthenticated()).toBe(false);

        expect( () => {
            auth.authenticate(validJWT);
        }).not.toThrow();

        expect(auth.isAuthenticated()).toBe(true);
        expect(auth.getError()).toBe(null);
    });

    test('With invalid memberId', () => {
        const invalidMemberID = JWS.sign(header.alg, header, {
            ...valid,
            memberId: "string"
        },key);

        expect(auth.isAuthenticated()).toBe(false);

        expect( () => {
            auth.authenticate(invalidMemberID);
        }).not.toThrow();

        expect(auth.isAuthenticated()).toBe(false);
        expect(auth.getError()).toBe('CONTENTID');
    });
    test('With missing memberId', () => {
        const content = { ...valid };
        delete content.memberId;
        const invalidMemberID = JWS.sign(header.alg, header,content,key);

        expect(auth.isAuthenticated()).toBe(false);

        expect( () => {
            auth.authenticate(invalidMemberID);
        }).not.toThrow();

        expect(auth.isAuthenticated()).toBe(false);
        expect(auth.getError()).toBe('CONTENTID');
    });
    test('With expired iat', () => {
        const invalidIAT = JWS.sign(header.alg, header, {
            ...valid,
            iat: 0,
        },key);

        expect(auth.isAuthenticated()).toBe(false);

        expect( () => {
            auth.authenticate(invalidIAT);
        }).not.toThrow();

        expect(auth.isAuthenticated()).toBe(false);
        expect(auth.getError()).toBe('TIME');
    });
    test('With fake (too fresh) iat', () => {
        const fakeIat = now+31536001;
        const invalidIAT = JWS.sign(header.alg, header, {
            ...valid,
            iat: fakeIat,
        },key);

        expect(auth.isAuthenticated()).toBe(false);

        expect( () => {
            auth.authenticate(invalidIAT);
        }).not.toThrow();

        expect(auth.isAuthenticated()).toBe(false);
        expect(auth.getError()).toBe('TIME');
    });
    test('With invalid iat', () => {
        const invalidIAT = JWS.sign(header.alg, header, {
            ...valid,
            iat: "string",
        },key);

        expect(auth.isAuthenticated()).toBe(false);

        expect( () => {
            auth.authenticate(invalidIAT);
        }).not.toThrow();

        expect(auth.isAuthenticated()).toBe(false);
        expect(auth.getError()).toBe('CONTENTIAT');
    });

    test('With missing iat', () => {
        const content = { ...valid };
        delete content.iat;
        const invalidIAT = JWS.sign(header.alg, header,content,key);

        expect(auth.isAuthenticated()).toBe(false);

        expect( () => {
            auth.authenticate(invalidIAT);
        }).not.toThrow();

        expect(auth.isAuthenticated()).toBe(false);
        expect(auth.getError()).toBe('CONTENTIAT');
    });

    test('With invalid JWT', () => {
        const invalidJWT = utf8tob64({})+'.'+utf8tob64({})+'.'+utf8tob64('not-a-signature');

        expect(auth.isAuthenticated()).toBe(false);

        expect( () => {
            auth.authenticate(invalidJWT);
        }).not.toThrow();

        expect(auth.isAuthenticated()).toBe(false);
        expect(auth.getError()).toBe('JSON');
    });

    test('With no referrer', () => {
        Object.defineProperty(document,'referrer', { value: '', configurable: true });

        expect(auth.isAuthenticated()).toBe(false);

        expect( () => {
            auth.authenticate(validJWT);
        }).not.toThrow();

        expect(auth.isAuthenticated()).toBe(false);
        expect(auth.getError()).toBe('NOREF');
    });
    test('With invalid referrer', () => {
        Object.defineProperty(document,'referrer', { value: 'http://this-is-invalid.localhost/test', configurable: true });

        expect(auth.isAuthenticated()).toBe(false);

        expect( () => {
            auth.authenticate(validJWT);
        }).not.toThrow();

        expect(auth.isAuthenticated()).toBe(false);
        expect(auth.getError()).toBe('REF');
    });
    test('With broken payload which is valid JSON', () => {
        const brokenPayload = utf8tob64({})+'.'+utf8tob64("null")+'.'+utf8tob64('not-a-signature');
        expect( () => {
            auth.authenticate(brokenPayload);
        }).not.toThrow();
        expect(auth.getError()).toBe('PAYLOAD');
    });
    const safariITPUA = [
        '(iPhone; CPU iPhone OS 11_1_2 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Mobile/15B202 Safari/604.1',
        '(Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1 Safari/605.1.15'
    ];
    describe('Safari ITP', () => {
        test.each(safariITPUA)('With no referrer: %s', (UA) => {
            Object.defineProperty(document,'referrer', { value: '', configurable: true });
            Object.defineProperty(navigator,'userAgent', { value: 'Mozilla/5.0 '+UA, configurable: true });

            expect(auth.isAuthenticated()).toBe(false);

            expect( () => {
                auth.authenticate(validJWT);
            }).not.toThrow();

            expect(auth.isAuthenticated()).toBe(true);
            expect(auth.getError()).toBe(null);
        });
        test.each(safariITPUA)('With invalid referrer: %s', (UA) => {
            Object.defineProperty(document,'referrer', { value: 'http://this-is-invalid.localhost/test', configurable: true });
            Object.defineProperty(navigator,'userAgent', { value: 'Mozilla/5.0 '+UA, configurable: true });

            expect(auth.isAuthenticated()).toBe(false);

            expect( () => {
                auth.authenticate(validJWT);
            }).not.toThrow();

            expect(auth.isAuthenticated()).toBe(false);
            expect(auth.getError()).toBe('REF');
        });
    });
});

test.each(['isAuthenticated','invalidate','authenticate'])('Auto-initialization: %s', (name) => {
    const method = auth[name].bind(auth);
    expect(auth.initialized).toBe(false);
    method();
    expect(auth.initialized).toBe(true);
});

test('invalidate', () => {
    auth.initialize();
    auth.authenticated = true;
    window.localStorage.pau = "test";
    window.localStorage.pleiarAuthRedirect = "/test";

    expect( () => {
        auth.invalidate();
    }).not.toThrow();

    expect(auth.authenticated).toBe(false);
    expect(window.localStorage.pau).toBe(undefined);
    expect(window.localStorage.pleiarAuthRedirect).toBe(undefined);
});

test('With auth disabled', () => {
    global.AUTH_DEFAULT_STATE = true;

    expect(auth.authenticated).toBe(false);
    expect(auth.authenticate()).toBe(true);
});

const whitelisted = [
    'googlebot',
    'yahoo',
    'bingbot',
    'baiduspider',
    'yandex',
    'yeti',
    'yodaobot',
    'gigabot',
    'ia_archiver',
    'facebookexternalhit',
    'twitterbot',
    'developers.google.com',
    'ReactSnap'
];
test.each(whitelisted)('Whitelisted UA: %p', (ua) => {
    Object.defineProperty(navigator,'userAgent', { value: 'Mozilla/5.0 '+ua, configurable: true });
    expect(auth.authenticated).toBe(false);
    expect(auth.isAuthenticated()).toBe(true);
});
