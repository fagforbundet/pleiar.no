/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
import RoutingAssistant, { RoutingAssistantInit, RoutingAssistantInitRaw, RoutingAssistantSyncRoutes, RoutingAssistantSyncRoutesRaw } from '../src/routing';
import React from 'react';
import { getComponentWith, checkSnapshot, mockLocation, mockedRouterHistoryObj } from './test-helpers';

const location = mockLocation();

jest.useFakeTimers();

const RACopy = Object.assign({},RoutingAssistant);
const resetAssistant = () => {
    Object.assign(RoutingAssistant,RACopy);
};
// Clean up after each test
afterEach(() => {
    jest.clearAllTimers();
    resetAssistant();
});

test('Basic state', () => {
    expect(RoutingAssistant).toMatchObject({
        _debounceTimeout: 500,
        ourChange: "",
        changingToURL: "",
    });
    RoutingAssistant.ourChange = "/test";
    RoutingAssistant.changingToURL = "/hello";
    expect(RoutingAssistant).toMatchObject({
        ourChange: "/test",
        changingToURL: "/hello",
    });
    RoutingAssistant.stateReset();
    expect(RoutingAssistant).toMatchObject({
        _debounceTimeout: 500,
        ourChange: "",
        changingToURL: "",
    });
});

test('RoutingAssistantInit', () => {
    // By default it's an empty object
    expect(RoutingAssistant.history).toEqual({});
    // Providing a history object to RoutingAssistantInitRaw should update the
    // history property
    getComponentWith(<RoutingAssistantInitRaw history={{mockHistory: true}} />);
    expect(RoutingAssistant.history).toMatchObject({
        mockHistory: true
    });
    // Initializing with a real router should give a proper history object
    getComponentWith(<RoutingAssistantInit />,{
        router: true,
        route: "/sok",
        previousPath: "/",
    });
    expect(RoutingAssistant.history).not.toMatchObject({
        mockHistory: true
    });
    expect(RoutingAssistant.history).not.toEqual({});
});

test('RoutingAssistantSyncRoutes', () => {
    let gotQuery;
    RoutingAssistant.syncToStore = jest.fn( RoutingAssistant.syncToStore.bind(RoutingAssistant) );
    const onSync = jest.fn( (query) =>
    {
        gotQuery = query;
    });
    checkSnapshot(<RoutingAssistantSyncRoutes onSync={onSync} />,{
        routingAssistant: true,
        enzyme: true,
        router: true,
        // FIXME
        previousPath: "/sok/:query?/:page?",
        route: "/sok/test/2",
    });
    expect(RoutingAssistant.syncToStore).toHaveBeenCalled();

    jest.runAllTimers();

    expect(onSync).toHaveBeenCalled();
    expect(gotQuery).toBe("test");
});

test('RoutingAssistantSyncRoutes componentWillUmount', () => {
    RoutingAssistant.stateReset = jest.fn( RoutingAssistant.stateReset.bind(RoutingAssistant) );
    const onSync = jest.fn();
    // FIXME: params
    const comp = getComponentWith(<RoutingAssistantSyncRoutesRaw onSync={onSync} match={{params:{query:""}}} />,{});
    comp.getInstance().componentWillUmount();
    expect(RoutingAssistant.stateReset).toHaveBeenCalled();
});

const prepareMocks = () => {
    const history = mockedRouterHistoryObj(location);

    RoutingAssistant._debounceTimeout = 1;
    RoutingAssistant.history = history;
    RoutingAssistant.debouncedReplace = undefined;
    return { location, history };
};

test('Basic navigation', () => {
    const { location } = prepareMocks();

    RoutingAssistant.changingToURL = "/";
    RoutingAssistant.push("/test");
    expect(location.pathname).toBe("/test");
    expect(RoutingAssistant.changingToURL).toBe("");

    RoutingAssistant.generatePush("/test","word",20);
    expect(location.pathname).toBe("/test/word/20");
    RoutingAssistant.generatePush("/test","word",10);
    expect(location.pathname).toBe("/test/word");
    RoutingAssistant.generatePush("/test","",1);
    expect(location.pathname).toBe("/test");
});

test('Basic navigation async .replace', () => {return new Promise((done) => {
    const { location } = prepareMocks();

    RoutingAssistant.replace('/test/thing');
    jest.runAllTimers();

    setTimeout(() => {
        expect(location.pathname).toBe("/test/thing");
        expect(RoutingAssistant.changingToURL).toBe("");
        done();
    },1);
    jest.runAllTimers();
});});

test('Basic navigation async .generateReplace', () => {
    const { location } = prepareMocks();

    RoutingAssistant.generateReplace("/thing","words",30);
    jest.runAllTimers();

    expect(location.pathname).toBe("/thing/words/30");
    expect(RoutingAssistant.changingToURL).toBe("");
});

test('Basic navigation async .generateReplaceNOW', () => {
    const { location } = prepareMocks();

    RoutingAssistant.generateReplaceNOW("/thing","words",50);
    expect(location.pathname).toBe("/thing/words/50");
    expect(RoutingAssistant.changingToURL).toBe("");
});

test('.generateReplaceNOW debounce protection', () => {
    const { location } = prepareMocks();

    RoutingAssistant.generateReplace("/thing","words",44);
    expect(location.pathname).not.toBe("/thing/words/30");
    RoutingAssistant.generateReplaceNOW("/thing","words",77);
    expect(location.pathname).toBe("/thing/words/77");
    expect(RoutingAssistant.changingToURL).toBe("");
    jest.runAllTimers();
    expect(location.pathname).toBe("/thing/words/77");
});

test('Basic navigation async .syncToStore', () => {
    const { history } = prepareMocks();

    let gotQuery;
    const onSync = jest.fn( (query) =>
    {
        gotQuery = query;
    });

    /*
     * First, do some navigating internally *with* the RoutingAssistant
     */
    RoutingAssistant.push('/hello');
    RoutingAssistant.push('/hello/world');
    RoutingAssistant.push('/hello/world/3');
    expect(history.pushedValues).toEqual(['/hello','/hello/world','/hello/world/3']);

    // Navigate back, simulating an update from the browser
        // First, navigate back
    history.goBack();
        // At this point, react will trigger a call to syncToStore
    RoutingAssistant.syncToStore({
        match: { params: { query: "world" } },
        onSync
    });

    jest.runAllTimers();
    expect(onSync).toHaveBeenCalled();
    expect(gotQuery).toBe("world");
});
test('syncToStore with el:30', () =>
{
    let gotQuery;
    const onSync = jest.fn( (query) =>
    {
        gotQuery = query;
    });

        // At this point, react will trigger a call to syncToStore
    RoutingAssistant.syncToStore({
        match: { params: { query: "el:30" } },
        onSync
    });

    jest.runAllTimers();
    expect(onSync).toHaveBeenCalled();
    expect(gotQuery).toBe("");
});

describe('getAutoRenderElements', () => {
    test('with full URL', () => {
        RoutingAssistant.match = {
            params: {
                query: "test",
                renderElements: 20
            }
        };
        expect(RoutingAssistant.getAutoRenderElements()).toBe(20);
    });
    test('with el:30', () => {
        RoutingAssistant.match = {
            params: {
                query: "el:30",
            }
        };
        expect(RoutingAssistant.getAutoRenderElements()).toBe(30);
    });
    test('with no renderElements', () => {
        RoutingAssistant.match = {
            params: {
                query: "query",
            }
        };
        expect(RoutingAssistant.getAutoRenderElements()).toBe(10);
    });
    test('With a string number', () => {
        RoutingAssistant.match = {
            params: {
                query: "test",
                renderElements: "50"
            }
        };
        expect(RoutingAssistant.getAutoRenderElements()).toBe(50);
    });
});

describe('_constructURL', () => {
    const URLs = [
         {
             root: '/test',
             search: 'search',
             renderElements: 20,
             filter: "",
             result: "/test/search/20",
         },
         {
             root: '/test',
             search: undefined,
             renderElements: 20,
             filter: undefined,
             result: "/test/el:20",
         },
         {
             root: '/test',
             search: undefined,
             renderElements: undefined,
             filter: undefined,
             result: "/test",
         },
         {
             root: '/test',
             search: "query",
             renderElements: "auto",
             filter: "test",
             result: "/test/query/10/test",
         },
    ];
    test.each(URLs)("%p", (url) => {
        expect(RoutingAssistant._constructURL( url.root, url.search, url.renderElements, url.filter)).toBe(url.result);
    });
});

test('Navigation triggered by RoutingAssistant (push)', () => {
    let gotQuery = null;
    const onSync = jest.fn( (query) =>
    {
        gotQuery = query;
    });

    // If we are on /test, and someone triggers a push to /sok/test via
    // RoutingAssistant, then syncToStore should NOT trigger
    const { location } = prepareMocks();
    location.pathname = '/test';
    RoutingAssistant.push('/sok/test');
    expect(window.location.pathname).toBe('/sok/test');
    expect(RoutingAssistant.ourChange).toBe('/sok/test');
    expect(RoutingAssistant.isManagedChange()).toBe(true);

    // Call syncToState
    RoutingAssistant.syncToStore({
        match: { params: { query: "test" } },
        onSync
    });

    jest.runAllTimers();

    expect(onSync).not.toHaveBeenCalled();
    expect(gotQuery).toBe(null);
});

test('Basic debouncing in .replace', () => {
    const { history } = prepareMocks();

    history.replace = jest.fn(history.replace.bind(history));

    RoutingAssistant.replace('/test/u');
    RoutingAssistant.replace('/test/us');
    RoutingAssistant.replace('/test/user');
    RoutingAssistant.replace('/test/user');
    RoutingAssistant.replace('/test/user-');
    RoutingAssistant.replace('/test/user-i');
    RoutingAssistant.replace('/test/user-is');
    RoutingAssistant.replace('/test/user-is-');
    RoutingAssistant.replace('/test/user-is-t');
    RoutingAssistant.replace('/test/user-is-ty');
    RoutingAssistant.replace('/test/user-is-typ');
    RoutingAssistant.replace('/test/user-is-typi');
    RoutingAssistant.replace('/test/user-is-typin');
    RoutingAssistant.replace('/test/user-is-typing');
    jest.runAllTimers();
    expect(history.replace).toHaveBeenCalledTimes(1);
});

test('onManagedChange() for history.replace', () => {
    // If we are on /test, and someone triggers a push to /sok/test via
    // RoutingAssistant, then syncToStore should NOT trigger
    const { history, location } = prepareMocks();
    location.pathname = '/test';
    expect(RoutingAssistant.isManagedChange()).toBe(false);
    history.replace('/hello/world');
    expect(RoutingAssistant.isManagedChange()).toBe(true);
});

test('onManagedChange() for RoutingAssistant.replace', () => {
    // If we are on /test, and someone triggers a push to /sok/test via
    // RoutingAssistant, then syncToStore should NOT trigger
    const { location } = prepareMocks();
    location.pathname = '/test';
    expect(RoutingAssistant.isManagedChange()).toBe(false);
    expect(location.pathname).toBe('/test');
    RoutingAssistant.replace('/hello/world');
    jest.runAllTimers();
    expect(location.pathname).toBe('/hello/world');
    expect(RoutingAssistant.isManagedChange()).toBe(true);
});

test('syncToStore() async protection', () => {
    // This is a mocked version of isManagedChange. It will return false the
    // first time it is called, and true the second time. This is to test the
    // async protection bit of syncToStore, which is there to avoid it trying
    // to manage old data (ie. something has changed between when the change
    // was queued and when it is executing resulting in isManagedChange to be
    // true).
    let manager = 0;
    RoutingAssistant.isManagedChange = jest.fn(() => {
        if(manager === 0)
        {
            manager++;
            return false;
        }
        return true;
    });

    const onSync = jest.fn();

    RoutingAssistant.syncToStore({
        match: { params: { query: "test" } },
        onSync
    });
    jest.runAllTimers();

    expect(onSync).not.toHaveBeenCalled();
    expect(RoutingAssistant.isManagedChange).toHaveBeenCalledTimes(2);
});

test('Invalid syncToStore parameters', () => {
    let gotQuery = null;
    const onSync = jest.fn( (query) =>
    {
        gotQuery = query;
    });

    // If we are on /test, and someone triggers a push to /sok/test via
    // RoutingAssistant, then syncToStore should NOT trigger
    const { location } = prepareMocks();
    location.pathname = '/test';

    // Call syncToState
    RoutingAssistant.syncToStore({
        match: { params: { query: undefined } },
        onSync
    });

    jest.runAllTimers();
    expect(onSync).toHaveBeenCalled();
    expect(gotQuery).toBe("");
});
