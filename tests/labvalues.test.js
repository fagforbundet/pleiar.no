/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import { prepareEnv, getComponentWith, checkSnapshot } from './test-helpers';

import { LabValueEntryCleartext, LabValueEntryCleartextElement, LabValueCategoryTable, LabValueMaterialMap } from '../src/react-components/labvalues';

const baseEntry = {"navn":"B12","materiale":"S","ref":"140-600 pmol/l","category":"Hematologi, koagulasjon mm."};

prepareEnv();

test('LabValueEntryCleartextElement', () =>
{
    expect(getComponentWith(<LabValueEntryCleartextElement title="Title" value="Test" />)).toMatchSnapshot();
    expect(getComponentWith(<LabValueEntryCleartextElement title="Title" value={"Test\nwith\nnewlines"} />)).toMatchSnapshot();
});

test('LabValueEntryCleartext basic rendering', () =>
{
    expect(<LabValueEntryCleartext entry={baseEntry} />).toMatchSnapshot();
});

test('LabValueCategoryTable', () => {
    checkSnapshot(<LabValueCategoryTable values={{ index: {"Hormonanalyser i urin":[]} }} category="Hormonanalyser i urin" />);
    checkSnapshot(<LabValueCategoryTable values={{ index: {"Klinisk kjemi i urin og spinalvæske":[]} }} category="Klinisk kjemi i urin og spinalvæske" />);
    checkSnapshot(<LabValueCategoryTable values={{ index: {"Klinisk farmakologi":[]} }} category="Klinisk farmakologi" />);
});

test.each(Object.keys(LabValueMaterialMap))('LabValueMaterialMap %s', (mapKey) => {
    expect(mapKey).toMatch(/^\w+$/);
    expect(LabValueMaterialMap[mapKey]).toMatchObject({
        name: /^\S+[^.]$/,
        description: /^\S+.*\.$/,
    });
});

test('LabValueEntryCleartext', () =>
{
    // First basic, with category "Hematologi, koagulasjon mm."
    const comp = getComponentWith(<LabValueEntryCleartext entry={baseEntry} />, { enzyme: true });
    expect(comp.find(LabValueEntryCleartextElement).at(1).props()).toMatchObject({
        title: 'Referanseområde'
    });

    // Then switch the category to "Klinisk farmakologi"
    const klinFarm = Object.assign({},baseEntry,{
        category: "Klinisk farmakologi"
    });
    const klinComp = getComponentWith(<LabValueEntryCleartext entry={klinFarm} />, { enzyme: true });
    // It should now be using "Terapeutisk område"
    expect(klinComp.find(LabValueEntryCleartextElement).at(1).props()).toMatchObject({
        title: 'Terapeutisk område'
    });

    // Material should be expanded
    expect(klinComp.find(LabValueEntryCleartextElement).at(0).props()).toMatchObject({
        value: "Serum",
    });
    // Should only have 3 LabValueEntryCleartextElements since it has no merknad
    expect(klinComp.find(LabValueEntryCleartextElement)).toHaveLength(3);

    // If material is not a valid value, it should be returned unchanged
    const invalidMaterial = Object.assign({},baseEntry,{
        materiale: "invalid"
    });
    const invalidMaterialComp = getComponentWith(<LabValueEntryCleartext entry={invalidMaterial} />, { enzyme: true });
    expect(invalidMaterialComp.find(LabValueEntryCleartextElement).at(0).props()).toMatchObject({
        value: "invalid",
    });

    // Now, add a merknad
    const withMerknad = Object.assign({},baseEntry,{
        merknad: "A merknad"
    });
    const merknadComp = getComponentWith(<LabValueEntryCleartext entry={withMerknad} />, { enzyme: true });
    // Should now have 4 LabValueEntryCleartextElements because it added a merknad
    expect(merknadComp.find(LabValueEntryCleartextElement)).toHaveLength(4);

    // For uirine, we add additional information, going so far as to add a
    // merknad even without there being one
    const urineEntry = Object.assign({},baseEntry,{
        materiale: "U",
        merknad: undefined
    });
    const urineComp = getComponentWith(<LabValueEntryCleartext entry={urineEntry} />, { enzyme: true });
    // Should have 4 LabValueEntryCleartextElements even without a specific merknad
    expect(urineComp.find(LabValueEntryCleartextElement)).toHaveLength(4);
    // And it should include the right info
    expect(urineComp.find(LabValueEntryCleartextElement).at(3).props()).toMatchObject({
        value: expect.stringMatching(/utskillelsen av elektrolytter/)
    });

    // Changing the category of the urine entry, should change the merknad added
    urineEntry.category = "Hormonanalyser i urin";
    const urineHormoneComp = getComponentWith(<LabValueEntryCleartext entry={urineEntry} />, { enzyme: true });
    expect(urineHormoneComp.find(LabValueEntryCleartextElement).at(3).props()).toMatchObject({
        value: expect.stringMatching(/konsentrasjonen av mange hormoner/)
    });
    // Adding a merknad should include that in addition to the category notice
    urineEntry.merknad = "test-merknad";
    const urineHormoneMerknadComp = getComponentWith(<LabValueEntryCleartext entry={urineEntry} />, { enzyme: true });
    expect(urineHormoneMerknadComp.find(LabValueEntryCleartextElement).at(3).props()).toMatchObject({
        value: expect.stringMatching(/test-merknad\n.*konsentrasjonen av mange hormoner/)
    });
});
