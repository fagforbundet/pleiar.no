/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import featureDetection from '../src/helper.feature-detection';
import React from 'react';
import { getComponentWith } from './test-helpers';
import { ExternalLink } from '../src/react-components/shared/links';

test('Supporting noopener', () => {
    featureDetection.aSupportsRel = () => { return true; };
    const comp = getComponentWith(<ExternalLink href="https://example.com">Test</ExternalLink>,{
        enzyme: true
    });
    expect(comp.find('a').props().rel).not.toMatch('noreferrer');
    expect(comp.find('a').props().rel).toMatch('noopener');
});
test('Not supporting noopener', () => {
    featureDetection.aSupportsRel = () => { return false; };
    const comp = getComponentWith(<ExternalLink href="https://example.com">Test</ExternalLink>,{
        enzyme: true
    });
    expect(comp.find('a').props().rel).toMatch('noreferrer');
});
describe('Props passing', () => {
    test('Default', () => {
        const comp = getComponentWith(<ExternalLink href="https://example.com" className="passthrough">Test</ExternalLink>,{
            enzyme: true
        });
        expect(comp.find('a').props().className).toBe("external-link passthrough");
    });
    test('With basicStyle', () => {
        const comp = getComponentWith(<ExternalLink href="https://example.com" basicStyle className="passthrough">Test</ExternalLink>,{
            enzyme: true
        });
        expect(comp.find('a').props().className).toBe("passthrough");
    });
});
test('Default className', () => {
    const comp = getComponentWith(<ExternalLink href="https://example.com">Test</ExternalLink>,{
        enzyme: true
    });
    expect(comp.find('a').props().className).toBe("external-link");
});
test('basicStyle', () => {
    const comp = getComponentWith(<ExternalLink basicStyle href="https://example.com">Test</ExternalLink>,{
        enzyme: true
    });
    expect(comp.find('a').props().className).toBe("");
});
test('sameWindow=true', () => {
    const comp = getComponentWith(<ExternalLink sameWindow={true} href="https://example.com">Test</ExternalLink>,{
        enzyme: true
    });
    expect(comp.find('a').props().rel).toBe(undefined);
    expect(comp.find('a').props().target).toBe(undefined);
    // sameWindow should trigger basicStyle
    expect(comp.find('a').props().className).toBe("");
});
test('sameWindow=false', () => {
    const comp = getComponentWith(<ExternalLink sameWindow={false} href="https://example.com">Test</ExternalLink>,{
        enzyme: true
    });
    expect(comp.find('a').props().rel).not.toBe(undefined);
    expect(comp.find('a').props().target).not.toBe(undefined);
});
