/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import { main } from '../src/react-components/main';

/*
 * We have to mock all toplevel components, because Pleiar is our toplevel
 * component and calls everything underneath it.
 */
jest.mock('../src/react-components/frontpage.js', () => {return () => { return <div>FRONT PAGE</div>; };});
jest.mock('../src/react-components/menu.js', () => {return () => { return <div>MENU</div>; };});
jest.mock('../src/react-components/medcalc.js', () => {return () => { return <div>MEDCALC</div>; };});
jest.mock('../src/react-components/nutricalc.js', () => {return () => { return <div>NUTRICALC</div>; };});
jest.mock('../src/react-components/about.js', () => {return () => { return <div>ABOUT</div>; };});
jest.mock('../src/react-components/shared.js', () => { return { Handle404 () { return <div></div>; }, ScrollToTopHelper () { return <div></div>; }, };});
jest.mock('../src/workarounds.js', () => {
    return {
        applyBrowserWorkaroundsIfNeeded ()
        {
            global.appliedMSWorkarounds = true;
            return;
        }
    };
});
jest.mock('react-dom', () => {
    return {
        hydrate (element)
        {
            global.mockedReactDOMState.element = element;
            global.mockedReactDOMState.method = 'hydrate';
        },
        render (element)
        {
            global.mockedReactDOMState.element = element;
            global.mockedReactDOMState.method = 'render';
        },
    };
});
jest.mock('../src/searcher', () => {
    return {
        hasInitialized: jest.fn( () => false ),
        initialize: jest.fn()
    };
});
import PleiarSearcher from '../src/searcher';

beforeEach( () => {
    document.body.innerHTML = '';
    window.__REDUX_DEVTOOLS_EXTENSION__ = jest.fn();
    global.mockedReactDOMState = {};
    global.PLEIAR_ENV = 'production';
    PleiarSearcher.hasInitialized.mockClear();
    PleiarSearcher.initialize.mockClear();
});

function createDummyRootElement ()
{
    const root = document.createElement('div');
    root.id = 'root';
    document.querySelector('body').appendChild(
        root
    );
    return root;
}

test('main with render()', () => {
    createDummyRootElement();

    main();
    // Should use render when root has no children
    expect(global.mockedReactDOMState.method).toBe('render');
    // Should have provided a redux object as the store
    expect(global.mockedReactDOMState.element.props.store.dispatch).toBeTruthy();
    // Should have called applyBrowserWorkaroundsIfNeeded
    expect(global.appliedMSWorkarounds).toBe(true);
});
test('main with hydrate()', () => {
    const root = createDummyRootElement();
    root.appendChild(
        document.createElement('div')
    );

    main();
    // Should use hydrate when root has children
    expect(global.mockedReactDOMState.method).toBe('hydrate');
    // Should have provided a redux object as the store
    expect(global.mockedReactDOMState.element.props.store.dispatch).toBeTruthy();

    // Should NOT enable redux devtools in production
    expect(window.__REDUX_DEVTOOLS_EXTENSION__.mock.calls).toHaveLength(0);
});
test('redux devtools in development', () => {
    createDummyRootElement();
    // SHOULD enable redux devtools in development
    global.PLEIAR_ENV = 'development';
    main();
    expect(window.__REDUX_DEVTOOLS_EXTENSION__.mock.calls).toHaveLength(1);
});
test('main without root', () => {
    expect( () => {
        main();
    }).toThrow('No root element to render into, aborting');
});
test('main auto-init of PleiarSearcher', () => {
    jest.useFakeTimers();
    createDummyRootElement();
    main();
    jest.runAllTimers();
    expect(PleiarSearcher.hasInitialized).toHaveBeenCalled();
    expect(PleiarSearcher.initialize).toHaveBeenCalled();
});
describe('main storage persistence', () => {
    describe('In app mode', () => {
        test('with persistence support', () => {
            window.navigator.standalone = true;
            createDummyRootElement();
            window.navigator.storage = {
                persist: jest.fn()
            };
            main();
            expect(window.navigator.storage.persist).toHaveBeenCalled();
        });
        test('without persistence support', () => {
            window.navigator.standalone = true;
            createDummyRootElement();
            delete window.navigator.storage.persist;
            expect( main ).not.toThrow();
        });
    });
    describe('In non-app mode', () => {
        test('with persistence support', () => {
            delete window.navigator.standalone;
            createDummyRootElement();
            window.navigator.storage.persist = jest.fn();
            main();
            expect(window.navigator.storage.persist).not.toHaveBeenCalled();
        });
    });
});
