/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import fest from '../data/fest.json';

let curDrugID = -1;
test.each(fest.drugs)('Verify drugs', (entry) => {
    curDrugID++;
    expect(typeof(entry.name)).toBe('string');
    if(entry.atc !== undefined)
    {
        expect(typeof(entry.atc)).toBe('string');
        expect(entry.exchangeGroup).not.toBeDefined();
    }
    else if(entry.exchangeGroup !== undefined)
    {
        expect(typeof(entry.exchangeGroup)).toBe('number');
        expect(entry.atc).not.toBeDefined();
        // Verify links
        const group = fest.exchangeList[ entry.exchangeGroup ];
        expect(group).toBeDefined();
        expect(group.drugIndexes).toBeInstanceOf(Array);
        expect(group.drugIndexes.length).toBeGreaterThan(1);
        expect(group.drugIndexes).toContain( curDrugID );
    }
    else
    {
        throw('Invalid drug entry: '+JSON.stringify(entry));
    }
});
test.each(fest.exchangeList)('Verify groups', (group) =>
{
    expect(group.drugIndexes).toBeInstanceOf(Array);
    expect(group.drugIndexes.length).toBeGreaterThan(1);
    expect(group.drugIndexes.length).toBeGreaterThan(1);
    expect(typeof(group.atc)).toBe('string');
    expect(group.atc).toMatch(/^[A-Z]/);
    expect(group.atc).toMatch(/\d/);
    if(group.merknad !== undefined)
    {
        expect(typeof(group.merknad)).toBe('string');
        expect(group.merknad.length).toBeGreaterThan(3);
    }
});
