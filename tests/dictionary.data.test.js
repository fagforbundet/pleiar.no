/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('../data/datasett/dictionary.json');
jest.unmock('../data/datasett/website-identification.json');
jest.unmock('../data/handbok/handbok.json');
import data from '../data/datasett/dictionary.json';

import React from 'react';
import { ReadMoreEntry } from '../src/react-components/dictionary';
import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import Dictionary from '../src/react-components/dictionary';
import { getComponentWith } from './test-helpers';
import { HandbookEntry } from '../src/data.handbook';
Enzyme.configure({ adapter: new Adapter() });
import { testInitPleiarSearcher } from './search-helper.js';
import PleiarSearcher from '../src/searcher';

test.each(data)('Dictionary data syntax: %p', (entry) =>
{
    expect(entry).toMatchObject({
        // Must be non-empty string
        expression: expect.stringMatching(/\S+/),
        // Must be non-empty string ending with a period
        description: expect.stringMatching(/\S+.*\.$/),
    });
    // Can't have space in links, use %20
    expect(entry.description).not.toMatch(/\S\]\([^)]*\s+[^)]*\)/);
    // keywords is optional, but must be non-zero-length string if present
    if(entry.keywords !== undefined)
    {
        expect(entry).toMatchObject({
            keywords: expect.stringMatching(/.+/)
        });
    }
    // lookupKey is optional, but must be non-zero-length string with no whitespace, if present
    if(entry.lookupKey !== undefined)
    {
        expect(entry).toMatchObject({
            lookupKey: expect.stringMatching(/^\S+$/)
        });
    }
    // moreInfoURLs is optional, but must be an array of URLs if present
    if(entry.moreInfoURLs !== undefined)
    {
        expect(entry.moreInfoURLs).toBeInstanceOf(Array);
        for(const url of entry.moreInfoURLs)
        {
            expect(url).toMatch(/^https?:\/\//);
        }
    }
    // seeAlso is optional, but must be an array of strings if present
    if(entry.seeAlso !== undefined)
    {
        expect(entry.seeAlso).toBeInstanceOf(Array);
        for(const see of entry.seeAlso)
        {
            expect(see).toMatch(/\S+/);
        }
    }
    // seeHandbook is optional, but must be a string if present
    if(entry.seeHandbook !== undefined)
    {
        expect(entry.seeHandbook).toMatch(/^\S+$/);
        const path = entry.seeHandbook.replace(/^\/+/,'');
        let hbEntry;
        expect( () => {
            hbEntry = new HandbookEntry(path);
        }).not.toThrow();
        expect(hbEntry.exists).toBe(true);
    }

    // There's probably a better way to do this, the point is to check if
    // there are any unknown keys
    const permittedEntries = [ 'expression','description','keywords','moreInfoURLs','seeAlso', 'seeHandbook','lookupKey' ];

    for(const key in entry)
    {
        expect(permittedEntries).toContain(key);
    }
});

/* eslint jest/expect-expect: ["error", { "assertFunctionNames": ["expect","check"] }] */
describe('Dictionary URL identification', () =>
{
    const check = (href) => {
        const component = Enzyme.mount(
                <ReadMoreEntry entry={href}  />
        );
        expect(component.find('a').at(0).text()).not.toMatch( /\?\?\?/);
    };
    for(const entry of data)
    {
        if(entry.moreInfoURLs !== undefined)
        {
            for(const url of entry.moreInfoURLs)
            {
                test(' '+url+' from '+entry.expression, () => {
                    check(url);
                });
            }
        }
    }
});

test('Dictionary data rendering', () =>
{
    expect( () =>
    {
        getComponentWith(Dictionary,{
            enzyme: true,
            redux: true,
            router: true,
            route: "/ordliste",
            previousPath: "/",
            routingAssistant: true
        });
    }).not.toThrow();
});

describe('Dictionary data indexing', () =>
{
    test("Basic indexing", () => {
        expect( () =>
            {
                testInitPleiarSearcher();
            }).not.toThrow();
    });
    test('Number of entries', () => {
        const entries = PleiarSearcher.safePartialSearch("",(accumulator,result) =>
            {
                if(result.ref.indexOf("dictionary") === 0)
                {
                    accumulator.push(result);
                }
                return accumulator;
            });
        expect(entries.length).toBe(data.length);
    });
    // eslint-disable-next-line jest/expect-expect
    test.each(data)('Dictionary search() results %#', (entry) => {
        const expr = entry.expression.replace(/\s+[+-]/g,' ');
        const results = PleiarSearcher.search(expr,(accumulator,result) =>
            {
                if(result.ref.indexOf("dictionary") === 0)
                {
                    accumulator.push(result);
                }
                return accumulator;
            });
        if(results.length == 0)
        {
            if(entry.expression !== expr)
            {
                throw new Error("No search() results searching for '"+expr+"' (for expression='"+entry.expression+"')");
            }
            else
            {
                throw new Error("No search() results searching for '"+entry.expression+"'");
            }
        }
    });
    test.each(data)('Dictionary safePartialSearch() results %#', (entry) => {
        const expr = entry.expression.replace(/\s+[+-]/g,' ');
        const reducer = (accumulator,result) =>
            {
                if(result.ref.indexOf("dictionary") === 0)
                {
                    accumulator.push(result);
                }
                return accumulator;
            };
        const results = PleiarSearcher.safePartialSearch(expr,reducer);
        if(results.length == 0)
        {
            if(entry.expression !== expr)
            {
                throw new Error("No search() results searching for '"+expr+"' (for expression='"+entry.expression+"')");
            }
            else
            {
                throw new Error("No search() results searching for '"+entry.expression+"'");
            }
        }
        /*
         * If this fails, then our hack in src/searcher.js search() method can be
         * removed
         */
        if (/^[ÆØÅæøå]/.test(expr))
        {
            expect(PleiarSearcher._index().search(expr).reduce(reducer,[])).toHaveLength(0);
        }
    });
});
