/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import GlobalSearch, { InfiniteScrollSearchResultRenderer, InfiniteScrollSearchResultRendererUnRouted, TopSearchUnconnected, TopSearch, GlobalSearchRedirect, SearchResultRendererUnconnected, SearchResultRenderer, WhateverRenderer, RequiresPleiarSearcher, SearchCounter, SearchCounterUnconnected, ExternalSearchRenderer } from '../src/react-components/global-search.js';
import { setGlobalSearchString } from '../src/actions/global-search';
import { prepareEnv, checkSnapshot, getRealRedux, getComponentWith, changeFieldValue, mockLocation, silentFunc, mockedRouterHistoryObj } from './test-helpers';
import PleiarSearcher from '../src/searcher.js';
import { Link } from 'react-router-dom';
import RoutingAssistant from '../src/routing';
import { testInitPleiarSearcher } from './search-helper';

prepareEnv();
mockLocation();
testInitPleiarSearcher();

const compSettings = {
    redux: true,
    includeReduxStore: true,
    router: true,
    route: "/sok",
    enzyme: true,
    previousPath: "/",
    routingAssistant: true
};

test('Basic rendering of components and search page', () => {
    checkSnapshot(GlobalSearch,compSettings);
    expect(global.__title).toMatch('Søk');
    checkSnapshot(TopSearch,compSettings);
    checkSnapshot(GlobalSearchRedirect,compSettings);
});

test("redux store", () => {
    const store = getRealRedux();
    // Default state
    expect( store.getState().globalSearch).toMatchObject({
        search: "",
    });

    store.dispatch(setGlobalSearchString("test"));
    expect( store.getState().globalSearch.search).toBe("test");
});

describe('GlobalSearch', () =>
{
    test('component', () => {
        const {store, component} = getComponentWith(<GlobalSearch />,compSettings);
        // Default, with search="" we should have no SearchResultRenderer
        expect( component.find(SearchResultRenderer) ).toHaveLength(0);
        // After setting a string, the SearchResultRenderer should show up
        store.dispatch(setGlobalSearchString("test"));
        expect( component.find(SearchResultRenderer) ).toHaveLength(1);
        // And, if we search for something that exists, we should have a proper
        // result
        store.dispatch(setGlobalSearchString("afasi"));
        expect( component.find(SearchResultRenderer).find('.dictionaryEntry').length ).toBeGreaterThan(0);
    });
    test('with reducer=verktoy', () => {
        const {component,store} = getComponentWith(<GlobalSearch />,{
            ...compSettings,
            route: '/sok/afasi/10/verktoy',
            previousPath: '/sok/:query?/:renderElements?/:filter?'
        });
        store.dispatch(setGlobalSearchString("afasi"));
        // With the tool filter, afasi shouldn't return anything, since we don't have
        // any tool matching that, even if we do have other things
        expect(component.find(SearchResultRenderer).find('.dictionaryEntry')).toHaveLength(0);
    });
    test('with reducer=handbok', () => {
        const {component,store} = getComponentWith(<GlobalSearch />,{
            ...compSettings,
            route: '/sok/GNU/10/handbok',
            previousPath: '/sok/:query?/:renderElements?/:filter?'
        });
        store.dispatch(setGlobalSearchString("GNU"));
        // With the handbok filter, GNU should return data
        expect(component.find(SearchResultRenderer).find('.handbookSearchEntry').length).toBeGreaterThan(0);
    });
    test('with no results', () => {
        const {store, component} = getComponentWith(<GlobalSearch />,compSettings);
        // And, if we search for something that exists, we should have a proper
        // result
        store.dispatch(setGlobalSearchString("searchthatdoesnotexist"));
        expect( component.find(SearchResultRenderer).find('.dictionaryEntry').length ).toBe(0);

        // Should have rendered the search suggestions
        expect( component.find('.otherSearchSuggestions') ).toHaveLength(2);
    });
    test('Clear search on unmount', () => {
        const {store, component} = getComponentWith(<GlobalSearch />,compSettings);
        // And, if we search for something that exists, we should have a proper
        // result
        store.dispatch(setGlobalSearchString("searchthatdoesnotexist"));
        expect( store.getState().globalSearch.search).toBe("searchthatdoesnotexist");
        component.unmount();
        expect( store.getState().globalSearch.search).toBe("");
    });
    describe('applyReducer', () => {
        const {component} = getComponentWith(<GlobalSearch />,compSettings);
        const filters = [
            {
                name: "labverdier",
                map: "labValue",
            },
            {
                name: "handbok",
                map: "handbook",
            },
            {
                name: "verktoy",
                map: "tool",
            },
            {
                name: "eksternt",
                map: "external"
            },
            {
                name: "ernaering",
                map: "nutrition",
            },
            {
                name: "oppslag",
                map: "quickref",
            },
            {
                name: "elaering",
                map: "quiz"
            }
        ];
        const testResult = [
            {
                ref: 'labValue/0',
            },
            {
                ref: 'handbook/0',
            },
            {
                ref: 'tool/0',
            },
            {
                ref: 'externalResources/0',
            },
            {
                ref: 'external/0',
            },
            {
                ref: 'nutrition/0',
            },
            {
                ref: 'quickref/0',
            },
            {
                ref: 'quiz/0',
            }
        ];
        const globalSearchInstance = component.find('GlobalSearch').at(0).instance();
        test.each(filters)("%p", (filter) => {
            // This is rather ugly, yes, but saves some component recreation -
            // we're just calling a side-effect-free method, and don't need to
            // test the entire object construction, just the method, and it uses
            // this one prop.
            globalSearchInstance.props.match.params.filter = filter.name;

            const result = globalSearchInstance.applyReducer(testResult);
            expect(result.result).toHaveLength(1);
            expect(result.filter).toBe(filter.name);
            expect(result.result[0].ref).toMatch(filter.map);
        });
        /*
         * What this test does is make sure that the number of entries in
         * "filters" match the number of categories displayed in the front-end.
         * It is possible to add categories, but then forget to modify
         * applyReducer(), thus breaking category-filtering.
         *
         * When adding categories to SearchCounter, also modify
         * GlobalSearch.applyReducer()
         */
        test('make sure categories are matching', () => {
            const results = PleiarSearcher.search('');
            const fakeHistory = {
                push: jest.fn()
            };
            const comp = getComponentWith(<SearchCounterUnconnected search="fake-results" results={results} history={fakeHistory} />,{
                router: true,
                route: '/sok/fake-results',
                enzyme: true,
                previousPath: '/'
            });
            // (there are two categories that don't have a reducer, since they
            // have their own private search function, so we expect it to be
            // the filters specified above +2)
            expect(comp.find('.pill')).toHaveLength(filters.length+2);
        });
    });
});

describe('SearchResultRenderer component', () =>
{
    test('SearchResultRendererUnconnected', () =>
    {
        /* eslint-disable no-console */
        // We add a temporary console.error here, because the test below will
        // trigger react to log something to assist with debugging the error that
        // occurs, but since we're expecting an error to occur, that message isn't
        // useful in this case.
        const err = console.error;
        console.error = ()=>{};
        expect( () => {
            getComponentWith(<SearchResultRendererUnconnected result={[[]]} search="" />);
        }).toThrow(/got non-string element but no type/);
        console.error = err;
        /* eslint-enable no-console */

        checkSnapshot(<SearchResultRendererUnconnected result={ [ "dictionary/0" ] } search="" />,compSettings);
        checkSnapshot(<SearchResultRendererUnconnected result={ [ {ref: "dictionary/0" } ] } search="" />,compSettings);
        checkSnapshot(<SearchResultRendererUnconnected result={ [ ] } search="test" suggestAlternatives={true} />,compSettings);
        checkSnapshot(<SearchResultRendererUnconnected result={ [ ] } search="test" suggestAlternatives={false} />,compSettings);
        checkSnapshot(<SearchResultRendererUnconnected result={ [ ] } search="test" suggestGlobalSearch={true} />,compSettings);
    });

    test('SearchResultRenderer', () =>
    {
        /* eslint-disable no-console */
        // We add a temporary console.error here, because the test below will
        // trigger react to log something to assist with debugging the error that
        // occurs, but since we're expecting an error to occur, that message isn't
        // useful in this case.
        const err = console.error;
        console.error = ()=>{};
        expect( () => {
            getComponentWith(<SearchResultRenderer result={[[]]} search="" />, {
                redux: true
            });
        }).toThrow(/got non-string element but no type/);
        console.error = err;
        /* eslint-enable no-console */

        checkSnapshot(<SearchResultRenderer result={ [ "dictionary/0" ] } search="" />,compSettings);
        checkSnapshot(<SearchResultRenderer result={ [ {ref: "dictionary/0" } ] } search="" />,compSettings);
        checkSnapshot(<SearchResultRenderer result={ [ ] } search="test" suggestAlternatives={true} />,compSettings);
        checkSnapshot(<SearchResultRenderer result={ [ ] } search="test" suggestAlternatives={false} />,compSettings);
        checkSnapshot(<SearchResultRenderer result={ [ ] } search="test" suggestGlobalSearch={true} />,compSettings);
    });

    test('SearchResultRenderer redux connection', () =>
    {
        /* eslint-disable no-console */
        // We add a temporary console.error here, because the test below will
        // trigger react to log something to assist with debugging the error that
        // occurs, but since we're expecting an error to occur, that message isn't
        // useful in this case.
        const err = console.error;
        console.error = ()=>{};
        const { component, store } =  getComponentWith(<SearchResultRenderer result={[ ]} search="test" suggestAlternatives={true} />, compSettings);
        expect(typeof component.find(SearchResultRendererUnconnected).props().onSetMedSearch).toBe('function');
        component.find(SearchResultRendererUnconnected).props().onSetMedSearch("SearchResultRenderer-redux-test-search");
        expect(store.getState().medSynonyms.search).toBe("SearchResultRenderer-redux-test-search");
        expect(typeof component.find(SearchResultRendererUnconnected).find(Link).at(0).props().onClick).toBe('function');
        expect(component.find(SearchResultRendererUnconnected).find(Link).at(0).props().to).toBe('/verktoy/medikament/synonymer');
        console.error = err;
        /* eslint-enable no-console */
    });
});

describe('ExternalSearchRenderer', () => {
    test('other-entry', () => {
        checkSnapshot(<ExternalSearchRenderer entry={{
            title: "Test",
            text: "Test",
            type: "other",
            url: "https://fagforbundet.no/"
        }} />);
    });
    test('fagprat-entry', () => {
        checkSnapshot(<ExternalSearchRenderer entry={{
            title: "Test",
            text: "Test",
            type: "fagprat",
            url: "https://soundcloud.com/fagprat/"
        }} />);
    });
});

describe('WhateverRenderer component', () =>
{
    const search = PleiarSearcher.search("jest-test-ord", (accumulator, result) => {
        accumulator.push( result );
        return accumulator;
    });

    const snapSettings = {
        enzyme: true,
        redux: true,
        router: true,
        route: "/sok",
        previousPath: "/"
    };

    test('Should have four results', () => {
        expect(search).toHaveLength(4);
    });

    test.each(search)('WhateverRenderer search result %#', (result) => {
        const search0Comp = getComponentWith(<WhateverRenderer result={result.ref}  metadata={result.matchData} />,snapSettings);
        expect(search0Comp.find('#whateverRendererError')).toHaveLength(0);
        expect(search0Comp).toMatchSnapshot();
    });

    test('B12 search', () =>
    {
        const b12search = PleiarSearcher.search("B12", (accumulator, result) => {
            accumulator.push( result.ref );
            return accumulator;
        });
        checkSnapshot(<WhateverRenderer result={b12search[0]} />,snapSettings);
    });
});

describe('WhateverRenderer type rendering', () => {
    const types = [ 'dictionary', 'externalResources', 'labValue', 'handbook', 'tool', 'nutrition', 'external' ];
    const results = PleiarSearcher.search('');
    const whateverSettings = {
        enzyme: true,
        redux: true,
        router: true,
        route: "/sok",
        previousPath: "/"
    };

    // This renders one of each type and makes sure it doesn't error out
    test.each(types)('%p', (entry) => {
        let found = false;
        for(const attempt of results)
        {
            if(attempt.ref.indexOf(entry) === 0)
            {
                let renderer;
                expect( () => {
                    renderer = getComponentWith(<WhateverRenderer result={attempt.ref} metadata={attempt.matchData} />, whateverSettings);
                }).not.toThrow();

                // $FlowIgnore - doesn't realize that renderer is defined at this point in the test
                expect( renderer.find('#whateverRendererError') ).toHaveLength(0);
                found = true;
                break;
            }
        }
        // We should have found an entry to render of this type
        expect(found).toBe(true);
    });
    // This writes a snapshot of each type
    test.each(types)('Snapshot: %p', (entry) => {
        for(const attempt of results)
        {
            if(attempt.ref.indexOf(entry) === 0)
            {
                checkSnapshot(<WhateverRenderer result={attempt.ref} metadata={attempt.matchData} />, whateverSettings);
                break;
            }
        }
    });
    // This renders with an invalid type
    test('Invalid type', () => {
        expect( () => {
            let renderer;
            expect( () => {
                renderer = getComponentWith(<WhateverRenderer type="invalid-entry" result="invalid-entry" />, whateverSettings);
            }).not.toThrow();

            // $FlowIgnore - doesn't realize that renderer is defined at this point in the test
            expect( renderer.find('#whateverRendererError') ).toHaveLength(1);
        }).not.toThrow();
    });
});

/* eslint-disable no-console */
describe('InfiniteScrollSearchResultRenderer component', () =>
{

    // We add a temporary console.error here, because the test below will
    // trigger react to log errors about duplicate keys. This is expected, since
    // we're being a bit too clever here and adding the same element to the search results
    // 100 times for test purposes.
    const err = console.error;

    const fakeResults = [];
    for(let no = 0; no < 100; no++)
    {
        fakeResults.push('dictionary/0');
    }
    const AEL = window.addEventListener;
    const REL = window.removeEventListener;

    const getInfiniteScroller = () => {
        console.error = ()=>{};
        return getComponentWith(<InfiniteScrollSearchResultRenderer result={fakeResults} search="fake-search" searchPathPrefix="/sok-test" />,{
            enzyme: true,
            redux: true,
            router: true,
            route: "/sok",
            previousPath: "/",
        });
    };
    test('Default rendering', () => {
        const component = getInfiniteScroller();
        // Should render 10 by default
        expect(component.find(WhateverRenderer)).toHaveLength(10);
    });
    test('Event listener', () => {
        window.addEventListener = jest.fn();
        window.removeEventListener = jest.fn();
        const component = getInfiniteScroller();
        // Should add listener
        expect(window.addEventListener).toHaveBeenCalledWith("scroll",expect.any(Function));
        expect(window.removeEventListener).not.toHaveBeenCalledWith("scroll",expect.any(Function));
        component.unmount();
        expect(window.removeEventListener).toHaveBeenCalledWith("scroll",expect.any(Function));
    });
    test('Scroll callback and render updates', () => {
        const origGenerateReplaceNOW = RoutingAssistant.generateReplaceNOW;
        RoutingAssistant.generateReplaceNOW = jest.fn();
        const component = getInfiniteScroller({
            enzyme: {
                fullDOM: true
            }
        });
        const instance = component.find(InfiniteScrollSearchResultRendererUnRouted).at(0).instance();
        expect(component.find(WhateverRenderer)).toHaveLength(10);
        instance.bottomScrollElement = {
            current: {
                getBoundingClientRect: () => { return { bottom: 1000 }; }
            }
        };
        instance.onScrollEvent();
        expect(RoutingAssistant.generateReplaceNOW).toHaveBeenCalledWith("/sok-test","fake-search",20,undefined);

        RoutingAssistant.generateReplaceNOW.mockClear();
        instance.bottomScrollElement = {
            current: {
                getBoundingClientRect: () => { return { bottom: 1600 }; }
            }
        };
        instance.onScrollEvent();
        expect(RoutingAssistant.generateReplaceNOW).not.toHaveBeenCalled();

        instance.bottomScrollElement = { current: undefined };
        expect(() => {
            instance.onScrollEvent();
        }).not.toThrow();
        RoutingAssistant.generateReplaceNOW = origGenerateReplaceNOW;
    });
    // Reset console.error
    console.error = err;
    window.addEventListener = AEL;
    window.removeEventListener = REL;
});
/* eslint-enable no-console */

describe('SearchCounter', () => {
    const fakeResults = [];
    for(let no = 0; no < 10; no++)
    {
        fakeResults.push({ ref: 'dictionary/5' });
        fakeResults.push({ ref: 'handbook/5' });
        fakeResults.push({ ref: 'externalResources/5' });
        fakeResults.push({ ref: 'labValue/5' });
        fakeResults.push({ ref: 'tool/1' });
        fakeResults.push({ ref: 'external/0' });
    }
    test('Rendering', () =>
    {
        checkSnapshot(<SearchCounter results={fakeResults} />,{
            router: true,
            route: '/sok/fake-results',
            previousPath: '/'
        });
    });
    test('Invalid entries', () => {
        expect( silentFunc(() => {
            getComponentWith(<SearchCounterUnconnected results={[{ref: "non-existing/20" } ]}/>);
        })).toThrow(/unknown result/);
    });
    test('All active', () => {
        const comp = getComponentWith(<SearchCounter results={fakeResults} />,{
            router: true,
            route: '/sok/fake-results',
            enzyme: true,
            previousPath: '/'
        });
        expect(comp.find('.filtered')).toHaveLength(0);
        // "All active" doesn't require a pill-active, since it's the default state.
        expect(comp.find('.pill-active')).toHaveLength(0);
    });
    test('One active', () => {
        const comp = getComponentWith(<SearchCounter results={fakeResults} filter="verktoy" />,{
            router: true,
            route: '/sok/fake-results',
            enzyme: true,
            previousPath: '/'
        });
        expect(comp.find('.filtered')).toHaveLength(1);
        expect(comp.find('.pill-active')).toHaveLength(1);
    });
    test('Clicking a redirect entry', () => {
        const fakeHistory = {
            push: jest.fn()
        };
        const comp = getComponentWith(<SearchCounterUnconnected search="fake-results" results={fakeResults} history={fakeHistory} />,{
            router: true,
            route: '/sok/fake-results',
            enzyme: true,
            previousPath: '/'
        });
        comp.findWhere( (ent) => ent.key() === "dictionary" ).at(0).simulate('click');
        expect(fakeHistory.push).toHaveBeenCalledWith("/ordliste/fake-results");
    });
    test('Clicking a filter entry', () => {
        const origGeneratePush = RoutingAssistant.generatePush;
        RoutingAssistant.generatePush = jest.fn();

        const fakeHistory = {
            push: jest.fn()
        };
        const comp = getComponentWith(<SearchCounterUnconnected search="fake-results" results={fakeResults} history={fakeHistory} />,{
            router: true,
            route: '/sok/fake-results',
            enzyme: true,
            previousPath: '/'
        });
        comp.findWhere( (ent) => ent.key() === "labValue" ).at(0).simulate('click');
        expect(fakeHistory.push).not.toHaveBeenCalled();
        expect(RoutingAssistant.generatePush).toHaveBeenCalledWith("/sok","fake-results","auto","labverdier");

        RoutingAssistant.generatePush = origGeneratePush;
    });
    test('Clicking a filter entry when already filtered', () => {
        const origGeneratePush = RoutingAssistant.generatePush;
        RoutingAssistant.generatePush = jest.fn();

        const fakeHistory = {
            push: jest.fn()
        };
        const comp = getComponentWith(<SearchCounterUnconnected filter="labverdier" search="fake-results" results={fakeResults} history={fakeHistory} />,{
            router: true,
            route: '/sok/fake-results',
            enzyme: true,
            previousPath: '/'
        });
        comp.findWhere( (ent) => ent.key() === "labValue" ).at(0).simulate('click');
        expect(fakeHistory.push).not.toHaveBeenCalled();
        expect(RoutingAssistant.generatePush).toHaveBeenCalledWith("/sok","fake-results","auto");

        RoutingAssistant.generatePush = origGeneratePush;
    });
});

test('GlobalSearchRedirect component', () =>
{
    const {component} = getComponentWith(<GlobalSearchRedirect search="test" />,compSettings);
    expect(component.find(Link).find('a').prop('href')).toBe('/sok/test');
});

describe('TopSearch component', () => {
    test('Basic functionality', () => {
        const {store, component} = getComponentWith(<TopSearch search="" />,compSettings);

        expect(component.find("input").at(0).props().value).toBe("");

        changeFieldValue(component.find("input").at(0),"Test");

        expect(component.find("input").at(0).props().value).toBe("Test");

        expect( store.getState().globalSearch.search).toBe("Test");
    });
    test("Redirect to / when empty", () => {
        const mockedHistory = mockedRouterHistoryObj();
        window.location.pathname = "/sok/Test";
        RoutingAssistant.history = mockedHistory;
        const component = getComponentWith(<TopSearchUnconnected search="Test" onSearchUpdate={ jest.fn() } />,{
            enzyme: true
        });
        component.instance().onChanged("");
        expect(RoutingAssistant.history.replace).toHaveBeenCalledWith("/");
    });
});
test('TopSearchUnconnected component', () => {
    window.location.href = "/";
    window.location.pathname = "/";
    const onSearchUpdate = jest.fn();
    const onPageUpdate = jest.fn();

    /* eslint-disable-next-line no-unused-vars */
    const {store, component} = getComponentWith(<TopSearchUnconnected search="" onPageUpdate={onPageUpdate} onSearchUpdate={onSearchUpdate} />,compSettings);

    expect(onSearchUpdate).not.toHaveBeenCalled();
    expect(component.find("input").at(0).props().value).toBe("");

    // Check that onSearchUpdate and hist.push gets called when path is not /sok
    // and we change the value
    changeFieldValue(component.find("input").at(0),"Test 2");
    expect(onSearchUpdate).toHaveBeenCalledTimes(1);
    expect(RoutingAssistant.history.location.pathname).toBe("/sok");

    // But when path IS /sok, only onSearchUpdate should be called
    window.location.href = "/sok";
    window.location.pathname = "/sok";
    changeFieldValue(component.find("input").at(0),"Test 3");
    expect(onSearchUpdate).toHaveBeenCalledTimes(2);
    //    expect(hist.push).toHaveBeenCalledTimes(1);
});

test('Routing callbacks', () => {
    const {store, component} = getComponentWith(<GlobalSearch />,compSettings);
    store.dispatch(setGlobalSearchString("test"));
    expect( store.getState().globalSearch.search).toBe("test");
    component.find(RequiresPleiarSearcher).at(0).instance().props.onRouteSync("hello");
    expect( store.getState().globalSearch.search).toBe("hello");
});

describe('RequiresPleiarSearcher', () => {
    const dummyComponent =  () => {
        return <div id="conditionalRenderSuccess">FINAL_RENDERED</div>;
    };
    const origInit = PleiarSearcher.initialize;
    let doResolve;
    const initMock = jest.fn(() => {
        return new Promise( (resolve, ) => {
            doResolve = resolve;
        });
    });
    test('Should just render since PleiarSearcher is initialized', () => {
        const comp = getComponentWith(<RequiresPleiarSearcher app="test" component={dummyComponent} />, { enzyme: true });
        expect(comp.find('#conditionalRenderSuccess')).toHaveLength(1);
    });
    test('Should initialize if PleiarSearcher is not initialized', () => {return new Promise((done) => {
        PleiarSearcher.__index = null;
        PleiarSearcher._ALR_initializing = null;
        PleiarSearcher._ALR_onInitialize = [];
        PleiarSearcher._ALR_hasInitialized = false;
        PleiarSearcher.initialize = initMock;
        expect(initMock).not.toHaveBeenCalled();
        const comp = getComponentWith(<RequiresPleiarSearcher app="test" component={dummyComponent} />, { enzyme: true });
        expect(initMock).toHaveBeenCalledTimes(1);
        doResolve();
        setTimeout( () => {
            comp.update();
            expect(comp.find('#conditionalRenderSuccess')).toHaveLength(1);
            done();
        },1);
    });});

    PleiarSearcher.initialize = origInit;
});
