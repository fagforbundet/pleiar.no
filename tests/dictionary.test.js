/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('reactstrap');
import React from 'react';
import Dictionary, { DictionaryEntry } from '../src/react-components/dictionary';
import { ReadMoreEntry } from '../src/react-components/dictionary';
import data from '../data/datasett/dictionary.json';
import { setDictSearchString } from '../src/actions/dictionary';
import { prepareEnv, checkSnapshot, getRealRedux, changeFieldValue, getComponentWith } from './test-helpers';
import { RequiresPleiarSearcher } from '../src/react-components/global-search.js';
import Enzyme from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import RoutingAssistant from '../src/routing';
import { testInitPleiarSearcher } from './search-helper';
import device from '../src/helper.device';
import { Input } from 'reactstrap';
import auth from '../src/auth';
import { InlineAuthBlock } from '../src/react-components/auth';

Enzyme.configure({ adapter: new Adapter() });

jest.useFakeTimers();
RoutingAssistant._debounceTimeout = 1;
prepareEnv();
testInitPleiarSearcher();

window.console.log = jest.fn();

const compSettings = {
    redux: true,
    includeReduxStore: true,
    router: true,
    route: "/ordliste",
    enzyme: true,
    previousPath: "/",
    routingAssistant: true
};
const getDictionaryWithRedux = () =>
{
    return getComponentWith(Dictionary,compSettings);
};

test('Basic rendering of the dictionary page', () => {
    checkSnapshot(Dictionary,compSettings);
    expect(global.__title).toMatch('Ordliste');
});

test("redux store", () => {
    const store = getRealRedux();
    // Default state
    expect( store.getState().dictionary).toMatchObject({
        search: "",
    });

    store.dispatch(setDictSearchString("test"));
    expect( store.getState().dictionary.search).toBe("test");
});

test('UI interaction', () => {
    const {store,component} = getDictionaryWithRedux();
    // Default should be an empty search
    expect(store.getState().dictionary.search).toBe('');

    // And the first item should match the first data source
    if(data[0].seeHandbook)
    {
        expect(component.find('.dictionaryEntry').at(0).find('a').at(1).props().href).toBe('/handbok'+data[0].seeHandbook);
        expect(component.find('.dictionaryEntry').at(0).find('a').at(2).props().href).toBe(data[0].moreInfoURLs[0]);
    }
    else
    {
        expect(component.find('.dictionaryEntry').at(0).find('a').at(1).props().href).toBe(data[0].moreInfoURLs[0]);
    }
    expect(component.find('.dictionaryEntry')).toHaveLength( data.length*2 );

    // Try changing the search value
    changeFieldValue(component.find('[name="search"]').at(0),'Afasi');
    expect(store.getState().dictionary.search).toBe('Afasi');
    expect(component.find('[name="search"]').at(0).props().value).toBe('Afasi');
    expect(component.find('.dictionaryEntry')).toHaveLength(4);

    // Try removing the search value
    component.find('.clearSearchField').at(0).simulate('click');
    expect(store.getState().dictionary.search).toBe('');
    expect(component.find('[name="search"]').at(0).props().value).toBe('');
    expect(component.find('.dictionaryEntry')).toHaveLength(data.length*2);

    // Try searching for a string that has no hits
    changeFieldValue(component.find('[name="search"]').at(0),'thishasnoresults');
    expect(component.find('.dictionaryEntry')).toHaveLength(0);
    expect(component.find('#extNoResults').at(0).text()).toMatch(/ingen treff/);
    expect(component.find('#extNoResults img').at(0).prop('src')).toMatch(/beaver/);
    expect(RoutingAssistant.history.action).toBe('PUSH');

    // Try removing the search value by emptying the field
    changeFieldValue(component.find('[name="search"]').at(0),'');
    expect(store.getState().dictionary.search).toBe('');
    expect(component.find('[name="search"]').at(0).props().value).toBe('');
    expect(component.find('.dictionaryEntry')).toHaveLength(data.length*2);
    jest.runAllTimers();
    expect(RoutingAssistant.history.action).toBe('REPLACE');
});

describe('Field focus', () => {
    test('Should focus on desktop', () => {
        device.isTouchScreen = jest.fn( () => { return false; });
        const {component} = getDictionaryWithRedux();
        expect(component.find(Input).props().autoFocus).toBe(true);
    });
    test('Should not focus on touch screens', () => {
        device.isTouchScreen = jest.fn( () => { return true; });
        const {component} = getDictionaryWithRedux();
        expect(component.find(Input).props().autoFocus).toBe(false);
    });
});

describe('Not logged in', () => {
    auth.isAuthenticated = jest.fn( () => false);
    test('Default page', () => {
        const {component} = getDictionaryWithRedux();
        expect( component.find(InlineAuthBlock) ).toHaveLength(2);
    });
    test('Search page', () => {
        const {store,component} = getDictionaryWithRedux();
        store.dispatch(setDictSearchString(" "));
        expect( component.find(InlineAuthBlock) ).toHaveLength(7);
        expect( component.find(InlineAuthBlock).at(0).text() ).toMatch(/til søk i/);
    });
});

test('Routing callbacks', () => {
    const {store,component} = getDictionaryWithRedux();
    store.dispatch(setDictSearchString("test"));
    expect( store.getState().dictionary.search).toBe("test");
    component.find(RequiresPleiarSearcher).at(0).instance().props.onRouteSync("hello",2);
    expect( store.getState().dictionary.search).toBe("hello");
});

test('ReadMoreEntry element', () => {
    const testElement = (href,expectedMatch) => {
        const component = Enzyme.mount(
                <ReadMoreEntry entry={href}  />
        );
        if(expectedMatch instanceof RegExp)
        {
            expect(component.find('a').at(0).text()).toMatch( expectedMatch );
        }
        else
        {
            expect(component.find('a').at(0).text()).toBe( expectedMatch );
        }
    };

    testElement('https://sml.snl.no/bakterier',"SML");
    testElement('https://snl.no/bakterier',"SNL");
    testElement('https://analyseoversikten.no/',"Analyseoversikten");
    testElement('https://ehelse.no/',"Direktoratet for e-helse");
    testElement('https://helsenorge.no/',"helsenorge.no");
    testElement("https://helsedirektoratet.no/","Helsedirektoratet");
    testElement("https://fhi.no/","Folkehelseinstituttet");
    testElement("https://kreftlex.no/","Kreftlex");
    testElement("https://oncolex.no/","Oncolex");
    testElement("https://example.com/","???");
});

describe('DictionaryEntry', () => {
    test('Not authenticated, default text', () => {
        auth.isAuthenticated = jest.fn( () => false);
        const { component } = getComponentWith(<DictionaryEntry entry={data[0]} />, compSettings);
        expect( component.find(InlineAuthBlock) ).toHaveLength(1);
        expect( component.find(InlineAuthBlock).text() ).toMatch(/til oppføringer i/);
    });
    test('Not authenticated, whitelist text', () => {
        auth.isAuthenticated = jest.fn( () => false);
        const { component } = getComponentWith(<DictionaryEntry entry={data[0]} whitelistedAuthRequiredEntries="5" />, compSettings);
        expect( component.find(InlineAuthBlock) ).toHaveLength(1);
        expect( component.find(InlineAuthBlock).text() ).toMatch(/til flere oppføringer i/);
    });
    test('Not authenticated, search', () => {
        auth.isAuthenticated = jest.fn( () => false);
        const { component } = getComponentWith(<DictionaryEntry entry={data[0]} whitelistedAuthRequiredEntries={-1} />, compSettings);
        expect( component.find(InlineAuthBlock) ).toHaveLength(1);
        expect( component.find(InlineAuthBlock).text() ).toMatch(/til søk i/);
    });
    test('Not authenticated, forced', () => {
        auth.isAuthenticated = jest.fn( () => false);
        const { component } = getComponentWith(<DictionaryEntry entry={data[0]} forceAuth={true} />, compSettings);
        expect( component.find(InlineAuthBlock) ).toHaveLength(0);
    });
    test('rawEntry=true (not authenticated)', () => {
        auth.isAuthenticated = jest.fn( () => false);
        const { component } = getComponentWith(<DictionaryEntry entry={data[0]} rawEntry={true} />, compSettings);
        expect( component.find(InlineAuthBlock) ).toHaveLength(0);
    });
    test('Authenticated', () => {
        auth.isAuthenticated = jest.fn( () => true);
        const { component } = getComponentWith(<DictionaryEntry entry={data[0]} />, compSettings);
        expect( component.find(InlineAuthBlock) ).toHaveLength(0);
    });
});
