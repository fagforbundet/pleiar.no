/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { MainContainer, PageTitle } from '../src/react-components/layout.js';
import React from 'react';
import { getComponentWith, checkSnapshot, prepareEnv } from './test-helpers';

prepareEnv();


jest.unmock('../src/react-components/layout.js');

describe('MainContainer', () =>
{
    test('Base', () => {
        checkSnapshot(
            <MainContainer app="test">
                Test
            </MainContainer>,
            {
                router: true,
                route: '/test',
                previousPath:'/'
            }
        );
        getComponentWith(
            <MainContainer app="test">
                Test
            </MainContainer>,
            {
                router: true,
                route: '/test',
                previousPath:'/'
            }
        ).unmount();
    });
    test('Small', () => {
        checkSnapshot(
            <MainContainer app="test" small>
                Test
            </MainContainer>,
            {
                router: true,
                route: '/test',
                previousPath:'/'
            }
        );
        getComponentWith(
            <MainContainer app="test" small>
                Test
            </MainContainer>,
            {
                router: true,
                route: '/test',
                previousPath:'/'
            }
        ).unmount();
    });
});
test('PageTitle', () =>
{
    PageTitle({title: 'Test'});
    expect(document.title).toBe('Pleiar.no - Test');
    getComponentWith(<PageTitle title="Test2" />);
    expect(document.title).toBe('Pleiar.no - Test2');
    getComponentWith(<PageTitle />);
    expect(document.title).toBe('Pleiar.no');
});
