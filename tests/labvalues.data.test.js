/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import fs from 'fs';
import yaml from 'js-yaml';
import { LabValueEntryCleartext, LabValueMaterialMap } from '../src/react-components/labvalues';
import { prepareEnv, getComponentWith } from './test-helpers';
import { testInitPleiarSearcher } from './search-helper.js';

jest.unmock('../data/datasett/lab.json');

import jsonData from '../data/datasett/lab.json';

function loadYAMLDataset (...list)
{
    for(const file of list)
    {
        if(fs.existsSync('./data/datasett/'+file))
        {
            return yaml.safeLoad(fs.readFileSync('./data/datasett/'+file).toString());
        }
    }
    throw('None of the data files requested exists in ./data/datasett: '+list.join(', '));
}

const yamlData = loadYAMLDataset('Labverdier.yml','lab.yml');

prepareEnv();

test('Lab data syntax', () =>
{
    for(const category in yamlData)
    {
        expect(category).toMatch(/\S+/);
        expect(yamlData[category]).toBeInstanceOf(Array);
        const dupeCheckList = {};

        for(const entry of yamlData[category])
        {
            expect(entry).toMatchObject({
                // Must be non-empty string
                navn: expect.stringMatching(/\S+/),
                // Must be one of these strings
                materiale: expect.stringMatching(/^(B|E|SP|P|U|S|PT)$/),
                // Must include at least one digit
                // (or, possibly, be a reference to another entry with the text "Se …")
                ref: expect.stringMatching(/(Se\s+|\d|Mangler)/)
            });
            expect(LabValueMaterialMap[ entry.materiale ]).not.toBeFalsy();
            // merknad is optional, but must be non-zero-length string if present
            if(entry.merknad !== undefined)
            {
                expect(entry).toMatchObject({
                    merknad: expect.stringMatching(/\S/)
                });
            }
            // keywords is optional, but must be non-zero-length string if present
            if(entry.keywords !== undefined)
            {
                expect(entry).toMatchObject({
                    keywords: expect.stringMatching(/\S/)
                });
            }
            const key = entry.navn+'||'+entry.ref;
            expect(dupeCheckList[key]).toBe(undefined);
            dupeCheckList[key] = true;
        }
    }
});

test('Lab data compiled syntax', () => {
    for(const entry of jsonData.values)
    {
        expect(entry).toMatchObject({
            // Must be non-empty string
            navn: expect.stringMatching(/\S+/),
            // Must be one of these strings
            materiale: expect.stringMatching(/^(B|E|SP|P|U|S|PT|D|M|K)$/),
            // Must include at least one digit
            // (or, possibly, be a reference to another entry with the text "Se …")
            ref: expect.stringMatching(/(Se\s+|\d|Mangler)/),
            // In the compiled version, a category string must be present
            category: expect.stringMatching(/\S/),
        });
        // merknad is optional, but must be non-zero-length string if present
        if(entry.merknad !== undefined)
        {
            expect(entry).toMatchObject({
                merknad: expect.stringMatching(/\S/)
            });
        }
    }
});

test.each(jsonData.values)('Lab data rendering', (data) =>
{
    expect( () =>
    {
        getComponentWith(<LabValueEntryCleartext entry={data} />,{
            enzyme: true,
        });
    }).not.toThrow();
});

test('Lab data category index', () => {
    for(const category in yamlData.index)
    {
        for(const entry of yamlData.index[category])
        {
            expect(yamlData.values[entry]).toMatchObject({
                navn: expect.stringMatching(/\S+/)
            });
        }
    }
});

test('Lab data search indexing', () =>
{
    expect( () =>
    {
        testInitPleiarSearcher();
    }).not.toThrow();
});
