/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import { MedicationSynonyms, MedicationSynonymsResult, MedicationSynonymsSearchBar, MedicationSynonymsSearchBarUnconnected, MedicationSynonymsResultLoader, MedicationSynonymsResultUnconnected, MedicationSynonymsRenderSingleResult, ATCCode , ATCCodeUnconnected, MedicationSynonymsResultATC, MedicationSynonymsResultText} from '../src/react-components/med-synonyms';
import MedSearcher from '../src/med-synonym-searcher';
import { checkSnapshot, getRealRedux, getComponentWith, getFakeMedSearcherData, changeFieldValue } from './test-helpers';
import { Col, Input } from 'reactstrap';
import { setMedSynonymsSearchString } from '../src/actions/med-synonyms';
import { RequiresAuth } from '../src/react-components/auth';

beforeEach( () => {
    MedSearcher._loadedData( getFakeMedSearcherData() );
    MedSearcher._ALR_hasInitialized = true;
});

const baseCompSettings = {
    redux: true,
    router: true,
    route: '/verktoy/medikament/synonymer',
    previousPath: '/verktoy',
};
const enzymeCompSettings = {
    ...baseCompSettings,
    enzyme: true
};
const enzymeCompSettingsGetRedux = {
    ...enzymeCompSettings,
    includeReduxStore: true,
};

test('Redux store', () => {
    const store = getRealRedux();
    // Default state
    expect( store.getState().medSynonyms).toMatchObject({
        search: "",
    });

    store.dispatch(setMedSynonymsSearchString("test"));
    expect( store.getState().medSynonyms.search).toBe("test");
});

describe('MedicationSynonyms', () => {
    test('Base snapshot', () => {
        checkSnapshot(<MedicationSynonyms />, baseCompSettings);
    });
    test('Auth check', () => {
        const component = getComponentWith(<MedicationSynonyms />, enzymeCompSettings);
        expect(component.find(RequiresAuth)).toHaveLength(0);
    });
});
describe('MedicationSynonymsResultUnconnected', () => {
    test('Base snapshot: Azurill', () => {
        checkSnapshot(<MedicationSynonymsResultUnconnected search="Azurill" />, baseCompSettings);
    });
    test('Azurill', () => {
        const component = getComponentWith(<MedicationSynonymsResultUnconnected search="Azurill" />, enzymeCompSettings);
        // Should render a drug list, not an ATC list
        expect(component.find(MedicationSynonymsResultText)).toHaveLength(1);
        expect(component.find(MedicationSynonymsResultATC)).toHaveLength(0);
        // Should render three results
        expect(component.find(MedicationSynonymsRenderSingleResult)).toHaveLength(3);
        // Should have two sets of synonyms
        expect(component.find('.med-syn-header')).toHaveLength(2);
        // Should have a header for the non-synonym component
        expect(component.find('h5')).toHaveLength(1);
        // Should have a non-synonym med matching this
        expect(component.find('b').last().text()).toBe('Azurill 40 mg');
    });
    test('Vulpix', () => {
        const component = getComponentWith(<MedicationSynonymsResultUnconnected search="Vulpix" />, enzymeCompSettings);
        // Should render a drug list, not an ATC list
        expect(component.find(MedicationSynonymsResultText)).toHaveLength(1);
        expect(component.find(MedicationSynonymsResultATC)).toHaveLength(0);
        // Should render one result
        expect(component.find(MedicationSynonymsRenderSingleResult)).toHaveLength(1);
        // Should have no synonyms
        expect(component.find('.med-syn-header')).toHaveLength(0);
        // Should not have a non-synonym header
        expect(component.find('h5')).toHaveLength(0);
        // Should have a non-synonym med matching this
        expect(component.find('b').last().text()).toBe('Vulpix 200 mg');
        expect(component.find(Col).last().text()).toMatch(/har ingen kjente synonympreparat/);
    });
    test('Not found', () => {
        const component = getComponentWith(<MedicationSynonymsResultUnconnected search="not-found" />, enzymeCompSettings);
        // Should render zero results
        expect(component.find(MedicationSynonymsRenderSingleResult)).toHaveLength(0);
        // Should have no synonyms
        expect(component.find('.med-syn-header')).toHaveLength(0);
        // Should not have a non-synonym header
        expect(component.find('h5')).toHaveLength(0);
        // Should output not found info
        expect(component.find('div').last().text()).toMatch(/Ingen treff/);
    });
    test('Too short', () => {
        const component = getComponentWith(<MedicationSynonymsResultUnconnected search="n" />, enzymeCompSettings);
        // Should render zero results
        expect(component.find(MedicationSynonymsRenderSingleResult)).toHaveLength(0);
        // Should have no synonyms
        expect(component.find('.med-syn-header')).toHaveLength(0);
        // Should not have a non-synonym header
        expect(component.find('h5')).toHaveLength(0);
        // Should output not found info
        expect(component.find('div').last().text()).toMatch(/Skriv minst to tegn/);
    });
    test('No search string', () => {
        const component = getComponentWith(<MedicationSynonymsResultUnconnected search="" />, enzymeCompSettings);
        // Should render zero results
        expect(component.find(MedicationSynonymsRenderSingleResult)).toHaveLength(0);
        // Should give a single empty div
        expect(component.find('div')).toHaveLength(1);
    });
    test('By ATC code', () => {
        const component = getComponentWith(<MedicationSynonymsResultUnconnected search="N02AA01" />, enzymeCompSettings);
        // Should render an ATC list, not a drug list
        expect(component.find(MedicationSynonymsResultATC)).toHaveLength(1);
        expect(component.find(MedicationSynonymsResultText)).toHaveLength(0);
    });
});
describe('MedicationSynonymsResult', () => {
    test('Connection', () => {
         const { component, store } = getComponentWith(<MedicationSynonymsResult />, enzymeCompSettingsGetRedux);
        expect(component.find(MedicationSynonymsRenderSingleResult)).toHaveLength(0);
        expect(component.find('div')).toHaveLength(1);
        expect(component.find('div').text()).toHaveLength(0);
        store.dispatch(setMedSynonymsSearchString('Vulpix'));
        // Should have updated to render one result
        expect(component.find(MedicationSynonymsRenderSingleResult)).toHaveLength(1);
    });
});

describe('MedicationSynonymsResultATC', () => {
    test('Base snapshot', () => {
        checkSnapshot(<MedicationSynonymsResultATC search="test" />, baseCompSettings);
    });
    test('Snapshot with invalid/incomplete ATC', () => {
        checkSnapshot(<MedicationSynonymsResultATC search="atcThatDoesNotExist" />, baseCompSettings);
    });
    test('Snapshot with a notice', () => {
        checkSnapshot(<MedicationSynonymsResultATC search="PMN400" />, baseCompSettings);
    });
});

describe('MedicationSynonymsSearchBarUnconnected', () => {
    test('Base snapshot', () => {
        checkSnapshot(<MedicationSynonymsSearchBarUnconnected />, baseCompSettings);
    });
    test('With string snapshot', () => {
        checkSnapshot(<MedicationSynonymsSearchBarUnconnected search="test" />, baseCompSettings);
    });
    test('Without content', () => {
        const component = getComponentWith(<MedicationSynonymsSearchBarUnconnected onSearchUpdate={jest.fn()} />, enzymeCompSettings);
        expect(component.find(Input).prop('value')).toBe(undefined);
    });
    test('With content', () => {
        const component = getComponentWith(<MedicationSynonymsSearchBarUnconnected onSearchUpdate={jest.fn()} search="n" />, enzymeCompSettings);
        expect(component.find(Input).prop('value')).toBe('n');
    });
    test('onSearchUpdate', () => {
        const onSearchUpdate = jest.fn();
        const component = getComponentWith(<MedicationSynonymsSearchBarUnconnected onSearchUpdate={onSearchUpdate} search="n" />, enzymeCompSettings);
        changeFieldValue(component.find(Input).at(0),"Test");
        expect(onSearchUpdate).toHaveBeenCalled();
        expect(onSearchUpdate.mock.calls[0][0]).toMatchObject({
            target: {
                value: "Test"
            }
        });
    });
});
describe('MedicationSynonymsSearchBar', () => {
    test('Without content', () => {
        const { component } = getComponentWith(<MedicationSynonymsSearchBar />, enzymeCompSettingsGetRedux);
        expect(component.find(Input).prop('value')).toBe("");
    });
    test('With content', () => {
        const { component, store } = getComponentWith(<MedicationSynonymsSearchBar />, enzymeCompSettingsGetRedux);
        expect(component.find(Input).prop('value')).toBe("");
        store.dispatch(setMedSynonymsSearchString('Vulpix'));
        expect(component.find(Input).prop('value')).toBe('Vulpix');
    });
    test('onSearchUpdate', () => {
        const { component, store } = getComponentWith(<MedicationSynonymsSearchBar />, enzymeCompSettingsGetRedux);
        expect( store.getState().medSynonyms).toMatchObject({
            search: "",
        });
        changeFieldValue(component.find(Input).at(0),"Test");
        expect( store.getState().medSynonyms).toMatchObject({
            search: "Test",
        });
    });
});
describe('MedicationSynonymsRenderSingleResult', () => {
    test('Result without synonyms', () => {
        const result = {
            synonyms: [],
            atc: "X12123",
            name: 'Test'
        };
        const component = getComponentWith(<MedicationSynonymsRenderSingleResult result={result} />, enzymeCompSettings);
        expect(component.find(Col).text()).toMatch(/Test \(ATC X12 123\) har ingen kjente synonympreparat/);
        expect(component.find(".med-syn-header")).toHaveLength(0);
        expect(component.find('li')).toHaveLength(0);
        expect(component.find('.merknad')).toHaveLength(0);
        expect(component.find('ul')).toHaveLength(0);
    });
    test('Result with a single synonym', () => {
        const result = {
            synonyms: [
                'Syn 1',
            ],
            atc: 'ATC1234',
            name: 'Test'
        };
        const component = getComponentWith(<MedicationSynonymsRenderSingleResult result={result} />, enzymeCompSettings);
        expect(component.find(".med-syn-header").text()).toMatch("Test (ATC ATC 1234)");
        expect(component.find('li')).toHaveLength(1);
        expect(component.find('.merknad')).toHaveLength(0);
        expect(component.find('ul').text()).toMatch(/Syn 1/);
        expect(component.text()).toMatch(/Synonym:/);
    });
    test('Result with synonyms', () => {
        const result = {
            synonyms: [
                'Syn 1',
                'Syn 2'
            ],
            atc: 'ATC1234',
            name: 'Test'
        };
        const component = getComponentWith(<MedicationSynonymsRenderSingleResult result={result} />, enzymeCompSettings);
        expect(component.find(".med-syn-header").text()).toMatch("Test (ATC ATC 1234)");
        expect(component.find('li')).toHaveLength(2);
        expect(component.find('.merknad')).toHaveLength(0);
        expect(component.find('ul').text()).toMatch(/Syn 1.*Syn 2/);
        expect(component.text()).toMatch(/Synonymer:/);
    });
    test('Result with synonyms and merknad', () => {
        const result = {
            synonyms: [
                'Syn 1',
                'Syn 2'
            ],
            atc: 'ATC1234',
            name: 'Test',
            notice: 'Test merknad',
        };
        const component = getComponentWith(<MedicationSynonymsRenderSingleResult result={result} />, enzymeCompSettings);
        expect(component.find(".med-syn-header").text()).toMatch("Test (ATC ATC 1234)");
        expect(component.find('li')).toHaveLength(2);
        expect(component.find('.merknad')).toHaveLength(1);
        expect(component.find('.merknad').text()).toMatch(/MERK:.*Test merknad/);
        expect(component.find('ul').text()).toMatch(/Syn 1.*Syn 2/);
        expect(component.text()).toMatch(/Synonymer/);
    });
});
/**
 * This *tests* MedicationSynonymsResultLoader, but we use
 * MedicationSynonymsResult in most of these tests because
 * MedicationSynonymsResult is really just MedicationSynonymsResultLoader
 * connected to redux
 */
describe('MedicationSynonymsResultLoader', () => {
    let doResolve;
    const initMock = jest.fn(() => {
        return new Promise( (resolve, ) => {
            doResolve = resolve;
        });
    });
    test('Should just render since MedSearcher is initialized', () => {
         const { component, store } = getComponentWith(<MedicationSynonymsResult />, enzymeCompSettingsGetRedux);
        store.dispatch(setMedSynonymsSearchString('Vulpix'));
        expect(component.find(MedicationSynonymsRenderSingleResult)).toHaveLength(1);
    });
    test('Should initialize if MedSearcher is not initialized', () => {return new Promise((done) => {
        initMock.mockClear();
        MedSearcher.__index = null;
        MedSearcher._ALR_initializing = null;
        MedSearcher._ALR_onInitialize = [];
        MedSearcher._ALR_hasInitialized = false;
        MedSearcher.initialize = initMock;
        expect(initMock).not.toHaveBeenCalled();
        const { component, store } = getComponentWith(<MedicationSynonymsResult />, enzymeCompSettingsGetRedux);
        expect(initMock).toHaveBeenCalledTimes(1);
        store.dispatch(setMedSynonymsSearchString('Vulpix'));
        doResolve();
        setTimeout( () => {
            component.update();
            expect(component.find(MedicationSynonymsRenderSingleResult)).toHaveLength(1);
            done();
        },1);
    });});
    test('Loading WITH a search string', () => {
        initMock.mockClear();
        MedSearcher.__index = null;
        MedSearcher._ALR_initializing = null;
        MedSearcher._ALR_onInitialize = [];
        MedSearcher._ALR_hasInitialized = false;
        MedSearcher.initialize = initMock;
        expect(initMock).not.toHaveBeenCalled();
        getComponentWith(<MedicationSynonymsResultLoader search="test" />, enzymeCompSettingsGetRedux);
        expect(initMock).toHaveBeenCalledTimes(1);
    });
});
describe('ATCCode', () => {
    const entries = [{
        code: 'ABC0000',
        out: 'ABC 0000',
    },
    {
        code: 'A271234',
        out: 'A27 1234',
    }];
    test.each(entries)('Valid code: %p',(code) => {
        const component = getComponentWith(<ATCCode atc={code.code} />, { enzyme: true, redux: true });
        expect(component.text()).toBe('ATC '+code.out);
    });
    test('Search callback', () => {
        const cb = jest.fn();
        const component = getComponentWith(<ATCCodeUnconnected atc="N02AA01" onSearchUpdate={cb} />, { enzyme: true });
        component.find('.atc-code').at(0).simulate('click');
        expect(cb).toHaveBeenCalled();
    });
    test('onSearchUpdate', () => {
        const { component, store } = getComponentWith(<ATCCode atc="N02AA01" />, enzymeCompSettingsGetRedux);
        expect( store.getState().medSynonyms).toMatchObject({
            search: "",
        });
        component.find('.atc-code').at(0).simulate('click');
        expect( store.getState().medSynonyms).toMatchObject({
            search: "ATC N02 AA01",
        });
    });
    test('Undefined entry', () => {
        const component = getComponentWith(<ATCCodeUnconnected atc={undefined} onSearchUpdate={jest.fn} />, { enzyme: true });
        expect(component.find('.atc-code')).toHaveLength(0);
    });
    test('Null entry', () => {
        const component = getComponentWith(<ATCCodeUnconnected atc={null} onSearchUpdate={jest.fn} />, { enzyme: true });
        expect(component.find('.atc-code')).toHaveLength(0);
    });
    test('Empty entry', () => {
        const component = getComponentWith(<ATCCodeUnconnected atc="" onSearchUpdate={jest.fn} />, { enzyme: true });
        expect(component.find('.atc-code')).toHaveLength(0);
    });
});
