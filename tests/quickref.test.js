/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import React from 'react';
import { prepareEnv, checkSnapshot, getComponentWith, setConsole } from './test-helpers';

import QuickRef, { QuickRefSearchResult, QuickRefIndexRender } from '../src/react-components/quickref';
import { QuickRefEntry } from '../src/data.quickref';
import { NotFound } from '../src/react-components/shared';

window.console.log = jest.fn();

prepareEnv();

const globalDefaultCompSettings = {
    redux: false,
    includeReduxStore: false,
    router: true,
    route: "/oppslag",
    routingAssistant: false,
    enzyme: true,
    previousPath: "/"
};

const genericSearchData = new QuickRefEntry(['Kommunikasjon']);
const genericSearchSnippet = [
    {
        type: 'normal',
        str: 'normal text',
    },
    {
        type: 'highlight',
        str: 'highlighted text',
    },
    {
        type: 'delimiter',
    },
];
genericSearchData.searchSnippet = genericSearchSnippet;

test('Basic rendering of the quickref index page', () => {
    checkSnapshot(QuickRef,{
        ...globalDefaultCompSettings,
        enzymeSub: QuickRef
    });
    expect(global.__title).toMatch('Hurtigoppslag');
});

test('Basic rendering of a quickref subpage', () => {
    const compSettings = Object.assign({},globalDefaultCompSettings,{
        route: '/oppslag/Kommunikasjon',
        previousPath: "/oppslag/:quickrefPath*",
        enzymeSub: QuickRef
    });
    checkSnapshot(QuickRef,compSettings);
    expect(global.__title).toMatch('Hurtigoppslag');
});

test('Basic rendering of a second quickref subpage', () => {
    const compSettings = Object.assign({},globalDefaultCompSettings,{
        route: '/oppslag/Undersokelse',
        previousPath: "/oppslag/:quickrefPath*",
        enzymeSub: QuickRef,
    });
    checkSnapshot(QuickRef,compSettings);
    expect(global.__title).toMatch('Hurtigoppslag');
});

test('QuickRefSearchResult', () => {
    const compSettings = Object.assign({},globalDefaultCompSettings,{
        enzyme: false
    });
    checkSnapshot(<QuickRefSearchResult entry={genericSearchData} />, compSettings);
});

test('QuickRefSearchResult with invalid content type', () => {
    setConsole("disabled");
    const compSettings = Object.assign({},globalDefaultCompSettings,{
        enzyme: false
    });

    const brokenSearch = new QuickRefEntry(['Kommunikasjon']);
    brokenSearch.searchSnippet = [].concat(genericSearchSnippet);
    brokenSearch.searchSnippet.push({
        type: "does-not-exist"
    });
    expect( () => {
        getComponentWith(<QuickRefSearchResult entry={brokenSearch} />,compSettings);
    }).toThrow("Unknown string type: does-not-exist");

    setConsole("enabled");
});

test('404 handling', () => {
    const compSettings = Object.assign({},globalDefaultCompSettings,{
        route: '/oppslag/non-existant-chapter',
        previousPath: "/oppslag/:quickrefPath*",
        ignore404: true
    });
    let comp = getComponentWith(QuickRef,compSettings);

    expect(comp.find(NotFound)).toHaveLength(1);

    compSettings.route = '/oppslag/Kommunikasjon/non-existant-section';
    comp = getComponentWith(QuickRef,compSettings);

    expect(comp.find(NotFound)).toHaveLength(1);

    compSettings.route = '/oppslag/Kommunikasjon/';
    comp = getComponentWith(QuickRef,compSettings);
    expect(comp.find(NotFound)).toHaveLength(0);
});

test('QuickRefIndexRender', () => {
    checkSnapshot(<QuickRefIndexRender />,globalDefaultCompSettings);
});
