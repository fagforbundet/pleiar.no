/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import featureDetection from '../src/helper.feature-detection';

test('tokenSupports', () => {
    let testObj = {
        supports ()
        {
            return true;
        }
    };

    expect(featureDetection.tokenSupports(testObj,'test')).toBe(true);

    testObj = {
        supports ()
        {
            return false;
        }
    };
    expect(featureDetection.tokenSupports(testObj,'test')).toBe(false);

    testObj = {};
    expect(() => { featureDetection.tokenSupports(testObj,'test'); }).not.toThrow();
    expect(featureDetection.tokenSupports(testObj,'test')).toBe(false);

    testObj = {
        supports ()
        {
            throw('err');
        }
    };
    expect(() => { featureDetection.tokenSupports(testObj,'test'); }).not.toThrow();
    expect(featureDetection.tokenSupports(testObj,'test')).toBe(false);
});

test('aSupportsRel', () => {
    let testObj = {
        supports ()
        {
            return true;
        }
    };
    document.createElement = () => {
        return { relList: testObj };
    };
    expect(featureDetection.aSupportsRel('test')).toBe(true);

    testObj = {
        supports ()
        {
            return false;
        }
    };
    expect(featureDetection.aSupportsRel('test')).toBe(false);
});
