/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

jest.unmock('reactstrap');
import React from 'react';

import InfusionSpeedCalc from '../src/react-components/medcalc-infusion';
import { CalcExplanationLine } from '../src/react-components/calculators-shared';
import { prepareEnv, getRealRedux, changeFieldValue, getComponentWith } from './test-helpers';

import { setContent, setMinutes, setHours, setCalculatorMode, toggleAdditionalCalculationsVisibility } from '../src/actions/medcalc-infusion';
import { toggleRounding, toggleFormulaVisibility } from '../src/actions/global';

const getCalculatorAndReduxWithMode = (mode) =>
{
    const { store, component } = getComponentWith(<InfusionSpeedCalc />,{
        enzyme: true,
        redux: true,
        includeReduxStore: true
    });

    store.dispatch(setCalculatorMode(mode));

    return { store, component };
};

const getResultFromComp = (comp) =>
{
    const infusionElement = comp.find('#medcalc-infusion-result');
    expect( infusionElement ).toHaveLength(1);
    const result = infusionElement.text().replace(/\s/g,'').replace(/,/g,'.');
    return Number.parseFloat(result);
};

prepareEnv();

test('Basic rendering of the ml/hr calculator', () => {
    const { store, component } = getComponentWith(<InfusionSpeedCalc />,{
        enzyme: true,
        redux: true,
        includeReduxStore: true
    });
    store.dispatch(setCalculatorMode("ml/t"));
    expect(component).toMatchSnapshot();
    expect(global.__title).toBe('Medisinrekning - ml/t');
});

test('Basic rendering of the drops calculator', () => {
    const { store, component } = getComponentWith(<InfusionSpeedCalc />,{
        enzyme: true,
        redux: true,
        includeReduxStore: true
    });
    store.dispatch(setCalculatorMode("dråpar/sek"));
    expect(component).toMatchSnapshot();
    expect(global.__title).toBe('Medisinrekning - dråpar/sek');
});

test('ml/hr calculations', () => {
    const {component, store} = getCalculatorAndReduxWithMode("ml/t");
    expect(component.find('.calculator-descriptions small .row')).toHaveLength(3);
    expect( getResultFromComp(component) ).toBe(1000);
    store.dispatch(setMinutes(30));
    expect( getResultFromComp(component) ).toBe(666.7);
    store.dispatch(toggleRounding());
    expect( getResultFromComp(component) ).toBe(666.667);
    store.dispatch(setHours(2));
    expect( getResultFromComp(component) ).toBe(400);
    store.dispatch(setContent(2000));
    expect( getResultFromComp(component) ).toBe(800);
    expect(component.find('.calculator-descriptions small .row')).toHaveLength(5);
    // Enable formulas
    store.dispatch(toggleFormulaVisibility());
    expect( component.find('.calculator-descriptions small .row').last().find('.formula').last().text() ).toMatch(/totalvolum.*tid.*ml\/t/);
});

// ml/hr DOES NOT have "additional calculations", so toggling should do nothing
test("ml/hr should not have additional calculations", () => {
    const {component, store} = getCalculatorAndReduxWithMode("ml/t");
    expect(component.find('.calculator-descriptions small .row')).toHaveLength(3);
    store.dispatch(toggleAdditionalCalculationsVisibility());
    expect(component.find('.calculator-descriptions small .row')).toHaveLength(3);
    component.unmount();
});

test('drops calculations', () => {
    const {component,store} = getCalculatorAndReduxWithMode("dråpar/sek");
    // The number of descriptions should be 6 at this point
    expect(component.find('.calculator-descriptions small .row')).toHaveLength(7);
    // The drops should be 6
    expect( getResultFromComp(component) ).toBe(6);

    // Set the minutes to 30
    store.dispatch(setMinutes(30));
    // The drops should have changed
    expect( getResultFromComp(component) ).toBe(4);

    // Same for hours and the ml to infuse
    store.dispatch(setHours(2));
    expect( getResultFromComp(component) ).toBe(2);
    store.dispatch(setContent(2000));
    expect( getResultFromComp(component) ).toBe(4);

    // Drops has "additional calculations", show them
    store.dispatch(toggleAdditionalCalculationsVisibility());
    expect(component.find('.calculator-descriptions small .row')).toHaveLength(11);

    // Disable rounding
    store.dispatch(toggleRounding());
    expect( getResultFromComp(component) ).toBe(4.444);

    // Disable additional calculations again for the next test
    store.dispatch(toggleAdditionalCalculationsVisibility());

    // Enable formulas
    store.dispatch(toggleFormulaVisibility());
    expect( component.find('.calculator-descriptions small .row').last().find('.formula').last().text() ).toMatch(/dråpar.*min.*60.*dråpar\/sek/);
});

test("redux store", () => {
    const store = getRealRedux();
    // Default state
    expect( store.getState().medCalc.infusion ).toMatchObject({
        hours: 1,
        minutes: 0,
        content: 1000,
        mode: "ml/t",
        additionalCalculations: false
    });
    // Set the minutes to 30
    store.dispatch(setMinutes(30));
    // Update should have hit the store
    expect( store.getState().medCalc.infusion.minutes ).toBe(30);

    // Same for hours and the ml to infuse
    store.dispatch(setHours(2));
    expect( store.getState().medCalc.infusion.hours ).toBe(2);
    store.dispatch(setContent(2000));
    expect( store.getState().medCalc.infusion.content ).toBe(2000);
    store.dispatch(toggleAdditionalCalculationsVisibility());
    expect( store.getState().medCalc.infusion.additionalCalculations ).toBe(true);


    // Update of the mode should hit the store
    store.dispatch(setCalculatorMode("dråpar/sek"));
    expect( store.getState().medCalc.infusion.mode ).toBe("dråpar/sek");
});

test('drops UI interaction', () => {
    const {component,store} = getCalculatorAndReduxWithMode("dråpar/sek");

    changeFieldValue(component.find('#mlt-time').first(),"20");
    expect(store.getState().medCalc.infusion.hours).toBe(20);

    changeFieldValue(component.find('#mlt-min').first(),"20");
    expect(store.getState().medCalc.infusion.minutes).toBe(20);

    changeFieldValue(component.find('#mlt-contents').first(),"2000");
    expect(store.getState().medCalc.infusion.content).toBe(2000);

    expect(component.find('.calculator-descriptions small .row')).toHaveLength(7);

    // Tests the "show additional calculations" checkbox
    component.find('.additionalCalculationsToggle input').first().simulate('change');

    expect(component.find('.calculator-descriptions small .row')).toHaveLength(10);
});

test('ml/hr UI interaction', () => {
    const {component,store} = getCalculatorAndReduxWithMode("ml/t");

    changeFieldValue(component.find('#mlt-time').first(),"20");
    expect(store.getState().medCalc.infusion.hours).toBe(20);

    changeFieldValue(component.find('#mlt-min').first(),"20");
    expect(store.getState().medCalc.infusion.minutes).toBe(20);

    changeFieldValue(component.find('#mlt-contents').first(),"2000");
    expect(store.getState().medCalc.infusion.content).toBe(2000);
});

test('Time descriptions', () => {
    const {component,store} = getCalculatorAndReduxWithMode("ml/t");
    // Disable rounding for this test
    store.dispatch(toggleRounding());
    // Should have two description lines (header+basic ml/hr)
    expect(component.find(CalcExplanationLine)).toHaveLength(2);
    store.dispatch(setMinutes(30));
    // Should now have added one line, for hour/minute calculation
    expect(component.find(CalcExplanationLine)).toHaveLength(4);
    store.dispatch(setHours(0));
    // Should now have removed one line, the hour line
    expect(component.find(CalcExplanationLine)).toHaveLength(3);
});

test.each(["ml/t","dråpar/sek"])("Warnings (%s)", (mode) => {
    const {component,store} = getCalculatorAndReduxWithMode(mode);

    expect(component.find('.infusion-warning')).toHaveLength(0);

    store.dispatch(setContent(5001));

    expect(component.find('.infusion-warning')).toHaveLength(2);
    expect(component.find('.infusion-warning').at(0).text()).toBe("Advarsel: Dette er ein stor mengde væske. Dobbeltsjekk at du har rett mengde.");

    store.dispatch(setContent(4000));

    expect(component.find('.infusion-warning')).toHaveLength(2);
    expect(component.find('.infusion-warning').at(0).text()).toBe("Advarsel: Dette er mykje væske på kort tid. Dobbeltsjekk infusjonstid og mengde.");

    store.dispatch(setContent(2500));
    store.dispatch(setHours(5));

    expect(component.find('.infusion-warning')).toHaveLength(2);
    expect(component.find('.infusion-warning').at(0).text()).toBe("Advarsel: Dette er mykje væske over en lengre periode. Dobbeltsjekk infusjonstid og mengde.");
});

test.each(["ml/t","dråpar/sek"])("zero-content calculation (%s)", (mode) => {
    const {component,store} = getCalculatorAndReduxWithMode(mode);

    store.dispatch(setContent(0));
    store.dispatch(setHours(0));
    store.dispatch(setMinutes(0));
    expect( getResultFromComp(component) ).toBe(0);
    expect(component.find('.calculator-descriptions small .row')).toHaveLength(2);
});

describe('Mode switching', () => {
    const {component,store} = getCalculatorAndReduxWithMode("ml/t");
    test('Valid modes', () => {
        expect(component.render().find('#medcalc-infusion-result').parent().text()).toMatch('ml/t');
        expect( store.getState().medCalc.infusion.mode ).toBe("ml/t");

        // Try changing a select entry
        component.find('select').props().onChange({ currentTarget: { id: '', value: 'dråpar/sek' } });
        expect(component.render().find('#medcalc-infusion-result').parent().text()).toMatch('dråpar/sek');
        expect( store.getState().medCalc.infusion.mode ).toBe("dråpar/sek");

        // Try changing it back
        component.find('select').props().onChange({ currentTarget: { id: '', value: 'ml/t' } });
        expect(component.render().find('#medcalc-infusion-result').parent().text()).toMatch('ml/t');
        expect( store.getState().medCalc.infusion.mode ).toBe("ml/t");
    });
    test('Invalid mode', () => {
        expect( () => {
            component.find('select').props().onChange({ currentTarget: { id: '', value: 'this-is-invalid' } });
        }).toThrow('onChangeMode: got invalid mode:');
    });
});
