/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import { AboutPleiarNo, AboutFreeSoftware, AboutPrivacy, AboutSystem, About, InstallApp } from '../src/react-components/about.js';
import { prepareEnv, checkRoutedSnapshot, getComponentWith, silentFunc, setUA } from './test-helpers';
jest.mock('../src/searcher', () => {
    return {
        hasInitialized: jest.fn( () => false ),
        onInitialize: jest.fn(),
        initialize: jest.fn()
    };
});
jest.mock('../src/helper.feature-detection', () => {
    return {
        aSupportsRel: jest.fn( () => false),
    };
});
import featureDetection from '../src/helper.feature-detection';
import PleiarSearcher from '../src/searcher';
import { appInstall } from '../src/mobile-appinstall';


prepareEnv();

const getElementsByTagName = document.getElementsByTagName;
beforeEach( () => {
    PleiarSearcher.hasInitialized.mockClear();
    PleiarSearcher.initialize.mockClear();
    PleiarSearcher.onInitialize.mockClear();
    document.getElementsByTagName = getElementsByTagName;
});

test('About pleiar.no', () => {
    checkRoutedSnapshot('/om','/',AboutPleiarNo);
    expect(global.__title).toMatch('om');
});

test('About free software', () => {
    checkRoutedSnapshot('/om/friprogramvare','/om',AboutFreeSoftware);
    expect(global.__title).toMatch('Fri programvare');
});

test('About privacy', () => {
    checkRoutedSnapshot('/om/personvern','/om',AboutPrivacy);
    expect(global.__title).toMatch('Personvern');
});

describe('AboutSystem', () => {
    test('Routing', () => {
        let comp;
        expect( () => {
            comp = getComponentWith(About,{
                router: true,
                route: '/om/system',
                enzyme: true,
                previousPath: '/om'
            });
        }).not.toThrow();
        expect(comp.find(AboutSystem)).toHaveLength(1);
    });
    test('FullDOM snapshot', () => {
        const body = document.getElementsByTagName('body')[0];
        const script = document.createElement('script');
        script.setAttribute('src','/');
        body.append(script);
        const link = document.createElement('link');
        link.setAttribute('rel','prefetch');
        link.setAttribute('href','/');
        body.append(link);
        Date.prototype.toLocaleString = jest.fn( () => "3.4.2018, 15:53:00");
        Date.prototype.getTime = jest.fn( () => 1522763580000 );
        // Needed so the test doesn't change between jsdom upgrades or when run
        // on non-Linux systems (ie. on mac '(linux)' becomes '(darwin)',
        // breaking the snapshot)
        setUA('(linux) AppleWebKit/537.36 (KHTML, like Gecko) jsdom/16.7.0');
        let comp;
        expect( () => {
            comp = getComponentWith(AboutSystem,{
                enzyme: { fullDOM: true },
                route: '/om/system',
                previousPath: '/om',
                router: true
            });
        }).not.toThrow();
        expect(comp).toMatchSnapshot();
    });

    test('With fake service worker, not enabled', () => {
        Object.defineProperty(navigator,'serviceWorker', { value: { controller: null}, configurable: true });
        const comp = getComponentWith(AboutSystem,{
            enzyme: { fullDOM: true },
            route: '/om/system',
            previousPath: '/om',
            router: true
        });
        expect(comp.text()).toMatch(/Service worker: tilgjengelig, ikke i bruk/);
    });

    test('With fake service worker, enabled', () => {
        Object.defineProperty(navigator,'serviceWorker', { value: { controller: true}, configurable: true });
        const comp = getComponentWith(AboutSystem,{
            enzyme: { fullDOM: true },
            route: '/om/system',
            previousPath: '/om',
            router: true
        });
        expect(comp.text()).toMatch(/Service worker: tilgjengelig, i bruk/);
    });

    test('body class updates', () => {return new Promise((done) => {
        const body = document.getElementsByTagName('body')[0];
        body.setAttribute('class','');
        const comp = getComponentWith(AboutSystem,{
            enzyme: { fullDOM: true },
            route: '/om/system',
            previousPath: '/om',
            router: true
        });
        expect(comp.text()).toMatch(/body-flagg: \(ingen\)/);
        body.setAttribute('class','test-class');
        setTimeout( () => {
            expect(comp.text()).toMatch(/body-flagg: test-class/);
            done();
        },2);
    });});
    test('Non-android platform', () => {
        Object.defineProperty(navigator,'userAgent', { value: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36', configurable: true });
        Object.defineProperty(navigator,'platform', { value: 'Linux x86_64', configurable: true });
        const comp = getComponentWith(AboutSystem,{
            enzyme: true,
            route: '/om/system',
            previousPath: '/om',
            router: true
        });
        expect(comp.text()).toMatch(/Plattform: Linux x86_64/);
    });
    test('Android platform', () => {
        Object.defineProperty(navigator,'userAgent', { value: 'Mozilla/5.0 (Linux; Android 9; SM-G950F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.90 Mobile Safari/537.36', configurable: true });
        Object.defineProperty(navigator,'platform', { value: 'Linux armv8l', configurable: true });
        const comp = getComponentWith(AboutSystem,{
            enzyme: true,
            route: '/om/system',
            previousPath: '/om',
            router: true
        });
        expect(comp.text()).toMatch(/Plattform: Android/);
    });
    test('PleiarSearcher updates', () => {
        const comp = getComponentWith(AboutSystem,{
            enzyme: true,
            route: '/om/system',
            previousPath: '/om',
            router: true
        });
        expect(PleiarSearcher.hasInitialized).toHaveBeenCalled();
        expect(PleiarSearcher.onInitialize).toHaveBeenCalled();
        expect(comp.text()).toMatch(/PleiarSearcher status: ikke lastet/);
        PleiarSearcher.hasInitialized = jest.fn( () => true );
        PleiarSearcher.onInitialize.mock.calls[0][0]();
        expect(comp.text()).toMatch(/PleiarSearcher status: lastet/);
    });
    test('Crashing source list', () => {
        document.getElementsByTagName = jest.fn( () => {
            throw('OH NOES');
        });
        let comp;
        expect( () => {
            comp = getComponentWith(AboutSystem,{
                enzyme: true,
                route: '/om/system',
                previousPath: '/om',
                router: true
            });
        }).not.toThrow();
        expect(comp.text()).toMatch(/klarte ikke hente kildeliste/);
    });
    test('With features detected', () => {
        Object.defineProperty(window.navigator,'connection', { value: true, configurable: true });
        const comp = getComponentWith(AboutSystem,{
            enzyme: true,
            route: '/om/system',
            previousPath: '/om',
            router: true
        });
        expect(comp.text()).not.toMatch(/Motor-flagg: \(ingen\)/);
    });
    test('Without features detected', () => {
        featureDetection.aSupportsRel = jest.fn( () => false);
        Object.defineProperty(window.navigator,'storage', { value: { persist: true }, configurable: true });
        Object.defineProperty(window.navigator,'connection', { value: false, configurable: true });
        Object.defineProperty(window.navigator,'mozConnection', { value: false, configurable: true });
        Object.defineProperty(window.navigator,'webkitConnection', { value: false, configurable: true });
        Object.defineProperty(window,'applicationCache', { value: { UNCACHED: 0, status: 99}, configurable: true });
        delete window.applicationCache;
        const comp = getComponentWith(AboutSystem,{
            enzyme: true,
            route: '/om/system',
            previousPath: '/om',
            router: true
        });
        expect(comp.text()).toMatch(/Motor-flagg: \(ingen\)/);
    });
});

test('Routing', () => {
    checkRoutedSnapshot('/om','/om', About);
    checkRoutedSnapshot('/om/personvern','/om', About);
    checkRoutedSnapshot('/om/friprogramvare','/om', About);
    checkRoutedSnapshot('/om/flerefagtilbud','/om', About);
    // Routing of /om/app is already being tested later
});

describe('InstallApp', () => {
    test.each(['firefox-android','safari-ios','ios-other','samsung','facebook-ios','facebook-android','chrome','opera','android-webview'])('with %p', (mode) => {
        appInstall.mode = jest.fn( () => mode);
        checkRoutedSnapshot('/om/app','/om', About);
    });
    test('Unsupported installApp', () => {
        appInstall.mode = jest.fn( () => null);
        expect(silentFunc(() => {
            getComponentWith(InstallApp);
        })).toThrow("You should not use <Redirect> outside a <Router>");
    });
});
