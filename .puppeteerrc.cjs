/*
 * Store puppeteer chrome's in ./.cache instead of ~/.cache, to allow for
 * sharing into containers
 */
const {join} = require('path');

/**
 * @type {import("puppeteer").Configuration}
 */
module.exports = {
  cacheDirectory: join(__dirname, '.cache', 'puppeteer'),
};
