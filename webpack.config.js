/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* global module __dirname process */
/* eslint-disable flowtype/require-return-type */
/* eslint-disable flowtype/require-variable-type */
/* eslint-disable flowtype/require-valid-file-annotation */
/* eslint-disable require-jsdoc */
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const OfflinePlugin = require('offline-plugin');
const childProcess = require('child_process');
const CompressionPlugin = require("compression-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const dataConfig = require('./data/data.config.js');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const _ = require('lodash');

let GIT_REVISION = "(git)";
let GIT_REVISION_FULL = "(git)";
let GIT_REVISION_DATA = "(git)";
let AUTH_URL = dataConfig.authentication.development.url;
let AUTH_APPID = dataConfig.authentication.development.app_id;
let AUTH_DEFAULT_STATE = dataConfig.authentication.development.defaultState;
let AUTH_ALLOW_URL = dataConfig.authentication.development.allowURL;
if(process.env.PLEIAR_ENV !== "development")
{
    GIT_REVISION = childProcess.execFileSync('git', [ 'rev-parse', '--short=4', 'HEAD' ]).toString().replace(/\n/g,'');
    if(process.env.CI_COMMIT_REF_NAME && process.env.CI_COMMIT_REF_NAME != 'production' && process.env.CI_COMMIT_REF_NAME != '')
    {
        GIT_REVISION += '-'+process.env.CI_COMMIT_REF_NAME;
    }
    GIT_REVISION_FULL = childProcess.execFileSync('git', [ 'rev-parse', 'HEAD' ]).toString().replace(/\n/g,'');
    GIT_REVISION_DATA = childProcess.execFileSync('git', [ '-C','data', 'rev-parse','--short=4', 'HEAD' ]).toString().replace(/\n/g,'');
}
if(process.env.PLEIAR_ENV === "production")
{
    AUTH_URL = dataConfig.authentication.production.url;
    AUTH_APPID = dataConfig.authentication.production.app_id;
    AUTH_DEFAULT_STATE = dataConfig.authentication.production.defaultState;
    AUTH_ALLOW_URL = dataConfig.authentication.production.allowURL;
}

function jsonTemplating (content)
{
    const result = templating(content);
    // Minify
    return JSON.stringify(JSON.parse(result));
}

function templating (content)
{
    const template = _.template(content);
    return template({
        dataConfig
    });
}

module.exports = {
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    "css-loader"
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: "css-loader",
                        // Disables url() handling. Just assume they are correct.
                        options: { url: false }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sassOptions: {
                                includePaths: ["static/"]
                            },
                        }
                    }
                ],
            },
            {
                test: /\.yml$/,
                use: [
                    {
                        loader: "json-loader"
                    },
                    {
                        loader: "yaml-loader"
                    },
                ],
            },
            {
                test: /\.md$/,
                use: [
                    'raw-loader',
                ],
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    entry: {
        main: './src/index.js',
        design: [ './src/bootstrap.scss', './src/style.scss', './data/design.scss' ],
    },
    output: {
        filename: process.env.PLEIAR_ENV === "production" ? '[name].js?b=[chunkhash]' : '[name].[hash].js',
        path: path.resolve(__dirname, 'build'),
        publicPath:'/',
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'index.html',
            minify: {
                collapseWhitespace: true,
                removeComments: true,
                removeRedundantAttributes: true,
                removeScriptTypeAttributes: true,
                removeStyleLinkTypeAttributes: true,
                useShortDoctype: true
            },
            inject: false,
            dataConfig: dataConfig,
        }),
        new CopyWebpackPlugin([
            { from:'static/' },
            { from:'data/static/' },
            { from:'node_modules/@babel/polyfill/dist/polyfill.min.js', to:'babel-polyfill.min.js' },
            { from: 'src/appmanifest.json', transform: jsonTemplating}
        ]),
        new webpack.DefinePlugin({
            'GIT_REVISION': JSON.stringify(GIT_REVISION),
            'GIT_REVISION_FULL': JSON.stringify(GIT_REVISION_FULL),
            'GIT_REVISION_DATA': JSON.stringify(GIT_REVISION_DATA),
            'AUTH_URL': JSON.stringify(AUTH_URL),
            'AUTH_APPID': JSON.stringify(AUTH_APPID),
            'AUTH_DEFAULT_STATE':AUTH_DEFAULT_STATE,
            'AUTH_ALLOW_URL':AUTH_ALLOW_URL,
            'PLEIAR_ENV':JSON.stringify(process.env.PLEIAR_ENV)
        }),
        // OfflinePlugin should be last
        new OfflinePlugin({
            externals: dataConfig.cacheExternals,
            // Enable auto-updating once an hour. Limits how stale an instance can
            // become.
            autoUpdate: true,
            // Events ensure that we will apply updates as soon as possible.
            // The actual handling is done in src/index.js
            "ServiceWorker": {
                events: true,
                // Minify the service worker in production
                minify: (process.env.PLEIAR_ENV === "production")
            },
        }),
        new MiniCssExtractPlugin({
            // There's a bug with OptimizeCssAssetsPlugin that breaks minifying
            // if query strings are present in the CSS filename. Therefore we,
            // for now, have to use chunkhash in the filename instead.
            // Ref. https://github.com/NMFR/optimize-css-assets-webpack-plugin/issues/62
            filename: '[name].[chunkhash].css'
        }),
        new webpack.BannerPlugin({
            banner: 'Pleiar.no\nCopyright (C) Eskild Hustvedt, Fagforbundet\nLicensed under the GNU Affero General Public License version 3',
            include: /(main|style)/,
        }),
        new webpack.BannerPlugin({
            banner: dataConfig.copyrightHeader,
            include: /(data|design|search-index|elearn)/,
        }),
        new OptimizeCssAssetsPlugin({
        }),
    ],
    optimization:
    {
        runtimeChunk: "single",
        splitChunks: {
            // The default webpack config tries to be smart about bundle sizes, which
            // doesn't work that well when we define them by ourselves, thus we need to
            // override some of the defaults.
            maxAsyncRequests: 8,
            maxInitialRequests: 8,
            cacheGroups: {
                "search-index":{
                    name: "search-index",
                    test: /data\/search-index.json$/,
                    priority: 501,
                    minSize: 0,
                    chunks: "all",
                },
                "elearn":{
                    name: "elearn",
                    test: /data\/quiz\/quiz.json$/,
                    priority: 501,
                    minSize: 0,
                    chunks: "all",
                },
                data: {
                    name: 'data',
                    test: /data\/.*\/.*js(on)?$/,
                    priority: 500,
                    minSize: 0,
                    chunks: "all"
                },
                feather: {
                    name: "feather",
                    test: /feather/,
                    priority: 1,
                    chunks: "all"
                },
                vendor: {
                    name: 'vendor',
                    test: /(node_modules|bootstrap)/,
                    priority: 0,
                    chunks: "all"
                },
                // We extract style instead of design in order for design to be
                // loaded last, in the offchance that the load order matters
                // for the css
                style: {
                    name: 'style',
                    test: /src\/style/,
                    priority: 499,
                    minSize: 0,
                    chunks: "all",
                },
            }
        },
    },
    performance:
    {
        hints: false
    },

    devServer: {
        hot: process.platform !== "darwin", 
        historyApiFallback: true,
    }
};
// In production we pre-gzip CSS+JS
if(process.env.NODE_ENV == "production")
{
    module.exports.plugins.push(
            new CompressionPlugin({
                filename: "[path][base].gz",
                test: /\.(css|js)/
            })
    );
}
else
{
    module.exports.devtool = 'inline-source-map';
}
