/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 * Copyright (C) Eskild Hustvedt 2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

/* global process */

import fs from 'fs';
import glob from 'glob';
import path from 'path';

function die(message)
{
    console.log(message);
    process.exit(1);
}

function main ()
{
    if (!fs.existsSync('./build/200.html'))
    {
        die('Compile the site with webpack+react-snap first');
    }
    const allFiles = glob.sync('./build/**/index.html');
    const paths = [];
    const seenPaths = {};
    for(const file of allFiles)
    {
        const dir = path.dirname(file).replace(/\.\/build/,'');
        // Ignore these paths
        if (/^\/(auth|om\/app)/.test(dir))
        {
            continue;
        }
        const lower = dir.toLowerCase();
        if (!seenPaths[lower])
        {
            paths.push(dir);
            seenPaths[dir] = true;
            seenPaths[lower] = true;
        }
    }
    const sitemap = [
        '<?xml version="1.0" encoding="UTF-8"?>',
        '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'
    ];
    for(const file of paths)
    {
        sitemap.push(`<url><loc>https://www.pleiar.no${file}</loc><changefreq>weekly</changefreq></url>`);
    }
    sitemap.push("</urlset>");
    fs.writeFileSync('./build/sitemap.xml',sitemap.join("\n"));
}

main();
