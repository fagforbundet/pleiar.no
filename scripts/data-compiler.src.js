/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

/* global process */

import fs from 'fs';
import glob from 'glob';
import yaml from 'js-yaml';
import mdToc from 'markdown-toc';
import PleiarIndexer from '../src/indexer.js';
import type { HandbookPath, HandbookDataEntry, HandbookDataStructure, LabValueDataStructure, LabValueEntry, DictionaryDataStructure, ExternalResourcesDataStructure, ExternallyIndexedList } from '../src/types/data';
import toolsList from '../src/tools.list.js';

"use strict";

/*
 * Global helpers
 */

function errorOut (message, statusCode)
{
    console.log(message);
    if(!statusCode)
    {
        statusCode = 1;
    }
    process.exit(statusCode);
}

function fnameToIdentifier (fname)
{
    return dumbBasename(fname).replace(/^\d+-/,'').replace(/\.md$/,'');
}

function dumbBasename (file)
{
    return file.replace(/^.*\/([^/]+)$/,'$1');
}

function writeJSON (jsObj, name: string, targetFile: string)
{
    const result = JSON.stringify(jsObj);
    if(result === undefined)
    {
        throw('Failed to stringify object to JSON for writing to '+targetFile);
    }
    fs.writeFileSync(targetFile,result);
    console.log('Compiled '+name+' to '+targetFile);
}

function loadYAMLDataset (...list)
{
    for(const file of list)
    {
        if(fs.existsSync('./data/datasett/'+file))
        {
            return yaml.safeLoad(fs.readFileSync('./data/datasett/'+file).toString());
        }
    }
    throw('None of the data files requested exists in ./data/datasett: '+list.join(', '));
}

/*
 * Handbook compilation
 */

type handbookMetadata = {|
    title: string,
    public?: boolean,
    displayIndexLink?: boolean,
    sections: {|
        [string]: {|
            title: string,
            toc?: string,
            public?: boolean,
            hideTOC?: boolean,
            customURL?: string,
        |},
    |}
|};

const allFiles = [];
function getTOC (content)
{
    const intermediateTOC = mdToc(content).json;
    const toc = [];
    for(const entry of intermediateTOC)
    {
        toc.push(entry.content);
    }
    return toc;
}
function compileFile (file: string,meta: handbookMetadata, parents: HandbookPath): HandbookDataEntry
{
    const content = fs.readFileSync(file).toString();
    if (!meta.sections[ dumbBasename(file) ])
    {
        errorOut(file+' has no section in the metadata file');
    }
    const fileMeta = meta.sections[ dumbBasename(file) ];
    const entry: HandbookDataEntry = {
        content,
        title: fileMeta.title,
        public: !!fileMeta.public,
        hideTOC: !!fileMeta.hideTOC,
        toc: fileMeta.hideTOC ? [] : getTOC(content),
        path: [].concat(parents, [ fnameToIdentifier(file) ])
    };
    if(fileMeta.toc !== undefined && fileMeta.toc === "labValues")
    {
        entry.toc = [];
        // eslint-disable-next-line prefer-const
        let labValues = loadYAMLDataset('Labverdier.yml','lab.yml');
        // Cast the loaded data to the handbookMetadata type
        labValues = ((labValues: $UnsafeRecast): labYML);
        for(const category in labValues)
        {
            entry.toc.push( category );
        }
    }
    if(fileMeta.customURL !== undefined)
    {
        entry.customURL = fileMeta.customURL;
    }

    const prev = allFiles.length -1;
    if(allFiles[prev] !== undefined)
    {
        if(allFiles[prev].path[0] === parents[0])
        {
            entry.previous = allFiles[prev].path;
            allFiles[prev].next = entry.path;
        }
    }

    allFiles.push(entry);
    return entry;
}

function compileRecursiveTree (root, parents?: HandbookPath)
{
    if(parents === undefined)
    {
        parents = [];
    }
    else
    {
        parents = [].concat(parents);
        parents.push( fnameToIdentifier(root) );
    }
    const entries = [];
    let title;
    if(! fs.existsSync(root+'/Metadata.yml'))
    {
        throw(root+': has no Metadata.yml');
    }
    // eslint-disable-next-line prefer-const
    let loaded = yaml.safeLoad(fs.readFileSync(root+'/Metadata.yml', 'utf8'));
    // Cast the loaded data to the handbookMetadata type
    loaded = ((loaded: $UnsafeRecast): handbookMetadata);
    const meta = loaded;
    if(meta == null || typeof(meta) !== 'object' || meta.sections === undefined)
    {
        throw(root+'/Metadata.yml: no defined sections');
    }
    for(const part in meta.sections)
    {
        entries.push(root+'/'+part);
    }
    const displayIndexLink = meta.displayIndexLink === true;
    const isPublic = meta.public === true;
    title = meta.title;
    if(parents.length === 0)
    {
        title = 'Root';
    }
    const result: HandbookDataStructure = {
        displayIndexLink,
        title,
        order: [],
        tree: {}
    };
    // If there is an article with the same name as our section, then we add
    // that implicitly
    if(fs.existsSync(root+'.md'))
    {
        // We mock up a "fake" handbookMetadata structure since the .md
        // doesn't actually exist in the one we loaded.
        const specialMeta: handbookMetadata = {
            title,
            sections: {
                [ dumbBasename(root+'.md') ]: {
                    public: isPublic,
                    title
                }
            }
        };
        // We need to pop ourselves off of the parents array, since we're technically
        // not the parent of this article, since it lives one level above us.
        const us = parents.pop();

        result.article = compileFile(root+'.md', specialMeta, parents);

        // Add us back to the parents array for the remaining entries
        parents.push(us);
    }
    for(const treeEntry of entries)
    {
        let compiled;
        const name = fnameToIdentifier(treeEntry);
        if(fs.statSync(treeEntry).isDirectory())
        {
            compiled = compileRecursiveTree(treeEntry, parents);
        }
        else
        {
            if(meta === undefined)
            {
                throw('Tried to compileFile() without metadata. This shouldn\'t be possible');
            }
            compiled = compileFile(treeEntry, meta, parents);
        }
        result.tree[ name ] = compiled;
        result.order.push(name);
    }
    return result;
}

function compileHandbook ()
{
    const data = compileRecursiveTree('./data/handbok/');
    writeJSON(data, 'handbook', './data/handbok/handbok.json');
    return data;
}

function compileQuickRef ()
{
    const data = compileRecursiveTree('./data/hurtigoppslag/');
    writeJSON(data, 'quickref', './data/hurtigoppslag/quickref.json');
    return data;
}

/*
 * A quiz file compiler that converts shorthand syntax into full syntax
 *
 * Shorthand syntaxes:
 * - Questions with a "header" key instead of "question" -> renames to "question"
 * - Answers that are only strings -> objects with an entry key
 * - Answers where there is no "entry" but there's an "correct" entry that's a
 *   string instead of a boolean -> renames "correct" to "entry" and sets "correct" to true.
 * - Questions with no answers -> splashSlide: true
 */
function compileSingleQuizFile (file)
{
    const content = yaml.safeLoad(fs.readFileSync(file).toString());
    let entryNumber = 0;
    // $FlowIssue[incompatible-type] - doesn't know what kind of data is coming from yaml
    for(const question of content.quiz)
    {
        entryNumber++;
        if(typeof(question) !== "object")
        {
            throw("Unknown non-object question in "+file+": "+JSON.stringify(question)+' (found at entry number '+entryNumber+' of the quiz array)');
        }
        // "header" can be used as an alias for "question"
        if(typeof(question.header) === "string" && question.question === undefined)
        {
            question.question = question.header;
            delete question.header;
        }
        if(question.answers)
        {
            for(let n = 0; n < question.answers.length; n++)
            {
                const answer = question.answers[n];
                // Convert single strings into proper objects
                if(typeof(answer) === 'string')
                {
                    question.answers[n] = { entry: answer };
                }
                // Convert { correct: "Something" } into { correct: true, entry: "Something" }
                else if(answer.entry === undefined && typeof(answer.correct) === "string")
                {
                    question.answers[n] = {
                        entry: answer.correct,
                        correct: true,
                    };
                }
            }
        }
        else
        {
            question.splashSlide = true;
        }
    }
    return content;
}

function compileQuiz ()
{
    const quizEntries = {};
    for(const file of glob.sync('data/quiz/*.yml'))
    {
        const name = dumbBasename(file).replace('.yml','');
        quizEntries[name] = compileSingleQuizFile(file);
    }
    writeJSON(quizEntries, 'quiz entries','./data/quiz/quiz.json');
    return quizEntries;
}

/*
 * Lab value compilation
 */

type labYML = {|
    [string]: Array<LabValueEntry>
|};

function compileLabValues ()
{
    // $FlowIssue[incompatible-type] - doesn't know what kind of data is coming from yaml
    const labValues: labYML = loadYAMLDataset('Labverdier.yml','lab.yml');
    const values = [];
    const index = {};
    let iter = 0;
    for(const category in labValues)
    {
        for(const entry of labValues[category])
        {
            entry.category = category;
            values.push(entry);
            if(index[category] === undefined)
            {
                index[category] = [];
            }
            index[category].push(iter);
            iter++;
        }
    }

    const labValueStructure: LabValueDataStructure = {
        index,
        values
    };

    writeJSON(labValueStructure,'lab values','./data/datasett/lab.json');
    return labValueStructure;
}

/*
 * Dictionary compilation
 */
function compileDictionary ()
{
    // $FlowIssue[incompatible-type] - doesn't know what kind of data is coming from yaml
    const dictionary: DictionaryDataStructure = loadYAMLDataset('Ordliste.yml','dictionary.yml');
    dictionary.sort( (a,b) =>
    {
        return a.expression.localeCompare(b.expression, 'no');
    });
    writeJSON(dictionary,'dictionary','./data/datasett/dictionary.json');
    return dictionary;
}

/*
 * External resources compilation
 */
function compileExternalResources ()
{
    // $FlowIssue[incompatible-type] - doesn't know what kind of data is coming from yaml
    const data: ExternalResourcesDataStructure = loadYAMLDataset('AndreSider.yml','external-resources.yml');
    writeJSON(data, 'external resources','./data/datasett/external-resources.json');
    return data;
}

/*
 * nutricalc-data compilation
 */
function compileNutricalcData()
{
    // $FlowIssue[cannot-resolve-name] - doesn't know what kind of data is coming from yaml
    const data: NutritionDataStructure = loadYAMLDataset('ErnæringsData.yml','nutricalc-data.yml');
    writeJSON(data, 'nutricalc data','./data/datasett/nutricalc-data.json');
    return data;
}

/*
 * lunr index construction
 */
function buildLunrIndex (external,dict,labValues,handbook,nutrition, externallyIndexed,quickref, quiz)
{
    const index = PleiarIndexer.index(
        external,
        dict,
        labValues,
        handbook,
        toolsList,
        nutrition,
        externallyIndexed,
        quickref,
        quiz
    );
    writeJSON(index,'search-index','./data/search-index.json');
}

/*
 * Compiles website-identification.yml to JSON
 */
function compileWebsiteIdentification ()
{
    writeJSON(loadYAMLDataset('NettsideIdentifisering.yml','website-identification.yml'),
        'website identification','./data/datasett/website-identification.json');
}

function compileSearchMetadata (quiz) {
    const quizIndexed = {};
    for(const entry of Object.keys(quiz))
    {
        const data = quiz[entry];
        const indexed = {
            title: data.title,
            url: '/elaering/'+entry,
        };
        if(data.description !== undefined)
        {
            // $FlowExpectedError[prop-missing]
            indexed.description = data.description;
        }
        quizIndexed[entry]  = indexed;
    }
    writeJSON({quiz: quizIndexed }, 'search-metadata','./data/search-meta.json');
    return quizIndexed;
}

/*
 * Main entry point
 */
function main ()
{
    if (! (fs.existsSync('./src/helper.numbers.js') && fs.existsSync('./webpack.config.js') ) )
    {
        errorOut('Script must be executed from the root directory of the pleiar.no repository');
    }
    if (! (fs.existsSync('./data/handbok') ) )
    {
        errorOut('Data repository must be present');
    }
    const quiz = compileQuiz();
    const handbook  = compileHandbook();
    const quickref  = compileQuickRef();
    const labValues = compileLabValues();
    const dict      = compileDictionary();
    const external  = compileExternalResources();
    const nutrition = compileNutricalcData();
    const externallyIndexed: ExternallyIndexedList = JSON.parse(fs.readFileSync('./data/externallyIndexed.json').toString());
    compileWebsiteIdentification();
    compileSearchMetadata(quiz);

    buildLunrIndex(external,dict,labValues,handbook,nutrition,externallyIndexed,quickref, quiz);
}

main();
