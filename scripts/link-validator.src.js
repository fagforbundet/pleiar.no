/**
 * @prettier
 */
/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

/* global process */

import fs from "fs";
import glob from "glob";
import mdLinkExtract from "markdown-link-extractor";
import got from "got";
type ResponseMetadata = {|
    status: number,
    // eslint-disable-next-line flowtype/require-exact-type
    headers: {} | null,
    body?: string,
    errorCode?: string,
|};
const URLcache: {| [string]: ResponseMetadata |} = {};

let DomainResponseWorkaroundList = {};
if (fs.existsSync("./data/.linkcheck-workarounds.json")) {
    DomainResponseWorkaroundList = loadJSON(
        "./data/.linkcheck-workarounds.json",
    );
}

function loadJSON(file) {
    return JSON.parse(fs.readFileSync(file).toString());
}

async function retrieveURL(URL) {
    if (URLcache[URL] !== undefined) {
        return URLcache[URL];
    }

    let req;
    try {
        req = await got(URL, {
            followRedirect: true,
            methodRewriting: true,
            cacheOptions: {
                ignoreCargoCult: true,
            },
            headers: {
                "Cache-Control": "max-age=0",
            },
        });
    } catch (e) {
        if (process.env.PLEIAR_LINK_DEBUG) {
            console.log(e);
        }
        if (e.response !== undefined) {
            req = e.response;
            if (e && typeof e.code === "string") {
                req.errorCode = e.code;
            }
        } else {
            let code = "ERROR";
            if (e && typeof e.code === "string") {
                code = e.code;
            }
            req = {
                statusCode: -1,
                errorCode: code,
                body: code,
                headers: null,
            };
        }
    }

    URLcache[URL] = {
        status: req.statusCode,
        headers: req.headers,
        errorCode: req.errorCode,
        body: "",
    };

    if (req.statusCode !== 200) {
        URLcache[URL].body = req.body;
    }

    return URLcache[URL];
}

async function validateSingleLink(link, sourceInfo: string) {
    try {
        const req = await retrieveURL(link);
        if (req.status !== 200) {
            const body = req.body;
            if (req.status === 403) {
                // If cloudflare is acting up, just assume the page exists
                if (
                    body.indexOf("Why do I have to complete a CAPTCHA?") !== -1
                ) {
                    return true;
                }
                // If Vimeo et.al. is acting up, also assume the page exists
                if (
                    body.indexOf(
                        "We detected a high number of errors from your connection.",
                    ) !== -1 ||
                    body.indexOf(
                        "Pardon the inconvenience, but our servers have detected a high number of errors from your connection.",
                    ) !== -1
                ) {
                    return true;
                }
            }
            const errorCodesForWorkaround = [];
            if (req.status !== -1) {
                errorCodesForWorkaround.push(req.status);
            }
            if (typeof req.errorCode === "string") {
                errorCodesForWorkaround.push(req.errorCode);
            }
            // Domain-specific workarounds
            for (const domainName of Object.keys(
                DomainResponseWorkaroundList,
            )) {
                if (
                    link
                        .replace(/^https?:\/\/(www\.)?/, "")
                        .indexOf(domainName) === 0
                ) {
                    for (const returnValue of DomainResponseWorkaroundList[
                        domainName
                    ]) {
                        for (const errorCodeForWorkaround of errorCodesForWorkaround) {
                            if (errorCodeForWorkaround === returnValue) {
                                console.log(
                                    "Got " +
                                        returnValue +
                                        " from " +
                                        link +
                                        " but the domain is on the workaround list, so treating it as a success",
                                );
                                return true;
                            }
                        }
                    }
                }
            }
            let error = req.status;
            if (typeof req.errorCode === "string") {
                if (req.status == -1) {
                    error = req.errorCode;
                } else {
                    error += " (" + req.errorCode + ")";
                }
            }
            console.log(
                "ERROR: " + link + ": from " + sourceInfo + ": " + error,
            );
            if (process.env.PLEIAR_LINK_DEBUG) {
                console.log("PLEIAR_LINK_DEBUG is active, dumping body:");
                console.log("=========");
                console.log(req.headers);
                console.log(body);
                console.log("=========");
            }
            return false;
        }
    } catch (e) {
        console.log(
            "ERROR: " + link + ": from " + sourceInfo + ": threw " + e.stack,
        );
        return false;
    }
    return true;
}

async function validateExternalResources() {
    const links = loadJSON("./data/datasett/external-resources.json");
    let hadErrors = false;
    for (const entry of links) {
        const success = await validateSingleLink(
            entry.url,
            "external-resources",
        );
        if (!success) {
            hadErrors = true;
        }
    }
    return hadErrors;
}

async function validateHandbook() {
    const allFiles = glob.sync("data/**/*.md");
    let hadErrors = false;
    for (const file of allFiles) {
        // Ignore anything in a node_modules directory. Our data directory can, in
        // certain cases, include a node_modules, which in turn contains
        // markdown-files, but those should not be validated.
        if (/node_modules/.test(file)) {
            continue;
        }
        const links = mdLinkExtract(fs.readFileSync(file).toString());
        for (const link of links) {
            if (/^(\/\/|http)/.test(link)) {
                if (!(await validateSingleLink(link, file))) {
                    hadErrors = true;
                }
            }
        }
    }
    return hadErrors;
}

async function main() {
    let hadErrors = false;
    if (await validateExternalResources()) {
        hadErrors = true;
    }
    if (await validateHandbook()) {
        hadErrors = true;
    }
    if (hadErrors) {
        process.exit(1);
    }
    process.exit(0);
}

main();
