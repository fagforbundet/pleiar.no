/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

/* global process */

import rssParser from 'rss-parser';
import fs from 'fs';
import yaml from 'js-yaml';

function verboseOut (message: string)
{
    if(process.env.PLEIAR_VERBOSE_INDEXER !== undefined)
    {
        console.log('[pleiar.no external-site-indexer] '+message);
    }
}

async function fagpratScraper ()
{
    verboseOut("Processing fagprat RSS");
    const parser = new rssParser();
    const feed = await parser.parseURL("http://feeds.soundcloud.com/users/soundcloud:users:780826267/sounds.rss");
    const content = [];
    for(const item of feed.items)
    {
        content.push({
            title: item.title,
            url: item.link,
            text: item.content,
            type: "fagprat",
        });
    }
    return content;
}

function manuallyScrapedData ()
{
    if (fs.existsSync('./data/datasett/EksternIndeks.yml'))
    {
        verboseOut("Added manually indexed data");
        return yaml.safeLoad(fs.readFileSync('./data/datasett/EksternIndeks.yml').toString());
    }
    else
    {
        return [];
    }
}

/*
 * This handles use of the CI cache.
 *
 * If we detect that we're running in a CI environment (by checking if the `CI`
 * env variable is set) we will look for a ./externallyIndexed.json file in the
 * project root. If that file exists, and is newer than 24 hours, we use that
 * instead of re-indexing.
 *
 * If the CI env var isn't set, the ./externallyIndexed.json file doesn't exist
 * or the ./externallyIndexed.json file is older than 24 hours, we ignore it
 * and perform the indexing as per usual.
 */
function useCICacheIfPresent ()
{
    if(process.env.CI !== undefined)
    {
        if(fs.existsSync('./externallyIndexed.json'))
        {
            const stat = fs.statSync('./externallyIndexed.json');
            const nowS = Math.floor(Date.now() / 1000);
            const ctimeS = Math.floor(stat.ctimeMs / 1000);
            if ( (nowS-ctimeS) < 86400 )
            {
                fs.copyFileSync('./externallyIndexed.json','./data/externallyIndexed.json');
                console.log('external-site-indexer: using CI cache');
                return true;
            }
        }
    }
    return false;
}

async function main ()
{
    if (! (fs.existsSync('./src/helper.numbers.js') && fs.existsSync('./webpack.config.js') ) )
    {
        console.log('Script must be executed from the root directory of the pleiar.no repository');
        process.exit(1);
    }
    if (! (fs.existsSync('./data/handbok') ) )
    {
        console.log('Data repository must be present');
        process.exit(1);
    }
    // If we're under CI and a CI-cache is present, use that.
    if(useCICacheIfPresent())
    {
        process.exit(0);
    }

    /*
     * Scraping
     */

    // For the Fagprat podcast
    const fagprat = await fagpratScraper();
    // For fagforbundet.no
    const manual = manuallyScrapedData();
    // Merge them
    const content = [].concat(fagprat).concat(manual);

    /*
     * Finalizing
     */
    // Write out the index
    fs.writeFileSync('./data/externallyIndexed.json',JSON.stringify(content));

    // If we're running under CI, also write another copy to ./externallyIndexed.json
    if(process.env.CI !== undefined)
    {
        fs.writeFileSync('./externallyIndexed.json',JSON.stringify(content));
    }

    process.exit(0);
}

main();
