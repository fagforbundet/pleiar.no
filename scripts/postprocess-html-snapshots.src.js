/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 * Copyright (C) Eskild Hustvedt 2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

/* global process */

import fs from 'fs';
import glob from 'glob';

const descriptions = {
    '/om/friprogramvare'                 : 'Pleiar.no er fri programvare. Det innebærer at du kan kjøre, kopiere, distribuere, studere, endre og forbedre programvaren fritt, under visse vilkår.',
    '/om/personvern'                      : 'Pasientinformasjon er sensitive opplysninger. Pleiar.no utfører all utregning og handlinger lokalt i nettleseren. Etter at du har lasta inn Pleiar.no vil ingen flere forespørsler bli sendt til Pleiar.no sin tjener, foruten å sjekke etter en oppdatert versjon av Pleiar.no. Det eneste som blir sendt er anonymisert statistikk',
    '/om/app'                             : 'Installer Pleiar.no som app',
    '/om'                                 : 'Om Pleiar.no',
    '/covid19'                            : 'Informasjon om COVID-19/SARS-CoV-2',
    '/andresider'                           : 'Liste over nyttige nettsider for pleiepersonell',
    '/verktoy'                            : 'Verktøy fra Fagforbundet for å hjelpe til med medisinregning',
    '/verktoy/medikamentregning/infusjon' : 'Verktøy fra Fagforbundet for å hjelpe til med å regne ut dråpetakt ellet infusjonshastighet i ml/t',
    '/verktoy/medikament/synonymer'       : 'Verktøy fra Fagforbundet for å søke i byttelisten for legemidler',
    '/verktoy/ernaering/dagsinntak'       : 'Verktøy fra Fagforbundet for å hjelpe til med å regne ut hvor mange kcal en pasient har fått i seg gjennom et døgn basert på en kostregistrering',
    '/verktoy/ernaering/energibehov'      : 'Verktøy fra Fagforbundet for å hjelpe til med å regne ut dagsbehovet for kcal til en pasient',
    '/verktoy/ernaering/bmi'              : 'Verktøy fra Fagforbundet for å hjelpe til med å regne ut BMI/KMI',
    '/verktoy/news'                       : 'Kalkulator for screeningverktøyet NEWS 2',
    '/ordliste'                               : 'Faglig oppslagsverk fra Fagforbundet',
    '/handbok'                           : 'Faglig håndbok for sykepleiere, vernepleiere, helsefagarbeidere og hjelpepleiere fra Fagforbundet',
    '/oppslag'                           : 'Hurtigoppslag (f.eks. ISBAR, ABCDE) til bruk i klinikken fra Fagforbundet',
    '/search'                             : 'Søkeresultat fra Pleiar.no',
    '/om/flerefagtilbud'                  : 'Liste over flere faglige tilbud fra Fagforbundet',
    '/elaering' : 'E-læringskurs fra Fagforbundet',
    '/elaering/legemiddelhandtering'      : 'Test deg selv i kunnskap om legemiddelhåndtering'
};
const partialsDescriptions = {
    '/search'   : descriptions['/search'],
    '/ordliste'     : descriptions['/ordliste'],
    '/andresider' : descriptions['/andresider'],
    '/handbok' : descriptions['/handbok'],
    '/oppslag' : descriptions['/oppslag'],
    '/elaering' : descriptions['/elaering'],
};

function die(message)
{
    console.log(message);
    process.exit(1);
}

function findDescription (file)
{
    let normalized = file.replace(/^\.\/build/,'');
    normalized = normalized.replace(/\/index(\.robot)?\.html$/,'').toLowerCase();
    if(descriptions[normalized])
    {
        return descriptions[normalized];
    }
    for(const sub in partialsDescriptions)
    {
        if(normalized.indexOf(sub) === 0)
        {
            return partialsDescriptions[sub];
        }
    }
    return null;
}

function tryToHandle (file)
{
    const description = findDescription(file);
    let ret = true;
    const content = fs.readFileSync(file).toString();
    let replaced;
    if (description !== null)
    {
        // Replace meta description
        replaced = content.replace(/<meta content="[^"]+" (property="og:description"|name="description")\s*>/g,`<meta content="${description}" $1>`)
            .replace(/<meta (property="og:description"|name="description") content="[^"]+"\s*\/?>/g,`<meta content="${description}" $1>`);
        if(replaced === content)
        {
            die(`Failed to make meta description replacements in ${file}`);
        }
    }
    else
    {
        ret = false;
        replaced = content;
    }
    let snapshotFile = file.replace(/\.\/build\//,'./build/snap/').replace(/(\/index)?(\.robot)?\.html$/,'.png');
    if (!fs.existsSync(snapshotFile))
    {
        snapshotFile = './build/snap/index.png';
    }
    if (fs.existsSync(snapshotFile))
    {
        let webrootRelativeSnapshotFile = snapshotFile.replace(/^\.\/build/,'');
        // In production deployments, use absolute file paths
        if (process.env.CI_COMMIT_REF_NAME === "production")
        {
            webrootRelativeSnapshotFile = 'https://www.pleiar.no'+webrootRelativeSnapshotFile;
        }

        //<meta property="og:image" content="https://www.pleiar.no/fbed.jpg" />
        replaced = replaced.replace(/<meta property="og:image" content="[^"]+"\s*\/?>/g, '<meta property="og:image" content="'+webrootRelativeSnapshotFile+'" />')
                           .replace(/<meta content="[^"]+" property="og:image"\s*\/?>/g, '<meta property="og:image" content="'+webrootRelativeSnapshotFile+'" />')
                           .replace(/"\/fbed.jpg"/,webrootRelativeSnapshotFile);

        if (/fbed.jpg/.test(replaced))
        {
            die(`Failed to make og:image replacements in ${file}`);
        }
    }
    else
    {
        console.log('No snapshot: '+snapshotFile);
    }
    fs.writeFileSync(file,replaced);
    return ret;
}

function postprocessMetaTags ()
{
    const allFiles = glob.sync('./build/**/*.html');
    for(const file of allFiles)
    {
        if (!tryToHandle(file))
        {
            let ignored: boolean = false;
            // Files to ignore
            for (const ignore of ['./build/index.html','./build/404.html','./build/200.html','./build/appcache/manifest.html','./build/om/system/index.html','./build/auth/index.html','./build/auth/logout/index.html'])
            {
                const robotIgnore = ignore.replace(/\.html/,'.robot.html');
                if(file === ignore || file === robotIgnore)
                {
                    ignored = true;
                    continue;
                }
            }
            if (!ignored)
            {
                die(`ERROR: ${file}: no meta description set`);
            }
        }
    }
}

// Will decode any uri-encoded URL that react-snap has written
function urlDecode ()
{
    let maxLoop = 200;
    while(maxLoop--)
    {
        const percentFiles = glob.sync('./build/**/*%*');
        if(percentFiles.length === 0)
        {
            return;
        }
        for(const file of percentFiles)
        {
            if(fs.existsSync(file))
            {
                fs.renameSync(file, decodeURIComponent(file));
            }
        }
    }
    throw('urlDecode(): max loops detected');
}

function main ()
{
    if (!fs.existsSync('./build/200.html'))
    {
        die('Compile the site with webpack+react-snap first');
    }
    postprocessMetaTags();
    urlDecode();
}

main();
