const { run } = require("react-snap");

run({
    destination: "build/snap",
    saveAs: "png",
    viewport: {
        width: 700,
        height: 350
    },
    puppeteerArgs: [
      "--no-sandbox",
      "--disable-setuid-sandbox"
    ]
});
