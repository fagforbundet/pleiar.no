/**
 * @prettier
 */
/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

// Let flow know about our AUTH_URL, AUTH_ALLOW_URL and AUTH_DEFAULT_STATE
// globals (from webpack/data config)
declare var AUTH_URL: string;
declare var AUTH_ALLOW_URL: RegExp;
declare var AUTH_DEFAULT_STATE: boolean;

export type AuthError =
    | "JSON"
    | "PAYLOAD"
    | "CONTENTID"
    | "CONTENTIAT"
    | "TIME"
    | "NOREF"
    | "REF";

type AuthObjType = {|
    initialized: boolean,
    authenticated: boolean,
    _error: AuthError | null,
    initialize: () => void,
    isAuthenticated: () => boolean,
    invalidate: () => void,
    authenticate: (string) => boolean,
    getError: () => AuthError | null,
|};

/**
 * A basic authentication helper. It stores authentication state between sessions,
 * and performs validation of authentication (JWT) tokens. It can then be queried at
 * any point for the actual authentication state (by calling isAuthenticated).
 *
 * TODO: Verify JWT
 * TODO: Store authentication state
 * TODO: Whitelist bots
 */
const auth: AuthObjType = {
    /*
     * AUTH_DEFAULT_STATE is taken from the data config. This defines the
     * default state of any user (ie. authenticated or not). Essentially, it
     * enables or disables the authentication module. It will be `false` if the
     * default state is that someone is logged out, ie. authentication enabled.
     * It will be `true` if the default state is that someone is logged in, ie.
     * authentication disabled (everything available to everyone).
     */
    initialized: AUTH_DEFAULT_STATE,
    authenticated: AUTH_DEFAULT_STATE,
    _error: null,

    /**
     * This method initializes an auth object. It will load auth state (if any).
     */
    initialize() {
        auth.initialized = true;
        if (window.localStorage.pau !== undefined && window.localStorage.pau) {
            // FIXME: typing
            let state: {||};
            try {
                state = JSON.parse(atob(window.localStorage.pau));
            } catch (e) {
                return;
            }
            // Current time in unix time
            const now = Math.round(new Date().getTime() / 1000);

            if (state.date < now + 1 && state.date > now - 31536000) {
                auth.authenticated = true;
            }
        }
        // Authentication whitelists for bots and react-snap
        if (
            /(googlebot|yahoo|bingbot|baiduspider|yandex|yeti|yodaobot|gigabot|ia_archiver|facebookexternalhit|twitterbot|developers\.google\.com|ReactSnap)/i.test(
                navigator.userAgent,
            )
        ) {
            auth.authenticated = true;
        }
    },

    /**
     * Checks if the current user is authenticated or not. Returns `true` if the user is
     * authenticated, `false` otherwise.
     */
    isAuthenticated(): boolean {
        if (auth.initialized === false) {
            auth.initialize();
        }
        return auth.authenticated;
    },

    /**
     * Invalidates the current login, regardless of current state
     */
    invalidate() {
        if (auth.initialized === false) {
            auth.initialize();
        }
        auth.authenticated = false;
        delete window.localStorage.pau;
        delete window.localStorage.pleiarAuthRedirect;
    },

    /**
     * Tries to authenticate a user with the given token. If the token validates, the user
     * is treated as logged in, otherwise the user is treated as logged out. Returns `true`
     * if the user was logged in, `false` otherwise.
     */
    authenticate(token: string): boolean {
        if (AUTH_DEFAULT_STATE === true) {
            return true;
        }
        if (auth.initialized === false) {
            auth.initialize();
        }
        auth.authenticated = false;
        /* JWT can be verified by:  JWS.verifyJWT(token,{ b64: '...' }, { alg: [ 'HS256' ], }); */
        let payloadObj: {||};
        try {
            payloadObj = JSON.parse(atob(token.split(".")[1]));
        } catch (e) {
            auth._error = "JSON";
            return false;
        }
        if (
            payloadObj === undefined ||
            payloadObj === null ||
            typeof payloadObj !== "object"
        ) {
            auth._error = "PAYLOAD";
            return false;
        }
        // Needs a valid memberId
        if (
            payloadObj.memberId === null ||
            payloadObj.memberId === undefined ||
            payloadObj.memberId.length === 0 ||
            !/^\d+$/.test(payloadObj.memberId)
        ) {
            auth._error = "CONTENTID";
            return false;
        }
        // Current time in unix time
        const now = Math.round(new Date().getTime() / 1000);
        const minTime = now - 600;
        const maxTime = now + 600;
        if (payloadObj.iat === null || payloadObj.iat === undefined) {
            auth._error = "CONTENTIAT";
            return false;
        }
        const payloadIat = Number.parseInt(payloadObj.iat);
        if (Number.isNaN(payloadIat)) {
            auth._error = "CONTENTIAT";
            return false;
        }
        if (payloadIat > maxTime || payloadIat < minTime) {
            auth._error = "TIME";
            return false;
        }
        const UA = window.navigator.userAgent;
        const SafariITP =
            document.referrer === "" &&
            /AppleWebKit.*Safari/.test(UA) &&
            !/(AppleWebKit\/537\.36|Chrome)/.test(UA);
        // Finally, validate referrer
        if (document.referrer === "" && !SafariITP) {
            auth._error = "NOREF";
            return false;
        }
        if (
            document.referrer.indexOf(AUTH_URL) !== 0 &&
            !AUTH_ALLOW_URL.test(document.referrer) &&
            !SafariITP
        ) {
            auth._error = "REF";
            return false;
        }
        auth.authenticated = true;
        auth._error = null;
        window.localStorage.pau = btoa(JSON.stringify({ date: now }));
        return true;
    },

    /**
     * Returns the error that caused authenticate() not to succeed, or null
     * if 1) the user is authenticated, or 2) we don't have an error.
     *
     * The error is of the type AuthError (or null)
     */
    getError(): AuthError | null {
        if (auth.isAuthenticated()) {
            return null;
        }
        return auth._error;
    },
};

export default auth;
