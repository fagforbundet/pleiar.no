/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// @flow

const featureDetection = {
    /**
     * This performs feature detection on an element. It checks if a "token list"
     * (like the a element's rel= attribute) supports a specific value, for
     * instance if it supports rel="noopener".
     */
    tokenSupports (source: DOMTokenList, check: string): boolean
    {
        let supported: boolean = false;
        // We use $UnsafeRecast to work around flow not knowing about source.supports()
        source = (source: $UnsafeRecast);
        try
        {
            if(typeof(source.supports) === "function")
            {
                supported = source.supports(check);
            }
        }
        catch(err) {} // eslint-disable-line
        return supported;
    },

    /**
     * Performs rel="" feature detection on the anchor element
     */
    aSupportsRel(check: string): boolean
    {
        // We use $UnsafeRecast to work around flow not knowing about element.relList
        const element = (document.createElement('a'): $UnsafeRecast);
        if(element.relList !== undefined)
        {
            return featureDetection.tokenSupports(element.relList, check);
        }
        return false;
    }
};

export default featureDetection;
