/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import type { Reducer } from 'redux';

/**
 * Age groups for nutricalcreq
 */
export type nutriCalcReqAgeGroup = '18-30' | '31-69' | '70+';
/**
 * Patient states for nutricalcreq
 */
export type nutriCalcReqPatientState = 'bed' | 'up' | 'building';
/**
 * Fields for nutricalcreq that accepts non-boolean values
 */
export type nutriCalcSetFields = 'ageGroup' | 'patientState' | 'temp';
/**
 * Fields for nutricalcreq that accepts boolean values
 */
export type nutriCalcBoolFields = 'febrile' | 'skinny';
/**
 * nutriCalcReq redux state
 */
export type nutriCalcReqState = {|
    ageGroup: nutriCalcReqAgeGroup,
    patientState: nutriCalcReqPatientState,
    febrile: boolean,
    skinny: boolean,
    temp: number
|};

/**
 * nutriCalcReq actions that toggle boolean fields
 */
export type nutriCalcReqToggleAction =
  | {| type: 'NUTRICALC_REQ_TOGGLE', field: 'febrile' |}
  | {| type: 'NUTRICALC_REQ_TOGGLE', field: 'skinny' |};
/**
 * Actions accepted by the nutriCalcReq redux reducer
 */
export type nutriCalcReqAction =
  | nutriCalcReqToggleAction
  | {| type: 'NUTRICALC_REQ_SET', field: 'ageGroup', value: nutriCalcReqAgeGroup |}
  | {| type: 'NUTRICALC_REQ_SET', field: 'patientState', value: nutriCalcReqPatientState |}
  | {| type: 'NUTRICALC_REQ_SET', field: 'temp', value: number |};


const initialState: nutriCalcReqState = {
    ageGroup: '31-69',
    patientState: 'up',
    febrile: false,
    skinny: false,
    temp: 38.0
};

const nutriCalcReq: Reducer<nutriCalcReqState,nutriCalcReqAction> = (state: nutriCalcReqState = initialState,action: nutriCalcReqAction): nutriCalcReqState =>
{
    switch(action.type)
    {
    case 'NUTRICALC_REQ_SET':
        {
            if (initialState[action.field] === undefined)
            {
                throw('nutriCalcReq: NUTRICALC_REQ_SET: unknown field: '+action.field);
            }
            const stateUpdate = {...state};
            // $FlowFixMe[incompatible-type]: nutriCalcReq should be rewritten with non-generic setters (#116)
            stateUpdate[action.field] = action.value;
            return {
                ...state,
                ...stateUpdate
            };
        }
    case 'NUTRICALC_REQ_TOGGLE':
        {
            if (initialState[action.field] === undefined)
            {
                throw('nutriCalcReq: NUTRICALC_REQ_TOGGLE: unknown field: '+action.field);
            }
            const stateUpdate = {};
            stateUpdate[action.field] = !state[action.field];
            // $FlowFixMe[incompatible-exact]: nutriCalcReq should be rewritten with non-generic setters (#116)
            return {
                ...state,
                ...stateUpdate
            };
        }
    default:
        return state;
    }
};

export default nutriCalcReq;

