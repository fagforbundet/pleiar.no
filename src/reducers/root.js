/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import { combineReducers } from 'redux';
import type { Reducer } from 'redux';
import nutriCalc from './nutricalc';
import medCalc from './medcalc';
import externalResources from './external-resources';
import dictionary from './dictionary';
import globalSearch from './global-search';
import global from './global';
import news from './news';
import medSynonyms from './med-synonyms';

import type { nutriCalcState} from './nutricalc';
import type { medCalcState} from './medcalc';
import type { externalResourcesState } from './external-resources';
import type { dictionaryState } from './dictionary';
import type { globalSearchState } from './global-search';
import type { globalState } from './global';
import type { newsState } from './news';
import type { medSynonymsState } from './med-synonyms';

/**
 * Global pleiar redux state
 */
export type pleiarReduxState = {|
    nutriCalc: nutriCalcState,
    medCalc: medCalcState,
    externalResources: externalResourcesState,
    dictionary: dictionaryState,
    globalSearch: globalSearchState,
    news: newsState,
    medSynonyms: medSynonymsState,
    global: globalState
|};

const pleiarStore: Reducer<pleiarReduxState,{| |}> = combineReducers({
    nutriCalc,
    medCalc,
    externalResources,
    dictionary,
    globalSearch,
    news,
    medSynonyms,
    global
});

export default pleiarStore;
