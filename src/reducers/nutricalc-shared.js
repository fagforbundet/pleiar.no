/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import type { Reducer } from 'redux';
/**
 * Shared nutricalc redux state
 */
export type nutriCalcSharedState = {|
    weight: number | ""
|};
/**
 * Actions accepted by the nutriCalcShared redux reducer
 */
export type nutriCalcSharedAction = {|
    type: 'NUTRICALC_SHARED_SET_WEIGHT',
    value: number | ""
|};
const initialState: nutriCalcSharedState = {
    weight: 75
};

const nutriCalcShared: Reducer<nutriCalcSharedState,nutriCalcSharedAction> = (state: nutriCalcSharedState = initialState,action: nutriCalcSharedAction): nutriCalcSharedState =>
{
    switch(action.type)
    {
    case 'NUTRICALC_SHARED_SET_WEIGHT':
        {
            return {
                ...state,
                weight: action.value
            };
        }
    default:
        return state;
    }
};

export default nutriCalcShared;

