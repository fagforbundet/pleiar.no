/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import type { Reducer } from 'redux';

/**
 * The nutricalc bmi redux state
 */
export type nutriCalcBMIState = {|
    height: number | ""
|};
/**
 * Actions accepted by the nutriCalcBMI reducer
 */
export type nutriCalcBMIAction = {|
    type: 'NUTRICALC_BMI_SET_HEIGHT',
    value: number | ""
|};
const initialState: nutriCalcBMIState = {
    height: 180
};

const nutriCalcBMI: Reducer<nutriCalcBMIState,nutriCalcBMIAction> = (state: nutriCalcBMIState = initialState,action: nutriCalcBMIAction): nutriCalcBMIState =>
{
    switch(action.type)
    {
    case 'NUTRICALC_BMI_SET_HEIGHT':
        {
            return {
                ...state,
                height: action.value
            };
        }
    default:
        return state;
    }
};

export default nutriCalcBMI;

