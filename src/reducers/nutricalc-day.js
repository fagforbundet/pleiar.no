/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import type { Reducer } from 'redux';
import data from '../../data/datasett/nutricalc-data.json';
import type { NutriCalcDayLineState } from '../react-components/nutricalc-day';

/**
 * T he nutricalc-day redux state
 */
export type nutriCalcDayState = {|
    +isFoodListOpen: boolean,
    +entries: Array<NutriCalcDayLineState>,
    +minimum: number | ""
|};

/**
 * Actions accepted by the nutriCalcDay redux reducer
 */
export type nutriCalcDayAction =
    | {| type: 'NUTRICALC_DAY_TOGGLE_FOODLIST' |}
    | {| type: 'NUTRICALC_DAY_SET_ENTRY_VALUE', entryNo: number, key: string, val: string |}
    | {| type: 'NUTRICALC_DAY_SET_MINIMUM', minimum: number | "" |}
    | {| type: 'NUTRICALC_DAY_REMOVE_ENTRY', entryNo: number |}
    | {| type: 'NUTRICALC_DAY_RESET' |};

const initialState: nutriCalcDayState = {
    isFoodListOpen: false,
    entries: [],
    minimum: 2400
};

const nutriCalcDay: Reducer<nutriCalcDayState,nutriCalcDayAction> = (state: nutriCalcDayState= initialState,action: nutriCalcDayAction): nutriCalcDayState =>
{
    switch(action.type)
    {
    case 'NUTRICALC_DAY_TOGGLE_FOODLIST':
        return {
            ...state,
            isFoodListOpen: !state.isFoodListOpen
        };
    case 'NUTRICALC_DAY_SET_ENTRY_VALUE':
        {
            const { entryNo, key, val } = action;
            const entries: Array<NutriCalcDayLineState> = state.entries.slice();
            // If this entry doesn't exist, we add defaults for it
            if(entries[entryNo] === undefined)
            {
                entries[entryNo] = {food: "", kcal:0,custom:false,items:1};
            }
            // Reads the data from the entry
            const curE = entries[entryNo];
            // Changes the field requested
            curE[key] = val;
            // If the field is for food, we handle custom labels specifically
            if(key == 'food')
            {
                if(val == data.customLabel)
                {
                    curE.custom = true;
                    curE['food'] = '';
                }
                else if(curE.custom != true)
                {
                    curE['kcal'] = data.basicGroups[val].kcal;
                }
            }
            return {
                    ...state,
                    entries
                };
        }
    case 'NUTRICALC_DAY_SET_MINIMUM':
        return {
            ...state,
            minimum: action.minimum
        };
    case 'NUTRICALC_DAY_REMOVE_ENTRY':
        {
            return {
            ...state,
                entries: [
                    ...state.entries.slice(0, action.entryNo),
                    ...state.entries.slice(action.entryNo+ 1)
                ]
            };
        }
    case 'NUTRICALC_DAY_RESET':
        return initialState;
    default:
        return state;
    }
};

export default nutriCalcDay;
