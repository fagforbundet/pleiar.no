/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import { combineReducers } from 'redux';
import type { Reducer } from 'redux';
import infusion from './medcalc-infusion';
import type { medCalcInfusionState } from './medcalc-infusion';

/**
 * Medcalc redux state
 */
export type medCalcState = {|
    infusion: medCalcInfusionState
|};

const medCalc: Reducer<medCalcState, {| |}> = combineReducers({
    infusion
});

export default medCalc;
