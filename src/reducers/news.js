/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

/**
 * This is an object that is used to build the union in {@link BPValuesType}. It is
 * also used to build {@link BPValues}, which is probably what you're after.
 */
const BPValuesList = {
    'under91': 'under91',
    '91-100' : '91-100',
    '101-110': '101-110',
    '111-219': '111-219',
    'over219': 'over219',
};

/**
 * Type for BP values
 */
export type BPValuesType = $Keys<typeof BPValuesList>;

/*
 * You can use this to refine a value and make sure it is one of the elements in
 * BPValuesType. If a BPValues[string] === 1, then it is a valid value for
 * BPValuesType.
 */
export const BPValues: { [key: string]: ?BPValuesType} = BPValuesList;

/**
 * This is an object that is used to build the union in {@link pulseValuesType}. It is
 * also used to build {@link pulseValues}, which is probably what you're after.
 */
const pulseValuesList ={
    'under41': 'under41',
    '41-50'  : '41-50',
    '51-90'  : '51-90',
    '91-110' : '91-110',
    '111-130': '111-130',
    'over130': 'over130',
};

/**
 * Type for pulse values
 */
export type pulseValuesType = $Keys<typeof pulseValuesList>;

/*
 * You can use this to refine a value and make sure it is one of the elements in
 * pulseValuesType. If a pulseValues[string] === 1, then it is a valid value for
 * pulseValuesType.
 */
export const pulseValues: { [key: string]: ?pulseValuesType} = pulseValuesList;

/**
 * This is an object that is used to build the union in {@link tempValuesType}. It is
 * also used to build {@link tempValues}, which is probably what you're after.
 */
const tempValuesList = {
    'under35.1': 'under35.1',
    '35.1-36.0': '35.1-36.0',
    '36.1-38.0': '36.1-38.0',
    '38.1-39.0': '38.1-39.0',
    'over39.0' : 'over39.0',
};

/**
 * Type for temp values
 */
export type tempValuesType = $Keys<typeof tempValuesList>;

/*
 * You can use this to refine a value and make sure it is one of the elements in
 * tempValuesType. If a tempValues[string] === 1, then it is a valid value for
 * tempValuesType.
 */
export const tempValues: { [key: string]: ?tempValuesType} = tempValuesList;

/**
 * This is an object that is used to build the union in {@link RFValuesType}. It is
 * also used to build {@link RFValues}, which is probably what you're after.
 */
const RFValuesList = {
    'under9': 'under9',
    '9-11'  : '9-11',
    '12-20' : '12-20',
    '21-24' : '21-24',
    'over24': 'over24',
};

/**
 * Type for RF values
 */
export type RFValuesType = $Keys<typeof RFValuesList>;

/*
 * You can use this to refine a value and make sure it is one of the elements in
 * RFValuesType. If a RFValues[string] === 1, then it is a valid value for
 * RFValuesType.
 */
export const RFValues: { [key: string]: ?RFValuesType} = RFValuesList;

/**
 * This is an object that is used to build the union in {@link SpO2ValuesType}. It is
 * also used to build {@link SpO2Values}, which is probably what you're after.
 */
const SpO2ValuesList = {
    'under92'   : 'under92',
    '92-93'     : '92-93',
    '94-95'     : '94-95',
    'over95'    : 'over95',

    // Alternate
    'under84'   : 'under84',
    '84-85'     : '84-85',
    '86-87'     : '86-87',
    '88-92'     : '88-92',
    '93-94'     : '93-94',
    '94-96'     : '94-96',
    'over96'    : 'over96',
};

/**
 * Type for SpO2 values
 */
export type SpO2ValuesType = $Keys<typeof SpO2ValuesList>;

/*
 * You can use this to refine a value and make sure it is one of the elements in
 * SpO2ValuesType. If a SpO2Values[string] === 1, then it is a valid value for
 * SpO2ValuesType.
 */
export const SpO2Values: { [key: string]: ?SpO2ValuesType} = SpO2ValuesList;

/**
 * This is an object that is used to build the union in {@link
 * consciousnessValuesType}. It is also used to build {@link
 * consciousnessValues}, which is probably what you're after.
 */
export const consciousnessValuesList = {
    'awake'   : 'awake',
    'confused': 'confused',
    'speech'  : 'speech',
    'pain'    : 'pain',
    'none'    : 'none',
};

/**
 * Type for consciousness state
 */
export type consciousnessValuesType = $Keys<typeof consciousnessValuesList>;

/*
 * You can use this to refine a value and make sure it is one of the elements
 * in consciousnessValuesType. If a consciousnessValues[string] === 1, then it
 * is a valid value for consciousnessValuesType.
 */
export const consciousnessValues: { [key: string]: ?consciousnessValuesType} = consciousnessValuesList;

/**
 * The news redux state
 */
export type newsState = {|
    BP              : BPValuesType            | null,
    pulse           : pulseValuesType         | null,
    temp            : tempValuesType          | null,
    RF              : RFValuesType            | null,
    SpO2            : SpO2ValuesType          | null,
    consciousness   : consciousnessValuesType | null,
    usingO2         : boolean,
    alternateO2Table: boolean,
|};
/**
 * Actions accepted by the news reducer
 */
export type newsAction = {|
    type         : 'NEWS_SET_BP',
    BP           : BPValuesType              | null
|} | {|
    type         : 'NEWS_SET_PULSE',
    pulse        : pulseValuesType           | null
|} | {|
    type         : 'NEWS_SET_TEMP',
    temp         : tempValuesType            | null
|} | {|
    type         : 'NEWS_SET_RF',
    RF           : RFValuesType              | null
|} | {|
    type         : 'NEWS_SET_SPO2',
    SpO2         : SpO2ValuesType            | null
|} | {|
    type         : 'NEWS_SET_CONSCIOUSNESS',
    consciousness: consciousnessValuesType   | null
|} | {|
    type         : 'NEWS_TOGGLE_ALTERNATEO2TABLE'
|} | {|
    type         : 'NEWS_TOGGLE_O2'
|};

const initialState: newsState = {
    BP              : null,
    pulse           : null,
    temp            : null,
    RF              : null,
    SpO2            : null,
    consciousness   : null,
    usingO2         : false,
    alternateO2Table: false,
};

function news (state: newsState = initialState,action: newsAction): newsState
{
    switch(action.type)
    {
        case 'NEWS_SET_BP':
        return {
                ...state,
                BP: action.BP
            };
        case 'NEWS_SET_PULSE':
        return {
                ...state,
                pulse: action.pulse
            };
        case 'NEWS_SET_TEMP':
        return {
                ...state,
                temp: action.temp
            };
        case 'NEWS_SET_RF':
        return {
                ...state,
                RF: action.RF
            };
        case 'NEWS_SET_SPO2':
        return {
                ...state,
                SpO2: action.SpO2
            };
        case 'NEWS_SET_CONSCIOUSNESS':
        return {
                ...state,
                consciousness: action.consciousness
            };
        case 'NEWS_TOGGLE_O2':
            return {
                ...state,
                usingO2: !state.usingO2
            };
        case 'NEWS_TOGGLE_ALTERNATEO2TABLE':
            return {
                ...state,
                alternateO2Table: !state.alternateO2Table
            };
        default:
            return state;
    }
}
export default news;
