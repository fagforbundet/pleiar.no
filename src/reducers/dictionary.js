/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
/**
 * The dictionary redux state
 */
export type dictionaryState = {|
    search: string,
|};
/**
 * Actions accepted by the dictionary reducer
 */
export type dictionaryAction = {| type: 'DICT_SET_SEARCH_STRING', search: string |};

const initialState: dictionaryState = {
    search: '',
};

function dictionary (state: dictionaryState = initialState,action: dictionaryAction): dictionaryState
{
    switch(action.type)
    {
    case 'DICT_SET_SEARCH_STRING':
        return {
                ...state,
                search: action.search
            };
    default:
        return state;
    }
}
export default dictionary;
