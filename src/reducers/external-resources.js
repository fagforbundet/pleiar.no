/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
/**
 * The external resources redux state
 */
export type externalResourcesState = {|
    +search: string,
    +onlyNorwegian: boolean,
|};
/**
 * Actions accepted by the external resources reducer
 */
export type externalResourcesAction =
    | {| type: 'EXT_RES_SET_SEARCH_STRING', search: string |}
    | {| type: 'EXT_RES_TOGGLE_ONLYNORWEGIAN' |};

const initialState: externalResourcesState = {
    search: '',
    onlyNorwegian: false,
};

function externalResources (state: externalResourcesState = initialState,action: externalResourcesAction): externalResourcesState
{
    switch(action.type)
    {
    case 'EXT_RES_SET_SEARCH_STRING':
        return {
                ...state,
                search: action.search
            };
    case 'EXT_RES_TOGGLE_ONLYNORWEGIAN':
        return {
                ...state,
                onlyNorwegian: !state.onlyNorwegian
            };
    default:
        return state;
    }
}
export default externalResources;
