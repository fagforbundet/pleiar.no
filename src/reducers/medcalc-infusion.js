/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import type { Reducer } from 'redux';

/**
 * The medcalc redux infusion state
 */
export type medCalcInfusionState = {|
    +content: number | '',
    +hours: number | '',
    +minutes: number | '',
    +mode: "ml/t" | "dråpar/sek",
    +additionalCalculations: boolean
|};

/**
 * Actions accepted by the medcalc infusion reducer
 */
export type medCalcInfusionAction =
    | {| type: 'MEDCALC_INFUSION_SET_CONTENT', content: number | '' |}
    | {| type: 'MEDCALC_INFUSION_SET_HOURS', hours: number | '' |}
    | {| type: 'MEDCALC_INFUSION_SET_MINUTES', minutes: number | '' |}
    | {| type: 'MEDCALC_INFUSION_SET_MODE', mode: 'ml/t' | "dråpar/sek" |}
    | {| type: 'MEDCALC_INFUSION_TOGGLE_ADDITIONAL_CALCULATIONS' |};

const initialState: medCalcInfusionState = {
    // This holds the amount of liquid to be infused
    content:1000,
    // The number of hours to spend on the infusion
    hours:1,
    // The number of minutes to spend on the infusion
    minutes:0,
    // Which mode the calculator is in
    mode: "ml/t",
    // If "additional calculations" are displayed (if any)
    additionalCalculations: false
};

const medCalcInfusion: Reducer<medCalcInfusionState, medCalcInfusionAction> = (state: medCalcInfusionState = initialState,action: medCalcInfusionAction): medCalcInfusionState =>
{
    switch(action.type)
    {
    case 'MEDCALC_INFUSION_SET_CONTENT':
        return {
            ...state,
            content: action.content
        };
    case 'MEDCALC_INFUSION_SET_HOURS':
        return {
            ...state,
            hours: action.hours
        };
    case 'MEDCALC_INFUSION_SET_MINUTES':
        return {
            ...state,
            minutes: action.minutes
        };
    case 'MEDCALC_INFUSION_SET_MODE':
        return {
            ...state,
            mode: action.mode
        };
    case 'MEDCALC_INFUSION_TOGGLE_ADDITIONAL_CALCULATIONS':
        return {
            ...state,
            additionalCalculations: !state.additionalCalculations
        };
    default:
        return state;
    }
};

export default medCalcInfusion;
