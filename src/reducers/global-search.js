/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
/**
 * The global search redux state
 */
export type globalSearchState = {|
    search: string,
|};
/**
 * The actions accepted by the global search redux reducer
 */
export type globalSearchAction = {| type: 'GLOBAL_SEARCH_SET_SEARCH_STRING', search: string |};
const initialState: globalSearchState = {
    search: '',
};

function globalSearch (state: globalSearchState = initialState,action: globalSearchAction): globalSearchState
{
    switch(action.type)
    {
    case 'GLOBAL_SEARCH_SET_SEARCH_STRING':
        return {
                ...state,
                search: action.search
            };
    default:
        return state;
    }
}
export default globalSearch;
