/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import externalResources from '../data/datasett/external-resources.json';
import dictionary from '../data/datasett/dictionary.json';
import labValues from '../data/datasett/lab.json';
import nutrition from '../data/datasett/nutricalc-data.json';
import external from '../data/externallyIndexed.json';
import searchMetadata from '../data/search-meta.json';
import lunr from './helper.lunr';
import lunrResultSnippet from '../src/helper.lunrSnippet';
import type { DictionaryEntry, ExternalResourcesEntry, LabValueEntry, ToolListEntry, NutritionInfoEntry, ExternallyIndexedItem, SearchMetadataEntry } from './types/data';
import type { lunrResult, lunrMetadata } from './types/libs';
import { HandbookEntry } from './data.handbook';
import { QuickRefEntry } from './data.quickref';
import { removeMarkdown } from './helper.markdown';
import ToolsList from './tools.list';
import { numbers } from './helper.numbers';
import { asyncLoaderRole } from './helper.asyncLoader';

/**
 * A single entry in the search index
 */
export type IndexEntry = {|
    id: string,
    tittel?: string,
    body?: string,
    keywords?: string,
    url?: string
|};

/**
 * A single result from getResultFromRef
 */
export type ResolvedLunrRef =
{|
    type: "dictionary",
    dictionary: DictionaryEntry
|} |
{|
    type: "externalResources",
    externalResources: ExternalResourcesEntry,
|} |
{|
    type: "labValue",
    labValues: LabValueEntry
|} |
{|
    type: "handbook",
    handbook: HandbookEntry
|} |
{|
    type: "tool",
    tool: ToolListEntry
|} |
{|
    type: "nutrition",
    nutrition: NutritionInfoEntry,
|} |
{|
    type: "external",
    external: ExternallyIndexedItem
|} |
{|
    type: "quickref",
    quickref: QuickRefEntry
|} |
{|
    type: "quiz",
    quiz: SearchMetadataEntry, 
|};

/**
 * The signature required from a reducer function provided to
 * `PleiarSearcher.search`
 */
type SearchReducer = (accumulator: Array<lunrResult>, result: lunrResult ) => Array<lunrResult>;

type LunrType = typeof lunr ;

/**
 * PleiarSearcher is the indexer for the entire site. It wraps `lunr`, performs
 * indexing and lets you search this index. It also adds some automatic
    * fuzzyness to the searcher, which gets applied if the search has no hits
    * as-written.
 */
const PleiarSearcher = {
    ...asyncLoaderRole,

    __index: null,

    /**
     * Private method that returns our `lunr` instance. If `initialize` has not been called
     * this will throw().
     */
    _index (): LunrType
    {
        if(PleiarSearcher.__index !== null)
        {
            return PleiarSearcher.__index;
        }

        throw('PleiarSearcher called without first performing initialization');
    },

    /**
     * _performAsyncImport method for {@link asyncLoaderRole}
     */
    _performAsyncImport (): Promise<null>
    {
        return import(/* webpackChunkName: "search-index", webpackPreload: true */ '../data/search-index.json');
    },

    /**
     * _loadedData method for {@link asyncLoaderRole}
     */
    _loadedData
    // $FlowIssue[signature-verification-failure] - This is data imported for lunr, we don't have a type for it
    (data): null
    {
        PleiarSearcher.__index = lunr.Index.load(data);
        return null;
    },

    /**
     * Retrieves the actual data for a single result `ref`. Optionally
     * accepts the associated `lunrMetadata` as well, which is used for
     * `handbook` results to build an excerpt from the document.
     */
    getResultFromRef(ref: string, metadata?: lunrMetadata | null): ResolvedLunrRef
    {
        const refParts = ref.split('/');
        if(refParts[0] === "dictionary")
        {
            return {
                type: refParts[0],
                dictionary: (dictionary[ refParts[1] ]: DictionaryEntry),
            };
        }
        else if(refParts[0] === "externalResources")
        {
            return {
                type: refParts[0],
                externalResources: (externalResources[ refParts[1] ]: ExternalResourcesEntry),
            };
        }
        else if(refParts[0] === "labValue")
        {
            return {
                type: refParts[0],
                labValues: (labValues.values[ refParts[1] ]: LabValueEntry)
            };
        }
        else if(refParts[0] === "handbook")
        {
            refParts.shift();
            const handbook = new HandbookEntry(refParts);
            if (!handbook.exists)
            {
                throw('Wanted to look up handbook entry that does not exist: '+refParts.join('/'));
            }
            if(metadata === undefined || metadata === null)
            {
                throw('PleiarSearcher.getResultFromRef: fetching handbook entries requires metadata');
            }
            const bodyString = removeMarkdown(handbook.content);
            handbook.searchSnippet = lunrResultSnippet(bodyString,metadata);
            return {
                type: "handbook",
                handbook: handbook
            };
        }
        else if(refParts[0] === "tool")
        {
            const category = numbers.parseFloat(refParts[1]);
            const component = numbers.parseFloat(refParts[2]);
            let tool: ToolListEntry;
            if ( ToolsList[ category ] === undefined)
            {
                throw('Wanted to look up tools entry where the category does not exist: '+refParts.join('/'));
            }
            if ( ToolsList[ category ].tools[ component ] === undefined)
            {
                throw('Wanted to look up tools entry where the tools entry does not exist: '+refParts.join('/'));
            }
            else
            {
                tool = ToolsList[ category ].tools[ component ];
            }
            return {
                type: "tool",
                tool
            };
        }
        else if(refParts[0] === "nutrition")
        {
            const rest = ref.substr(10);
            return {
                type: "nutrition",
                nutrition: (nutrition.basicGroups[rest]: NutritionInfoEntry)
            };
        }
        else if(refParts[0] === "quiz")
        {
            const rest = ref.substr(5);
            return {
                type: "quiz",
                quiz: (searchMetadata.quiz[rest]: SearchMetadataEntry),
            };
        }
        else if(refParts[0] === "external")
        {
            const rest = ref.substr(9);
            return {
                type: "external",
                external: (external[rest]: ExternallyIndexedItem)
            };
        }
        else if(refParts[0] === "quickref")
        {
            refParts.shift();
            const quickref = new QuickRefEntry(refParts);
            if (!quickref.exists)
            {
                throw('Wanted to look up quickref entry that does not exist: '+refParts.join('/'));
            }
            if(metadata === undefined || metadata === null)
            {
                throw('PleiarSearcher.getResultFromRef: fetching quickref entries requires metadata');
            }
            const bodyString = removeMarkdown(quickref.content);
            quickref.searchSnippet = lunrResultSnippet(bodyString,metadata);
            return {
                type: "quickref",
                quickref: quickref
            };
        }
        else
        {
            throw('searcher.getResultFromRef: '+ref+': unrecognized ref type "'+refParts[0]+'"');
        }
    },

    /**
     * Perform a search. Searches for string, and optionally applies a reducer
     * to each result before returning.
     */
    search (expression: string, reducer?: SearchReducer): Array<lunrResult>
    {
        /*
         * lunr has a problem when searching for ÆØÅ-prefixed strings. Therefore
         * we apply this hack as a workaround, permitting it to be a bit more
         * fuzzy in its matching.
         *
         * There's a test for this in tests/dictionary.data.test.js that will start
         * failing once the problem in lunr is fixed.
         */
        if (/^[ÆØÅæøå]/.test(expression))
        {
            expression = '*'+expression;
        }

        const result = PleiarSearcher._index().search(expression);
        if(reducer)
        {
            reducer = reducer.bind(PleiarSearcher);
            return result.reduce(reducer,[]);
        }
        return result;
    },

    /**
     * A safe variant of searching that handles cases where the user has
     * entered an invalid expression, which would usually cause `lunr` to
     * `throw`. This will instead remove any special characters from the search
     * expression and try again.  It can also apply a custom function to modify
     * the expression before it gets run.
     */
    _safeSearch(expression: string, reducer?: SearchReducer, expressionPreparer?: (string) => string): Array<lunrResult>
    {
        let searchExp: string = expression;
        if(expressionPreparer)
        {
            searchExp = expressionPreparer( searchExp );
        }
        let result: Array<lunrResult>;
        try
        {
            result = PleiarSearcher.search(searchExp, reducer);
        }
        catch(e)
        {
            searchExp = expression.replace(/[^\w\s]/g,'');
            if(expressionPreparer)
            {
                searchExp = expressionPreparer( searchExp );
            }
            result = PleiarSearcher.search(searchExp, reducer);
        }
        return result;
    },

    /**
     * A wrapper for `_safeSearch` that adds increasing levels of fuzzyness to the
     * result if it yields no results.
     */
    _safeFuzzySearch(expression: string, reducer?: SearchReducer): Array<lunrResult>
    {
        for(let fuzzyness: number = 0; fuzzyness < 4; fuzzyness++)
        {
            const searchExp = PleiarSearcher._mkFuzzySearch(expression,fuzzyness);
            const result = PleiarSearcher._safeSearch(searchExp,reducer,(str) =>
            {
                return PleiarSearcher._mkFuzzySearch(str, fuzzyness);
            });
            if(result.length > 0)
            {
                return result;
            }
        }
        return [];
    },

    /**
     * Applies fuzzyness of the level supplied to the expression, then returns
     * it.
     */
    _mkFuzzySearch (expression: string, fuzzyness: number): string
    {
        const components = expression.split(/\s+/);
        const result = [];
        for(let component: string of components)
        {
            if (/^\w/.test(component))
            {
                if(fuzzyness === 0)
                {
                    component = '*'+component+'*';
                }
                else
                {
                    component = component+'~'+fuzzyness;
                }
            }
            result.push(component);
        }
        return result.join(' ');
    },

    /**
     * This is a safe search method (won't `throw`) that also applies fuzzyness
     * if needed. Accepts a `reducer` that it will pass on down the chain to
    * `search()` if needed.
     */
    safePartialSearch(expression: string, reducer?: SearchReducer): Array<lunrResult>
    {
        let result: Array<lunrResult> = PleiarSearcher._safeSearch( expression, reducer );
        if(result.length == 0)
        {
            result = PleiarSearcher._safeFuzzySearch( expression, reducer );
        }
        return result;
    },
};

export default PleiarSearcher;
