/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import lunr from './helper.lunr.js';
import { removeMarkdown } from './helper.markdown.js';
import type {
    QuizDataContainer,
    DictionaryEntry,
    ExternalResourcesEntry,
    LabValueEntry,
    HandbookDataEntry,
    HandbookDataStructure,
    LabValueDataStructure,
    DictionaryDataStructure,
    ExternalResourcesDataStructure,
    ToolsListDataStructure,
    NutritionDataStructure,
    ExternallyIndexedList
} from './types/data';

/**
 * A single entry in the search index
 */
export type IndexEntry = {|
    id: string,
    tittel?: string,
    body?: string,
    keywords?: string,
    url?: string
|};

type LunrObject = typeof lunr;

const PleiarIndexer = {
    /**
     * Method that performs the actual indexing. This has no data import of its
     * own, so you have to supply all of the datastructures yourself.
     */
    index (
        externalResources: ExternalResourcesDataStructure,
        dictionary: DictionaryDataStructure,
        labValues: LabValueDataStructure,
        handbook: HandbookDataStructure,
        tools: ToolsListDataStructure,
        nutrition: NutritionDataStructure,
        externallyIndexed: ExternallyIndexedList,
        quickref: HandbookDataStructure,
        quizData: QuizDataContainer,
    ): LunrObject
    {
        /*
         * Validate parameters
         */
        if(externalResources === undefined)
        {
            throw('Missing parameter to PleiarIndexer.index: externalResources');
        }
        else if (!Array.isArray(externalResources))
        {
            throw('Invalid parameter to PleiarIndexer.index: externalResources');
        }
        if(dictionary === undefined)
        {
            throw('Missing parameter to PleiarIndexer.index: dictionary');
        }
        else if (!Array.isArray(dictionary))
        {
            throw('Invalid parameter to PleiarIndexer.index: dictionary');
        }
        if(labValues === undefined)
        {
            throw('Missing parameter to PleiarIndexer.index: labValues');
        }
        else if(!Array.isArray(labValues.values))
        {
            throw('Invalid parameter to PleiarIndexer.index: labValues');
        }
        if(handbook === undefined)
        {
            throw('Missing parameter to PleiarIndexer.index: handbook');
        }
        else if(!Array.isArray(handbook.order))
        {
            throw('Invalid parameter to PleiarIndexer.index: handbook');
        }
        if(tools === undefined)
        {
            throw('Missing parameter to PleiarIndexer.index: tools');
        }
        else if(!Array.isArray(tools))
        {
            throw('Invalid parameter to PleiarIndexer.index: tools');
        }
        if(nutrition === undefined)
        {
            throw('Missing parameter to PleiarIndexer.index: nutrition');
        }
        else if(nutrition.basicGroups === undefined)
        {
            throw('Invalid parameter to PleiarIndexer.index: nutrition');
        }
        if(externallyIndexed === undefined)
        {
            throw('Missing parameter to PleiarIndexer.index: externallyIndexed');
        }
        else if(!Array.isArray(externallyIndexed))
        {
            throw('Invalid parameter to PleiarIndexer.index: externallyIndexed');
        }
        if(quickref === undefined)
        {
            throw('Missing parameter to PleiarIndexer.index: quickref');
        }
        else if(!Array.isArray(quickref.order))
        {
            throw('Invalid parameter to PleiarIndexer.index: quickref');
        }
        if(quizData === undefined)
        {
            throw("Missing parameter to PleiarIndexer.index: quizData");
        }
        else if(Object.keys(quizData).length === 0)
        {
            throw("Invalid parameter to PleiarIndexer.index: quizData");
        }

        const index = lunr( function ()
            {
                // The title field is most important
                this.field('tittel',{
                    boost: 2.0
                });
                this.field('body');
                // Keywords are not quite as important as title and body
                this.field('keywords',{
                    boost: 0.5
                });
                // Url is not important at all
                this.field('url',{
                    boost: 0.1
                });
                // Enable storing of positional metadata in the index
                this.metadataWhitelist = ['position'];
                // Build the index
                PleiarIndexer.constructIndex({
                    external: externalResources,
                    dict: dictionary,
                    labValues: labValues.values,
                    handbook: handbook,
                    tools: tools,
                    nutrition: nutrition,
                    externallyIndexed: externallyIndexed,
                    quickref: quickref,
                    quizData,
                    indexer: this
                });
            });
        return index;
    },

    /**
     * This constructs the actual index, providing data and title maps
     * to `indexFromSource`.
     */
    constructIndex (idxData: {|
        external: ExternalResourcesDataStructure,
        dict: DictionaryDataStructure,
        labValues: Array<LabValueEntry>,
        handbook: HandbookDataStructure,
        tools: ToolsListDataStructure,
        nutrition: NutritionDataStructure,
        externallyIndexed: ExternallyIndexedList,
        quickref: HandbookDataStructure,
        indexer: LunrObject,
        quizData: QuizDataContainer,
    |})
    {
        /* External resources */
        PleiarIndexer.indexFromSource({
            source: idxData.external,
            indexer: idxData.indexer,
            titleField: "title",
            descriptionField: "description",
            keywordField: "keywords",
            urlField: "url",
            sourceName: "externalResources"
        });
        /* Dictionary */
        PleiarIndexer.indexFromSource({
            source: idxData.dict,
            indexer: idxData.indexer,
            descriptionMarkdown: true,
            titleField: "expression",
            descriptionField: "description",
            keywordField: "keywords",
            alternateKeywords: "seeAlso",
            sourceName: "dictionary"
        });
        /* Lab values */
        PleiarIndexer.indexFromSource({
            source: idxData.labValues,
            indexer: idxData.indexer,
            keywordField: "keywords",
            titleField: "navn",
            sourceName: "labValue",
        });
        /* Handbook entries */
        PleiarIndexer._recursiveLunrIndexFromHandbook(idxData.indexer, idxData.handbook, 'handbook');
        /* Tools */
        PleiarIndexer._indexFromTools(idxData.indexer, idxData.tools);
        /* Nutrition entries */
        PleiarIndexer._indexFromNutriData(idxData.indexer, idxData.nutrition);
        /* Externally indexed data */
        PleiarIndexer._indexFromExternal(idxData.indexer, idxData.externallyIndexed);
        /* Quickref entries */
        PleiarIndexer._recursiveLunrIndexFromHandbook(idxData.indexer, idxData.quickref, 'quickref');
        /* Quickref entries */
        PleiarIndexer._indexFromElearn(idxData.indexer, idxData.quizData );
    },

    /**
     * Adds tools to the index
     */
    _indexFromTools (idx: LunrObject, tools: ToolsListDataStructure)
    {
        for(let categoryNO: number = 0; categoryNO < tools.length; categoryNO++)
        {
            const category = tools[categoryNO];
            for(let entryNO: number = 0; entryNO < category.tools.length; entryNO++)
            {
                const entry = category.tools[entryNO];
                const doc : IndexEntry = {
                    id: 'tool/'+categoryNO+'/'+entryNO,
                    tittel: entry.title,
                    url: entry.path
                };
                if(entry.keywords)
                {
                    doc.keywords = entry.keywords;
                }
                idx.add(doc);
            }
        }
    },

    /**
     * Adds externally indexed items to the index
     */
    _indexFromExternal(idx: LunrObject, indexList: ExternallyIndexedList)
    {
        for(let indexNO: number = 0; indexNO < indexList.length; indexNO++)
        {
            const item = indexList[indexNO];
            const keywordmap = {
                'fagprat':'Fagprat, podkast',
                'other':'',
            };
            let keywords: ?string;
            if(item.keywords)
            {
                keywords = item.keywords;
            }
            else
            {
                keywords = keywordmap[item.type];
            }
            const doc : IndexEntry = {
                id: 'external/'+indexNO,
                tittel: item.title,
                url: item.url,
                body: item.text,
                keywords,
            };
            idx.add(doc);
        }
    },

    /**
     * Adds nutrition data to the index
     */
    _indexFromNutriData (idx: LunrObject, nutrition: NutritionDataStructure)
    {
        for(const ident in nutrition.basicGroups)
        {
            const entry = nutrition.basicGroups[ident];
            if(entry.name === nutrition.customLabel)
            {
                continue;
            }
            const doc : IndexEntry = {
                id: 'nutrition/'+ident,
                tittel: "",
                body: entry.name
            };
            if(doc.tittel !== ident)
            {
                doc.keywords = ident;
            }
            idx.add(doc);
        }
    },

    /**
     * Recursively indexes the handbook datastructure
     */
    _recursiveLunrIndexFromHandbook (idx: LunrObject, handbook : HandbookDataEntry | HandbookDataStructure, idxType: 'handbook' | 'quickref')
    {
        if(handbook.tree !== undefined)
        {
            for(const subentry in handbook.tree)
            {
                PleiarIndexer._recursiveLunrIndexFromHandbook(idx, handbook.tree[subentry], idxType);
            }
        }
        else
        {
            // Flow isn't very smart, if we don't have tree then this is a HandbookDataEntry,
            // not a HandbookDataStructure
            handbook = ((handbook: $UnsafeRecast): HandbookDataEntry);
            const content = removeMarkdown( handbook.content );
            const doc : IndexEntry = {
                id: idxType+'/'+handbook.path.join('/'),
                body: content,
                tittel: handbook.title
            };
            idx.add(doc);
        }
    },

    /**
     * Adds a single data source to the index, based upon the fields provided.
     */
    indexFromSource(src: {|
            source: Array<ExternalResourcesEntry> | Array<DictionaryEntry> | Array<LabValueEntry>,
            indexer: LunrObject,
            titleField: string,
            descriptionField?: string,
            descriptionMarkdown?: boolean,
            keywordField?: string,
            urlField?: string,
            sourceName: string,
            alternateKeywords?: string,
    |})
    {
        for(let no: number = 0; no < src.source.length; no++)
        {
            const entry = src.source[no];
            const id = src.sourceName+'/'+no;
            let body: string;
            if(src.descriptionField)
            {
                body = entry[ src.descriptionField ];
            }
            if (src.descriptionMarkdown && src.descriptionField)
            {
                body = removeMarkdown( entry[ src.descriptionField ] );
            }
            const doc: IndexEntry = {
                id,
                tittel: entry[ src.titleField ],
                body
            };
            if(src.keywordField)
            {
                doc.keywords = entry[ src.keywordField ];
            }
            if( src.alternateKeywords && (!doc.keywords || doc.keywords.length == 0) && Array.isArray( entry[ src.alternateKeywords ] ))
            {
                doc.keywords = entry[ src.alternateKeywords ].join(', ');
            }
            if(src.urlField)
            {
                doc.url = entry[ src.urlField ];
            }
            src.indexer.add(doc);
        }
    },


    /**
     * Adds tools to the index
     */
    _indexFromElearn(idx: LunrObject, quiz: QuizDataContainer)
    {
        for(const name of Object.keys(quiz))
        {
            const entry = quiz[name];
            const doc : IndexEntry = {
                id: 'quiz/'+name,
                tittel: entry.title,
                url: '/elaering/'+name,
            };
            if(entry.description)
            {
                doc.body = entry.description;
            }
            if(entry.keywords)
            {
                doc.keywords = entry.keywords;
            }
            idx.add(doc);
        }
    },
};

export default PleiarIndexer;
