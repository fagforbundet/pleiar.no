/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// @flow

import GithubSlugger from 'github-slugger';

/**
 * This is a global helper object that can be used to generate slugs. Uses
 * `github-slugger` to generate the slugs.
 *
 * Caveat: You must make sure that no code that you are calling is also
 * generating slugs at the same time, since you then may end up with
 * inconsistent slugs (see the docs for `reset()`.
 */
const slug = {
    __ghs: null,

    /**
     * Retrieve a single slug
     */
    get(name: string): string
    {
        if (slug.__ghs === null)
        {
            slug.__ghs = new GithubSlugger();
        }
        return slug.__ghs.slug(name,false);
    },

    /**
     * Resets the slug object. `github-slugger` has some logic that avoids ever
     * generating two identical slugs. So you should always do a .reset() before you
     * start generating slugs, so that you get consistent ones.
     */
    reset ()
    {
        if(slug.__ghs !== null)
        {
            slug.__ghs.reset();
        }
    }
};

export default slug;
