/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// @flow

import type { ToolsListDataStructure } from './types/data';

const ToolsList: ToolsListDataStructure = [
    {
        title: "Medikamenthåndtering",
        tools: [
            {
                path: '/medikamentregning/infusjon',
                title: 'Regn ut infusjonshastighet (i ml/t eller dråper/sek)',
                keywords: "milliliter infusjon dråpetakt",
            },
            {
                path: '/medikament/synonymer',
                title: 'Søk i byttelisten for medikamenter',
                keywords: 'synonymer synonymliste bytteliste generika generisk medisiner legemiddel legemidler',
            }
        ]
    },
    {
        title: "Screening",
        tools: [
            {
                path: '/news',
                title: 'NEWS 2-kalkulator',
                keywords: 'news pews mews national early warning score screening sepsis',
                description: 'Bruk screeningverktøyet NEWS 2 til å oppdage endringer i en pasient sin tilstand',
            }
        ]
    },
    {
        title: "Ernæring",
        tools: [
            {
                path: '/ernaering/dagsinntak',
                title: 'Regn ut dagsinntaket basert på kostregistrering',
                keywords: 'kcal',
            },
            {
                path: '/ernaering/energibehov',
                title: 'Regn ut energibehov',
                keywords: 'kcal',
            },
            {
                path: '/ernaering/bmi',
                title: 'Regn ut BMI',
                keywords: 'kg kilo',
            }
        ],
    },
];

export default ToolsList;
