/**
 * @flow
 * @prettier
 *
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2023
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import cloneDeep from "lodash/cloneDeep";
import shuffle from "lodash/shuffle";
import { asyncLoaderRole } from "./helper.asyncLoader";
import type {
    endSlideContent,
    quizManagerSingleAnswer,
    quizManagerQuestionContainer,
    quizRootData,
    QuizDataContainer,
} from "./types/data";

type listenerCallback = (quizManagerHandler) => void;

// eslint-disable-next-line flowtype/require-exact-type
type QuizDataManagerType = {
    _data: ?QuizDataContainer,
    data: () => QuizDataContainer,
    onInitialize: (() => mixed) => mixed,
    initialize: () => Promise<null>,
    hasInitialized: () => boolean,
};

/**
 * MedSearcher is the search wrapper for the medication synonyms data.  This is
 * different from PleiarSearcher. PleiarSearcher uses lunr to index everything
 * on the site, except for medications. *This* class uses a fairly dumb
 * method of searching through the medication list.
 */
const quizDataManager: QuizDataManagerType = {
    ...asyncLoaderRole,

    /** The data structure, a QuizDataContainer*/
    _data: null,

    /** Retrieve the quiz data */
    data(): QuizDataContainer {
        if (
            quizDataManager._data === null ||
            quizDataManager._data === undefined
        ) {
            throw "Attempt to access quiz data before it has been loaded";
        }
        return quizDataManager._data;
    },

    /**
     * _performAsyncImport method for {@link asyncLoaderRole}
     */
    _performAsyncImport(): Promise<null> {
        return import(
            /* webpackChunkName: "elearn" */ "../data/quiz/quiz.json"
        );
    },

    /**
     * _loadedData method for {@link asyncLoaderRole}
     */
    _loadedData(data: QuizDataContainer) {
        quizDataManager._data = data;
    },
};

/**
 * Quiz manager. All of the logic for a quiz.
 */
class quizManagerHandler {
    points: number = 0;
    questionList: Array<quizManagerQuestionContainer>;
    _currentQuestions: Array<quizManagerQuestionContainer>;
    _invalidList: Array<quizManagerQuestion> = [];
    _listeners: Array<listenerCallback> = [];
    _quiz: ?quizRootData = null;
    current: quizManagerQuestion | null;
    currentID: number = 0;
    totalQuestions: number = 0;
    exists: boolean;
    title: string = "";

    // eslint-disable-next-line require-jsdoc
    constructor(quiz: string) {
        if (quizDataManager.data()[quiz] === undefined) {
            this.exists = false;
            this.questionList = [];
        } else {
            this.questionList = quizDataManager.data()[quiz].quiz;
            this._quiz = quizDataManager.data()[quiz];
            this.title = quizDataManager.data()[quiz].title;
            this.exists = true;
        }
        this.totalQuestions = this.questionList.length;
        this.reset();
    }

    /**
     * Returns the number of possible points for a quiz
     */
    possiblePoints(): number {
        return this.questionList.length;
    }

    /**
     * Returns the next quizManagerQuestion or null if there are no more questions.
     */
    next(): quizManagerQuestion | null {
        this.currentID++;
        const entry = this._currentQuestions.shift();
        if (entry !== undefined) {
            this.current = new quizManagerQuestion(this, entry, this.currentID);
        } else {
            this.current = null;
        }
        this.notifyListeners();
        return this.current;
    }

    /**
     * Checks if the current quiz is done or not
     */
    isDone(): boolean {
        return this.current === null;
    }

    /**
     * Resets the quiz, restarting from the beginning
     */
    reset() {
        this._currentQuestions = cloneDeep(this.questionList);
        this.currentID = 0;
        this.points = 0;
        this.next();
    }

    /**
     * Registers a correct answer (to the supplied quizManagerQuestion)
     */
    registerCorrect(): number {
        this.notifyListeners();
        return this.points++;
    }

    /**
     * Registers an incorrect answer (to the supplied quizManagerQuestion)
     */
    registerIncorrect(question: quizManagerQuestion): number {
        this._invalidList.push(question);
        this.notifyListeners();
        return this.points;
    }

    /**
     * Adds a listener for change events
     */
    listen(cb: listenerCallback) {
        this._listeners.push(cb);
    }

    /**
     * Calls all change event listeners
     */
    notifyListeners() {
        for (const cb of this._listeners) {
            cb(this);
        }
    }

    /**
     * Retrieves the final slide content, or null
     */
    getEndSlide(): endSlideContent | null {
        if (
            this._quiz?.endSlideContent !== undefined &&
            this._quiz?.endSlideContent !== null
        ) {
            return this._quiz.endSlideContent;
        }
        return null;
    }
}

/**
 * A single quizManager question
 */
class quizManagerQuestion {
    question: string;
    possibleAnswers: Array<quizManagerSingleAnswer>;
    handler: quizManagerHandler;
    markdown: ?string;
    splashSlide: boolean;
    correct: boolean = false;
    attemptedAnswers: { [string]: boolean } = {};
    numberOfCorrectAnswersRequired: number = 1;
    _numberOfCorrectAnswersDone: number = 0;
    questionNumber: number;
    totalQuestions: number;

    // eslint-disable-next-line require-jsdoc
    constructor(
        handler: quizManagerHandler,
        question: quizManagerQuestionContainer,
        questionNumber: number,
    ) {
        this.handler = handler;
        this.question = question.question;
        this.markdown = question.markdown;
        this.splashSlide = question.splashSlide === true;
        this.possibleAnswers = shuffle(question.answers);
        this.questionNumber = questionNumber;
        this.totalQuestions = handler.totalQuestions;
        if (
            question.numberOfCorrectAnswersRequired !== undefined &&
            question.numberOfCorrectAnswersRequired !== null
        ) {
            this.numberOfCorrectAnswersRequired =
                question.numberOfCorrectAnswersRequired;
        }
    }

    /**
     * Forces stable sorting of the possibleAnswers. This should only be used
     * for testing, which needs consistent input.
     */
    testUseStableQuestionSorting() {
        this.possibleAnswers = this.possibleAnswers.sort((a, b) =>
            a.entry.localeCompare(b.entry, "no"),
        );
    }

    /**
     * Answers the question. Will notify the handler about correctness.
     */
    answer(option: quizManagerSingleAnswer): boolean {
        const hadAlreadyAnswered = this.hasAnswered(option);
        this.attemptedAnswers[option.entry] = true;
        if (this.splashSlide || option.correct === true) {
            if (!hadAlreadyAnswered) {
                this._numberOfCorrectAnswersDone++;
                if (
                    this._numberOfCorrectAnswersDone >=
                    this.numberOfCorrectAnswersRequired
                ) {
                    this.correct = true;
                }
                this.handler.registerCorrect();
            }
            return true;
        } else {
            this.handler.registerIncorrect(this);
            return false;
        }
    }

    /**
     * Checks if the user has tried to answer the quizManagerSingleAnswer provided
     */
    hasAnswered(option: quizManagerSingleAnswer): boolean {
        return this.attemptedAnswers[option.entry] === true;
    }

    /**
     * Forces a "correct" answer to this question.
     * Primarily useful to continue from a splashSlide without needing to pass
     * a valid quizManagerSingleAnswer to answer().
     */
    answerCorrect(): boolean {
        this.correct = true;
        this.handler.registerCorrect();
        return true;
    }
}
export default quizManagerHandler;
export { quizManagerHandler, quizManagerQuestion, quizDataManager };
