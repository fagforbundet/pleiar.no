/**
 * @prettier
 */
/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

// eslint-disable-next-line flowtype/require-exact-type
export type asyncLoaderRoleType = {
    _ALR_initializing: ?Promise<null>,
    _ALR_hasInitialized: ?boolean,
    _ALR_onInitialize: ?Array<() => mixed>,

    onInitialize: (() => mixed) => mixed,
    initialize: () => Promise<null>,
    hasInitialized: () => boolean,
};

/**
 * This is a "base" object that you can add to another object to add functionality
 * to that object to asynchronously load some data.
 *
 * It expects the following methods, with these exact signatures, to be present
 * in any object that consumes it:
 *
 * _performAsyncImport() - This should return the promise from the import call,
 * like: import(/* webpackChunkName: "search-index", webpackPreload: true * /
 * '../data/search-index.json').
 *
 * _loadedData(data) - this gets the data structure once it has been loaded.
 */
const asyncLoaderRole: asyncLoaderRoleType = {
    _ALR_initializing: null,
    _ALR_hasInitialized: false,
    _ALR_onInitialize: null,

    /**
     * Must be overridden by consumer
     */
    _performAsyncImport() {
        throw "ERROR: Consuming object of asyncLoaderRole has not provided _performAsyncImport";
    },

    /**
     * Must be overridden by consumer
     */
    _loadedData() {
        throw "ERROR: Consuming object of asyncLoaderRole has not provided _loadedData";
    },

    /**
     * Subscribes to an "on-initialize" event. Each callback will be called
     * when the initialize() promise resolves (but it won't trigger an
     * initialize() call). If we have already been initialized, the function
     * is executed immediately.
     */
    onInitialize(func: () => mixed) {
        // $FlowExpectedError[object-this-reference]
        if (this.hasInitialized()) {
            func();
        } else {
            // $FlowExpectedError[object-this-reference]
            if (this._ALR_onInitialize === null) {
                // $FlowExpectedError[object-this-reference]
                this._ALR_onInitialize = [];
            }
            // $FlowExpectedError[object-this-reference]
            this._ALR_onInitialize.push(func);
        }
    },

    /**
     * Initializes this instance of our consuming object. Returns a promise that gets
     * resolved when the object has been initialized
     */
    initialize(): Promise<null> {
        // $FlowExpectedError[object-this-reference]
        if (this._ALR_initializing === null) {
            // $FlowExpectedError[object-this-reference]
            this._ALR_initializing = new Promise((resolve, reject) => {
                // $FlowExpectedError[object-this-reference]
                if (this.hasInitialized()) {
                    resolve(null);
                } else {
                    // $FlowExpectedError[object-this-reference]
                    const parentPromise = this._performAsyncImport();
                    parentPromise
                        .then((module) => {
                            // $FlowExpectedError[object-this-reference]
                            this._loadedData(module.default);
                            // $FlowExpectedError[object-this-reference]
                            this._ALR_hasInitialized = true;
                            // $FlowExpectedError[object-this-reference]
                            const onInitialize = this._ALR_onInitialize;
                            // $FlowExpectedError[object-this-reference]
                            this._ALR_onInitialize = [];
                            resolve(null);
                            for (const func of onInitialize) {
                                func();
                            }
                        })
                        .catch((error) => {
                            reject(
                                "search-index.json failed to load: " + error,
                            );
                        });
                }
            });
        }
        // $FlowExpectedError[object-this-reference]
        return this._ALR_initializing;
    },

    /**
     * This returns a boolean, true if PleiarSearcher has been initialized,
     * false otherwise.
     */
    hasInitialized(): boolean {
        // $FlowExpectedError[object-this-reference]
        return this._ALR_hasInitialized;
    },
};

export { asyncLoaderRole };
