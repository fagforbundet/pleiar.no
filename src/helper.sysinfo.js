/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// @flow
import type { appInstallModes } from './mobile-appinstall';
import auth from './auth';
import device from './helper.device';
import PleiarSearcher from './searcher';
import MedSearcher from './med-synonym-searcher';
import featureDetection from './helper.feature-detection';
import { appInstall } from './mobile-appinstall';
import { quizDataManager } from "./quiz-manager";

export type SourceInfoType = {|
    type: "script" | "link",
    src: string,
    rel?: string
|};
export type SearcherState = "lastet" | "ikke lastet";
export type FeatureStatus = "ikke tilgjengelig" | "tilgjengelig, i bruk" | "tilgjengelig, ikke i bruk" | "tilgjengelig";
export type AppInstallSystemInfo = {|
    mode: appInstallModes,
    canInstall: boolean,
|};
export type SystemInfoDimensions = {|
    w: number,
    h: number
|};
export type SystemInfoDevice = {|
    isAppMode: boolean,
    isTouchScreen: boolean,
|};
export type SystemInfoType = {|
    featuresDetected: Array<string>,
    sources: Array<SourceInfoType>,
    auth: boolean,
    platform: string,
    UA: string,
    bodyClass: string | null,
    appInstall: AppInstallSystemInfo,
    dimensions: SystemInfoDimensions,
    quizDataManagerStatus: SearcherState,
    medSearcherStatus: SearcherState,
    pleiarSearcherStatus: SearcherState,
    serviceWorkerStatus: FeatureStatus,
    device: SystemInfoDevice,
    appVersion: string,
    dataVersion: string,
|};
// Let flow know about our GIT_REVISION globals (from webpack)
declare var GIT_REVISION:string;
declare var GIT_REVISION_FULL:string;
declare var GIT_REVISION_DATA:string;

/**
 * A collection of helper functions that can be used to identify the device
 * or mode that we're running in
 */
const SysInfo = {
    /**
     * Retrieves all of the system information in a SystemInfoType object.
     * This is all of the information that is NOT dependant on CSS.
     */
    getSystemInfo (): SystemInfoType
    {
        // The user agent
        const UA = navigator.userAgent;
        // Load state for MedSearcher
        const medSearcherStatus: SearcherState = SysInfo.getSearcherState(MedSearcher);
        // Load state for quizDataManager
        const quizDataManagerStatus: SearcherState = SysInfo.getSearcherState(quizDataManager);
        // Load state for PleiarSearcher
        const pleiarSearcherStatus: SearcherState = SysInfo.getSearcherState(PleiarSearcher);
        // List of features
        const featuresDetected = SysInfo.featureDetection();
        // State of the service worker
        let serviceWorkerStatus: FeatureStatus = "ikke tilgjengelig";
        if(navigator.serviceWorker && navigator.serviceWorker.controller !== null)
        {
            serviceWorkerStatus = "tilgjengelig, i bruk";
        }
        else if(navigator.serviceWorker)
        {
            serviceWorkerStatus = "tilgjengelig, ikke i bruk";
        }
        // An array of sources we have loaded
        const sources: Array<SourceInfoType> = [];
        try
        {
            // Script sources
            const scripts = document.getElementsByTagName('script');
            for(let sourceI: number = 0; sourceI < scripts.length; sourceI++)
            {
                const source = scripts[sourceI];
                const src = source.getAttribute('src');
                if(src !== undefined && src !== null)
                {
                    sources.push({
                        type: "script",
                        src
                    });
                }
            }
            // Link (preload or stylesheet) sources
            const links = document.getElementsByTagName('link');
            for(let linkI: number = 0; linkI < links.length; linkI++)
            {
                const source = links[linkI];
                const href = source.getAttribute('href');
                const rel = source.getAttribute('rel');
                if(href !== undefined && href !== null && (rel === "stylesheet" || rel === "prefetch" || rel === "preload"))
                {
                    sources.push({
                        type: "link",
                        src: href,
                        rel,
                    });
                }
            }
        }
        catch(e)
        {
            console && console.log && console.log('(klarte ikke hente kildeliste: '+e+')');
        }
        // Screen dimensions
        const dimensions: SystemInfoDimensions = {
            w: Math.max( (document.documentElement ? document.documentElement.clientWidth : window.innerWidth) || 0),
            h: Math.max( (document.documentElement ? document.documentElement.clientHeight : window.innerHeight ) || 0),
        };
        // The plattform as specified by the navigator
        let platform: string = navigator.platform;
        // Android identifies as "Linux" only, add Android to it
        if (navigator.userAgent.indexOf('Android') !== -1)
        {
            platform = platform.replace(/^Linux/,'Android (Linux)');
        }
        // The "class" tag of the body, if any.
        let bodyClass: string | null = null;
        if(document.body !== null)
        {
            const BC = document.body.getAttribute('class');
            if(BC !== undefined)
            {
                bodyClass = BC;
            }
        }
        // The information object
        return {
            UA,
            sources,
            dimensions,
            bodyClass,
            auth: auth.isAuthenticated(),
            platform,
            quizDataManagerStatus,
            medSearcherStatus,
            pleiarSearcherStatus,
            serviceWorkerStatus,
            appVersion: GIT_REVISION_FULL,
            dataVersion: GIT_REVISION_DATA,
            featuresDetected,
            device: {
                isAppMode: device.isAppMode(),
                isTouchScreen: device.isTouchScreen(),
            },
            appInstall: {
                mode: appInstall.mode(),
                canInstall: appInstall.canInstall()
            },
        };
    },

    /**
     * Detects various browser features and returns human-readable innformation
     * about which was found
     */
    featureDetection (): Array<string>
    {
        const featuresDetected: Array<string> = [];
        if(featureDetection.aSupportsRel('noopener'))
        {
            featuresDetected.push('a-rel-noopener');
        }
        if (window.navigator.storage && window.navigator.storage.persist && typeof(window.navigator.storage.persist) === 'function')
        {
            featuresDetected.push('storage.persist');
        }
        // $FlowIssue[prop-missing]: These are nonstandard or in-development, so flow doesn't know about them
        if(navigator.connection || navigator.mozConnection || navigator.webkitConnection)
        {
            featuresDetected.push('navigator.connection');
        }
        if(window.applicationCache && window.applicationCache.UNCACHED === window.applicationCache.status)
        {
            featuresDetected.push('appCache(unused)');
        }
        else if(window.applicationCache)
        {
            featuresDetected.push('appCache(inUse)');
        }
        return featuresDetected;
    },

    /**
     * Method that retrieves the load state of any object that inherits from {@link asyncLoaderRole}
     */
    getSearcherState(searcher: typeof MedSearcher | typeof PleiarSearcher | typeof quizDataManager): "lastet" | "ikke lastet"
    {
        if(searcher.hasInitialized())
        {
            return "lastet";
        }
        else
        {
            return "ikke lastet";
        }
    }
};

export default SysInfo;
