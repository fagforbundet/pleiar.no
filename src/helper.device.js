/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 * Copyright (C) Eskild Hustvedt 2017-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// @flow

/**
 * A collection of helper functions that can be used to identify the device
 * or mode that we're running in
 */
const device = {
    /**
     * Helper function that detects if we're running in "app mode" (on a mobile
     * device) or "web mode". Returns `true` if we're in app mode.
     */
    isAppMode (): boolean
    {
        return !!( (window.matchMedia && window.matchMedia('(display-mode: standalone)').matches) || window.navigator.standalone === true);
    },
    /**
     * Helper function that tries to detect if we're on a mobile device - a
     * phone or a tablet
     */
    isMobileDevice (): boolean
    {
        if (!device.isTouchScreen())
        {
            return false;
        }
        return /(iphone|ip[ao]d|android|mobile)/i.test(window.navigator.userAgent);
    },
    /**
     * Helper function that tries to detect if we're running in app mode on an
     * iOS device.
     *
     * The reasoning for detecting this is that iOS needs some tweaks for the
     * UI when running in app mode
     */
    isIOSApp (): boolean
    {
        return device.isAppMode() && /(iphone|ip[ao]d)/i.test(window.navigator.userAgent);
    },
    /**
     * Detects if we're on a device where the primary input method is through
     * a touchscreen.
     *
     * This uses CSS media queries to check if we have an accurate pointing device,
     * if we don't that means that the user does not have a mouse, and we thus assume
     * that the user is on a touch screen (even though it could mean that the user is
     * on some device where the input method is not as accurate as a mouse, but still
     * isn't touch, like for instance when using a nintendo wii controller as input).
     *
     * This function falls on the side of caution, so for instance on IE11 it will
     * always return false because the media query is unavailable,
     */
    isTouchScreen (): boolean
    {
        // IE11 does not support the media query
        if(window.msMatchMedia)
        {
            return false;
        }
        return !!(
            // IE11 doesn't support the media query, so don't run the test on IE11.
            // Edge supports it, and has dropped window.msMatchMedia, so this will pass
            // on Edge.
            !window.msMatchMedia &&
            // We require matchMedia to be available, otherwise we assume false
            window.matchMedia &&
            // The actual media query
            !window.matchMedia("(hover: hover) and (pointer: fine)").matches
        );
    }
};

export default device;
