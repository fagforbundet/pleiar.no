/**
 * @prettier
 */
/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import type { FESTDataList, FESTDrugEntry } from "./types/data";
import { asyncLoaderRole } from "./helper.asyncLoader";

export type medSynonymBasicResult = {|
    name: string,
    atc?: string,
    notice?: ?string,
|};
export type medSynonymResult = {|
    synonyms: Array<string>,
    points: number,
    ...medSynonymBasicResult,
|};

export type medSynonymResultList = Array<medSynonymResult>;

type MedSearcherSearchResult = {|
    length: number,
    with: medSynonymResultList,
    without: medSynonymResultList,
|};

type MedSearcherATCResult = {|
    ATC: string,
    list: Array<medSynonymBasicResult>,
|};

type searchMatchEntry = {|
    hit: boolean,
    points: number,
|};

// eslint-disable-next-line flowtype/require-exact-type
type MedSearcherType = {
    fest: ?FESTDataList,
    _getFEST: () => FESTDataList,
    onInitialize: (() => mixed) => mixed,
    initialize: () => Promise<null>,
    hasInitialized: () => boolean,
    _searchMatch: (Array<string>, FESTDrugEntry) => searchMatchEntry,
    atcSearch: (string) => MedSearcherATCResult,
    search: (string) => MedSearcherSearchResult,
    looksLikeATC: (string) => boolean,
};

/**
 * MedSearcher is the search wrapper for the medication synonyms data.  This is
 * different from PleiarSearcher. PleiarSearcher uses lunr to index everything
 * on the site, except for medications. *This* class uses a fairly dumb
 * method of searching through the medication list.
 */
const MedSearcher: MedSearcherType = {
    ...asyncLoaderRole,

    /** The data structure, a FESTDataList */
    fest: null,

    /**
     * _performAsyncImport method for {@link asyncLoaderRole}
     */
    _performAsyncImport(): Promise<null> {
        return import(/* webpackChunkName: "fest-data" */ "../data/fest.json");
    },

    /**
     * _loadedData method for {@link asyncLoaderRole}
     */
    _loadedData(data: FESTDataList) {
        if (data.fVer !== 1) {
            if (typeof data.fCompV !== "number" || data.fCompV > 1) {
                throw (
                    "MedSearcher: FEST data of incompatible version: " +
                    data.fVer +
                    "/" +
                    data.fCompV
                );
            }
        }
        MedSearcher.fest = data;
    },

    /**
     * Retrieves the fest data structure, or errors out
     */
    _getFEST(): FESTDataList {
        if (MedSearcher.fest === null || MedSearcher.fest === undefined) {
            throw "Attempt to use MedSearcher without loading the data first";
        }
        return MedSearcher.fest;
    },

    /**
     * Checks if a string looks like an ATC code
     */
    looksLikeATC(thing: string): boolean {
        if (typeof thing !== "string") {
            return false;
        }
        const upperThing = thing.toUpperCase().replace(/\s+/g, "");
        // Prefixed with ATC? No doubt.
        if (upperThing.indexOf("ATC") === 0) {
            return true;
        }
        // Letter followed by number? Also an ATC
        if (/^[A-Z][A-Z]?\d/.test(upperThing)) {
            return true;
        }
        return false;
    },

    /**
     * Searches through the medication list
     */
    search(str: string): MedSearcherSearchResult {
        /** This is used to make sure we don't display a single exchangeGroup more than once */
        const foundExchangeGroups: Array<boolean> = [];
        /** The list of matching medications with synonyms */
        const results: medSynonymResultList = [];
        /** The list of matching medications without synonyms */
        const resultsWithoutSynonyms: medSynonymResultList = [];
        /** The search string in lower case */
        const searchStr: string = str.toLowerCase();
        /** The search string split on whitespace into an array */
        const searchElements: Array<string> = searchStr
            .split(/\s+/)
            .filter((entry) => entry.length !== 0);
        /** The FEST datastructure */
        const fest = MedSearcher._getFEST();
        /** Iterate over all entries in FEST */
        for (let drugID: number = 0; drugID < fest.drugs.length; drugID++) {
            /** This is the drug entry **/
            const drug = fest.drugs[drugID];
            /** Perform the match to see if it hits */
            const match = MedSearcher._searchMatch(searchElements, drug);
            /** hit is true if _searchMatch matched */
            if (match.hit) {
                /** Exchange groups for drug, if any */
                const exchangeGroup = drug.exchangeGroup;
                /* If we don't have any exchange groups, just add it to resultsWithoutSynonyms */
                if (exchangeGroup === undefined) {
                    resultsWithoutSynonyms.push({
                        name: drug.name,
                        points: match.points,
                        atc: drug.atc,
                        synonyms: [],
                    });
                } else if (
                    /* The exchangeGroup should be a number, and we skip this entry
                     * if we've already processed the same exchangeGroup */
                    typeof exchangeGroup === "number" &&
                    !foundExchangeGroups[exchangeGroup]
                ) {
                    /** A list of the names of synonyms for this drug */
                    const drugSynonyms: Array<string> = [];
                    /** A list of drugIndex ID numbers representing the synonyms for this drug */
                    const synonyms = fest.exchangeList[exchangeGroup];
                    /* Iterate over all synonyms */
                    for (const synonym of synonyms.drugIndexes) {
                        /** Fetch the drug entry for this synonym */
                        const synDrug = fest.drugs[synonym];
                        /** If the synonym doesn't exist then the data is corrupt */
                        if (synDrug === undefined) {
                            throw "Unable to look up drug with ID " + synonym;
                        }
                        // Ignore the main drug, so we don't list it twice
                        // (once in the header, once in the synonym list)
                        if (synonym !== drugID) {
                            /* Add this drug synonym to the results for this drug */
                            drugSynonyms.push(synDrug.name);
                        }
                    }
                    /* Label this exchangeGroup as processed */
                    foundExchangeGroups[exchangeGroup] = true;
                    /** The result for this match */
                    const entry: medSynonymResult = {
                        name: drug.name,
                        points: match.points,
                        synonyms: drugSynonyms,
                        atc: synonyms.atc,
                    };
                    /*
                     * Some drugs have a 'merkand', a notice to display along
                     * with the drug.  This could for instance be "don't
                     * exchange this drug for children under 18".
                     */
                    if (synonyms.merknad) {
                        entry.notice = synonyms.merknad;
                        entry.points += 1;
                    }
                    /* Push this match onto our result array */
                    results.push(entry);
                }
            }
        }
        /**
         * Sorts an array descending by points (and alphabetically if two
         * entries have the same number of points)
         */
        const sortFunction = (a, b) => {
            if (a.points > b.points) {
                return -1;
            }
            if (a.points < b.points) {
                return 1;
            }
            if (a.name > b.name) {
                return -1;
            }
            if (a.name < b.name) {
                return 1;
            }
            return 0;
        };
        return {
            with: results.sort(sortFunction),
            without: resultsWithoutSynonyms.sort(sortFunction),
            length: results.length + resultsWithoutSynonyms.length,
        };
    },

    /**
     * Searches through the medication list using an ATC code
     */
    atcSearch(atc: string): MedSearcherATCResult {
        atc = atc.replace(/\s+/g, "").toUpperCase().replace(/^ATC/, "");
        const result: MedSearcherATCResult = {
            ATC: atc,
            list: [],
        };
        const resultList: Array<medSynonymBasicResult> = [];
        /** The FEST datastructure */
        const fest = MedSearcher._getFEST();
        /** Iterate over all entries in FEST */
        for (let drugID: number = 0; drugID < fest.drugs.length; drugID++) {
            /** This is the drug entry **/
            const drug: FESTDrugEntry = fest.drugs[drugID];

            /** The ATC code */
            let drugATC: ?string = drug.atc;

            /** The merknad on this exchange group */
            let merknad: ?string;

            /** If we have no ATC code directly on the drug, fetch it from the synonyms list */
            /** Exchange groups for drug, if any */
            const exchangeGroup = drug.exchangeGroup;
            /* The exchangeGroup should be a number, and we skip this entry
             * if we've already processed the same exchangeGroup */
            if (typeof exchangeGroup === "number") {
                /** A list of synonyms for this drug */
                const synonyms = fest.exchangeList[exchangeGroup];
                if (drugATC === undefined) {
                    /** ATC code for the synonym list */
                    drugATC = synonyms.atc;
                }
                if (
                    synonyms.merknad !== undefined &&
                    synonyms.merknad !== null
                ) {
                    merknad = synonyms.merknad;
                }
            }

            /** If the ATC codes match exactly, append this to the result list */
            if (drugATC === atc && drugATC !== undefined && drugATC !== null) {
                resultList.push({
                    name: drug.name,
                    atc: drugATC,
                    notice: merknad,
                });
            }
        }
        result.list = resultList.sort((a, b) => {
            if (a.name > b.name) {
                return -1;
            }
            if (a.name < b.name) {
                return 1;
            }
            return 0;
        });
        return result;
    },

    /**
     * Tries a single match for a search string
     */
    _searchMatch(
        elements: Array<string>,
        item: FESTDrugEntry,
    ): searchMatchEntry {
        /** The number of points for this entry */
        let points: number = 0;
        /** A boolen that gets set to true if this entry matched */
        let hit: boolean = false;
        /** A variant of itemName in lower case */
        const itemName = item.name.toLowerCase();
        /* Iterate over all searchitems */
        for (const searchItem of elements) {
            const idx = itemName.indexOf(searchItem);
            /* If the current item matched at the start of itemName then
             * this is a very specific match. Grant 3 points.
             */
            if (idx === 0) {
                points += 3;
                hit = true;
            } else if (idx !== -1) {
                /*
                 * If it matched anywhere else, grant 1 point
                 */
                points += 1;
                hit = true;
            } else {
                /*
                 * If it didn't match, then return without bothering to check other
                 * entries. This is because all searches are done with AND, so if one
                 * component doesn't match, then nothing should match.
                 */
                return {
                    points: 0,
                    hit: false,
                };
            }
        }
        return {
            points,
            hit,
        };
    },
};

export default MedSearcher;
