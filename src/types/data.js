/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

/*
 * Dictionary types
 */

/**
 * A signle dictionary entry
 */
export type DictionaryEntry = {|
    expression: string,
    moreInfoURLs?: Array<string>,
    description: string,
    keywords?: string,
    seeHandbook?: string,
    lookupKey?: string,
    seeAlso: Array<string>
|};

/**
 * The data structure used to store `DictionaryEntry` items
 */
export type DictionaryDataStructure = Array<DictionaryEntry>;

/*
 * External resources types
 */

/**
 * A single external resource entry
 */
export type ExternalResourcesEntry = {|
    title: string,
    url: string,
    description: string,
    keywords?: string,
    language: "Norsk" | "Engelsk"
|};

/**
 * The external resources data structure
 */
export type ExternalResourcesDataStructure = Array<ExternalResourcesEntry>;

/*
 * Lab value types
 */

/**
 * A single lab value
 */
export type LabValueEntry = {|
    navn: string,
    materiale: "B" | "E" | "P" | "PT" | "S",
    ref: string,
    category: string,
    merknad?: string
|};

/**
 * The data structure used to store lab values
 */
export type LabValueDataStructure = {|
    values: Array<LabValueEntry>,
    index: {|
        [string]: Array<number>
    |}
|};

/*
 * Nutricalc types
 */

/**
 * A single element of nutritional info
 */
export type NutritionInfoEntry = {|
    kcal: number,
    name: string,
    fluidML?: number,
    protein?: number,
|};

/**
 * The data structrue for basicGroups
 */
export type NutritionGroupsDataStructure = {|
    [string]: NutritionInfoEntry
|};

/**
 * The data structure for the entire datastructure
 */
export type NutritionDataStructure = {|
    customLabel: string,
    basicGroups: NutritionGroupsDataStructure
|};

/*
 * Handbook types
 */

/**
 * Represents a single path in the handbook. An array of path component strings.
 */
export type HandbookPath = Array<string>;

/**
 * A single handbook page. Should generally not be referenced directly, but go
 * through the {@link HandbookEntry} object.
 */
export type HandbookDataEntry = {|
    toc: Array<string>,
    content: string,
    title: string,
    path: HandbookPath,
    public: boolean,
    hideTOC?: boolean,
    customURL?: string,
    previous?: HandbookPath,
    next?: HandbookPath,
|};

/**
 * The datastructure used to store the entire handbook
 */
export type HandbookDataStructure = {|
    order: Array<string>,
    title: string,
    displayIndexLink?: boolean,
    article?: HandbookDataEntry,
    tree: {|
        [string]: HandbookDataEntry | HandbookDataStructure
    |}
|};

/**
 * A single tool entry
 */
export type ToolListEntry = {|
    path: string,
    title: string,
    description?: string,
    keywords?: string
|};

/**
 * The datastructure used to define the list of tools
 */
export type ToolsListDataStructure = Array<{|
    title: string,
    tools: Array<ToolListEntry>,
|}>;

/*
 * Types used in search metadata
 */
export type SearchMetadataEntry = {|
    title: string,
    url: string,
    description?: string,
|};

/*
 * Types for externally indexed data
 */
export type ExternallyIndexedItem = {|
    title: string,
    text: string,
    url: string,
    type: "other" | "fagprat",
    keywords?: string,
|};
export type ExternallyIndexedList = Array<ExternallyIndexedItem>;


export type FESTDrugEntry = {|
    name: string,
    exchangeGroup?: number,
    atc?: string
|};

export type FESTExchangeGroupEntry = {|
    merknad?: string,
    drugIndexes: Array<number>,
    atc: string
|};

/**
 * The datastructure of the FEST data
 */
export type FESTDataList = {|
    drugs: Array<FESTDrugEntry>,
    exchangeList: Array<FESTExchangeGroupEntry>,
    fVer: number,
    fCompV: number
|};

/*
 * Elearn/quiz types
 */

export type endSlideContent = {|
    markdown: string,
    header: string,
|};

export type quizManagerSingleAnswer = {|
    entry: string,
    correct?: boolean,
|};
export type quizManagerQuestionContainer = {|
    question: string,
    markdown: ?string,
    splashSlide: ?boolean,
    numberOfCorrectAnswersRequired: ?number,
    answers: Array<quizManagerSingleAnswer>,
|};


export type quizRootData = {|
    title: string,
    description?: string,
    keywords?: string,
    endSlideContent: ?endSlideContent,
    quiz: Array<quizManagerQuestionContainer>,
|};
export type QuizDataContainer = {|
    [string]: quizRootData,
|};
