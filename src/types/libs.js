/*
 * Flowtype definitions for library parts used in pleiar.no.
 *
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

export type lunrStringOffsets = [ number, number ];

/**
 * Metadata returned from a lunr search
 */
export type lunrMetadata = {|
    [string]: {
        [string]: {|
            position: Array<lunrStringOffsets>
        |}
    }
|};

/**
 * Datastructure returned from lunr for a single search, and therefore also
 * from `PleiarSearcher`
 */
export type lunrResult = {|
    ref: string,
    matchData: {|
        metadata: lunrMetadata
    |}
|};

/**
 * Type for the react-select options object
 */
export type ReactSelectOptions = {| value: string, label: string |};

export type ReactSelectOnChangeValue = ?ReactSelectOptions | Array<ReactSelectOptions>;
