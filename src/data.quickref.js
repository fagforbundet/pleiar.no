/*
 * QuickRef data wrapper object
 *
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import quickref from '../data/hurtigoppslag/quickref.json';
import { HandbookEntry, HandbookIndex } from './data.handbook';
import type { HandbookDataStructure } from './types/data';
import type { HandbookPath } from './types/data';

/**
 * Represents a single "index" entry in the quickref. That is, an entry that
 * contains other entries. The quickref only contains one level, so this should
 * always be the root of the tree.
 */
class QuickRefIndex extends HandbookIndex
{
    /**
     * Retrieves the root datastructure for QuickRef entries
     */
    _rootDatastructure (): HandbookDataStructure
    {
        return quickref;
    }

    /**
     * Retrives the HandbookIndex variant for this Handbook variant: QuickRefIndex
     */
    _newHandbookIndex (path: HandbookPath): QuickRefIndex
    {
        return new QuickRefIndex(path);
    }

    /**
     * Retrives the HandbookEntry variant for this Handbook variant: QuickRefEntry
     */
    _newHandbookEntry (path: HandbookPath): QuickRefEntry
    {
        return new QuickRefEntry(path);
    }

    /**
     * Retrieves the root path for this Handbook variant: QuickRef
     */
    _rootPath(): string
    {
        return '/oppslag/';
    }
}

/**
 * Represents a single entry in the quickref. Use this instead of referencing
 * the quickref data structure directly, whenever possible. It makes looking up
 * prev/next entries etc. much easier.
 */
class QuickRefEntry extends HandbookEntry
{
    /**
     * Retrieves the root datastructure for QuickRef entries
     */
    _rootDatastructure (): HandbookDataStructure
    {
        return quickref;
    }

    /**
     * Retrives the HandbookIndex variant for this Handbook variant: QuickRefIndex
     */
    _newHandbookIndex (path: HandbookPath): QuickRefIndex
    {
        return new QuickRefIndex(path);
    }

    /**
     * Retrives the HandbookEntry variant for this Handbook variant: QuickRefEntry
     */
    _newHandbookEntry (path: HandbookPath): QuickRefEntry
    {
        return new QuickRefEntry(path);
    }

    /**
     * Retrieves the root path for this Handbook variant: QuickRef
     */
    _rootPath(): string
    {
        return '/oppslag/';
    }
}

export default QuickRefEntry;
export { QuickRefIndex, QuickRefEntry };
