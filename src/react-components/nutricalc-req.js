/**
 * @prettier
 */
/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from "react";
import { connect } from "react-redux";
import { Row, Col, Form, Input, Label } from "reactstrap";
import {
    CalcExplanation,
    CalcExplanationLine,
    CalcExplanationDescription,
    CalcExplanationCalculation,
    CalcExplanationFormula,
    Fraction,
    FractionResult,
    FractionComponent,
} from "./calculators-shared.js";
import { setValue, toggleValue } from "../actions/nutricalc-req";
import { setWeight } from "../actions/nutricalc-shared";
import { numbers } from "../helper.numbers";
import { PageTitle } from "./layout";
import { ExternalLink } from "./shared/links";
import type { ReduxEventOrStringCallback } from "../types/callbacks";
import type {
    nutriCalcBoolFields,
    nutriCalcSetFields,
    nutriCalcReqAgeGroup,
    nutriCalcReqPatientState,
} from "../reducers/nutricalc-req";
import type { CalcResultWithExplanation } from "../types/calculator-types";

// Props for the class itself (ie. nothing)
type NutriCalcReqOwnProps = {||};
// Redux dispatch function props
type NutriCalcReqReduxDispatch = {|
    onEnumSettingUpdate: (
        nutriCalcSetFields,
        nutriCalcReqPatientState | nutriCalcReqAgeGroup,
    ) => void,
    onTempUpdate: ReduxEventOrStringCallback,
    onWeightUpdate: (SyntheticEvent<HTMLInputElement>) => void,
    onBooleanUpdate: (nutriCalcBoolFields) => mixed,
|};
// Props from redux state
type NutriCalcReqPropsFromRedux = {|
    weight: number | "",
    ageGroup: string,
    febrile: boolean,
    skinny: boolean,
    temp: number,
    round: boolean,
    patientState: string,
|};

/**
 * Props for {@link NutriCalcReq}
 */
type NutriCalcReqProps = {|
    ...NutriCalcReqOwnProps,
    ...NutriCalcReqPropsFromRedux,
    ...NutriCalcReqReduxDispatch,
|};

/**
 * Calculates the required calories per day for a patient
 */
class NutriCalcReq extends React.PureComponent<NutriCalcReqProps> {
    /**
     * Renders the "additional values" part of the UI
     */
    renderAdditionalValues(): React.Node {
        let febrileColor: string = "";
        let febrileFieldColor: string = "";
        if (this.props.febrile) {
            if (this.props.temp === "" || this.props.temp < 38) {
                febrileFieldColor = "is-invalid";
            }
        } else {
            febrileColor = "text-muted";
        }
        return (
            <div>
                <Row>
                    <Col className="align-self-center form-inline form-check">
                        <Label check className="ml-3 ml-sm-0">
                            <Input
                                name="febrileState"
                                type="checkbox"
                                checked={this.props.febrile}
                                onChange={() => {
                                    this.props.onBooleanUpdate("febrile");
                                }}
                            />{" "}
                            <b>Febril</b>&nbsp;
                            <span
                                className={"d-none d-inline-md " + febrileColor}
                            >
                                med ein temperatur på
                            </span>
                            &nbsp;
                        </Label>
                        <div
                            className={
                                "align-self-center form-inline " + febrileColor
                            }
                        >
                            <Input
                                type="number"
                                step="0.1"
                                min="38"
                                max="41"
                                className={"mr-2 ml-2 " + febrileFieldColor}
                                value={this.props.temp}
                                onChange={(e) => {
                                    this.props.onTempUpdate(e);
                                }}
                                disabled={!this.props.febrile}
                                id="temp"
                            />
                            <Label htmlFor="temp" className="mr-2">
                                grader
                            </Label>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col className="form-inline form-check">
                        <Label check className="ml-3 ml-sm-0">
                            <Input
                                name="skinnyState"
                                type="checkbox"
                                checked={this.props.skinny}
                                onChange={() => {
                                    this.props.onBooleanUpdate("skinny");
                                }}
                            />
                            Pasienten er&nbsp;<b>mager</b>
                        </Label>
                    </Col>
                </Row>
            </div>
        );
    }
    /**
     * Perform the calculation
     */
    getResult(): CalcResultWithExplanation {
        const descr = [];
        let kcal: number = 0;
        let basePerKG: number = 33;
        let baseGroup: string = "oppegåande pasientar";

        if (this.props.patientState == "bed") {
            basePerKG = 29;
            baseGroup = "sengeliggjande pasientar";
        } else if (this.props.patientState == "building") {
            basePerKG = 40;
            baseGroup = "pasientar i ein oppbygjande fase";
        }

        // We permit an empty string as the weight, so we pretend it's 0 if
        // it's an empty string
        const weight = this.props.weight === "" ? 0 : this.props.weight;
        const base = numbers.conditionalRound(
            weight * basePerKG,
            this.props.round,
        );
        kcal += base;
        descr.push(
            <CalcExplanationLine key="basisbehov">
                <CalcExplanationDescription>
                    Basisbehov {basePerKG} kcal/kg for {baseGroup}
                </CalcExplanationDescription>
                <CalcExplanationCalculation>
                    {weight} × 33 = {base}
                </CalcExplanationCalculation>
                <CalcExplanationFormula>vekt × 33</CalcExplanationFormula>
            </CalcExplanationLine>,
        );

        if (this.props.ageGroup == "18-30") {
            const prev = kcal;
            kcal = numbers.conditionalRound(kcal * 1.1, this.props.round);
            descr.push(
                <CalcExplanationLine key="18-30">
                    <CalcExplanationDescription>
                        Ekstrabehov 10% for 18-30
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        {prev} × 1.10 = {kcal}
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula>
                        basisbehov × 1.10
                    </CalcExplanationFormula>
                </CalcExplanationLine>,
            );
        } else if (this.props.ageGroup == "70+") {
            const prev = kcal;
            kcal -= numbers.conditionalRound(base * 0.1, this.props.round);
            descr.push(
                <CalcExplanationLine key="70%">
                    <CalcExplanationDescription>
                        Redusert behov på 10% for 70+
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        basisbehov × 0.90
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula>
                        {prev} × 0.90 = {kcal}
                    </CalcExplanationFormula>
                </CalcExplanationLine>,
            );
        }
        if (this.props.skinny) {
            const prev = kcal;
            kcal += numbers.conditionalRound(base * 0.1, this.props.round);
            descr.push(
                <CalcExplanationLine key="ekstraMager">
                    <CalcExplanationDescription>
                        Ekstrabehov 10% for magre pasientar
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        <FractionComponent>{prev} + </FractionComponent>
                        <Fraction top={base} bottom="10" />
                        <FractionResult>= {kcal}</FractionResult>
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula>
                        <FractionComponent>behov + </FractionComponent>
                        <Fraction top="basisbehov" bottom="10" />
                    </CalcExplanationFormula>
                </CalcExplanationLine>,
            );
        }
        if (
            this.props.febrile &&
            this.props.temp !== "" &&
            this.props.temp >= 38
        ) {
            const prev = kcal;
            const temp = numbers.conditionalRound(
                this.props.temp - 37.0,
                this.props.round,
            );
            kcal += numbers.conditionalRound(
                base * 0.1 * temp,
                this.props.round,
            );
            const calculation: Array<React.Node> = [];
            let wrapRight: string = "";
            let wrapLeft: string = "";
            if (temp !== 1) {
                wrapLeft = " (";
                wrapRight = ") ";
            }
            calculation.push(
                <FractionComponent key="left">
                    {prev} +{wrapLeft}
                </FractionComponent>,
            );
            calculation.push(<Fraction key="base/10" top={base} bottom="10" />);
            if (temp !== 1) {
                calculation.push(
                    <FractionResult key="tempAbove1">× {temp}</FractionResult>,
                );
            }
            calculation.push(
                <FractionResult key="result">
                    {wrapRight} = {kcal}
                </FractionResult>,
            );
            descr.push(
                <CalcExplanationLine key="ekstraFebril">
                    <CalcExplanationDescription>
                        Ekstrabehov 10% pr. grad over 37
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        {calculation}
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula>
                        <FractionComponent>behov + </FractionComponent>
                        <Fraction top="basisbehov" bottom="10" />× grader over
                        37
                    </CalcExplanationFormula>
                </CalcExplanationLine>,
            );
        }
        if (this.props.round) {
            const rounded = numbers.roundNearestTen(kcal);
            if (rounded != kcal) {
                descr.push(
                    <CalcExplanationLine key="rounding">
                        <CalcExplanationDescription>
                            Rund av
                        </CalcExplanationDescription>
                        <CalcExplanationCalculation>
                            {kcal} ≈ {rounded}
                        </CalcExplanationCalculation>
                        <CalcExplanationFormula>
                            (rund av til nærmaste 10)
                        </CalcExplanationFormula>
                    </CalcExplanationLine>,
                );
                kcal = rounded;
            }
        }
        return {
            result: kcal,
            description: <CalcExplanation>{descr}</CalcExplanation>,
        };
    }
    /**
     * Main rendering
     */
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const additionalValues = this.renderAdditionalValues();
        const result = this.getResult();
        let veierLabel: string = "Vekt";
        if (this.props.patientState == "building") {
            veierLabel = "Ynskjeleg vekt";
        }

        return (
            <div>
                <PageTitle title="Ernæringskalkulator - behov" />
                <Row>
                    <Col>
                        <h3 className="d-md-none">Energibehov</h3>
                        <h3 className="d-none d-md-block">
                            Ernæringskalkulator - energibehov
                        </h3>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h4 className="d-none d-md-block">Pasientdata</h4>
                    </Col>
                </Row>
                <Form className="smallInputElements boldColLabels">
                    <Row>
                        <Label for="patientState" sm="3" md="2">
                            Pasienten er
                        </Label>
                        <Col className="align-self-center form-inline form-check">
                            <Label check className="mr-2 ml-3 ml-0-sm">
                                <Input
                                    type="radio"
                                    name="patientState"
                                    checked={this.props.patientState == "bed"}
                                    onChange={() => {
                                        this.props.onEnumSettingUpdate(
                                            "patientState",
                                            "bed",
                                        );
                                    }}
                                />{" "}
                                sengeliggjande
                            </Label>
                            <Label check className="mr-2 ml-3 ml-0-sm">
                                <Input
                                    type="radio"
                                    name="patientState"
                                    checked={this.props.patientState == "up"}
                                    onChange={() => {
                                        this.props.onEnumSettingUpdate(
                                            "patientState",
                                            "up",
                                        );
                                    }}
                                />{" "}
                                oppegåande
                            </Label>
                            <Label check className="mr-2 ml-3 ml-0-sm">
                                <Input
                                    type="radio"
                                    name="patientState"
                                    checked={
                                        this.props.patientState == "building"
                                    }
                                    onChange={() => {
                                        this.props.onEnumSettingUpdate(
                                            "patientState",
                                            "building",
                                        );
                                    }}
                                />{" "}
                                i ein oppbyggjande fase (målet er vektoppgang)
                            </Label>
                        </Col>
                    </Row>
                    <Row>
                        <Label for="weight" xs="3" sm="3" md="2">
                            {veierLabel}
                        </Label>
                        <Col className="form-inline form-check">
                            <Input
                                type="number"
                                min="1"
                                max="300"
                                className="mr-2"
                                value={this.props.weight}
                                onChange={(e) => {
                                    this.props.onWeightUpdate(e);
                                }}
                                id="weight"
                            />{" "}
                            kg
                        </Col>
                    </Row>
                    <Row>
                        <Label for="ageGroup" xs="3" sm="3" md="2">
                            <span className="d-none d-md-block">
                                Aldersgruppe
                            </span>
                            <span className="d-md-none">Alder</span>
                        </Label>
                        <Col className="align-self-center form-inline form-check">
                            <Label check className="mr-2 ml-3 ml-sm-0">
                                <Input
                                    type="radio"
                                    name="ageGroup"
                                    checked={this.props.ageGroup == "18-30"}
                                    onChange={() => {
                                        this.props.onEnumSettingUpdate(
                                            "ageGroup",
                                            "18-30",
                                        );
                                    }}
                                />{" "}
                                18-30
                            </Label>
                            <Label check className="mr-2 ml-3 ml-sm-0">
                                <Input
                                    type="radio"
                                    name="ageGroup"
                                    checked={this.props.ageGroup == "31-69"}
                                    onChange={() => {
                                        this.props.onEnumSettingUpdate(
                                            "ageGroup",
                                            "31-69",
                                        );
                                    }}
                                />{" "}
                                31-69
                            </Label>
                            <Label check className="mr-2 ml-3 ml-sm-0">
                                <Input
                                    type="radio"
                                    name="ageGroup"
                                    checked={this.props.ageGroup == "70+"}
                                    onChange={() => {
                                        this.props.onEnumSettingUpdate(
                                            "ageGroup",
                                            "70+",
                                        );
                                    }}
                                />{" "}
                                70+
                            </Label>
                        </Col>
                    </Row>
                    {additionalValues}
                </Form>
                <Row className="mt-4">
                    <Col>
                        <h4>Resultat</h4>
                        Pasienten har behov for{" "}
                        <b id="nutriCalcReqResult">{result.result} kcal/døgn</b>
                    </Col>
                </Row>
                {result.description}
                <Row className="text-secondary mt-4">
                    <Col>
                        <h5>Kjelde</h5>
                        <small>
                            Formelen er henta frå s.12 i: Statens ernæringsråd
                            (1995).{" "}
                            <i>
                                <ExternalLink
                                    className="text-secondary"
                                    href="https://www.nb.no/items/URN:NBN:no-nb_digibok_2013070205012"
                                >
                                    Statens ernæringsråds retningslinjer for
                                    kostholdet i helseinstitusjoner
                                </ExternalLink>
                            </i>
                            . Oslo: Universitetsforlaget.
                        </small>
                    </Col>
                </Row>
            </div>
        );
    }
}

const NutriCalcReqContainer: React.AbstractComponent<NutriCalcReqOwnProps> =
    connect<
        NutriCalcReqProps,
        NutriCalcReqOwnProps,
        NutriCalcReqPropsFromRedux,
        NutriCalcReqReduxDispatch,
        _,
        _,
    >(
        (state): NutriCalcReqPropsFromRedux => {
            return {
                weight: state.nutriCalc.shared.weight,
                ageGroup: state.nutriCalc.req.ageGroup,
                patientState: state.nutriCalc.req.patientState,
                febrile: state.nutriCalc.req.febrile,
                skinny: state.nutriCalc.req.skinny,
                temp: state.nutriCalc.req.temp,
                round: state.global.round,
            };
        },
        (dispatch): NutriCalcReqReduxDispatch => {
            return {
                onEnumSettingUpdate(
                    key: nutriCalcSetFields,
                    val: nutriCalcReqPatientState | nutriCalcReqAgeGroup,
                ) {
                    dispatch(setValue(key, val));
                },
                onTempUpdate(ev: SyntheticEvent<HTMLInputElement> | string) {
                    const value = numbers.floatishInRangeFromThing(ev, 0, 41);
                    dispatch(setValue("temp", value));
                },
                onWeightUpdate(ev: SyntheticEvent<HTMLInputElement>) {
                    const value = numbers.floatishInRangeFromThing(ev, 0, 400);
                    dispatch(setWeight(value));
                },
                onBooleanUpdate(key: nutriCalcBoolFields) {
                    dispatch(toggleValue(key));
                },
            };
        },
    )(NutriCalcReq);

export default NutriCalcReqContainer;
