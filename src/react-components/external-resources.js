/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2018
 t Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { PageTitle } from './layout';
import { Row, Col, Input, Label, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import { setExtRSearchString, toggleOnlyNorwegian } from '../actions/external-resources';
import data from '../../data/datasett/external-resources.json';
import XCircle from 'react-feather/dist/icons/x-circle';
import PleiarSearcher from '../searcher';
import { InfiniteScrollSearchResultRenderer, RequiresPleiarSearcher } from './global-search';
import RoutingAssistant from '../routing';
import { getValueFromLiteralOrEvent } from '../helper.general';
import device from '../helper.device';
import { ExternalLink } from './shared/links';
import type { lunrResult } from '../types/libs';
import type { ExternalResourcesEntry as ExternalResourcesEntryType } from '../types/data';
import type { onSyncSignature } from '../routing';
import type { Location } from 'react-router-dom';

/**
 * Displays a line containing the language of an `ExternalResourcesEntry`, if
 * that language is not Norsk.
 */
class LanguageInformation extends React.PureComponent<{|language: string|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        if(this.props.language !== "Norsk")
        {
            return <div className="externalLanguage">Språk: {this.props.language}</div>;
        }
        return '';
    }
}

/**
 * Renders a list of suggested search phrases
 */
class SearchSuggestions extends React.PureComponent<{|onSearchUpdate: (string) => mixed|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const words = [ 'Legemiddel','Seksualitet','Kreft','Blodprøver','Forsking' ];
        const result = [];
        for(let i: number = 0; i < words.length; i++)
        {
            const word = words[i];
            let addon: string = '';

            if ( (i+1) < words.length)
            {
                addon += ', ';
            }

            result.push(<span key={i} className="externalKeyword" onClick={() => { this.props.onSearchUpdate(word);} }>
                <span className="likeLink">{word}</span>{addon}
                </span>);
        }
        return result;
    }
}

/**
 * Renders a single entry in the external resources list
 */
class ExternalResourcesEntry extends React.PureComponent<{| entry: ExternalResourcesEntryType |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const dataEntry = this.props.entry;
        return <Row key={dataEntry.url} className="externalEntry">
            <Col>
                <ExternalLink basicStyle={true} href={dataEntry.url} className="linkWrapper">
                    <div className="externalTitle likeLink" target="_blank">{dataEntry.title}</div>
                    <div className="externalURL">{dataEntry.url}</div>
                    <LanguageInformation language={dataEntry.language} />
                    {dataEntry.description}
                </ExternalLink>
            </Col>
        </Row>;
    }
}

/**
 * The props for `ExternalResources`
 */
type ExternalResourcesProps = {|
    search: string,
    location?: Location,
    onSearchUpdate: (string) => void,
    onlyNorwegian: boolean,
    onOnlyNorwegianToggle: () => void,
    onPageUpdate: (number) => void
|};

/**
 * Renders the full external resources page. Complete with a search field and
 * infinite scroll.
 */
class ExternalResources extends React.Component<ExternalResourcesProps>
{
    constructor (props: ExternalResourcesProps) // eslint-disable-line require-jsdoc

    {
        super(props);
    }

    /**
     * Callback, called when either the user types or clicks on a link that
     * updates the search value. This gets bound to this instance of
     * `ExternalResources` in the `constructor`
     */
    onChanged (str: string | SyntheticEvent<HTMLInputElement>,forcedPush?: boolean)
    {
        str = getValueFromLiteralOrEvent(str);
        if(str == "")
        {
            if(forcedPush)
            {
                RoutingAssistant.push('/andresider');
            }
            else
            {
                RoutingAssistant.replace('/andresider');
            }
        }
        else
        {
            RoutingAssistant.generateReplace('/andresider',str,"auto");
        }
        this.props.onSearchUpdate(str);
    }

    /**
     * Performs a search and returns the result. Also handles applying a reducer
     * that limits results to only Norwegian if the `onlyNorwegian` prop is true.
     */
    getResources (): Array<lunrResult>
    {
        const search = this.props.search;
        let results: Array<lunrResult> = [];
        if(search === null || search === "" || !/\w/.test(search))
        {
            results = data;
            if(this.props.onlyNorwegian)
            {
                results = data.reduce( (acc, value) =>
                {
                    if(value.language === "Norsk")
                    {
                        acc.push(value);
                    }
                    return acc;
                }, []);
            }
        }
        else
        {
            results = PleiarSearcher.safePartialSearch(search,(accumulator: Array<lunrResult>,result: lunrResult) =>
            {
                if(result.ref.indexOf("external") === 0)
                {
                    if(this.props.onlyNorwegian)
                    {
                        const entry = PleiarSearcher.getResultFromRef( result.ref );
                        if(entry && entry.externalResources && entry.externalResources.language === "Norsk")
                        {
                            accumulator.push(result);
                        }
                    }
                    else
                    {
                        accumulator.push(result);
                    }
                }
                return accumulator;
            });
        }
        return results;
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const entries = this.getResources();
        let autoFocus: boolean = true;
        if(device.isTouchScreen())
        {
            autoFocus = false;
        }

        return  <Row>
            <Col lg={{offset:2,size:8}}>
                <PageTitle title="Andre sider" />
                <h3>Andre sider</h3>
                <div>Her kan du finne andre nettstader som kan vere nyttige for pleiepersonell og studentar. Lista er relativt lang, så det enkleste er å søke.<br /><br />Nokre forslag til søkjeord:&nbsp;
                    {/* FIXME */}
                    <SearchSuggestions onSearchUpdate={(e,force) => this.onChanged(e,force)} />
                </div>
                <InputGroup>
                    <Input autoFocus={autoFocus} placeholder="Søk" value={this.props.search} onChange={(e,force) => this.onChanged(e,force) } name="search" />
                    <InputGroupAddon addonType="append">
                        <InputGroupText className="clearSearchField" onClick={ () => { this.onChanged('', true); } }>
                            <XCircle />
                        </InputGroupText>
                    </InputGroupAddon>
                </InputGroup>
                <Row>
                    <Col className="align-self-center form-inline">
                        <Label check>
                            <Input name="onlyNorwegian" type="checkbox" checked={this.props.onlyNorwegian} onChange={this.props.onOnlyNorwegianToggle} /> Vis kun sider/kjelder på Norsk
                        </Label>
                    </Col>
                </Row>
                <hr />
                <InfiniteScrollSearchResultRenderer result={entries} search={this.props.search} type="externalResources" suggestGlobalSearch={true} searchPathPrefix="/andresider" location={this.props.location} />
            </Col>
        </Row>;
    }
}

const ExternalResourcesContainer = connect(
    (state) =>
    {
        return {
            search: state.externalResources.search,
            onlyNorwegian: state.externalResources.onlyNorwegian,
        };
    },
    (dispatch) =>
    {
        return {
            onSearchUpdate(val: string)
            {
                dispatch(setExtRSearchString(val));
            },
            onOnlyNorwegianToggle ()
            {
                dispatch(toggleOnlyNorwegian());
            },
        };
    }
)(ExternalResources);

type ExternalResourcesContainerProps = {| onRouteSync: onSyncSignature |};
/**
 * This is a wrapper around ExternalResources. It wraps it in a
 * RequiresPleiarSearcher component to ensure PleiarSearcher is initialized
 * once ExternalResources starts rendering.
 */
class ExternalResourcesWrapper extends React.Component<ExternalResourcesContainerProps>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { onRouteSync, ...moreProps } = this.props;
        return <RequiresPleiarSearcher component={ExternalResourcesContainer} onRouteSync={onRouteSync} app="external" {...moreProps} />;
    }
}
const ExternalResourcesWrapperContainer: React.AbstractComponent<{| |}> = connect<ExternalResourcesContainerProps,{||},_,ExternalResourcesContainerProps,_,_>(
    () =>
    {
        return {};
    },
    (dispatch): ExternalResourcesContainerProps =>
    {
        return {
            onRouteSync(search)
            {
                dispatch(setExtRSearchString(search));
            },
        };
    }
)(ExternalResourcesWrapper);

// Public exports
export default ExternalResourcesWrapperContainer;
export { ExternalResourcesEntry };
// Testing exports
export { ExternalResources as ExternalResourcesContainerRaw };
