/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from "react";
import { Switch, Route } from 'react-router-dom';

import { PageTitle } from './layout';
import { ToolContainer } from './tools';
import InfusionSpeedCalc from './medcalc-infusion';
import type { Match as RouterMatch } from 'react-router-dom';

/**
 * The informational message displayed in the home page
 */
class MedCalcInfo extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <div>
                <PageTitle title="Medisinrekning" />
                <b>Merk:</b> dette er berre eit hjelpeverkty, det er den einskilde person sitt ansvar å bekrefte svara denne kalkulatoren gir for å forhindre feilmedisinering. Medisinrekning bør alltid dobbeltkontrollerast.
               </div>;
    }
}

/**
 * The root component for the medcalc page.
 *
 * This handles rendering the base UI, setting the page title correctly and
 * passing control off to the actual calculator components
 */
class MedCalc extends React.PureComponent<{|match: RouterMatch|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const match = this.props.match;
        return  <ToolContainer tool="medcalc">
                    <div className="mt-2">
                        <Switch>
                            <Route path={match.path+'/infusjon'} render={() => {return <InfusionSpeedCalc />; }} />
                            <Route path={match.path} render={() => { return <MedCalcInfo />; }} />
                        </Switch>
                    </div>
                </ToolContainer>;
    }
}

// The MedCalc component is the only external interface
export default MedCalc;
