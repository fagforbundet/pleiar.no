/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from "react";
import { MainContainer, PageTitle } from './layout';
import { Markdown } from './markdown';
import { MenuList } from './menu';
import type { appList } from './menu';
import { appInstall } from '../mobile-appinstall';

// $FlowIssue[cannot-resolve-module] - Flow doesn't understand markdown imports
import content from '../../data/sider/forside.md';

/**
 * Renders the front page info text
 */
class FrontPage extends React.PureComponent<{||}>
{
    constructor (props: {||}) // eslint-disable-line require-jsdoc
    {
        super(props);
        if (!appInstall.canInstall())
        {
            appInstall.listen( () =>
            {
                this.forceUpdate();
            });
        }
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const extraAppList: appList = [];
        if(appInstall.canInstall())
        {
            extraAppList.push({
                name: 'Installer app',
                id:'appInstall',
                route: '/om/app',
                onClick: (ev: Event) =>
                {
                    if (appInstall.trigger())
                    {
                        ev.preventDefault();
                    }
                }
            });
        }
        return <MainContainer app="base">
                <PageTitle />
                <div className="col-lg-8 offset-lg-2">
                    <Markdown content={content} />
                </div>
                <div className="col-lg-10 offset-lg-1">
                    <MenuList hideHome={true} extraApps={extraAppList} />
                </div>
            </MainContainer>;
    }
}

export default FrontPage;
