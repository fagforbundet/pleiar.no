/**
 * @flow
 * @prettier
 */
import * as React from "react";
import {
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardSubtitle,
    Col,
    Container,
    Badge,
    Row,
    ListGroup,
    ListGroupItem,
    Alert,
    Button,
} from "reactstrap";
import quizManagerHandler, {
    quizManagerQuestion,
    quizDataManager,
} from "../quiz-manager";
import { Markdown } from "./markdown";
import { NotFound, ConditionalRender } from "./shared";
import { MainContainer, PageTitle } from "./layout";
import { MenuListRenderer } from "./menu";
import device from "../helper.device";
import type { appList } from "./menu";

// Let flow know about our GIT_REVISION and PLEIAR_ENV globals (from webpack)
declare var PLEIAR_ENV: string;

type QuestionProps = {|
    question: quizManagerQuestion | null,
|};

/**
 * Displays the score of a completed quiz
 */
class CourseCompleted extends React.Component<{|
    handler: quizManagerHandler,
|}> {
    // eslint-disable-next-line require-jsdoc
    constructor() {
        super();
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { handler } = this.props;
        let content: ?React.Node;
        const endSlide = handler.getEndSlide();
        let header: string = "Fullført og bestått!";
        if (endSlide !== null) {
            header = endSlide.header;
            content = (
                <Markdown
                    key="markdown-endslide"
                    content={endSlide.markdown}
                    permitSpecialNodes={true}
                />
            );
        }
        return (
            <Container>
                <Row>
                    <Col>
                        <Alert color="success">
                            <h1 className="alert-header">{header}</h1>
                            <Row>
                                <Col>{content}</Col>
                            </Row>
                        </Alert>
                    </Col>
                </Row>
            </Container>
        );
    }
}

/**
 * Renders a single question
 */
class Question extends React.Component<QuestionProps> {
    /**
     * Scroll ourself into view whenever a new Question is mounted (which in
     * this case means whenever a new question is displayed)
     */
    componentDidMount() {
        window.scrollTo(0, 0);
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { question } = this.props;
        if (question === null) {
            return null;
        }
        const pushAnswer = (answer) => {
            question.answer(answer);
        };
        const content: Array<React.Node> = [];
        if (
            question.markdown !== undefined &&
            question.markdown !== null &&
            typeof question.markdown === "string"
        ) {
            content.push(
                <Markdown
                    key="markdown-content"
                    content={question.markdown}
                    permitSpecialNodes={true}
                />,
            );
        }

        // $FlowExpectedError[incompatible-type] - flow for some reason thinks something here is a string
        const percentageCompleted: number = (
            ((question.questionNumber - 1) / question.totalQuestions) *
            100
        ).toPrecision(2);
        let listGroup: ?React.Node;
        let id: number = 0;
        let continueIsDisabled: boolean = true;
        if (question.splashSlide === true || question.correct === true) {
            continueIsDisabled = false;
        }
        if (question.splashSlide !== true) {
            const options = [];
            for (const possible of question.possibleAnswers) {
                id++;

                let feedback: ?React.Node;
                let color: string;
                let className: string = "force-pointer";

                if (question.hasAnswered(possible)) {
                    className = "";
                    if (possible.correct === true) {
                        color = "success";
                        feedback = (
                            <Badge key="correct" pill>
                                Rett!
                            </Badge>
                        );
                    } else {
                        color = "danger";
                        feedback = (
                            <Badge key="wrong" pill>
                                Feil!
                            </Badge>
                        );
                    }
                }

                options.push(
                    <ListGroupItem
                        key={"lgi" + id}
                        data-question-id={id - 1}
                        color={color}
                        action
                        onClick={() => pushAnswer(possible)}
                        className={className}
                    >
                        {possible.entry} {feedback}
                    </ListGroupItem>,
                );
            }
            listGroup = <ListGroup>{options}</ListGroup>;
        }
        return (
            <Container>
                <Row>
                    <Col>
                        <Card className="badgeNoBold">
                            <CardBody>
                                <CardSubtitle tag="h6" className="text-muted">
                                    {percentageCompleted}% fullført
                                </CardSubtitle>
                                <CardTitle tag="h1">
                                    {question.question}
                                </CardTitle>
                                <CardText tag="div">{content}</CardText>
                                {listGroup}
                                <CardText tag="div">
                                    <div className="mt-2">
                                        <Button
                                            onClick={() => {
                                                if (
                                                    continueIsDisabled === false
                                                ) {
                                                    question.handler.next();
                                                }
                                            }}
                                            className="mt-2"
                                            disabled={continueIsDisabled}
                                            outline
                                            color={
                                                continueIsDisabled === true
                                                    ? "secondary"
                                                    : "success"
                                            }
                                        >
                                            Fortsett
                                        </Button>
                                    </div>
                                </CardText>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Container>
        );
    }
}

type QuizProps = {|
    handler: quizManagerHandler,
|};

/**
 * The Quiz base class that renders a single quiz
 */
class Quiz extends React.Component<QuizProps> {
    // eslint-disable-next-line require-jsdoc
    constructor(props: QuizProps) {
        super();
        props.handler.listen(() => {
            this.forceUpdate();
        });
    }
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { handler } = this.props;
        if (handler.isDone()) {
            return <CourseCompleted handler={handler} />;
        }
        return (
            <Question
                question={this.props.handler.current}
                key={"question" + this.props.handler.currentID}
            />
        );
    }
}

/**
 * Root of the about component.
 */
class ELearnRaw extends React.Component<{|
    match: {| params: {| learningSet?: string |} |},
|}> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const match = this.props.match;
        let subComponent: React.Node;
        let title: string = "E-læring";
        if (match.params.learningSet) {
            const entry = new quizManagerHandler(match.params.learningSet);
            if (PLEIAR_ENV === "development") {
                window.PleiarActiveQuizManager = entry;
            }
            if (entry.exists) {
                title += " - " + entry.title;
                subComponent = <Quiz handler={entry} />;
            } else {
                subComponent = <NotFound root={false} />;
            }
        } else {
            subComponent = <ELearnList />;
        }
        return (
            <MainContainer app="elearning">
                <Row>
                    <Col>
                        <PageTitle title={title} />
                        {subComponent}
                    </Col>
                </Row>
            </MainContainer>
        );
    }
}

/**
 * Builds a menu of all elearn entries
 */
class ELearnList extends React.PureComponent<{||}> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        let apps: appList = [];
        for (const name of Object.keys(quizDataManager.data()).sort()) {
            apps.push({
                name: quizDataManager.data()[name].title,
                id: "elaering/" + name,
            });
        }
        apps = apps.sort((a, b) => a.name.localeCompare(b.name));
        // On non-mobile devices the list will be displayed in two columns, so we split it
        // into two columns so that it becomes:
        // A   B
        // C   D
        //
        // and not
        //
        // A   C
        // B   D
        //
        // On mobile it is single-column, so the original sort order is correct.
        //
        // This will on some devices probably do the wrong thing, but the menu will
        // work just fine either way, it just won't be perfectly alphabetical.
        if (!device.isMobileDevice()) {
            const appListOne: appList = [];
            const appListTwo: appList = [];
            let last: number = -1;
            for (const app of apps) {
                let addTo: appList = appListOne;
                if (last === 1) {
                    addTo = appListTwo;
                    last = 2;
                } else {
                    last = 1;
                }
                addTo.push(app);
            }
            apps = [].concat(appListOne, appListTwo);
        }
        return (
            <>
                <div className="col-lg-10 offset-lg-1">
                    <h1>E-læringskurs</h1>
                    <div className="mb-4">
                        Her finner du korte nettkurs produsert for medlemmer i
                        Fagforbundet. Kursene passer godt for deg som vil lære
                        noe nytt, eller friske opp kunnskapene i et tema du
                        kjenner til fra før. Alle kursene er gratis.
                    </div>
                    <MenuListRenderer apps={apps} />
                </div>
            </>
        );
    }
}

/**
 * Root that loads our data
 */
class ELearn extends React.Component<{|
    match: {| params: {| learningSet?: string |} |},
|}> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        return (
            <ConditionalRender
                component={ELearnRaw}
                test={() => quizDataManager.hasInitialized()}
                resolve={() => quizDataManager.initialize()}
                {...this.props}
            />
        );
    }
}

export default ELearn;
export { CourseCompleted, Question, Quiz, ELearn, ELearnRaw, ELearnList };
