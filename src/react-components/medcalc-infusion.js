/**
 * @prettier
 */
/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from "react";
import { numbers } from "../helper.numbers";
import { connect } from "react-redux";
import { PageTitle } from "./layout";
import { Row, Col, Form, FormGroup, Input, Label } from "reactstrap";
import { getValueFromLiteralOrEvent } from "../helper.general";
import {
    CalcExplanation,
    CalcExplanationLine,
    CalcExplanationDescription,
    CalcExplanationCalculation,
    CalcExplanationFormula,
    Fraction,
    FractionResult,
} from "./calculators-shared.js";
import {
    setContent,
    setMinutes,
    setHours,
    setCalculatorMode,
    toggleAdditionalCalculationsVisibility,
} from "../actions/medcalc-infusion";

import type { ReduxEventOrStringCallback } from "../types/callbacks";
import type {
    CalcResultWithExplanationLines,
    CalcExplanationLineArray,
} from "../types/calculator-types";

type MedCalcInfWrapperOwnProps = {|
    description: CalcExplanationLineArray,
    children: React.Node,
    title: string,
    hasAdditionalCalculations?: boolean,
|};
type MedCalcInfWrapperReduxStateProps = {| additionalCalculations: boolean |};
type MedCalcInfWrapperReduxDispatchProps = {|
    onToggleAdditionalCalculations: () => void,
|};
type MedCalcInfWrapperProps = {|
    ...MedCalcInfWrapperReduxDispatchProps,
    ...MedCalcInfWrapperReduxStateProps,
    ...MedCalcInfWrapperOwnProps,
|};

/**
 * Wrapping container around the infusion calculators, used to
 * render trekant.svg in the correct location
 */
class MedCalcInfWrapperUnconnected extends React.PureComponent<MedCalcInfWrapperProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        return (
            <div>
                <PageTitle title={this.props.title} />
                <Row>
                    <Col lg="9">{this.props.children}</Col>
                    <Col lg="3" className="d-none d-lg-block">
                        <img
                            className="img-fluid"
                            src="/trekant.svg"
                            alt="Medisinrekning-trekanten"
                        />
                    </Col>
                </Row>
                <CalcExplanation
                    additionalCalculationsToggle={
                        this.props.onToggleAdditionalCalculations
                    }
                    additionalCalculations={this.props.additionalCalculations}
                    hasAdditionalCalculations={
                        this.props.hasAdditionalCalculations
                    }
                >
                    {this.props.description}
                </CalcExplanation>
                <div className="d-lg-none">
                    <img
                        className="img-fluid"
                        src="/trekant.svg"
                        alt="Medisinrekning-trekanten"
                    />
                </div>
            </div>
        );
    }
}

const MedCalcInfWrapper = connect<
    MedCalcInfWrapperProps,
    MedCalcInfWrapperOwnProps,
    MedCalcInfWrapperReduxStateProps,
    MedCalcInfWrapperReduxDispatchProps,
    _,
    _,
>(
    (state): MedCalcInfWrapperReduxStateProps => {
        return {
            additionalCalculations:
                state.medCalc.infusion.additionalCalculations,
        };
    },
    (dispatch): MedCalcInfWrapperReduxDispatchProps => {
        return {
            onToggleAdditionalCalculations: () => {
                dispatch(toggleAdditionalCalculationsVisibility());
            },
        };
    },
)(MedCalcInfWrapperUnconnected);

/**
 * Performs conversion of a set of minutes and hours to a decimal number.
 * Also returns the <CalcExplanationLine>s to describe the calculation,
 * IF calculations are needed.
 *
 * Returns:
 * {
 *  description: Array,
 *  result: Number
 * }
 */
function getTimeAsDecimal(
    hours: number,
    minutes: number,
    round: boolean,
): CalcResultWithExplanationLines {
    const description = [];

    // Initial calculations
    const minuteHourFraction = numbers.conditionalRound(minutes / 60, round);
    const result = numbers.conditionalRound(hours + minuteHourFraction, round);
    // Convert time to a decimal number if needed
    if (minutes > 0) {
        description.push(
            <CalcExplanationLine key="minuteFraction">
                <CalcExplanationDescription>
                    Konverter minutt til desimaltal ved å dele det på 60
                </CalcExplanationDescription>
                <CalcExplanationCalculation>
                    <Fraction top={numbers.format(minutes)} bottom="60" />
                    <FractionResult>
                        = {numbers.format(minuteHourFraction)}
                    </FractionResult>
                </CalcExplanationCalculation>
                <CalcExplanationFormula>
                    <Fraction top="minutt" bottom="60" />
                    <FractionResult>= fraksjon av time</FractionResult>
                </CalcExplanationFormula>
            </CalcExplanationLine>,
        );
        if (hours > 0) {
            description.push(
                <CalcExplanationLine key="hourCalc">
                    <CalcExplanationDescription>
                        Legg desimaltalet for minutt til timane
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        {numbers.format(hours)} +
                        {numbers.format(minuteHourFraction)} ={" "}
                        {numbers.format(result)}
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula>
                        time + minutt som fraksjon = tid
                    </CalcExplanationFormula>
                </CalcExplanationLine>,
            );
        }
    }

    return {
        result,
        description,
    };
}

/**
 * Performs conversion of a set of minutes and hours to the number of minutes.
 * Also returns the <CalcExplanationLine>s to describe the calculation,
 * IF calculations are needed.
 *
 * Returns:
 * {
 *  description: Array,
 *  result: Number
 * }
 */
function getTimeInMinutes(
    hours: number,
    minutes: number,
): CalcResultWithExplanationLines {
    const description: CalcExplanationLineArray = [];

    let result: number = minutes;

    if (hours > 0) {
        const hoursInMinutes = hours * 60;
        description.push(
            <CalcExplanationLine key="hourCalc">
                <CalcExplanationDescription>
                    Konverter timar til minutt ved å gange det med 60
                </CalcExplanationDescription>
                <CalcExplanationCalculation>
                    {hours} × 60 = {hoursInMinutes}
                </CalcExplanationCalculation>
                <CalcExplanationFormula>
                    timar × 60 = minutt
                </CalcExplanationFormula>
            </CalcExplanationLine>,
        );
        result += hoursInMinutes;
        if (minutes > 0) {
            description.push(
                <CalcExplanationLine key="decimalHour">
                    <CalcExplanationDescription>
                        Legg saman minutta med timane
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        {numbers.format(hoursInMinutes)} +
                        {numbers.format(minutes)} ={numbers.format(result)}
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula>
                        timar i minutt + minutt = totalt tal minutt
                    </CalcExplanationFormula>
                </CalcExplanationLine>,
            );
        }
    }

    return {
        result,
        description,
    };
}

/**
 * Performs conversion of a set of hour, minute, ml into ml/hour.
 * Returns descriptions and the result.
 *
 * Returns:
 * {
 *  description: Array,
 *  result: Number
 *  }
 */
function getMlPerHour(
    volume: number,
    hour: number,
    minute: number,
    round: boolean,
): CalcResultWithExplanationLines {
    const timeAsDecimal = getTimeAsDecimal(hour, minute, round);
    const description = timeAsDecimal.description;

    // Calculate ml an hour
    const result = numbers.conditionalRound(
        volume / timeAsDecimal.result,
        round,
    );
    description.push(
        <CalcExplanationLine key="mlPerHour">
            <CalcExplanationDescription>
                Del totalvolum på tid
            </CalcExplanationDescription>
            <CalcExplanationCalculation>
                {numbers.format(volume)} +{" "}
                {numbers.format(timeAsDecimal.result)} ={" "}
                {numbers.format(result)}
            </CalcExplanationCalculation>
            <CalcExplanationFormula>
                <Fraction top="totalvolum" bottom="tid" />
                <FractionResult>= ml/t</FractionResult>
            </CalcExplanationFormula>
        </CalcExplanationLine>,
    );

    return {
        result,
        description,
    };
}

/*
 * Renders the input and result boxes
 */

/**
 * Props for InputAndResult. equalsSign, total, result and in must be provided by the caller.
 * The rest is bound to redux.
 */
type InputAndResultProps = {|
    // Required props
    equalsSign: string,
    total: number,
    result: number,
    in: string,
    // Redux-bound
    mode: "ml/t" | "dråpar/sek",
    contents: number,
    hours: number,
    minutes: number,
    onSetMinutes: ReduxEventOrStringCallback,
    onSetHours: ReduxEventOrStringCallback,
    onSetContent: ReduxEventOrStringCallback,
    onChangeMode: ReduxEventOrStringCallback,
|};

/**
 * Displays the input boxes and the result for the infusion calculator
 */
class InputAndResult extends React.PureComponent<InputAndResultProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        let warningMessage: ?string;
        let warning: React.Node | null = null;
        if (this.props.contents > 5000) {
            warningMessage =
                "Dette er ein stor mengde væske. Dobbeltsjekk at du har rett mengde.";
        } else {
            const time = this.props.hours + this.props.minutes / 60;
            const mlHour = this.props.contents / time;
            if (mlHour > 1200) {
                warningMessage =
                    "Dette er mykje væske på kort tid. Dobbeltsjekk infusjonstid og mengde.";
            } else if (mlHour > 499 && time > 4) {
                warningMessage =
                    "Dette er mykje væske over en lengre periode. Dobbeltsjekk infusjonstid og mengde.";
            }
        }
        if (warningMessage !== undefined) {
            warning = (
                <Row className="infusion-warning">
                    <Col>
                        <b className="text-danger">Advarsel:</b>{" "}
                        {warningMessage}
                    </Col>
                </Row>
            );
        }
        // Hack to pluralize hour/hours
        let hourPluralization: string = "timer";
        if (this.props.hours == 1) {
            hourPluralization = "time";
        }
        return (
            <div>
                <h4>
                    Rekn ut infusjonshastighet i&nbsp;
                    <select
                        value={this.props.in}
                        onChange={this.props.onChangeMode}
                    >
                        <option value="ml/t">ml/t</option>
                        <option value="dråpar/sek">dråpar/sek</option>
                    </select>
                </h4>
                <Row>
                    <Col>
                        <Form inline className="smallInputElements">
                            <Row>
                                <Col xs={3} sm="auto" className="px-0 ml-3">
                                    <FormGroup className="mr-2">
                                        <Input
                                            type="number"
                                            min="0"
                                            max="10000"
                                            className="mr-2"
                                            value={this.props.contents}
                                            onChange={(e) => {
                                                this.props.onSetContent(e);
                                            }}
                                            id="mlt-contents"
                                        />
                                        <Label htmlFor="mlt-time">
                                            ml
                                            <span className="d-none d-md-inline">
                                                &nbsp;skal gå inn på
                                            </span>
                                        </Label>
                                    </FormGroup>
                                </Col>
                                <Col
                                    xs={3}
                                    sm="auto"
                                    className="d-md-none d-flex-v-middle"
                                >
                                    <div className="d-inline d-md-none">
                                        over
                                    </div>
                                </Col>
                                <Col xs={2} sm="auto" className="px-0">
                                    <FormGroup className="mr-2">
                                        <Input
                                            type="number"
                                            min="0"
                                            max="96"
                                            className="mr-2"
                                            value={this.props.hours}
                                            onChange={(e) => {
                                                this.props.onSetHours(e);
                                            }}
                                            id="mlt-time"
                                        />
                                        <Label htmlFor="mlt-time">
                                            {" "}
                                            {hourPluralization}
                                        </Label>
                                    </FormGroup>
                                </Col>
                                <Col xs={2} sm="auto" className="px-0">
                                    <FormGroup className="mr-2">
                                        <Input
                                            type="number"
                                            min="0"
                                            max="59"
                                            className="mr-2"
                                            value={this.props.minutes}
                                            onChange={(e) => {
                                                this.props.onSetMinutes(e);
                                            }}
                                            id="mlt-min"
                                        />
                                        <Label htmlFor="mlt-min"> minutt</Label>
                                    </FormGroup>
                                </Col>
                                <Col className="d-flex-v-middle" sm="auto">
                                    <span>
                                        {this.props.equalsSign}{" "}
                                        <span id="medcalc-infusion-result">
                                            {numbers.format(this.props.result)}
                                        </span>{" "}
                                        {this.props.in}
                                    </span>
                                </Col>
                            </Row>
                        </Form>
                    </Col>
                </Row>
                {warning}
            </div>
        );
    }
}

// Connects the InfusionSpeedCalc to our redux store
const InputAndResultContainer = connect(
    (state) => {
        return {
            mode: state.medCalc.infusion.mode,
            contents: state.medCalc.infusion.content,
            hours: state.medCalc.infusion.hours,
            minutes: state.medCalc.infusion.minutes,
        };
    },
    (dispatch) => {
        return {
            onSetHours: (ev) => {
                const value = numbers.floatishInRangeFromThing(ev, 0, 72);
                dispatch(setHours(value));
            },
            onSetMinutes: (ev) => {
                const value = numbers.floatishInRangeFromThing(ev, 0, 59);
                dispatch(setMinutes(value));
            },
            onSetContent: (ev) => {
                const value = numbers.floatishInRangeFromThing(ev, 0, 10000);
                dispatch(setContent(value));
            },
            onChangeMode: (ev) => {
                const value = getValueFromLiteralOrEvent(ev);
                if (value === "ml/t" || value === "dråpar/sek") {
                    dispatch(setCalculatorMode(value));
                } else {
                    throw "onChangeMode: got invalid mode: " + value;
                }
            },
        };
    },
)(InputAndResult);

/**
 * Renders "secondary" results
 */
class DropsAdditionalResults extends React.PureComponent<{|
    results: {| dropsPer15: number, dropsPerMinute: number |},
|}> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        return (
            <div>
                <Row className="mt-3 text-muted">
                    <Col>
                        <h5>Andre tidseiningar</h5>
                        <small>
                            <Row>
                                <Col>
                                    {this.props.results.dropsPer15} dråpar pr.
                                    15 sekund
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    {this.props.results.dropsPerMinute} dråpar
                                    pr. minutt
                                </Col>
                            </Row>
                        </small>
                    </Col>
                </Row>
            </div>
        );
    }
}

type EqualsType = "=" | "≈";
type CalculationResult = {|
    equalsSign: EqualsType,
    total: number,
    descr: CalcExplanationLineArray,
    additionalResults: ?React.Node,
|};

/**
 * Calculates the speed of an infusion, in drops/sec
 */
function getDropsResult(
    contents,
    hours,
    minutes,
    round,
    additionalCalculations,
): CalculationResult {
    // The equals sign to render. Can be a normal or "approximate" equals sign
    let equalsSign: EqualsType = "=";
    let descr: CalcExplanationLineArray = [];

    const {
        result: timeInMinutes,
        description: timeDescription,
    }: CalcResultWithExplanationLines = getTimeInMinutes(hours, minutes);
    descr = [...descr, ...timeDescription];

    const volumeInDrops = contents * 20;
    const dropsPerMinute = numbers.conditionalRound(
        volumeInDrops / timeInMinutes,
        round,
    );
    const dropsPerSecond = numbers.conditionalRound(dropsPerMinute / 60, round);
    let dropsPerSecondRounded: number = dropsPerSecond;
    // Use the significant rounder if drops > 2
    if (dropsPerSecond > 2) {
        dropsPerSecondRounded = numbers.conditionalRoundToSimplestSignificant(
            dropsPerSecond,
            round,
        );
    }
    // Otherwise, use the regular rounder
    else {
        dropsPerSecondRounded = numbers.conditionalRound(dropsPerSecond, round);
    }

    descr.push(
        <CalcExplanationLine key="totalDrops">
            <CalcExplanationDescription>
                Gang totalvolum med 20 for å få totalvolum i dråpar (1ml = 20
                dråpar)
            </CalcExplanationDescription>
            <CalcExplanationCalculation>
                {contents} × 20 = {numbers.format(volumeInDrops)}
            </CalcExplanationCalculation>
            <CalcExplanationFormula>
                volum i ml × 20 = volum i dråpar
            </CalcExplanationFormula>
        </CalcExplanationLine>,
    );

    if (volumeInDrops > 0) {
        descr.push(
            <CalcExplanationLine key="dropsPerMinute">
                <CalcExplanationDescription>
                    Del totalvolum i dråpar på tid for å få dråpar/minutt
                </CalcExplanationDescription>
                <CalcExplanationCalculation>
                    <Fraction
                        top={numbers.format(volumeInDrops)}
                        bottom={timeInMinutes}
                    />
                    <FractionResult>
                        = {numbers.format(dropsPerMinute)}
                    </FractionResult>
                </CalcExplanationCalculation>
                <CalcExplanationFormula>
                    <Fraction top="volum i dråpar" bottom="60" />
                    <FractionResult>= dråpar/minutt</FractionResult>
                </CalcExplanationFormula>
            </CalcExplanationLine>,
        );
    }

    if (dropsPerMinute > 0) {
        descr.push(
            <CalcExplanationLine key="drops">
                <CalcExplanationDescription>
                    Del dråpar/minutt med 60 for å få dråpetakt pr. sekund
                </CalcExplanationDescription>
                <CalcExplanationCalculation>
                    <Fraction
                        top={numbers.format(dropsPerMinute)}
                        bottom={60}
                    />
                    <FractionResult>
                        = {numbers.format(dropsPerSecond)}
                    </FractionResult>
                </CalcExplanationCalculation>
                <CalcExplanationFormula>
                    <Fraction top="dråpar/min" bottom="60" />
                    <FractionResult>= dråpar/sek</FractionResult>
                </CalcExplanationFormula>
            </CalcExplanationLine>,
        );
    }

    if (dropsPerSecond != dropsPerSecondRounded && dropsPerSecondRounded > 0) {
        equalsSign = "≈";
        descr.push(
            <CalcExplanationLine key="dropsPerSecondRounded">
                <CalcExplanationDescription>Rund av</CalcExplanationDescription>
                <CalcExplanationCalculation>
                    {dropsPerSecond} ≈ {dropsPerSecondRounded}
                </CalcExplanationCalculation>
                <CalcExplanationFormula></CalcExplanationFormula>
            </CalcExplanationLine>,
        );
    }

    /*
     * Calculate the same result in different formats
     */

    // Per 15 seconds
    const dropsPer15 = dropsPerSecond * 15;
    const dropsPer15Rounded = numbers.conditionalRoundToSimplestSignificant(
        dropsPer15,
        round,
    );
    // Per minute
    const dropsPerMinuteRounded = numbers.conditionalRoundToSimplestSignificant(
        dropsPerMinute,
        round,
    );

    // If we have additional calculations descriptions enabled, append
    // those to descr
    if (additionalCalculations) {
        descr.push(
            <CalcExplanationLine key="drops15">
                <CalcExplanationDescription>
                    Gang dråpar/sek med 15 for å få dråpar pr. 15 sek
                </CalcExplanationDescription>
                <CalcExplanationCalculation>
                    {dropsPerSecond} * 15 = {dropsPer15}
                </CalcExplanationCalculation>
                <CalcExplanationFormula>dråpar/sek * 15</CalcExplanationFormula>
            </CalcExplanationLine>,
        );

        if (dropsPer15 != dropsPer15Rounded) {
            descr.push(
                <CalcExplanationLine key="drops15-rounding">
                    <CalcExplanationDescription>
                        Rund av dråpar/15sek
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        {dropsPer15} ≈ {dropsPer15Rounded}
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula></CalcExplanationFormula>
                </CalcExplanationLine>,
            );
        }

        if (dropsPerMinute != dropsPerMinuteRounded) {
            descr.push(
                <CalcExplanationLine key="drops60-rounding">
                    <CalcExplanationDescription>
                        Rund av dråpar/min
                    </CalcExplanationDescription>
                    <CalcExplanationCalculation>
                        {dropsPerMinute} ≈ {dropsPerMinuteRounded}
                    </CalcExplanationCalculation>
                    <CalcExplanationFormula></CalcExplanationFormula>
                </CalcExplanationLine>,
            );
        }
    }
    const additionalResults = (
        <DropsAdditionalResults
            results={{
                dropsPer15: dropsPer15Rounded,
                dropsPerMinute: dropsPerMinuteRounded,
            }}
        />
    );
    return {
        equalsSign,
        total: dropsPerSecondRounded,
        descr,
        additionalResults,
    };
}

/**
 * Calculates the result in ml/hr
 */
function getMlResult(contents, hours, minutes, round): CalculationResult {
    // The equals sign to render. Can be a normal or "approximate" equals sign
    let equalsSign: EqualsType = "=";
    let descr: CalcExplanationLineArray = [];

    const { result: mlPerHour, description: mlPerHourDescription } =
        getMlPerHour(contents, hours, minutes, round);
    descr = descr.concat(mlPerHourDescription);

    const mlPerHourRounded = numbers.conditionalRound(mlPerHour, round, 1);

    // Flow chokes on this
    if (mlPerHour != mlPerHourRounded) {
        equalsSign = "≈";
        descr.push(
            <CalcExplanationLine key="mltRounding">
                <CalcExplanationDescription>Rund av</CalcExplanationDescription>
                <CalcExplanationCalculation>
                    {mlPerHour} ≈ {mlPerHourRounded}
                </CalcExplanationCalculation>
                <CalcExplanationFormula></CalcExplanationFormula>
            </CalcExplanationLine>,
        );
    }

    return {
        equalsSign,
        total: mlPerHourRounded,
        descr,
        additionalResults: null,
    };
}

type InfusionSpeedCalcReduxStateProps = {|
    mode: "ml/t" | "dråpar/sek",
    contents: number,
    hours: number,
    minutes: number,
    additionalCalculations: boolean,
    round: boolean,
|};

type InfusionSpeedCalcOwnProps = {||};

type InfusionSpeedCalcProps = {|
    ...InfusionSpeedCalcReduxStateProps,
    ...InfusionSpeedCalcOwnProps,
|};

/**
 * Calculates the speed of an infusion, in either ml/hr or drops/sec
 */
class InfusionSpeedCalcUnconnected extends React.Component<InfusionSpeedCalcProps> {
    // Rendering
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const {
            contents,
            hours,
            minutes,
            round,
            additionalCalculations,
            mode,
        } = this.props;
        let hasAdditionalCalculations: boolean = false;
        let result: CalculationResult;
        if (mode === "dråpar/sek") {
            result = getDropsResult(
                contents,
                hours,
                minutes,
                round,
                additionalCalculations,
            );
            hasAdditionalCalculations = true;
        } else {
            result = getMlResult(contents, hours, minutes, round);
        }
        if (Number.isNaN(result.total) || result.total === Infinity) {
            result.descr = [];
            result.total = 0;
            result.equalsSign = "=";
            result.additionalResults = null;
        }
        return (
            <MedCalcInfWrapper
                title={"Medisinrekning - " + mode}
                description={result.descr}
                hasAdditionalCalculations={hasAdditionalCalculations}
            >
                <InputAndResultContainer
                    equalsSign={result.equalsSign}
                    total={result.total}
                    result={result.total}
                    in={mode}
                />
                {result.additionalResults}
            </MedCalcInfWrapper>
        );
    }
}

// Connects the InfusionSpeedCalc to our redux store
const InfusionSpeedCalc: React.AbstractComponent<InfusionSpeedCalcOwnProps> =
    connect<
        InfusionSpeedCalcProps,
        InfusionSpeedCalcOwnProps,
        InfusionSpeedCalcReduxStateProps,
        _,
        _,
        _,
    >((state): InfusionSpeedCalcReduxStateProps => {
        return {
            mode: state.medCalc.infusion.mode,
            contents: state.medCalc.infusion.content,
            hours: state.medCalc.infusion.hours,
            minutes: state.medCalc.infusion.minutes,
            additionalCalculations:
                state.medCalc.infusion.additionalCalculations,
            round: state.global.round,
        };
    }, {})(InfusionSpeedCalcUnconnected);

export default InfusionSpeedCalc;
