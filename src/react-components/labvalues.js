/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import type { LabValueEntry as LabValueEntryType, LabValueDataStructure } from '../types/data';
import { Row, Col, Table } from 'reactstrap';
import slug from '../helper.slugs.js';
import { MobileAbbreviation, MobileAbbrWordDefinition } from './shared/abbrev';

/**
 * The type for the LabValueGenericNotices object
 */
type LabValueNoticeType = {
    [string]: string,
    "refStringSpecial": {
        [string]: string
    }
};

/**
 * A constant list of generic notices that apply to specific groups of
 * lab values. This is used both when rendering a single cleartext element
 * and when rendering tables.
 */
const LabValueGenericNotices: LabValueNoticeType = {
    UrineHormone: "Vær oppmerksom på at konsentrasjonen av mange hormoner kan variere betydelig gjennom døgnet, gjennom menstruasjonssyklus og gjennom livet.",
    UrineElectrolyte: "I urinen er utskillelsen av elektrolytter, urinstoff og urinsyre avhengig av kostholdet, og kan derfor variere sterkt.",
    UrineGeneral: "Referanseverdiene per døgn er de som vanligvis finnes, og beregningene forutsetter at urinvolum og samletid er oppgitt.",
    refStringGeneral: "Referanseområde",
    refStringSpecial: {
        "Klinisk farmakologi":"Terapeutisk område"
    },
};
/**
 * A constant list of "material shorthand" to "material full name" values.
 * This is used both when rendering a single cleartext element and when
 * rendering tables.
 */
const LabValueMaterialMap = {
    'B':{
        name: 'Fullblod',
        description: 'Fullblod er for analyser som må utføres uten at blodet har blitt filtrert. Disse prøvene tas ofte på glass uten tilsetning.'
    },
    'E':{
        name: 'Erytrocytter',
        description: 'Erytrocytter er de røde blodcellene.',
    },
    'SP':{
        name: 'Spinalvæske',
        description: 'Spinalvæske er væsken som finnes innen for hjernehinnene. Kalles også for cerbrospinalvæske, forkortet CSF.',
    },
    'P':{
        name: 'Plasma',
        description: 'Plasma er den delen av blodet som er igjen når man har sentrifugert bort alle blodceller.',
    },
    'U':{
        name: 'Urin',
        description: 'En urinprøve kan f.eks. avdekke problemer med nyrene eller annen sykdom i urinveiene.',
    },
    'S':{
        name: 'Serum',
        description: 'Serum er blodet når alle blodceller (erytrocytter, leukocytter, blodplater) og koagulasjonsfaktorer er fjernet.',
    },
    'PT':{
        name: 'Pasient',
        description: 'Prøver hvor man må ha pasienten fysisk til stede for å ta. Dette kan f.eks. være hvis man skal måle hvor lang tid det tar før en person slutter å blø.',
    },
};

/**
 * A single element (field) for a cleartext rendering of a lab value
 */
class LabValueEntryCleartextElement extends React.PureComponent<{| title: string, value: string |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const {title,value} = this.props;
        if (/\n/.test(value))
        {
            return <div><div className="labValueField">{title}:</div><div className="labValueLiteral">{value}</div></div>;
        }
        else
        {
            return <div><span className="labValueField">{title}:</span> {value}</div>;
        }
    }
}

/**
 * A component that renders a single lab value to a text box
 */
class LabValueEntryCleartext extends React.PureComponent<{|entry: LabValueEntryType |}>
{
    /**
     * Method that adds notices from LabValueGenericNotices to the "merknad" field
     * for a lab value, if needed.
     */
    getMerknad (): React.Node | null
    {
        const { entry } = this.props;
        let merknad: ?string = entry.merknad;
        if(entry.materiale == "U")
        {
            if (merknad)
            {
                merknad += "\n";
            }
            else
            {
                merknad = "";
            }
            if (/Hormon/.test(entry.category))
            {
                merknad += LabValueGenericNotices.UrineHormone;
            }
            else
            {
                merknad += LabValueGenericNotices.UrineElectrolyte;
            }
            merknad += " "+LabValueGenericNotices.UrineGeneral;
        }
        if(merknad)
        {
            return <LabValueEntryCleartextElement title="Merknad" value={merknad} />;
        }
        return null;
    }

    /**
     * Method that expands the material from the shorthand value to the full value
     * using the LabValueMaterialMap.
     */
    expandMaterial (): string
    {
        const { entry } = this.props;
        if ( LabValueMaterialMap[ entry.materiale ] )
        {
            return LabValueMaterialMap[ entry.materiale ].name;
        }
        return entry.materiale;
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { entry } = this.props;
        const merknad = this.getMerknad();
        const material = this.expandMaterial();
        let refString: string = "Referanseområde";
        if(entry.category == "Klinisk farmakologi")
        {
            refString = "Terapeutisk område";
        }
        return <Row key={entry.navn} className="labValueEntry">
            <Col>
                <div className="labValueTitle" target="_blank">Laboratorieprøve: {entry.materiale}-{entry.navn}</div>
                <LabValueEntryCleartextElement title="Materiale" value={material} />
                <LabValueEntryCleartextElement title={refString} value={entry.ref} />
                <span className="labValueSecondary">
                    <LabValueEntryCleartextElement title="Kategori" value={entry.category} />
                    {merknad}
                </span>
            </Col>
        </Row>;
    }
}

/**
 * The props for LabValueTableRow
 */
type LabValueTableRowProps = {| entry: LabValueEntryType |};
/**
 * Renders a single lab value as a table row
 */
class LabValueTableRow extends React.PureComponent<LabValueTableRowProps, {| tooltipVisible: boolean |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { entry } = this.props;
        const expandedMaterial = LabValueMaterialMap[ entry.materiale ];
        return <tr>
            <td>{ entry.navn }</td>
            <td>
                <MobileAbbrWordDefinition mobileText={entry.materiale} fullText={expandedMaterial.name} definition={expandedMaterial.description} id={entry.navn+entry.materiale} />
            </td>
            <td>{ entry.ref }</td>
            <td>{ entry.merknad }</td>
        </tr>;
    }
}

/**
 * Renders a single category of lab values as a table, with messages from
 * LabValueGenericNotices displayed above it, if needed.
 */
class LabValueCategoryTable extends React.PureComponent<{| values: LabValueDataStructure, category: string, inhibitTitle?: boolean |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { values, category } = this.props;
        const entries = [];
        let title: React.Node | null = <h3 id={slug.get(category)}>{category}</h3>;
        if(this.props.inhibitTitle)
        {
            title = null;
        }
        for(const entryNo of values.index[category])
        {
            entries.push(<LabValueTableRow key={entryNo} entry={ values.values[entryNo] } />);
        }
        let refHeader: string = LabValueGenericNotices.refStringGeneral;
        if ( LabValueGenericNotices.refStringSpecial[ category ] !== undefined )
        {
            refHeader = LabValueGenericNotices.refStringSpecial[ category ];
        }
        let notices: React.Node = null;
        if(category == "Hormonanalyser i urin")
        {
            notices = <div>{LabValueGenericNotices.UrineHormone} {LabValueGenericNotices.UrineGeneral}</div>;
        }
        else if(category === "Klinisk kjemi i urin og spinalvæske")
        {
            notices = <div>{LabValueGenericNotices.UrineElectrolyte} {LabValueGenericNotices.UrineGeneral}</div>;
        }
        return <div className="LabValueTable">
            {title}
            {notices}
            <Table hover responsive>
                <thead>
                    <tr>
                        <th>Navn</th>
                        <th>
                            <span className="d-none d-md-block">Materiale</span>
                            <span className="d-md-none">
                                <MobileAbbreviation abbrev="Mat." full="Materiale" />
                            </span>
                        </th>
                        <th>{refHeader}</th>
                        <th>Merknad</th>
                    </tr>
                </thead>
                <tbody>
                    {entries}
                </tbody>
            </Table>
        </div>;
    }
}

/**
 * Renders tables for each of the categories of lab values we have
 */
class LabValueTable extends React.PureComponent<{| values: LabValueDataStructure |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const renderTo = [];
        const LabValues = this.props.values;

        slug.reset();

        for(const category in LabValues.index)
        {
            renderTo.push(<LabValueCategoryTable key={category} values={this.props.values} category={category} />);
        }
        return <div className="LabValueTables">
            {renderTo}
        </div>;
    }
}

// Public exports
export { LabValueEntryCleartext, LabValueTable, LabValueCategoryTable };
// Testing exports
export { LabValueEntryCleartextElement, LabValueTableRow, LabValueMaterialMap };
