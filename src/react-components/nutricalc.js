/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from "react";
import { Switch, Route, Redirect } from 'react-router-dom';
import { Handle404 } from './shared';
import { ToolContainer } from './tools';
import NutriCalcDay from './nutricalc-day';
import NutriCalcReq from './nutricalc-req';
import NutriCalcBMI from './nutricalc-bmi';
import { Row, Col } from 'reactstrap';
import type { Match as RouterMatch } from 'react-router-dom';
import type { NutritionInfoEntry } from '../types/data';

/**
 * Root of the nutricalc component.
 */
class NutriCalc extends React.PureComponent<{|match: RouterMatch|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const match = this.props.match;
        return  <ToolContainer tool="nutricalc">
                    <div className="mt-2">
                        <Switch>
                            <Route path={match.path} exact render={() => { return <Redirect to={match.path+'/dagsinntak'} />; }} />
                            <Route path={match.path+'/dagsinntak'} component={NutriCalcDay} />
                            <Route path={match.path+'/energibehov'} component={NutriCalcReq } />
                            <Route path={match.path+'/bmi'} component={NutriCalcBMI} />
                            <Handle404 />
                        </Switch>
                    </div>
                </ToolContainer>;
    }
}

/**
 * Component used to render results from the nutrition database in search results
 */
class NutritionSearchRenderer extends React.PureComponent<{|entry: NutritionInfoEntry|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { entry } = this.props;
        const info = [
            <div key="kcal">{entry.kcal} kcal</div>
        ];
        if(entry.protein)
        {
            info.push(<div key="protein">{entry.protein}g protein</div>);
        }
        if(entry.fluidML)
        {
            info.push(<div key="fluidml">{entry.fluidML} ml væske</div>);
        }
        return <Row className="searchEntry">
            <Col>
                <div><span className="searchTitle" target="_blank">Ernæringsinformasjon</span>: {entry.name}</div>
                {info}
            </Col>
        </Row>;
    }
}

export default NutriCalc;
export { NutritionSearchRenderer };
