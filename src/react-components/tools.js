/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import * as React from 'react';
import { MainContainer, PageTitle } from './layout';
import { Row, Col } from 'reactstrap';
import NutriCalc from './nutricalc';
import MedCalc from './medcalc';
import NEWS from './news';
import MedicationSynonyms from './med-synonyms';
import { Handle404 } from './shared';
import { Switch, Route } from 'react-router-dom';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import ToolsList from '../tools.list.js';
import type { Match as RouterMatch } from 'react-router-dom';
import type { ToolListEntry } from '../types/data';
import { RequiresAuth } from './auth';
import SimpleAccordion from './shared/simple-accordion';

/**
 * This is the container each tool should wrap itself in. It functions like
 * `<MainContainer>`, but this expects a `<MainContainer>` to exist as its
 * parent already, so it won't do much other than return a div with a class set
 * and a navigation link. It will also wrap it in `<RequiresAuth>` unless
 * you pass requiresAuth={false}.
 */
class ToolContainer extends React.PureComponent<{| tool: string, requiresAuth?: boolean, children: React.Node |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { requiresAuth } = this.props;
        const wrapper = <div className={"app-"+this.props.tool}>
                <Link className="d-block tool-back-link" to='/verktoy'>« Tilbake til verktøylista</Link>
                {this.props.children}
            </div>;
        if (requiresAuth === false)
        {
            return wrapper;
        }
        return <RequiresAuth>
            {wrapper}
        </RequiresAuth>;
    }
}

/**
 * This builds the graphical index of tools
 */
class ToolsIndex extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const tools = [];
        for(const category of ToolsList)
        {
            const entries = [];
            for(const entry of category.tools)
            {
                entries.push(
                    <li key={entry.path}><Link to={"/verktoy"+entry.path}>{entry.title}</Link></li>
                );
            }
            tools.push({
                header: category.title,
                render: <ul>{entries}</ul>
            });
        }

        return <div>
                <h1>Verktøy</h1>
                <PageTitle title="Verktøy" />
                <SimpleAccordion elements={tools} alwaysOpen={true} />
            </div>;
    }
}

/**
 * Toplevel handler and router of everything under /verktoy
 */
class ToolsSection extends React.PureComponent<{|match: RouterMatch, location: Location|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { match, location } = this.props;
        let small: boolean = false;
        if(match.path === location.pathname)
        {
            small = true;
        }

        return <MainContainer app="tools" small={small}>
            <Switch>
                <Route path={match.path} exact component={ToolsIndex} />
                <Route path={match.path+'/ernaering'} component={NutriCalc} />
                <Route path={match.path+'/medikamentregning'} component={MedCalc} />
                <Route path={match.path+'/news'} component={NEWS} />
                <Route path={match.path+'/medikament/synonymer'} component={MedicationSynonyms} />
                <Handle404 />
            </Switch>
        </MainContainer>;
    }
}

/**
 * Component that renders an entry from our ToolsList as a search result
 * for global search. Used by {@link WhateverRenderer}
 */
class ToolsSearchRenderer extends React.PureComponent<{| entry: ToolListEntry |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { entry } = this.props;
        let description: ?string;
        if (entry.description)
        {
            description = entry.description;
        }
        return <Row className="toolEntry">
            <Col>
                <Link to={"/verktoy"+entry.path} className="linkWrapper">
                    <div className="toolSearchTitle likeLink" target="_blank">Verktøy: {entry.title}</div>
                    {description}
                </Link>
            </Col>
        </Row>;
    }
}

export default (withRouter(ToolsSection): React.AbstractComponent<{| |}>);
export { ToolsIndex, ToolContainer, ToolsSearchRenderer };
