/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { PageTitle } from './layout';
import { Row, Col, Label, Input, Collapse } from 'reactstrap';
import OptionSelector from './shared/option-selector';
import { setNewsBP, setNewsRF, setNewsTemp, setNewsSpO2, setNewsPulse, setNewsConsciousness, toggleNewsO2, toggleNewsAlternateO2Table } from '../actions/news';
import { ExternalLink } from './shared/links';
import { connect } from 'react-redux';
import { ToolContainer } from './tools';
import { WordDefinitionFromDictionary } from './shared/abbrev';

import type {  OptionSelectorKey } from './shared/option-selector';
import type { newsState, BPValuesType, pulseValuesType, tempValuesType, RFValuesType, SpO2ValuesType, consciousnessValuesType } from '../reducers/news';

/**
 * Props for the NEWS class
 */
type NEWSDispatchProps = {|
    onBPUpdate           : (string) => void,
    onPulseUpdate        : (string) => void,
    onTempUpdate         : (string) => void,
    onRFUpdate           : (string) => void,
    onSpO2Update         : (string) => void,
    onConsciousnessUpdate: (string) => void,
    onToggleO2           : () => void,
    onToggleAlternateSPO2: () => void,
|};
type NEWSProps = {|
    ...NEWSDispatchProps,
    ...newsState
|};

/**
 * State for the NEWS class
 */
type NEWSState = {|
    displayO2TableInfo: boolean
|};

/**
 * The return type from the calculation method on the NEWS class.
 * Score is the numeric score, itemsWithScore3 is (maybe) an array with a list
 * of fields that have a score of 3, while explanation is an array of
 * NEWSExplanationLines
 */
type calcReturnType = {|
    score: number,
    itemsWithScore3?: Array<string>,
    explanation?: Array<React.Node>
|};

/**
 * The props for the NEWSExplanationLine
 */
type NEWSExplanationLineProps = {|
    score: number,
    explanation: string,
|} | {|
    left: string,
    right: string | number,
    rightStyle?: string,
    style: string
|};

/**
 * An item that descibes an option for a NEWS entry.
 *
 * This is explicitly not exact, so that an array can contain both NEWSOptionItem's and
 * OptionSelectorEntry items.
 */
type NEWSOptionItem<Type> = {| // eslint-disable-line flowtype/require-exact-type
    +key: string & OptionSelectorKey & Type,
    +name?: string,
    +score: number,
    +explanation: string,
|} | {|
    +key: string & OptionSelectorKey & Type,
    +name?: string,
    +score: 0
|} | {|
    +key: string & OptionSelectorKey & Type,
    +name?: string,
    +resultCalculator: (boolean) => {| score: number, explanation?: string |}
|};
/**
 * A list of {@link NEWSOptionItem}s
 */
type BPOptionList = Array<NEWSOptionItem<BPValuesType>>;
type pulseOptionList = Array<NEWSOptionItem<pulseValuesType>>;
type tempOptionList = Array<NEWSOptionItem<tempValuesType>>;
type RFOptionList = Array<NEWSOptionItem<RFValuesType>>;
type SpO2OptionList = Array<NEWSOptionItem<SpO2ValuesType>>;
type consciousnessOptionList = Array<NEWSOptionItem<consciousnessValuesType>>;
export type NEWSOptionList = BPOptionList | pulseOptionList | tempOptionList | RFOptionList | SpO2OptionList | consciousnessOptionList;

/**
 * The options, score and explanation for the blood pressure parameter
 */
const BPOptions: BPOptionList = [
   {
        key: 'under91',
        name: '≤90',
        score: 3,
        explanation: "Systolisk blodtrykk ≤90"
    },
    {
        key: '91-100',
        name: '91 - 100',
        score: 2,
        explanation: "Systolisk blodtrykk mellom 91-100"
    },
    {
        key: '101-110',
        name: '101 - 110',
        score: 1,
        explanation: "Systolisk blodtrykk mellom 101-110"
    },
    {
        key: '111-219',
        name: '111 - 219',
        score: 0,
    },
    {
        key: 'over219',
        name: '≥220',
        score: 3,
        explanation: "Systolisk blodtrykk ≥220"
    }
];

/**
 * The options, score and explanation for the pulse parameter
 */
const pulseOptions: pulseOptionList = [
    {
        key: 'under41',
        name: '≤40',
        score: 3,
        explanation: 'Puls er ≤40',
    },
    {
        key: '41-50',
        score: 1,
        explanation: 'Puls er mellom 41 - 50',
    },
    {
        key: '51-90',
        score: 0,
    },
    {
        key: '91-110',
        score: 1,
        explanation: 'Puls er mellom 91-110',
    },
    {
        key: '111-130',
        score: 2,
        explanation: 'Puls er mellom 111-130',
    },
    {
        key: 'over130',
        name: '≥131',
        score: 3,
        explanation: 'Puls er ≥131',
    }
];

/**
 * The options score and explanation for the temperature parameter
 */
const tempOptions: tempOptionList = [
    {
        key: 'under35.1',
        name: '≤35,0',
        score: 3,
        explanation: 'Temperatur er ≤35,0',
    },
    {
        key: '35.1-36.0',
        name: '35,1 - 36,0',
        score: 1,
        explanation: 'Temperatur er mellom 35,1 - 36,0',
    },
    {
        key: '36.1-38.0',
        name: '36,1 - 38,0',
        score: 0,
    },
    {
        key: '38.1-39.0',
        name: '38,1 - 39,0',
        score: 1,
        explanation: 'Temperatur er mellom 38,1 - 39,0',
    },
    {
        key: 'over39.0',
        name: '≥39,1',
        score: 2,
        explanation: 'Temperatur er ≥39,1',
    }
];

/**
 * The options score and explanation for the respiration frequency parameter
 */
const rfOptions: RFOptionList = [
    {
        key: 'under9',
        name: '≤8',
        score: 3,
        explanation: 'Respirasjonsfrekvens ≤8',
    },
    {
        key: '9-11',
        score: 1,
        explanation: 'Respirasjonsfrekvens mellom 9 og 11',
    },
    {
        key: '12-20',
        score: 0,
    },
    {
        key: '21-24',
        score: 2,
        explanation: 'Respirasjonsfrekvens mellom 21 og 24',
    },
    {
        key: 'over24',
        name: '≥25',
        score: 3,
        explanation: 'Respirasjonsfrekvens ≥25',
    }
];

/**
 * The options for the SpO₂ parameter, when using the MAIN (default) table
 */
const SpO2Options: SpO2OptionList = [
    {
        key: 'under92',
        name: '≤91',
        score: 3,
        explanation: 'SpO₂ ≤91%',
    },
    {
        key: '92-93',
        name: '92-93',
        score: 2,
        explanation: 'SpO₂ mellom 92% og 93%',
    },
    {

        key: '94-95',
        name: '94-95',
        score: 1,
        explanation: 'SpO₂ mellom 94% og 95%',
    },
    {
        key: 'over95',
        name: '≥95',
        score: 0
    }
];

/**
 * The options for the SpO₂ parameter, when using the SECONDARY (with
 * CO₂-retention) table
 *
 * This includes calculation for some of the scoring, since the score is
 * suppose to be 0 when not on o₂, but 1-3 when the person is on o₂.
 */
const SpO2OptionsAlternate: SpO2OptionList = [
    {
        key: 'under84',
        name: '≤83',
        score: 3,
        explanation: 'SpO₂ ≤83%',
    },
    {
        key: '84-85',
        name: '84 - 85',
        score: 2,
        explanation: 'SpO₂ mellom 84% - 85%',
    },
    {
        key: '86-87',
        name: '86 - 87',
        score: 1,
        explanation: 'SpO₂ mellom 86% - 87%',
    },
    {
        key: '88-92',
        name: '88 - 92',
        score: 0,
    },
    {
        key: '93-94',
        name: '93 - 94',
        resultCalculator: (usingO2) =>
        {
            if(usingO2)
            {
                return {
                    score: 1,
                    explanation: 'SpO₂ mellom 93% og 94% med O₂ tilførsel',
                };
            }
            return { score: 0 };
        },
    },
    {
        key: '94-96',
        name: '95 - 96',
        resultCalculator: (usingO2) =>
        {
            if(usingO2)
            {
                return {
                    score: 2,
                    explanation: 'SpO₂ mellom 95% og 96% med O₂ tilførsel',
                };
            }
            return { score: 0 };
        },
    },
    {
        key: 'over96',
        name: '≥97',
        resultCalculator: (usingO2) =>
        {
            if(usingO2)
            {
                return {
                    score: 3,
                    explanation: 'SpO₂ ≥97% med O₂ tilførsel',
                };
            }
            return { score: 0 };
        },
    },
];

/**
 * The options for the consciousness parameter
 */
const consciousnessOptions: consciousnessOptionList = [
    {
        key: 'awake',
        name: 'Våken',
        score: 0,
    },
    {
        key: 'confused',
        name: 'Forvirret',
        score: 3,
        explanation: 'Nyoppstått forvirring',
    },
    {
        key: 'speech',
        name: 'Responderer kun på tiltale',
        score: 3,
        explanation: 'Pasienten responderer kun på tiltale',
    },
    {
        key: 'pain',
        name: 'Responeder på smertestimuli',
        score: 3,
        explanation: 'Pasienten responderer kun på smertestimuli',
    },
    {
        key: 'none',
        name: 'Ikke responsiv',
        score: 3,
        explanation: 'Pasienten er ikke bevisst',
    }
];


/**
 * The internal state for {@link NEWSWhatIsInfo}
 */
type NEWSWhatIsInfoState = {|
    visible: boolean
|};
/**
 * Component implementing the "What is NEWS" section
 */
class NEWSWhatIsInfo extends React.PureComponent<{||},NEWSWhatIsInfoState>
{
    state: NEWSWhatIsInfoState = {
        visible: false
    };

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <div>
            <span onClick={() => { this.setState({ visible: !this.state.visible}); }} className="likeLink">Hva er NEWS 2?</span>
            <Collapse isOpen={this.state.visible}>
                <div className="mb-4 cardLike">
                    NEWS 2 står for «National Early Warning Score» versjon 2. Det
                    er et screeningverktøy som er ment til å oppdage endringer i
                    tilstanden til en pasient tidlig, og dermed få satt i gang
                    tiltak i god tid. Verktøyet er ikke en erstatning for klinisk
                    skjønn. NEWS 2 egner seg heller ikke til bruk på barn under 16 år
                    eller gravide.
                </div>
            </Collapse>
        </div>;
    }
}

/**
 * Renders a line with an explanation of why something scored as it did
 */
class NEWSExplanationLine extends React.PureComponent<NEWSExplanationLineProps>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        let left: ?string;
        let right: ?string | number;
        let styling: ?string;
        let rightStyling: ?string;
        if(this.props.explanation)
        {
            left = this.props.explanation;
            right = this.props.score;
            if(this.props.score === 3)
            {
                styling = 'news-level-3-bg';
            }
            else if(this.props.score === 2)
            {
                styling = 'news-level-2-bg';
            }
            else if(this.props.score === 1)
            {
                styling = 'news-level-1-bg';
            }
            else
            {
                throw('NEWSExplanationLine got unhandled score: '+this.props.score+' for "'+this.props.explanation+'"');
            }
        }
        else if(this.props.left)
        {
            left = this.props.left;
            right = this.props.right;
            styling = this.props.style;
            rightStyling = this.props.rightStyle;
        }
        return <Row className={styling}>
            <Col xs={10} md={6}>
                {left}
            </Col>
            <Col xs={2} className={rightStyling}>
                {right}
            </Col>
        </Row>;
    }
}

/**
 * The NEWS 2 calculator
 */
class NEWS extends React.Component<NEWSProps, NEWSState>
{
    state: NEWSState = {
        displayO2TableInfo: false
    };

    /**
     * Calculates the score for a NEWS paramerter
     */
    calculateNEWSParameterScore (explanationLines: Array<React.Node>, itemsWithScore3: Array<string>, optionName: string, parameter: string, options: NEWSOptionList, calculatorCallbackValue: ?boolean): number
    {
        for(const option of options)
        {
            if(option.key === parameter)
            {
                const {key} = option;
                let score: number = 0;
                let explanation: ?string;
                if(option.resultCalculator)
                {
                    if(calculatorCallbackValue == null)
                    {
                        throw('resultCalculator found in options but no calculatorCallbackValue provided');
                    }
                    const calculated = option.resultCalculator(calculatorCallbackValue);
                    score = calculated.score;
                    explanation = calculated.explanation;
                }
                else
                {
                    score = option.score;
                }
                if(score !== 0)
                {
                    if(option.explanation)
                    {
                        explanation = option.explanation;
                    }
                    if (!explanation)
                    {
                        throw('No valid explanation provided for '+parameter);
                    }
                    if(score >= 3)
                    {
                        itemsWithScore3.push(optionName);
                    }
                    explanationLines.push(
                        <NEWSExplanationLine key={key} score={score} explanation={explanation} />
                    );
                    return score;
                }
                return score;
            }
        }
        throw(parameter+' not found in provided NEWSOptionList');
    }

    /**
     * Calculates the NEWS score
     */
    calculateNewsScore (): calcReturnType
    {
        const { BP, pulse, temp, RF, SpO2, consciousness, usingO2, alternateO2Table } = this.props;
        if(BP == null || pulse == null || temp == null || RF == null || SpO2 == null || consciousness == null)
        {
            return { score: -1 };
        }

        const explanation = [];
        const itemsWithScore3 = [];
        let score: number = 0;

        score += this.calculateNEWSParameterScore(explanation, itemsWithScore3, "bevissthetsnivå"     , consciousness, consciousnessOptions);

        score += this.calculateNEWSParameterScore(explanation, itemsWithScore3, "systolisk blodtrykk" , BP           , BPOptions);

        score += this.calculateNEWSParameterScore(explanation, itemsWithScore3, "puls"                , pulse        , pulseOptions);
        score += this.calculateNEWSParameterScore(explanation, itemsWithScore3, "temperatur"          , temp         , tempOptions);

        score += this.calculateNEWSParameterScore(explanation, itemsWithScore3, "respirasjonsfrekvens", RF           , rfOptions);

        let enabledSPO2Options: ?NEWSOptionList;
        if(alternateO2Table)
        {
            enabledSPO2Options = SpO2OptionsAlternate;
        }
        else
        {
            enabledSPO2Options = SpO2Options;
        }

        score += this.calculateNEWSParameterScore(explanation, itemsWithScore3, "SpO₂", SpO2,enabledSPO2Options,usingO2);

        if(usingO2)
        {
            score += 2;
            explanation.push(<NEWSExplanationLine key="usingO2" score={2} explanation="Får O2-tilførsel" />);
        }

        return {
            score, explanation, itemsWithScore3
        };
    }

    /**
     * Renders the news score with recommendations
     */
    renderScore (score: number, explanation?: Array<React.Node>, itemsWithScore3?: Array<string>): React.Node | Array<React.Node>
    {
        if(score > -1)
        {
            const result: Array<React.Node> = [];
            const scoreInterpretation: Array<React.Node> = [];
            const numberOfItemsWithScore3: number = itemsWithScore3 ? itemsWithScore3.length : 0;
            if(score > 0)
            {
                result.push(
                    <Row key="explanation">
                        <Col className="mt-4">
                            <h4>Forklaring</h4>
                            <NEWSExplanationLine left="Beskrivelse" right="Poengsum" style="font-weight-bold" rightStyle="d-none d-md-block" />
                            {explanation}
                            <NEWSExplanationLine left="Totalt" right={score} style=""  />
                        </Col>
                    </Row>
                );
            }
            if(score >= 7)
            {
                scoreInterpretation.push(<div key="kontaktOverlege" className="bg-danger text-white font-weight-bold text-center">KONTAKT ANSVARLIG OVERLEGE UMIDDELBART!</div>);
                scoreInterpretation.push(<div key="interpretation" className="mt-3">
                    NEWS 2 poengsummen er {score}. <span className="font-weight-bold">Kontakt ansvarlig overlege.</span>
                    <div className="actions mt-2">
                        <h5>Tiltak:</h5>
                        <ul>
                            <li>Kontinuerlig monitorering av vitalia.</li>
                            <li>Vurder pasienten for overflytting til intensivavdeling.</li>
                            <li>Vurder om pasienten er i risiko for sepsis.</li>
                            <li>Øyeblikkelig tilsyn av overlege og anestesilege.</li>
                        </ul>
                    </div>
                </div>);
            }
            else if(score >= 5 || numberOfItemsWithScore3 > 0)
            {
                let interpretationReason: ?string;
                let mainRecommendation: string = "Hvis denne har økt siden sist må du umiddelbart informere ansvarlig lege.";
                if(score >= 5 || !itemsWithScore3)
                {
                    interpretationReason = 'NEWS 2 poengsummen er '+score;
                }
                else if(score === 3)
                {
                    interpretationReason = "Pasienten skåret 3 på "+itemsWithScore3[0];
                    mainRecommendation = "Du bør informere ansvarlig lege om dette.";
                }
                else if(itemsWithScore3)
                {
                    interpretationReason = "NEWS 2 poengsummen er "+score+", men pasienten skåret 3 på "+itemsWithScore3[0];
                }
                scoreInterpretation.push(<div key="interpretation">
                    {interpretationReason}.{" "}
                    <span className="font-weight-bold">{mainRecommendation}</span>
                    <div className="actions mt-2">
                        <h5>Tiltak:</h5>
                        <ul>
                            <li>Mål vitalia på pasienten minst en gang i timen.</li>
                            <li>Vurder om pasienten er i risiko for sepsis.</li>
                            <li>Øykeblikkelig tilsyn av ansvarlig lege.</li>
                        </ul>
                    </div>
                </div>);
            }
            else if(score >= 1)
            {
                scoreInterpretation.push(<div key="interpretation">
                    NEWS poengsummen er {score}.
                    {" "}
                    <span className="font-weight-bold">Hvis denne har økt siden sist må du informere ansvarlig sykepleier.</span>
                    <div className="actions mt-2">
                        <h5>Tiltak:</h5>
                        <ul>
                            <li>Mål vitalia på pasienten minst hver 4-6 time.</li>
                            <li>Ansvarlig sykepleier må vurdere om man skal iverksette andre tiltak (f.eks. økt hyppighet på målinger).</li>
                        </ul>
                    </div>
                </div>);
            }
            else
            {
                scoreInterpretation.push(<div key="interpretation">
                    Fortsett rutinemessige målinger av NEWS, minst hver 12. time.
                </div>);
            }
            if(score < 7)
            {
                scoreInterpretation.push(<small key="clinical-notice">Husk at et screeningverktøy ikke kan
                erstatte klinisk skjønn. Du må likevel handle hvis du mener
                tilstanden er verre enn poengsummen skulle tilsi.</small>);
            }
            result.unshift(
                <Row key="result">
                    <Col className="mt-4">
                        <h4>Resultat ({score} poeng)</h4>
                        {scoreInterpretation}
                    </Col>
                </Row>
            );
            return result;
        }
        else
        {
            return <Row key="result">
                <Col className="mt-4">
                    <h4>Resultat</h4>
                    Du må fylle ut alle feltene over før du får et resultat.
                </Col>
            </Row>;
        }
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const {
            onBPUpdate, onPulseUpdate, onTempUpdate, onRFUpdate, onSpO2Update, onConsciousnessUpdate, onToggleO2, onToggleAlternateSPO2,
            BP, pulse, temp, RF, SpO2, consciousness, usingO2, alternateO2Table
        } = this.props;

        const { score, explanation, itemsWithScore3 } = this.calculateNewsScore();
        const renderedScore = this.renderScore(score, explanation, itemsWithScore3);

        let o2Table: ?NEWSOptionList;
        let o2TableLabel: ?string;
        if(alternateO2Table)
        {
            o2Table = SpO2OptionsAlternate;
            o2TableLabel = "skala 2";
        }
        else
        {
            o2Table = SpO2Options;
            o2TableLabel = "skala 1";
        }

        let O2TableInfo: ?React.Node;
        if(this.state.displayO2TableInfo)
        {
            O2TableInfo = <div id="whatIsAltO2" onClick={(ev) => ev.preventDefault() }><b>Forklaring til alternativ tolkning:</b> NEWS 2 har to tabeller for
                tolkning av SpO₂. Den vanlige standardtabellen brukes i nesten alle
                tilfeller, men hvis en pasient har kjent lungesvikt og
                CO₂-opphopning <i>kan en lege ordinere</i> bruk av den alternative tabellen.
                Den tillater litt lavere SpO₂-verdier, og kan i tillegg oppdage
                fare for CO₂-retensjon under o₂-behandling.</div>;
        }
        else
        {
            O2TableInfo = <span id="whatIsAltO2" className="likeLink" onClick={(ev) => { ev.preventDefault(); this.setState({displayO2TableInfo: true }); }}>Hva er dette?</span>;
        }

        /*
         * Builds labels that show definitions from the dictionary on hover
         */
        const SpO2Label = <WordDefinitionFromDictionary content="SpO₂ (%)"                           lookupKey="oksygenmetning"      id="news-spo2-opt" />;
        const RFLabel   = <WordDefinitionFromDictionary content="Respirasjonsfrekvens (pr.min)"  lookupKey="respirasjonsfrekvens"id="news-rf-opt" />;
        const BTLabel   = <WordDefinitionFromDictionary content="Systolisk blodtrykk (overtrykk), mmHg"lookupKey="blodtrykk"           id="news-bt-opt" />;

        return  <ToolContainer tool="news">
                <PageTitle title="NEWS 2" />
                <Row>
                    <Col>
                        <h3>NEWS 2</h3>
                    </Col>
                </Row>
                <Row className="mb-2">
                    <Col>
                        <NEWSWhatIsInfo />
                    </Col>
                </Row>
                <OptionSelector options={rfOptions}            selected={RF}            label={RFLabel}                         onChange={ onRFUpdate }            placeholder="RF"/>
                <OptionSelector options={o2Table}              selected={SpO2}          label={SpO2Label}                       onChange={ onSpO2Update }          placeholder={"SpO₂ ("+o2TableLabel+")"} />
                <Row>
                    <Col sm={{offset: 4}}>
                        <Label check className="checkbox-inline-fixup">
                            <Input name="alternateO2Table" type="checkbox" checked={alternateO2Table === true} onChange={ onToggleAlternateSPO2 } />
                            Pasienten har kjent CO₂-opphopning, og lege har ordinert alternativ tolkning (NEWS 2 SpO₂ skala 2) av SpO₂.
                            {" "}{O2TableInfo}
                        </Label>
                    </Col>
                </Row>
                <Row>
                    <Col sm={{offset: 4}}>
                        <Label check className="checkbox-inline-fixup">
                            <Input name="usingO2" type="checkbox" checked={usingO2 === true} onChange={onToggleO2} />
                            Får oksygentilførsel
                        </Label>
                    </Col>
                </Row>
                <OptionSelector options={BPOptions}            selected={BP}            label={BTLabel}                         onChange={ onBPUpdate }            placeholder="Systolisk BT"/>
                <OptionSelector options={pulseOptions}         selected={pulse}         label="Puls"                            onChange={ onPulseUpdate }         placeholder="Puls"/>
                <OptionSelector options={consciousnessOptions} selected={consciousness} label="Bevissthetsnivå"                 onChange={ onConsciousnessUpdate } placeholder="Bevissthetsnivå" />
                <OptionSelector options={tempOptions}          selected={temp}          label="Temperatur (°C)"                 onChange={ onTempUpdate }          placeholder="Temp" />
                {renderedScore}
                <Row>
                    <Col className="text-secondary mt-4">
                        <h5>Referanser</h5>
                        <small>
                            Royal College of Physicians (2017). <i><ExternalLink href="https://www.rcplondon.ac.uk/projects/outputs/national-early-warning-score-news-2" target="_blank" rel="noopener">National Early Warning Score (NEWS) 2</ExternalLink>: Standardising the assessment of acute-illness severity in the NHS. Updated report of a working party</i>. London: RCP.<br />
                            eHåndbok OUS (2020). <i>National Early Warning Score II (NEWS II)</i>. [internett] Tilgjengelig på: &lt;<ExternalLink href="https://ehandboken.ous-hf.no/document/78636">https://ehandboken.ous-hf.no/document/78636</ExternalLink>&gt; [Besøkt 22 april 2020].
                        </small>
                    </Col>
                </Row>
            </ToolContainer>;
    }
}

const NEWSConnected: React.AbstractComponent<{| |}> = connect<NEWSProps,{||},newsState,NEWSDispatchProps,_,_>(
    (state): newsState =>
    {
        return {
            BP: state.news.BP,
            pulse: state.news.pulse,
            temp: state.news.temp,
            RF: state.news.RF,
            SpO2: state.news.SpO2,
            consciousness: state.news.consciousness,
            usingO2: state.news.usingO2,
            alternateO2Table: state.news.alternateO2Table,
        };
    },
    (dispatch): NEWSDispatchProps =>
    {
        return {
            onBPUpdate (BP: string)
            {
                dispatch(setNewsBP(BP));
            },
            onPulseUpdate (Pulse: string)
            {
                dispatch(setNewsPulse(Pulse));
            },
            onTempUpdate (Temp: string)
            {
                dispatch(setNewsTemp(Temp));
            },
            onRFUpdate (RF: string)
            {
                dispatch(setNewsRF(RF));
            },
            onSpO2Update (SpO2: string)
            {
                dispatch(setNewsSpO2(SpO2));
            },
            onConsciousnessUpdate (Consciousness: string)
            {
                dispatch(setNewsConsciousness(Consciousness));
            },
            onToggleO2 ()
            {
                dispatch(toggleNewsO2());
            },
            onToggleAlternateSPO2 ()
            {
                // SpO2 has to be reset first, since the new table is incompatible with the old one.
                dispatch(setNewsSpO2(null));
                dispatch(toggleNewsAlternateO2Table());
            }
        };
    }
)(NEWS);

// Test-only exports
export { NEWS, NEWSConnected, NEWSExplanationLine, NEWSWhatIsInfo, BPOptions, pulseOptions, tempOptions, rfOptions, SpO2Options, SpO2OptionsAlternate, consciousnessOptions };
// Public exports
export default NEWSConnected;
