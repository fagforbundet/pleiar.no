/**
 * @prettier
 */
/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from "react";
import { connect } from "react-redux";
import { Row, Col, Form, Input, Label } from "reactstrap";
import {
    CalcExplanation,
    CalcExplanationLine,
    CalcExplanationDescription,
    CalcExplanationCalculation,
    CalcExplanationFormula,
    Fraction,
    FractionResult,
} from "./calculators-shared.js";
import { setWeight } from "../actions/nutricalc-shared";
import { setHeight } from "../actions/nutricalc-bmi";
import { numbers } from "../helper.numbers";
import { PageTitle } from "./layout";
import { ExternalLink } from "./shared/links";
import type { CalcResultWithExplanation } from "../types/calculator-types";

type NutriCalcBMIPropsFromRedux = {|
    weight: number,
    height: number,
    round: boolean,
|};
type NutriCalcBMIReduxDispatchProps = {|
    onWeightUpdate: (SyntheticEvent<HTMLInputElement>) => void,
    onHeightUpdate: (SyntheticEvent<HTMLInputElement>) => void,
|};
/**
 * Props for {@link NutriCalcBMI}
 */
type NutriCalcBMIProps = {|
    ...NutriCalcBMIReduxDispatchProps,
    ...NutriCalcBMIPropsFromRedux,
|};

/**
 * Calculates the required calories per day for a patient
 */
class NutriCalcBMI extends React.PureComponent<NutriCalcBMIProps> {
    /**
     * Perform the calculation
     */
    getResult(): CalcResultWithExplanation {
        const descr = [];
        let bmi: number = 0;

        const heightM = numbers.conditionalRound(
            this.props.height / 100,
            this.props.round,
        );
        descr.push(
            <CalcExplanationLine key="heightCM">
                <CalcExplanationDescription>
                    Konverter CM til M
                </CalcExplanationDescription>
                <CalcExplanationCalculation>
                    <Fraction top={this.props.height} bottom="100" />
                    <FractionResult>= {heightM}</FractionResult>
                </CalcExplanationCalculation>
                <CalcExplanationFormula>
                    <Fraction top="CM" bottom="100" />
                </CalcExplanationFormula>
            </CalcExplanationLine>,
        );

        bmi = numbers.conditionalRound(
            this.props.weight / (heightM * heightM),
            this.props.round,
            1,
        );
        descr.push(
            <CalcExplanationLine key="bmi">
                <CalcExplanationDescription>
                    Uterkning av BMI (kg/m²)
                </CalcExplanationDescription>
                <CalcExplanationCalculation>
                    <Fraction top={this.props.weight} bottom={heightM + "²"} />
                    <FractionResult>= {bmi}</FractionResult>
                </CalcExplanationCalculation>
                <CalcExplanationFormula>
                    <Fraction top="Vekt" bottom="Høyde × Høyde" />
                </CalcExplanationFormula>
            </CalcExplanationLine>,
        );

        return {
            result: bmi,
            description: <CalcExplanation>{descr}</CalcExplanation>,
        };
    }
    /**
     * Main rendering
     */
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const result = this.getResult();
        let interpretation: string;
        let interpPrefix: string = "at pasienten er";
        if (
            Number.isNaN(result.result) ||
            result.result === 0 ||
            result.result === Infinity
        ) {
            result.result = 0;
            result.description = null;
        } else if (result.result < 18.5) {
            interpretation = "undervektig (18,4 og lågare)";
        } else if (result.result < 25) {
            interpretation = "normalvektig (18,5-24,9)";
        } else if (result.result < 30) {
            interpretation = "overvektig (25-29,9)";
        } else {
            interpPrefix = "at pasienten har";
            if (result.result < 35) {
                interpretation = "fedme, grad 1 (30-34,9)";
            } else if (result.result < 40) {
                interpretation = "fedme, grad 2 (35-39,9)";
            } else {
                interpretation = "fedme, grad 3 (40+)";
            }
        }

        return (
            <div>
                <PageTitle title="Ernæringskalkulator - BMI" />
                <Row>
                    <Col>
                        <h3>BMI</h3>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <h4>Pasientdata</h4>
                    </Col>
                </Row>
                <Form className="smallInputElements boldColLabels">
                    <Row>
                        <Label for="weight" xs="3" md="2">
                            Vekt
                        </Label>
                        <Col className="form-inline">
                            <Input
                                type="number"
                                min="1"
                                max="300"
                                className="mr-2"
                                value={this.props.weight}
                                onChange={(e) => {
                                    this.props.onWeightUpdate(e);
                                }}
                                id="weight"
                            />{" "}
                            kg
                        </Col>
                    </Row>
                    <Row>
                        <Label for="weight" xs="3" md="2">
                            Høyde
                        </Label>
                        <Col className="form-inline">
                            <Input
                                type="number"
                                min="1"
                                max="300"
                                className="mr-2"
                                value={this.props.height}
                                onChange={(e) => {
                                    this.props.onHeightUpdate(e);
                                }}
                                id="height"
                            />{" "}
                            cm
                        </Col>
                    </Row>
                </Form>
                <Row className="mt-4">
                    <Col>
                        <h4>Resultat</h4>
                        BMI er <b id="nutriCalcBMIResult">
                            {result.result}
                        </b>{" "}
                        kg/m².
                        <span
                            className={
                                result.result === 0 || result.result < 8
                                    ? "invisible"
                                    : ""
                            }
                        >
                            {" "}
                            Det kan tolkast som {interpPrefix}{" "}
                            <span id="nutriCalcBMInterp">{interpretation}</span>
                            .
                        </span>
                    </Col>
                </Row>
                {result.description}
                <Row className="text-secondary mt-4">
                    <Col>
                        <h5>Kjelde</h5>
                        <small>
                            Formelen og tolkinga er henta frå
                            Folkehelseinstituttet (2015).{" "}
                            <i>
                                <ExternalLink
                                    className="text-secondary"
                                    href="https://www.fhi.no/fp/overvekt/kroppsmasseindeks-kmi-og-helse/"
                                >
                                    Kroppsmasseindeks (KMI) og helse.
                                </ExternalLink>
                            </i>{" "}
                            Oslo: Folkehelseinstituttet.
                        </small>
                    </Col>
                </Row>
            </div>
        );
    }
}

const NutriCalcBMIContainer: React.AbstractComponent<{||}> = connect<
    NutriCalcBMIProps,
    {||},
    NutriCalcBMIPropsFromRedux,
    NutriCalcBMIReduxDispatchProps,
    _,
    _,
>(
    (state): NutriCalcBMIPropsFromRedux => {
        return {
            weight: state.nutriCalc.shared.weight,
            height: state.nutriCalc.bmi.height,
            round: state.global.round,
        };
    },
    (dispatch): NutriCalcBMIReduxDispatchProps => {
        return {
            onWeightUpdate(ev: SyntheticEvent<HTMLInputElement>) {
                const value = numbers.floatishInRangeFromThing(ev, 0, 400);
                dispatch(setWeight(value));
            },
            onHeightUpdate(ev: SyntheticEvent<HTMLInputElement>) {
                const value = numbers.floatishInRangeFromThing(ev, 0, 250);
                dispatch(setHeight(value));
            },
        };
    },
)(NutriCalcBMI);

export default NutriCalcBMIContainer;
