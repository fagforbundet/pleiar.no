/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { Collapse, Container, Row, Col } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import type { RouterHistory } from 'react-router-dom';
import { TopSearch } from './global-search';
import auth from '../auth';
import device from '../helper.device';
import { getAuthURL } from './auth';
import { ExternalLink } from './shared/links';
import IOSBackIcon from 'react-feather/dist/icons/arrow-left';

export type appList = Array<{|
    name: string,
    id: string,
    onClick?: (e: Event) => void,
    route?: string,
    exact?: boolean,
    raw?: boolean
|}>;

/**
 * This is the list of all apps that should be displayed in the menu
 *
 * The name is the label, id is the path, route is the route if it can not
 * be inferred and exact is a boolean specifying if the exact prop
 * needs to be supplied to react-router
 */
const apps: appList = [
    {
        name: 'Heim',
        id:'base',
        route: '/',
        exact: true
    },
    {
        name: 'Håndbok',
        id: 'handbok',
        exact: false
    },
    {
        name:'Verktøy',
        id:'verktoy',
        exact: false
    },
    {
        name: 'Ordliste',
        id: 'ordliste',
        exact: false
    },
    {
        name:'Hurtigoppslag',
        id: 'oppslag',
        exact: false
    },
    {
        name: 'E-læringskurs',
        id: 'elaering',
        exact: false,
    },
    {
        name:'Andre sider',
        id: 'andresider',
        exact: false
    },
    {
        name: "Flere fagtilbud fra Fagforbundet",
        id: 'om/flerefagtilbud',
    }
];

/**
 * The renderer of the top menu
 */
class Menu extends React.PureComponent<{||}, {|collapseOpen: boolean|}>
{
    constructor(props: {||}) // eslint-disable-line require-jsdoc

    {
        super(props);
        this.state = {
            collapseOpen: false,
        };
    }

    /**
     * Callback called when the user clicks on a nav link, used to
     * auto-close the "collapse" on mobile.
     * This is bound to this instance of `Menu` in the `constructor`.
     */
    onNavLinkClick ()
    {
        this.setNavbarState(false);
    }

    /**
     * Callback to toggle the collapse state
     */
    toggleCollapse ()
    {
        this.setState({
            collapseOpen: !this.state.collapseOpen
        });
    }

    /**
     * Toggle method for toggling the menu on mobile.
     * This is bound to this instance of `Menu` in the `constructor`.
     */
    setNavbarState(state: boolean)
    {
        this.setState({
            collapseOpen: state
        });
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        // This is a hack that allows us to hide the "closing" transition for
        // the collapse
        const collapseStyle = {};
        if (!this.state.collapseOpen)
        {
            collapseStyle.display = 'none';
        }
        return <Container tag="nav">
            <Row>
                <Col className="col-xl-10 offset-xl-1">
                    <Row className="header-menu">
                        <Col className="text-right">
                            <TopSearch />
                            <MenuBranding />
                            <div className="menuButtonWrapper"><span className={"likeLink menuButton "+(this.state.collapseOpen ? "open" : "")} onClick={() => this.toggleCollapse()}>Meny</span></div>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Collapse isOpen={this.state.collapseOpen} style={collapseStyle}>
                                <div className="col-lg-8 offset-lg-2 mainMenu">
                                    <MenuList onNavLinkClick={() => this.onNavLinkClick()} />
                                </div>
                            </Collapse>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>;
    }
}

/**
 * Renders our branding and, on iOS, navigation element
 */
class MenuBranding extends React.PureComponent<{||}>
{
    isIOSApp: boolean = false;

    constructor () //eslint-disable-line require-jsdoc
    {
        super();
        this.isIOSApp = device.isIOSApp();
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        if(!this.isIOSApp)
        {
            return <Link key="branding" className="float-left BrandLogo" to="/"><img src="/pleiarlogo.svg" height="50" alt="Pleiar.no" /></Link>;
        }
        {
            return <IOSMenuBranding />;
        }
    }
}

/**
 * Even though we don't use the actual value of the history prop, we specify it
 * here to illustrate that it's a prop that we realy upon (because it's how
 * we're told about changes to the URL)
 */
type IOSMenuBrandingProps = {|
    history: RouterHistory,
|};
/**
 * This handles rendering the top menu branding in iOS, by appending a
 * back arrow to it, which is used to navigate the app
 */
class IOSMenuBrandingUnconnected extends React.PureComponent<IOSMenuBrandingProps>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const brand = [
            <img key="brand" src="/pleiarlogo.svg" height="45" alt="Pleiar.no" />
        ];
        let iosClass: string = "ios-menu-back";
        // If we're at /, then we return an inactive arrow
        if(location.pathname === "/")
        {
            iosClass += " ios-menu-back-inactive";
            brand.unshift(
                <div key="back" className={iosClass}>
                    <IOSBackIcon size={40} />
                </div>
            );
        }
        // If we're not at / and we have some history, then return an active arrow
        // that uses the history API to navigate back.
        else if (history.state)
        {
            brand.unshift(
                <div key="back" onClick={() => { history.back(); }} className={iosClass}>
                    <IOSBackIcon size={40} />
                </div>
            );
        }
        // Finally, if we're not at / but don't have any history either, just
        // pretend that we do have a history and that history is one back to /,
        // and emulate that behaviour
        else
        {
            brand.unshift(
                <Link to="/" key="back" className={iosClass}>
                    <IOSBackIcon size={40} />
                </Link>
            );
        }
        return <div className="float-left BrandLogo ios-brand">{brand}</div>;
    }
}

/**
 * IOSMenuBranding relies upon having its props updated by react-router-dom to
 * trigger re-renders. Thus we connect it to the router.
 */
const IOSMenuBranding = withRouter(IOSMenuBrandingUnconnected);

/**
 * A (reusable) list of our main menu items.
 * This is used to render the menu in the collapse, but can also be used
 * anywhere else to render a menu structure.
 */
class MenuList extends React.PureComponent<{|onNavLinkClick?: () => mixed, hideHome?: boolean, extraApps?: appList|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { onNavLinkClick, extraApps, hideHome } = this.props;
        let renderAppList: appList = [].concat(apps);
        if (! auth.isAuthenticated())
        {
            renderAppList.push({
                name: 'Logg inn',
                id: 'login',
                route: getAuthURL(),
                raw: true,
                onClick: () =>
                {
                    window.localStorage.pleiarAuthRedirect = location.pathname;
                }
            });
        }
        if(extraApps)
        {
            renderAppList = renderAppList.concat(extraApps);
        }
        if(hideHome)
        {
            renderAppList = renderAppList.filter( (entry) => entry.id !== "base" );
        }
        return <MenuListRenderer onNavLinkClick={onNavLinkClick} apps={renderAppList} />;
    }
}

/**
 * A reusable menu renderer (for instance used by MenuList). 
 */
class MenuListRenderer extends React.PureComponent<{|onNavLinkClick?: () => mixed, apps: appList|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { onNavLinkClick, apps } = this.props;
        const numberOfApps: number = apps.length;
        const leftMaxItems = Math.round( numberOfApps / 2 );
        const left = [];
        const right = [];
        for (const app of apps)
        {
            const target = left.length < leftMaxItems ? left : right;
            let path  : string = '/'+app.id;
            if(app.route)
            {
                path = app.route;
            }
            const onClick = (app.onClick ? app.onClick : (onNavLinkClick ? onNavLinkClick : null ) );
            if(app.raw)
            {
                target.push(
                    <ExternalLink sameWindow={true} key={path} href={path} onClick={onClick}>{app.name}</ExternalLink>
                );
            }
            else
            {
                target.push(
                    <Link key={path} to={path} onClick={onClick}>{app.name}</Link>
                );
            }
        }
        return <Row className="appMenu">
            <Col key="left" className="linkList" xs="12" md="6">
                {left}
            </Col>
            <Col key="right" className="linkList" xs="12" md="6">
                {right}
            </Col>
        </Row>;
    }
}

export default Menu;
export { MenuList, MenuListRenderer };
// Testing exports
export { apps };
