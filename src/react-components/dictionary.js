/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { connect } from 'react-redux';
import { PageTitle } from './layout';
import { Row, Col, Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';
import { setDictSearchString } from '../actions/dictionary';
import data from '../../data/datasett/dictionary.json';
import XCircle from 'react-feather/dist/icons/x-circle';
import ReactMarkdown from 'react-markdown';
import PleiarSearcher from '../searcher';
import RoutingAssistant from '../routing';
import { InfiniteScrollSearchResultRenderer, RequiresPleiarSearcher } from './global-search';
import { Link } from 'react-router-dom';
import { getValueFromLiteralOrEvent } from '../helper.general';
import device from '../helper.device';
import { ExternalLink } from './shared/links';
import { InlineAuthBlock } from './auth';
import auth from '../auth';
import { StructuredData } from './shared/structured-data';
import type { DictionaryEntry as DictionaryEntryType } from '../types/data';
import type { lunrResult } from '../types/libs';
import type { onSyncSignature } from '../routing';
import type { Location } from 'react-router-dom';
import domainToNameMap from '../../data/datasett/website-identification.json';

/**
 * A `<Link>` wrapper that returns links relative to `/ordliste/`
*/
class DictionaryLink extends React.PureComponent<{|href: string, children: React.Node|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <Link to={'/ordliste/'+this.props.href}>{this.props.children}</Link>;
    }
}

/**
 * Renders a single "read more" entry. It takes a URL and returns a link to
 * that URL with the link text being the name of the site. The names are
 * hardcoded.
 */
class ReadMoreEntry extends React.PureComponent<{|entry: string|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const link = this.props.entry;
        let site: string = '';
        for(const entry of domainToNameMap)
        {
            if(link.indexOf(entry.index) !== -1)
            {
                site = entry.name;
                break;
            }
        }

        let text: string = site;
        if(site === "")
        {
            /* eslint-disable no-console */
            console.log('Multi-link entry, but unidentified site: '+link);
            text = '???';
        }
        return <ExternalLink href={link}>{text}</ExternalLink>;
    }
}

/**
 * Renders a single "see also" dictionary entry through `DictionaryLink`.
 */
class SeeAlsoEntry extends React.PureComponent<{|entry: string |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <DictionaryLink key={'sea-'+this.props.entry} href={this.props.entry}>{this.props.entry}</DictionaryLink>;
    }
}

/**
 * Renders an array of elink erntries through a component, generating a nice
 * readable list with either , or " og " separating the items, depending on
 * where in the line they appear.
 */
class SomeDictEntries extends React.PureComponent<{|entries?: Array<string>, singleEntryRenderer: React.ComponentType<{|entry: string|}>, prefix: string|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const SingleEntryRenderer = this.props.singleEntryRenderer;
        const ret = [];
        const entries = this.props.entries;
        if(entries == undefined)
        {
            return null;
        }
        const prefix = this.props.prefix;
        for(let entryNo: number = 0; entryNo < entries.length; entryNo++)
        {
            const entry = entries[entryNo];
            ret.push(<SingleEntryRenderer key={entry} entry={entry} />);
            if(entryNo+2 == entries.length)
            {
                ret.push(<span key={entryNo}> og </span>);
            }
            else if(entryNo+1 < entries.length)
            {
                ret.push(<span key={entryNo}>, </span>);
            }
        }
        return <span className="readMore"><span>{prefix} </span>{ret}.</span>;
    }
}

/**
 * Renders a single entry in the dictionary
 *
 * Props:
 * - entry: The entry to render
 * - rawEntry: A boolean, if true it will return *just* the entry, with no
 *   "read more", no title etc. rawEntry implies forceAuth=true and inhibitTitle=true
 * - inhibitTitle: A boolean, if true will omit the title of the entry
 * - forceAuth: A boolean, if true, will not verify that a user is logged in
 * - whitelistedAuthRequiredEntries: The number of whitelisted entries
 *   displayed before authentication is required. Used to construct the text
 *   for the {@link InlineAuthBlock}.
 */
class DictionaryEntry extends React.PureComponent<{| entry: DictionaryEntryType, rawEntry?: boolean, inhibitTitle?: boolean, forceAuth?: boolean, whitelistedAuthRequiredEntries?: number |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const dataEntry = this.props.entry;
        let title: React.Node = null;
        let handbookLink: React.Node = null;
        let readMoreOnText: string = "Les mer på";
        if (!this.props.inhibitTitle && !this.props.rawEntry)
        {
            title = <div className="dictionaryTitle" target="_blank">{dataEntry.expression}</div>;
        }
        if(dataEntry.seeHandbook)
        {
            handbookLink = <Link to={'/handbok'+dataEntry.seeHandbook}>Les mer i håndboken.</Link>;
            readMoreOnText = "Ytterligere informasjon på";
        }
        let content: React.Node;
        if(auth.isAuthenticated() || this.props.forceAuth || this.props.rawEntry)
        {
            const entry = <ReactMarkdown components={{a: DictionaryLink}}>{dataEntry.description}</ReactMarkdown>;
            if(this.props.rawEntry)
            {
                return entry;
            }
            content = <span className="auth-content">
                {entry}{' '}
                <SomeDictEntries entries={dataEntry.seeAlso} singleEntryRenderer={SeeAlsoEntry} prefix="Se også" /> {handbookLink} <SomeDictEntries entries={dataEntry.moreInfoURLs} singleEntryRenderer={ReadMoreEntry} prefix={readMoreOnText} />
            </span>;
        }
        else
        {
            let text: string = "oppføringer i ordlisten";
            if(this.props.whitelistedAuthRequiredEntries !== undefined)
            {
                if (this.props.whitelistedAuthRequiredEntries > 0)
                {
                    text = "flere oppføringer i ordlisten";
                }
                else if(this.props.whitelistedAuthRequiredEntries === -1)
                {
                    text = 'søk i ordlisten';
                }
            }
            content = <InlineAuthBlock attemptedAccess={text} />;
        }
        return <Row key={dataEntry.expression} className="dictionaryEntry">
            <Col>
                {title}
                {content}
            </Col>
        </Row>;
    }
}

/**
 * Props for `Dictionary`
 */
type DictionaryProps = {|
    search: string,
    location?: Location,
    onSearchUpdate: (string) => void,
|};

/**
 * Renders the full dictionary page with a search field and infinite scrolling.
 */
class Dictionary extends React.Component<DictionaryProps>
{
    constructor (props: DictionaryProps) // eslint-disable-line require-jsdoc

    {
        super(props);
    }

    /**
     * Callback, called when either the user types or clicks on a link that
     * updates the search value. This gets bound to this instance of
     * `Dictionary` in the `constructor`
     */
    onChanged (str: string | SyntheticEvent<HTMLInputElement>,forcedPush?: boolean)
    {
        str = getValueFromLiteralOrEvent(str);
        if(str == "")
        {
            if(forcedPush)
            {
                RoutingAssistant.push('/ordliste');
            }
            else
            {
                RoutingAssistant.replace('/ordliste');
            }
        }
        else
        {
            RoutingAssistant.generateReplace('/ordliste',str,"auto");
        }
        this.props.onSearchUpdate(str);
    }

    /**
     * Performs a search and returns the result
     */
    getResources (): Array<lunrResult>
    {
        const search = this.props.search;
        let results: Array<lunrResult> = [];
        if(search === null || search === "" || !/\w/.test(search))
        {
            results = data;
        }
        else
        {
            results = PleiarSearcher.safePartialSearch(search,(accumulator: Array<lunrResult>,result: lunrResult) =>
            {
                if(result.ref.indexOf("dictionary") === 0)
                {
                    accumulator.push(result);
                }
                return accumulator;
            });
        }
        return results;
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const entries = this.getResources();
        let autoFocus: boolean = true;
        let whitelistedAuthRequiredEntries: number = 5;
        if(this.props.search !== null && this.props.search !== undefined && this.props.search != "")
        {
            whitelistedAuthRequiredEntries = -1;
        }
        if(device.isTouchScreen())
        {
            autoFocus = false;
        }
        return <Row>
            <Col lg={{offset:2,size:8}}>
                <PageTitle title="Ordliste" />
                <h3>Ordliste</h3>
                <InputGroup>
                    <Input autoFocus={autoFocus} placeholder="Søk" value={this.props.search} onChange={(e,force) => this.onChanged(e,force)} name="search" />
                    <InputGroupAddon addonType="append">
                        <InputGroupText className="clearSearchField" onClick={ () => { this.onChanged('',true); } }>
                            <XCircle />
                        </InputGroupText>
                    </InputGroupAddon>
                </InputGroup>
                <hr />
                {/* $FlowIssue[prop-missing] Actual flow bug with unions */}
                <InfiniteScrollSearchResultRenderer result={entries} search={this.props.search} onPageChange={this.onPageChange} type="dictionary" suggestGlobalSearch={true} whitelistedAuthRequiredEntries={whitelistedAuthRequiredEntries} searchPathPrefix="/ordliste" location={this.props.location} />
            </Col>
            <StructuredData type="auto" walled={true} />
        </Row>;
    }
}

const DictionaryContainer = connect(
    (state) =>
    {
        return {
            search: state.dictionary.search,
        };
    },
    (dispatch) =>
    {
        return {
            onSearchUpdate(ev: string)
            {
                dispatch(setDictSearchString(ev));
            }
        };
    }
)(Dictionary);

/**
 * Props for DictionaryWrapper
 */
type DictionaryWrapperProps = {| onRouteSync: onSyncSignature |};

/**
 * This is a wrapper around Dictionary. It wraps it in a
 * RequiresPleiarSearcher component to ensure PleiarSearcher is initialized
 * once Dictionary starts rendering.
 */
class DictionaryWrapper extends React.Component<DictionaryWrapperProps>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { onRouteSync, ...moreProps } = this.props;
        return <RequiresPleiarSearcher component={DictionaryContainer} onRouteSync={onRouteSync} app="dictionary" {...moreProps} />;
    }
}
const DictionaryWrapperContainer: React.AbstractComponent<{| |}> = connect<DictionaryWrapperProps,{| |},_,DictionaryWrapperProps,_,_>(
    () =>
    {
        return {};
    },
    (dispatch): DictionaryWrapperProps =>
    {
        return {
            onRouteSync(search)
            {
                dispatch(setDictSearchString(search));
            },
        };
    }
)(DictionaryWrapper);

// Public exports
export default DictionaryWrapperContainer;
export { DictionaryEntry };
// Testing exports
export { ReadMoreEntry, Dictionary as DictionaryContainerRaw };
