/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { Row, Col, Input, Label } from 'reactstrap';
import { getValueFromLiteralOrEvent } from '../../helper.general';
import type { NEWSOptionList } from '../news';


/**
 * A single entry used for {@link OptionSelector}.
 * (this is explicitly not exact, so that any of our users can use a
 * datastructure that they already have other items in)
 */
export type OptionSelectorKey = string;
export type OptionSelectorEntry = { //eslint-disable-line flowtype/require-exact-type
    +key: OptionSelectorKey,
    +name?: string,
    ...
};

/**
 * The list of {@link OptionSelectorEntry} items that is used by @{link OptionSelector}.
 */
export type OptionSelectorList = Array<OptionSelectorEntry> | NEWSOptionList;

/**
 * The required props to render any subcomponent of OptionSelector (and thus also OptionSelector)
 */
type OptionSelectorSubcomponentProps = {|
    options: OptionSelectorList,
    label: string | React.Node | Array<React.Node>,
    placeholder?: string,
    onChange: (string) => void,
    selected?: string | null
|};

/**
 * This component is used by {@link OptionSelector} to render a set of options
 * as a select-element. This is considered an implementation detail of
 * OptionSelector, and you should only use OptionSelector directly, never
 * OptionSelectorRadioButtons. You can tell OptionSelector that you only want
 * select-element.
 */
class OptionSelectorSelectElement extends React.Component<OptionSelectorSubcomponentProps>
{
    /**
     * Render the component
     */
    render (): React.Node
    {
        const list = [];
        let selected: ?string | null = this.props.selected;
        let seenSelectedKey: boolean = false;
        if(selected === null)
        {
            selected = "";
        }
        for(const option of this.props.options)
        {
            let name: string = option.key;
            if(option.name)
            {
                name = option.name;
            }
            if(option.key === selected)
            {
                seenSelectedKey = true;
            }
            list.push(<option key={option.key} value={option.key}>{name}</option>);
        }
        let placeholder: ?string = this.props.placeholder;
        if(placeholder == null || placeholder == "")
        {
            placeholder = '- Velg -';
        }
        if(selected === undefined || selected === null || selected === "" || seenSelectedKey === false)
        {
            list.unshift(<option value="" key="dummy-placeholder">{placeholder}</option>);
        }
        return <Row className="option-selector option-selector-select">
            <Col md="4">
                {this.props.label}
            </Col>
            <Col className="form-inline">
				<select value={selected} onChange={(ev) => { this.props.onChange(getValueFromLiteralOrEvent(ev));}}>
					{list}
				</select>

            </Col>
        </Row>;
    }
}

/**
 * This component is used by {@link OptionSelector} to render a set of options
 * as radio buttons. This is considered an implementation detail of
 * OptionSelector, and you should only use OptionSelector directly, never
 * OptionSelectorRadioButtons. You can tell OptionSelector that you only want
 * radio buttons.
 */
class OptionSelectorRadioButtons extends React.Component<OptionSelectorSubcomponentProps>
{
    /**
     * Render the component
     */
    render (): React.Node
    {
        const { selected } = this.props;
        const list = [];
        for(const option of this.props.options)
        {
            let name: string = option.key;
            if(option.name)
            {
                name = option.name;
            }
            list.push(<Label key={option.key} className="mr-3 ml-3 ml-sm-0" check>
                        <Input type="radio" name={option.key} checked={selected === option.key} onChange={() =>{  this.props.onChange(option.key); }}/>{' '}
                {name}
                </Label>);
        }
        return <Row className="option-selector option-selector-radio">
            <Col md="4">
                {this.props.label}
            </Col>
            <Col className="form-inline form-check">
                {list}
            </Col>
        </Row>;
    }
}

type OptionSelectorProps = {|
    mode?: "radio" | "select",
    ...OptionSelectorSubcomponentProps
|};

/**
 * This renders a generic "option selector" element. This can be either a set
 * of radio buttons, or a <select>-element.
 *
 * When you don't specify anything, the `OptionSelector` itself decides which
 * to render. It can use multiple heuristics to decide this, including device
 * type and number of elements (though it does neither at the moment).
 */
class OptionSelector extends React.Component<OptionSelectorProps>
{
    /**
     * Render the component
     */
    render (): React.Node
    {
        const { mode, ...passProps } = this.props;
        if(mode === 'radio')
        {
            return <OptionSelectorRadioButtons {...passProps} />;
        }
        else
        {
            // FIXME: Should differentiate between different OptionSelectors
            return <OptionSelectorSelectElement {...passProps} />;
        }
    }
}

export default OptionSelector;

// Exports for tests only
export { OptionSelector, OptionSelectorSelectElement, OptionSelectorRadioButtons };
