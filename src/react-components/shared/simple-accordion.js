/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { Collapse } from 'reactstrap';

/**
 * This is used to define elements for `SimpleAccordion`
 */
type AccordionElements = Array<{|
        header: string,
        render: React.Node | string
|}>;

/**
 * Props for `SimpleAccordion`
 */
type SimpleAccordionProps = {| elements: AccordionElements, alwaysOpen?: boolean |};

/**
 * Render an accordion, which is a set of Collapse elements that are managed
 * together so that only one is open.
 */
class SimpleAccordion extends React.PureComponent<SimpleAccordionProps, {| openNo: number |}>
{
    constructor(props: SimpleAccordionProps) //eslint-disable-line require-jsdoc
    {
        super(props);
        this.state = {
            openNo: -1
        };
    }

    /**
     * Toggles Collapse elements. The element number supplied will be opened if
     * it is closed, and all other elements will be closed. If it is already
     * open then all elements will be closed.
     */
    toggleEnterNo(entry: number)
    {
        if(this.state.openNo === entry)
        {
            this.setState({
                openNo: -1
            });
        }
        else
        {
            this.setState({
                openNo: entry
            });
        }
    }

    render (): React.Node // eslint-disable-line require-jsdoc
    {
        const { elements, alwaysOpen } = this.props;
        const { openNo } = this.state;
        const accordion = [];

        for(let entryNo: number = 0; entryNo < elements.length; entryNo++)
        {
            const entry = elements[entryNo];
            const open = (alwaysOpen || openNo === entryNo);
            let cb: () => mixed = () => this.toggleEnterNo(entryNo);
            // If we're always open, don't bother with the callback, we don't need to
            // do the processing since it doesn't actually do anything
            if(alwaysOpen)
            {
                cb = () => {};
            }
            accordion.push(<div key={entryNo}>
                    <div className={"AccordionHeader "+(open ? "state-open" : "state-closed")} onClick={cb}>{entry.header}</div>
                    <Collapse isOpen={open} className="AccordionContent">
                        {entry.render}
                    </Collapse>
                </div>);
        }

        return <div className={"linkList Accordion "+(alwaysOpen ? "always-open": "")}>
            {accordion}
        </div>;
    }
}

export default SimpleAccordion;
