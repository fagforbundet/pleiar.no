/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { Tooltip, Popover, PopoverBody} from 'reactstrap';
import GithubSlugger from 'github-slugger';
import dictionary from '../../../data/datasett/dictionary.json';
import { DictionaryEntry } from '../dictionary';
import type { DictionaryEntry as DictionaryEntryType } from '../../types/data';

type WordDefinitionProps = {|
    content: string | React.Node | Array<React.Node>,
    definition: string | React.Node | Array<React.Node>,
    id: string
|};

type WordDefinitionState = {|
    tooltipVisible: boolean
    |};

/**
 * This is a more complete replacement for `<abbr>`, providing the same
 * functionality through a reactstrap `<Tooltip>`, which also permits things
 * like embedding react elements within the definition.
 *
 * Props:
 * - content: The content
 * - definition: The definition
 * - id: An id for the element
 */
class WordDefinition extends React.PureComponent<WordDefinitionProps, WordDefinitionState>
{
    state: WordDefinitionState = {
        tooltipVisible: false
    };

    render (): React.Node // eslint-disable-line require-jsdoc
    {
        const { content, definition, id } = this.props;
        const toggleTooltip = () => this.setState({tooltipVisible: !this.state.tooltipVisible});
        const useID = 'wd-'+(new GithubSlugger().slug(id,false));
        /* The wordDefinitionPopover class on <PopoverBody> is used to increase the width on desktops */
        return [
            <span key="abbrev" onClick={toggleTooltip} className="abbreviation" id={useID}>{ content }</span>,
            <Popover className="wordDefinitionPopover" key="tooltip" placement="bottom" target={useID} toggle={toggleTooltip} isOpen={this.state.tooltipVisible} trigger="hover">
                <PopoverBody onClick={toggleTooltip}>
                    {definition}
                </PopoverBody>
            </Popover>
        ];
    }
}

type WordDefinitionFromDictionaryProps = {|
    content: string | React.Node | Array<React.Node>,
    lookupKey: string,
    id: string
|};

/**
 * This is a variant of {@link WordDefinition} that lets you provide the name of an entry
 * in the dictionary instead of providing the definition yourself.
 *
 * Props:
 * - content: The content
 * - lookupKey: The lookupKey from Dictionary.yml to display
 * - id: An id for the element
 */
class WordDefinitionFromDictionary extends React.PureComponent<WordDefinitionFromDictionaryProps>
{
    render (): React.Node // eslint-disable-line require-jsdoc
    {
        const { content, lookupKey, id } = this.props;
        let dictEntry: ?DictionaryEntryType;
        for(const entry of dictionary)
        {
            if(entry.lookupKey === lookupKey)
            {
                dictEntry = (entry: DictionaryEntryType);
            }
        }
        if (!dictEntry)
        {
            throw('Unable to find dictionary entry: '+lookupKey);
        }
        const entry = <DictionaryEntry entry={dictEntry} inhibitTitle={true} rawEntry={true} />;
        return <WordDefinition content={content} definition={entry} id={id} />;
    }
}

type MobileAbbrWordDefinitionProps ={|
    mobileText: string,
    fullText: string,
    definition: string | React.Node | Array<React.Node>,
    id: string
|};
/**
 * This is a variant of {@link WordDefinition} that combines it with some of
 * the functionality from {@link MobileAbbreviation}.
 *
 * This will show "mobileText" on mobile, and "fullText" on larger screens.
 * Both variants will use "definition" as their word definition.
 *
 * Props:
 * - mobileText: the text to display on small screens
 * - fullText: the text to display on larger screens
 * - definition: the definition, as you would have passed to {@link WordDefinition}
 * - id: An id for the element
 */
class MobileAbbrWordDefinition extends React.PureComponent<MobileAbbrWordDefinitionProps>
{
    render (): React.Node // eslint-disable-line require-jsdoc
    {
        const { mobileText, fullText, ...passProps } = this.props;
        const visibleText: Array<React.Node> = [
            <span key="full" className="d-none d-lg-inline">{ fullText }</span>,
            <span key="abbrev" className="d-lg-none">{ mobileText }</span>
        ];
        return <WordDefinition content={visibleText} {...passProps} />;
    }
}

/**
 * Props for {@link MobileAbbreviation}
 *
 * - abbrev is the abbreviated string
 * - full is the full string
 * - id is a string to append to the ID we're going to use, it is completely
 *   optional
 */
type MobileAbbreviationProps = {|
    abbrev: string,
    full: string,
    id?: string
|};

/**
 * This is an `<abbr>`-like component, which functions like `<abbr>` on
 * steroids on mobile, while simply outputting the full text on desktops.
 */
class MobileAbbreviation extends React.PureComponent<MobileAbbreviationProps, {|tooltipVisible: boolean|}>
{
    constructor (props: MobileAbbreviationProps) //eslint-disable-line require-jsdoc
    {
        super(props);
        this.state = {
            tooltipVisible: false
        };
    }

    /**
     * Callback that is called to show or hide the tooltip
     */
    onToggleTooltip ()
    {
        this.setState({
            tooltipVisible: !this.state.tooltipVisible
        });
    }

    render (): React.Node // eslint-disable-line require-jsdoc
    {
        const { abbrev, full, id } = this.props;
        const useID = 'abbrev'+(new GithubSlugger().slug(abbrev+(id ? id : ''),false));
        return [
            <span key="full" className="d-none d-lg-inline">{ full}</span>,
            <span key="abbrev" onClick={() => this.onToggleTooltip()} className="d-lg-none abbreviation" id={useID}>{ abbrev }</span>,
            <Tooltip key="tooltip" placement="top" target={useID} autohide={false} isOpen={this.state.tooltipVisible} onClick={() => this.onToggleTooltip()}>
                {full}
            </Tooltip>
        ];
    }
}

export { MobileAbbreviation, WordDefinition, WordDefinitionFromDictionary, MobileAbbrWordDefinition };
// Use of the default import is DEPRECATED
export default MobileAbbreviation;
