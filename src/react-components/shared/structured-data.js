/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';

// Very simplistic typing of the ld-json structure
type LDjsonTopKeys = "@context" | "@type" | "mainEntityOfPage" | "image" | "headline" | "author" | "isAccessibleForFree" | "publisher" | "hasPart";
type LDjsonSecondaryKeys = "@type" | "@id" | "name" | "logo" | "url" | "cssSelector" | "isAccessibleForFree";
type LDjsonStruct = {
    [LDjsonTopKeys]: string | {
        [LDjsonSecondaryKeys]: string | {
            [LDjsonSecondaryKeys]: string
        }
    }
};

/**
 * Props for StructuredData when type="auto"
 */
type StructuredDataAutoProps = {|
    walled: boolean,
    type: "auto"
|};
/**
 * Props for StructuredData when type="article"
 */
type StructuredDataArticleProps = {|
    walled: boolean,
    type: "article",
    title: string,
    author?: string,
    image?: string,
|};
/**
 * Props for StructuredData
 */
type StructuredDataProps = StructuredDataAutoProps | StructuredDataArticleProps;

/**
 * This renders structured data for this entry.
 */
class StructuredData extends React.PureComponent<StructuredDataProps>
{
    render (): React.Node // eslint-disable-line require-jsdoc
    {
        const { walled } = this.props;
        let image: string;
        let author: string;
        let authorType: "Person" | "Organization" = "Person";
        if(this.props.image)
        {
            image = this.props.image;
        }
        else
        {
            image = '/fbed.jpg';
        }
        if(this.props.author)
        {
            author = this.props.author;
        }
        else
        {
            author = 'Fagforbundet, yrkesseksjon helse & sosial';
            authorType = 'Organization';
        }
        const ld: LDjsonStruct = {
            "@context": "https://schema.org",
            "publisher": {
                "@type": "Organization",
                "name": "Fagforbundet",
                "logo": {
                    "@type": "ImageObject",
                    "url": location.origin+'/brand.png',
                }
            },
            "author": {
                "@type": authorType,
                "name": author
            },
        };
        if(this.props.type === "auto")
        {
            ld['@type'] = 'WebPage';
        }
        else if(this.props.type === "article")
        {
            ld.headline = this.props.title;
            ld.image = image;
            ld['@type'] = 'Article';
        }
        if(walled)
        {
            ld.isAccessibleForFree = "False";
            ld.hasPart = {
                "@type": "WebPageElement",
                "isAccessibleForFree": "False",
                "cssSelector" : ".auth-content"
            };
        }
        return <script type="application/ld+json">
            {JSON.stringify(ld)}
        </script>;
    }
}

export { StructuredData };
