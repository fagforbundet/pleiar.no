/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';

import { PageTitle } from './layout';
import { ToolContainer } from './tools';
import { Row, Col, Input } from 'reactstrap';
import { connect } from 'react-redux';
import { setMedSynonymsSearchString } from '../actions/med-synonyms';
import { getValueFromLiteralOrEvent } from '../helper.general';
import { ConditionalRender } from './shared';
import MedSearcher from '../med-synonym-searcher';
import { ExternalLink } from './shared/links';
import type { medSynonymResult } from '../med-synonym-searcher';

type ATCCodeDispatchProps = {|
    onSearchUpdate: (string) => void,
|};
type ATCCodeOwnProps = {|
    atc?: string
|};
type ATCCodeProps = {|
    ...ATCCodeDispatchProps,
    ...ATCCodeOwnProps
|};

/**
 * Renders a single ATC code
 */
class ATCCodeUnconnected extends React.PureComponent<ATCCodeProps>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { atc, onSearchUpdate } = this.props;
        if(atc === undefined || atc === null || atc === "")
        {
            return [];
        }
        const pre = atc.substr(0,3);
        const post = atc.substr(3);
        const searchForATC = () =>
        {
                onSearchUpdate('ATC '+pre+' '+post);
        };
        return <span className="atc-code" onClick={ searchForATC }>
                ATC {pre} {post}
            </span>;
    }
}

const ATCCode: React.AbstractComponent<ATCCodeOwnProps> = connect<ATCCodeProps,*,*,ATCCodeDispatchProps,*,*>(
    () => { return {}; },
    (dispatch): MedicationSynonymsSearchBarDispatchProps =>
    {
        return {
            onSearchUpdate(ev: string)
            {
                dispatch(setMedSynonymsSearchString(getValueFromLiteralOrEvent(ev)));
            }
        };
    }
)(ATCCodeUnconnected);

/**
 * Renders a single result entry
 */
class MedicationSynonymsRenderSingleResult extends React.PureComponent<{|result: medSynonymResult|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { result } = this.props;
        const synList: Array<React.Node> = [];
        let merknad: ?React.Node;
        let synStr: string = "Synonymer";
        if(!result.synonyms || result.synonyms.length === 0)
        {
            return <Row><Col><b>{result.name}</b> (<ATCCode atc={result.atc} />) har ingen kjente synonympreparat</Col></Row>;
        }
        for(const synonym of result.synonyms)
        {
            synList.push(
                <li key={synonym}>{synonym}</li>
            );
        }
        if(result.synonyms.length == 1)
        {
            synStr = "Synonym";
        }
        if(result.notice)
        {
            merknad = <div className="merknad"><b className="text-danger">MERK:</b> {result.notice}</div>;
        }
        return <Row className="med-syn-result">
            <Col>
                <b className="med-syn-header">{result.name} (<ATCCode atc={result.atc} />)</b>
                {merknad}
                <Row>
                    <Col sm="3" lg="2">{synStr}:</Col>
                    <Col>
                        <ul>
                            {synList}
                        </ul>
                    </Col>
                </Row>
            </Col>
        </Row>;
    }
}

type MedicationSynonymsResultProps = {|
    search: string
|};

/**
 * The result renderer for medication synonyms
 */
class MedicationSynonymsResultUnconnected extends React.PureComponent<MedicationSynonymsResultProps>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { search } = this.props;
        const renderedResults: Array<React.Node> = [];
        if(search === undefined || search === null || search === "")
        {
            return <div />;
        }
        if(typeof(search) === "string" && search.length <= 2)
        {
            renderedResults.push(<div key="requires-result">Skriv minst to tegn for å få et resultat</div>);
        }
        else if(typeof(search) === "string")
        {
            if (MedSearcher.looksLikeATC( search ))
            {
                renderedResults.push(<MedicationSynonymsResultATC key="atcResult" search={search} />);
            }
            else
            {
                renderedResults.push(<MedicationSynonymsResultText key="textResult" search={search} />);
            }
        }
        return <Row className="med-syn-result-list">
            <Col>
                {renderedResults}
            </Col>
        </Row>;
    }
}

/**
 * Result renderer for ATC searches
 */
class MedicationSynonymsResultATC extends React.PureComponent<MedicationSynonymsResultProps>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { search } = this.props;
        const results = MedSearcher.atcSearch(search);
        const renderedResults: Array<React.Node> = [];
        if(results.list.length === 0)
        {
            const outputSearch = search.replace(/^ATC\s*:?\s*/,'');
            renderedResults.push(<div key="no-hits">Ingen treff på ATC koden «{outputSearch}». Sjekk stavemåten, eventuelt prøv å søke på legemiddelnavn i stedenfor. Du kan bare søke etter fulle ATC koder (f.eks. N02AA01), du vil ikke få treff på deler av en ATC kode (f.eks. N02AA).</div>);
        }
        else
        {
            renderedResults.push(<Row key="warning">
                <Col className="med-syn-atc-warning">
                    <b>Merk:</b> ATC kode ikke nødvendigvis er det samme som byttbarhet, da legemidler med samme ATC kode kan ha forskjellig styrke og legemiddelform (depot/vanlig tablett, mikstur/tablett). Sjekk med ordinerende lege før du bytter.
                </Col>
            </Row>);
            for(const result of results.list)
            {
                let merknad: ?React.Node;
                if(result.notice)
                {
                    merknad = <div className="merknad"><b className="text-danger">MERK:</b> {result.notice}</div>;
                }
                renderedResults.push(<Row key={result.name}>
                    <Col>
                        {result.name} (<ATCCode atc={result.atc} />)
                        {merknad}
                    </Col>
                </Row>);
            }
        }
        return renderedResults;
    }
}

/**
 * Result renderer for cleartext searches
 */
class MedicationSynonymsResultText extends React.PureComponent<MedicationSynonymsResultProps>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { search } = this.props;
        const results = MedSearcher.search(search);
        const renderedResults: Array<React.Node> = [];
        if(results.length === 0)
        {
            renderedResults.push(<div key="no-hits">Ingen treff på «{search}». Sjekk at legemiddelet er stavet rett. Alternativt kan det være et legemiddel på godkjenningsfritak (disse er ikke i synonymlisten).</div>);
        }
        else
        {
            if(results.with.length > 0)
            {
                for(const result of results.with)
                {
                    renderedResults.push( <MedicationSynonymsRenderSingleResult key={result.name} result={result} /> );
                }
            }
            if(results.without.length > 0)
            {
                if(results.with.length > 0)
                {
                    renderedResults.push(<h5 key="without">Legemiddel uten synonymer</h5>);
                }
                for(const result of results.without)
                {
                    renderedResults.push( <MedicationSynonymsRenderSingleResult key={result.name} result={result} /> );
                }
            }
        }
        return renderedResults;
    }
}

/**
 * Wrapper around {@link MedicationSynonymsResultUnconnected} that loads the data if it's
 * missing
 */
class MedicationSynonymsResultLoader extends React.PureComponent<MedicationSynonymsResultProps>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const {search} = this.props;
        if(search === undefined || search === null || search === "")
        {
            MedSearcher.initialize();
            return <div />;
        }
        return <ConditionalRender component={MedicationSynonymsResultUnconnected} test={() => MedSearcher.hasInitialized()} resolve={() => MedSearcher.initialize()} {...this.props} />;
    }
}

const MedicationSynonymsResult: React.AbstractComponent<{| |}> = connect<MedicationSynonymsResultProps,*,MedicationSynonymsResultProps,*,*,*>(
    (state): MedicationSynonymsResultProps =>
    {
        return {
            search: state.medSynonyms.search,
        };
    },
    () => {return {};}
)(MedicationSynonymsResultLoader);

type MedicationSynonymsSearchBarStateProps = {|
    search: string,
|};
type MedicationSynonymsSearchBarDispatchProps = {|
    onSearchUpdate: (string) => void,
|};
type MedicationSynonymsSearchBarProps = {|
    ...MedicationSynonymsSearchBarDispatchProps,
    ...MedicationSynonymsSearchBarStateProps,
|};


/**
 * The search bar for the medication synonyms page
 */
class MedicationSynonymsSearchBarUnconnected extends React.PureComponent<MedicationSynonymsSearchBarProps>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <Row>
            <Col>
                <Input placeholder="Søk" type="search" value={this.props.search} onChange={this.props.onSearchUpdate} name="search" id="medicationSynonymsSearch" />
            </Col>
        </Row>;
    }
}

const MedicationSynonymsSearchBar: React.AbstractComponent<{| |}> = connect<MedicationSynonymsSearchBarProps,*,MedicationSynonymsSearchBarStateProps,MedicationSynonymsSearchBarDispatchProps,*,*>(
    (state): MedicationSynonymsSearchBarStateProps =>
    {
        return {
            search: state.medSynonyms.search,
        };
    },
    (dispatch): MedicationSynonymsSearchBarDispatchProps =>
    {
        return {
            onSearchUpdate(ev: string)
            {
                dispatch(setMedSynonymsSearchString(getValueFromLiteralOrEvent(ev)));
            }
        };
    }
)(MedicationSynonymsSearchBarUnconnected);

/**
 * The UI for querying the generic medication database
 */
class MedicationSynonyms extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <ToolContainer tool="med-synonyms" requiresAuth={false}>
            <PageTitle title="Synonymliste for medikament" />
            <Row>
                <Col lg={10}>
                    <Row>
                        <Col>
                            <h3>Synonymliste for medikament</h3>
                        </Col>
                    </Row>
                    <MedicationSynonymsSearchBar />
                    <Row>
                        <Col>
                            <small>Dette er et hjelpeverktøy. Du er selv ansvarlig for å sikre at bytter er forsvarlige og, hvis din organisasjon krever det, å få byttene godkjent av ordinerende lege.
                            Data hentet fra <ExternalLink href="https://legemiddelverket.no/andre-temaer/fest">forskrivnings- og ekspedisjonsstøtte (FEST)</ExternalLink> levert av <ExternalLink href="https://legemiddelverket.no/">Statens Legemiddelverk</ExternalLink></small>.
                        </Col>
                    </Row>
                    <MedicationSynonymsResult />
                </Col>
            </Row>
        </ToolContainer>;
    }
}

export default MedicationSynonyms;
// Test-only exports
export { MedicationSynonyms, MedicationSynonymsResult, MedicationSynonymsSearchBar, MedicationSynonymsSearchBarUnconnected, MedicationSynonymsResultLoader, MedicationSynonymsResultUnconnected, MedicationSynonymsRenderSingleResult, ATCCode, ATCCodeUnconnected, MedicationSynonymsResultATC, MedicationSynonymsResultText };
