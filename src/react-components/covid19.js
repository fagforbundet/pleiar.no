/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { MainContainer, PageTitle } from './layout';
import { Row, Col } from 'reactstrap';
import { HandbookEntry } from '../data.handbook';
import { HandbookRender } from './handbook';

/**
 * The Pleiar.no free software infromation page
 */
class Covid19 extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const entry = new HandbookEntry('Hygiene_og_smittevern/Infeksjoner/Covid19');
        return  <MainContainer app="handbook">
            <Row>
                <Col>
                    <PageTitle title="Covid-19" />
                    <HandbookRender entry={entry} />
                </Col>
            </Row>
        </MainContainer>;
    }
}


// Test-only exports
export { Covid19 };
// Public exports
export default Covid19;
