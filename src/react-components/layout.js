/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// @flow
import * as React from 'react';
import { Row, Col, Container, UncontrolledTooltip} from 'reactstrap';
import { Link } from 'react-router-dom';
import { ExternalLink } from './shared/links';
// Let flow know about our GIT_REVISION global (from webpack)
declare var GIT_REVISION:string;
declare var GIT_REVISION_FULL:string;
declare var GIT_REVISION_DATA:string;

/**
 * A helper component that updates the title of the page.
 * Accepts an optional title parameter. If this is provided then the title will
 * be set to "Pleiar.no - TITLE". Otherwise it will be set to "Pleiar.no".
 */
function PageTitle (props: {| title?: string |}): null
{
    let title: string = 'Pleiar.no';
    if(props.title)
    {
        title += ' - '+props.title;
    }
    document.title = title;
    return (null);
}

/**
 * This is the main container for the app. All toplevel components must use this
 * to ensure that the design is correct and that the page title gets updated
 */
class MainContainer extends React.Component<{|app: string, children: React.Node, small?: boolean|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const wrapperClass = 'contentWrapper app-'+this.props.app;
        return <Container mt="2">
            <Row className="justify-content-md-center">
                <Col xl="10" className={wrapperClass}>
                    <Row className="mainContent">
                        <Col className={ this.props.small ? "col-lg-8 offset-lg-2" : null }>
                            {this.props.children}
                        </Col>
                    </Row>
                    <footer>
                        <Row className="mt-2">
                            <Col>
                                <img src="/footer.png" alt="" className="img-fluid no-user-select" draggable="false" />
                            </Col>
                        </Row>
                        <Row className="mt-2 info-footer">
                            <Col md="5" lg="4" className="offset-md-1 info-box">
                                <ExternalLink sameWindow={true} href="https://www.fagforbundet.no/"><img src="/brand-nospacing.png" className="img-fluid" alt="Fagforbundet" /></ExternalLink>
                                <b>Fagforbundet i Norge</b><br />
                                Boks 7003 St. Olavsplass<br />
                                0130 Oslo
                            </Col>
                            <Col md="5" lg="4" className="offset-lg-2 basicLinkList">
                                <Link to="/om/">Om <b>Pleiar.no</b></Link>
                                <Link to="/om/friprogramvare">Pleiar.no er fri programvare</Link>
                                <Link to="/om/personvern">Informasjon om personvern</Link>
                            </Col>
                        </Row>
                        <Row className="mt-2">
                            <Col className="col-md-10 offset-md-1 version-info">
                                <b>Pleiar.no</b>{" "}<Link className="text-muted" id="GitRevision" to="/om/system">versjon {GIT_REVISION}</Link> fra <ExternalLink sameWindow={true} href="https://www.fagforbundet.no/">Fagforbundet</ExternalLink><br />
                                <UncontrolledTooltip placement="top" target="GitRevision" key="gitRevUncontrolled">
                                    Dataversjon {GIT_REVISION_DATA}
                                </UncontrolledTooltip>
                            </Col>
                        </Row>
                    </footer>
                </Col>
            </Row>
        </Container>;
    }
}

export { MainContainer, PageTitle };
