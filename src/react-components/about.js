/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { Handle404 } from './shared';
import { MainContainer, PageTitle } from './layout';
import { Row, Col } from 'reactstrap';
import { Switch, Route, Redirect, Link } from 'react-router-dom';
import { Markdown } from './markdown';
import type { Match as RouterMatch } from 'react-router-dom';
import auth from '../auth';
import device from '../helper.device';
import { appInstall } from '../mobile-appinstall';
import { ExternalLink } from './shared/links';
import PleiarSearcher from '../searcher';
import MedSearcher from '../med-synonym-searcher';
import IOSShareIcon from 'react-feather/dist/icons/share';
import IOSPlusIcon from 'react-feather/dist/icons/plus-square';
import FFHomeIcon from 'react-feather/dist/icons/home';
import FBMoreDots from 'react-feather/dist/icons/more-horizontal';
import OperaMoreDots from 'react-feather/dist/icons/more-vertical';
import SysInfo from '../helper.sysinfo';
import { quizDataManager } from "../quiz-manager";
import type { SystemInfoType } from '../helper.sysinfo';

// $FlowIssue[cannot-resolve-module] - Flow doesn't understand markdown imports
import friProgramvare from '../../data/sider/fri-programvare.md';
// $FlowIssue[cannot-resolve-module] - Flow doesn't understand markdown imports
import personvern from '../../data/sider/personvern.md';
// $FlowIssue[cannot-resolve-module] - Flow doesn't understand markdown imports
import resources from '../../data/sider/flerefagtilbud.md';
// $FlowIssue[cannot-resolve-module] - Flow doesn't understand markdown imports
import aboutApp from '../../data/sider/ompleiar.md';

// Let flow know about our GIT_REVISION globals (from webpack)
declare var GIT_REVISION:string;
declare var GIT_REVISION_FULL:string;
declare var GIT_REVISION_DATA:string;

/**
 * This provides various system information, useful for debugging
 */
class AboutSystem extends React.PureComponent<{||}, {|bodyClass: string | null|}>
{
    constructor () // eslint-disable-line
    {
        super();
        this.state = {
            bodyClass: null
        };
        const update = () =>
        {
            this.forceUpdate();
        };
        // Refresh ourself if something updates
        if (!appInstall.canInstall())
        {
            appInstall.listen(update);
        }
        if (!MedSearcher.hasInitialized())
        {
            MedSearcher.onInitialize(update);
        }
        if (!quizDataManager.hasInitialized())
        {
            quizDataManager.onInitialize(update);
        }
        if (!PleiarSearcher.hasInitialized())
        {
            PleiarSearcher.onInitialize(update);
        }
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        setTimeout( () =>
        {
            if(document.body !== null)
            {
                this.setState({
                    bodyClass: document.body.getAttribute('class')
                });
            }
        },1);
        const SystemInfo : SystemInfoType = SysInfo.getSystemInfo();
        const sources = [];
        if(SystemInfo.sources.length > 0)
        {
            for (const source of SystemInfo.sources)
            {
                const { src, rel } = source;
                if (source.type == "script")
                {
                    sources.push(<code key={src}>{src}</code>);
                }
                else if(rel)
                {
                    sources.push(<code key={src+rel}>{src+' ('+rel+')'}</code>);
                }
            }
        }
        else
        {
            sources.push('(klarte ikke hente kildeliste)');
        }
        let appVersion: string | React.Node = GIT_REVISION_FULL;
        if(appVersion !== "(git)")
        {
            appVersion = <ExternalLink sameWindow={true} href={"https://gitlab.com/fagforbundet/pleiar.no/tree/"+GIT_REVISION_FULL}>
                <span className="d-none d-sm-inline-block">{GIT_REVISION_FULL}</span>
                <span className="d-sm-none">{GIT_REVISION}</span>
                </ExternalLink>;
        }
        const breakPoints = [
            <span key="xs" className="d-inline-block">xs&nbsp;</span>,
            <span key="sm" className="d-none d-sm-inline-block">sm&nbsp;</span>,
            <span key="md" className="d-none d-md-inline-block">md&nbsp;</span>,
            <span key="lg" className="d-none d-lg-inline-block">lg&nbsp;</span>,
            <span key="xl" className="d-none d-xl-inline-block">xl&nbsp;</span>,
        ];
        const moreData = [];
        const appInstallMode = appInstall.mode();
        if(device.isAppMode() === false && device.isMobileDevice() === true && appInstallMode === "chrome")
        {
            let installState: string = "klar til installering";
            if(! appInstall.canInstall())
            {
                installState = "ikke klar";
            }
            moreData.push(<span key="installState"><b>Status for installasjon</b>: {installState}</span>);
            moreData.push(<br key="installStateNewline" />);
        }
        let authInfo: React.Node;
        if(auth.isAuthenticated())
        {
            authInfo = <span>innlogget <Link to="/auth/logout">(logg ut)</Link></span>;
        }
        else
        {
            authInfo = <span>ikke innlogget</span>;
        }
        return <Row>
            <PageTitle title="Systeminformasjon" />
            <Col className="systemInfo">
                <h2>System<wbr />&shy;informasjon</h2>
                <b>{device.isAppMode() ? 'Motor' : 'Nettleser' }</b>: {SystemInfo.UA}<br />
                <b>Plattform</b>: {SystemInfo.platform} {appInstallMode !== null ? ( "("+appInstallMode+")" ) : "" }<br />
                <b>Appversjon</b>: {appVersion}<br />
                <b>Dataversjon</b>: {GIT_REVISION_DATA}<br />
                <b>Status for autentisering</b>: {authInfo}<br />
                <b>Inndata</b>:
                    {" "}{device.isTouchScreen() ? "touch" : "nøyaktig" }
                    <span className="inputMediaQueries">
                        {" - "}
                        <span className="hover"><span className="yes">hover:hover,</span><span className="no">hover:none,</span></span>
                        {" "}<span className="pointer">{" "}<span className="fine">pointer:fine</span><span className="coarse">pointer:coarse</span><span className="none">pointer:none</span></span>
                    </span>
                <br />
                <b>Modus</b>: {device.isAppMode() ? "app" : (device.isMobileDevice() ? "mobil-web" : "web" ) }<br />
                <b>PleiarSearcher status</b>: {SystemInfo.pleiarSearcherStatus}<br />
                <b>MedSearcher status</b>: {SystemInfo.medSearcherStatus}<br />
                <b>QuizDataManager status</b>: {SystemInfo.quizDataManagerStatus}<br />
                <b>Motor-flagg</b>: {SystemInfo.featuresDetected.length > 0 ? SystemInfo.featuresDetected.join(', ') : '(ingen)'}<br />
                <b>body-flagg</b>: {this.state.bodyClass ? this.state.bodyClass : "(ingen)"}<br />
                <b>Synlige brytepunkt</b>: {breakPoints} ({SystemInfo.dimensions.w}x{SystemInfo.dimensions.h})<br />
                <b>Lokal tid</b>: {(new Date).toLocaleString()} ({Math.round((new Date).getTime()/1000)})<br />
                <b>URL:</b> {location.href}<br />
                <b>Referrer:</b> {document.referrer ? document.referrer : "(ingen)"}<br />
                <b>Service worker</b>: {SystemInfo.serviceWorkerStatus}<br />
                {moreData}
                <br />
                <b>Kildefiler:</b><br />
                <div className="sourceFiles">{sources}</div>
            </Col>
        </Row>;
    }
}

/**
 * The Pleiar.no privacy information page
 */
class AboutPrivacy extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <Row>
            <Col>
                <PageTitle title="Personvern" />
                <Markdown content={personvern} />
            </Col>
        </Row>;
    }
}

/**
 * The Pleiar.no free software infromation page
 */
class AboutFreeSoftware extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <Row>
            <Col>
                <PageTitle title="Fri programvare" />
                <Markdown content={friProgramvare} />
            </Col>
        </Row>;
    }
}

/**
 * The Pleiar.no free software infromation page
 */
class AboutPleiarNo extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <Row>
            <Col>
                <PageTitle title="om" />
                <Markdown content={aboutApp} />
            </Col>
        </Row>;
    }
}

/**
 * Renders an info page with links to more resources
 */
class OtherFFResources extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <Row>
            <Col>
                <PageTitle title="Flere fagtilbud fra Fagforbundet" />
                <Markdown content={resources} />
            </Col>
        </Row>;
    }
}

/**
 * Displays "app" installation instructions in supported browsers on supported
 * platforms
 */
class InstallApp extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const mode = appInstall.mode();
        let renderAppInfoBlurb: boolean = true;
        const components: Array<React.Node> = [<h3 key="header">Installer app</h3>];
        if(mode === "firefox-android")
        {
            components.push(<div key="ff-android-instructions">
                For å installere Pleiar.no som app, trykk på <FFHomeIcon /> i adresselinja øverst på skjermen og velg så «legg til på startskjermen». Hvis du bruker Firefox Focus må du bytte til vanlig Firefox (eller Chrome) for å kunne installere appen.
            </div>);
        }
        else if(mode === "ios-other")
        {
            renderAppInfoBlurb = false;
            components.push(<div key="ios-other-instructions">
                På iPhone og iPad er det bare Safari som kan installere apper. For å installere appversjonen av pleiar.no må du åpne sida i Safari.
            </div>);
        }
        else if(mode === "safari-ios")
        {
            components.push(<div key="ios-safari-instructions">
                Trykk på <IOSShareIcon /> nederst på skjermen. Scroll litt ned i menyen som kommer opp og velg <b><IOSPlusIcon />Legg til på hjemskjermen</b>. Ser du ikke <IOSShareIcon />? Prøv å scrolle opp og ned på skjermen for å få menylinja til å vises.
            </div>);
        }
        else if(mode === "samsung")
        {
            components.push(<div key="samsung-android-instructions">
                For å installere Pleiar.no som app, trykk på <IOSPlusIcon /> i adresselinja øverst på skjermen.<br /><br />
                Samsung Internett støtter app-installasjon kun hvis den er installert fra Galaxy Apps. Hvis du ikke får opp <IOSPlusIcon /> knappen, prøv å bruke en annen nettleser som f.eks. <ExternalLink href="https://play.google.com/store/apps/details?id=com.android.chrome">Chrome</ExternalLink> eller <ExternalLink href="https://play.google.com/store/apps/details?id=org.mozilla.firefox">Firefox</ExternalLink>.
            </div>);
        }
        else if(mode === "facebook-android" || mode === "facebook-ios" || mode === "android-webview")
        {
            renderAppInfoBlurb = false;
            let openIn: string = "";
            let name: string = "Facebook-vindu";
            let nameFrom: string = "Facebook-vinduet";
            if(mode === "facebook-android" || mode === "android-webview")
            {
                openIn = '"Åpne med Chrome" (eller "Åpne med Firefox", "Åpne med nettleser" eller lignende)';
                if(mode === "android-webview")
                {
                    name = "begrenset nettleser-vindu";
                    nameFrom = "begrensede nettleser-vinduer";
                }
            }
            else if(mode === "facebook-ios")
            {
                openIn = '"Åpne med Safari"';
            }
            components.push(<div key="facebook-iab-instructions">
                Du har pleiar.no åpent i et {name}. Det er desverre ikke mulig å installere apper fra {nameFrom}. Trykk på <FBMoreDots /> oppe til høyre og velg {openIn}. I vinduet som kommer opp kan du velge å installere appen.
            </div>);
        }
        else if(mode === "chrome" && !appInstall.canInstallWithAppInstallEvent())
        {
            components.push(<div key="chrome-problem">
                <b>En feil oppstod</b> under forsøket på å installere appen.
                Dette kan bety at appen allerede er installert.<br /><br />
                Hvis appen <i>ikke</i> er installert kan du installere den
                manuelt ved å trykke på <OperaMoreDots /> øvers på skjermen (du
                må kanskje scrolle opp for at knappen skal bli synlig) for så å
                velge «Legg til på hjemskjermen».<br /><br />
                Hvis du fremdeles ikke får installert appen, ta kontakt
                med oss på <ExternalLink sameWindow={true}
                    href="mailto:kontakt@pleiar.no">e-post
                (kontakt@pleiar.no)</ExternalLink> så skal vi feilsøke problemet.
            </div>);
        }
        else if(mode === "opera")
        {
            components.push(<div key="opera-instructions">
                For å installere Pleiar.no som app, scroll opp for å få adresselinja til å bli synlig. Trykk så på <OperaMoreDots /> øverst på skjermen og velg «Legg til på &gt; Startskjerm».
            </div>);
        }
        else
        {
            return <Redirect to="/" />;
        }
        if(renderAppInfoBlurb)
        {
            components.push(<div key="blurb" className="mt-2">
                Appversjonen av Pleiar.no fungerer uansett om du har internett eller ikke, og oppdaterer seg selv.
            </div>);
        }
        return components;
    }
}

/**
 * Root of the about component.
 */
class About extends React.PureComponent<{|match: RouterMatch|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const match = this.props.match;
        return  <MainContainer app="about" small={true}>
                    <Switch>
                        <Route path={match.path} exact render={ () => { return <AboutPleiarNo />; }} />
                        <Route path={match.path+'/personvern'} render={() => {return <AboutPrivacy />; }} />
                        <Route path={match.path+'/friprogramvare'} render={() => {return <AboutFreeSoftware />; }} />
                        <Route path={match.path+'/system'} render={() => {return <AboutSystem />; }} />
                        <Route path={match.path+'/flerefagtilbud'} render={ () => { return <OtherFFResources />; }} />
                        <Route path={match.path+'/app'} render={ () => { return <InstallApp />; }} />
                        <Handle404 />
                    </Switch>
                </MainContainer>;
    }
}

// Test-only exports
export { AboutPrivacy, AboutFreeSoftware, AboutPleiarNo, About, AboutSystem, InstallApp };
// Public exports
export default About;
