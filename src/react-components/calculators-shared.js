/**
 * @prettier
 */
/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from "react";
import { Row, Col, Input, Label } from "reactstrap";
import { connect } from "react-redux";
import { toggleRounding, toggleFormulaVisibility } from "../actions/global";
import type { pleiarReduxState } from "../reducers/root";

type CalcExplanationEntryPropsFromRedux = {|
    formulaVisibility: boolean,
|};
type CalcExplanationType = "description" | "formula" | "calculation";
type CalcExplanationEntryOwnProps = {|
    children?: React.Node,
    className: string,
    lg: number,
    type: CalcExplanationType,
|};
type CalcExplanationEntryProps = {|
    ...CalcExplanationEntryOwnProps,
    ...CalcExplanationEntryPropsFromRedux,
|};
/**
 * Lowest level of a CalcExplanationLine, outputs the column if needed
 */
class CalcExplanationEntryUnconnected extends React.PureComponent<CalcExplanationEntryProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { formulaVisibility, children, lg, type } = this.props;
        let className: string = this.props.className;
        className += " calcExplanationEntry";
        if (!formulaVisibility && type === "formula") {
            return <div />;
        }

        const xsWidthValues = {
            formulaVisible: {
                description: 5,
                calculation: 3,
                formula: 4,
            },
            formulaHidden: {
                description: 7,
                calculation: 5,
                formula: 0,
            },
        };
        const xsWidthSource = formulaVisibility
            ? "formulaVisible"
            : "formulaHidden";
        const xsWidth: number = xsWidthValues[xsWidthSource][type];
        return (
            <Col xs={xsWidth} lg={lg} className={className}>
                {children}
            </Col>
        );
    }
}
const CalcExplanationEntry: React.AbstractComponent<CalcExplanationEntryOwnProps> =
    connect<
        CalcExplanationEntryProps,
        CalcExplanationEntryOwnProps,
        CalcExplanationEntryPropsFromRedux,
        _,
        pleiarReduxState,
        _,
    >(
        (state: pleiarReduxState): CalcExplanationEntryPropsFromRedux => {
            return {
                formulaVisibility: state.global.formulaVisibility,
            };
        },
        () => {
            return {};
        },
    )(CalcExplanationEntryUnconnected);

type CalcExplanationDescriptionProps = {|
    children?: React.Node,
    header?: boolean,
|};

/**
 * The description of a calculation
 */
class CalcExplanationDescription extends React.PureComponent<CalcExplanationDescriptionProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { header, children } = this.props;
        let className: string = "calcDescription-desc";
        if (header) {
            className = "header";
        }
        return (
            <CalcExplanationEntry
                type="description"
                className={className}
                lg={5}
            >
                {children}
            </CalcExplanationEntry>
        );
    }
}

type CalcExplanationCalculationProps = {|
    children?: React.Node,
    header?: boolean,
|};

/**
 * The description of a calculation
 */
class CalcExplanationCalculation extends React.PureComponent<CalcExplanationCalculationProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { header, children } = this.props;
        let className: string = "calcCalculation-calc calculation";
        if (header) {
            className = "header";
        }
        return (
            <CalcExplanationEntry
                type="calculation"
                className={className}
                lg={4}
            >
                {children}
            </CalcExplanationEntry>
        );
    }
}

type CalcExplanationFormulaProps = {|
    children?: React.Node,
    header?: boolean,
|};
/**
 * The description of a calculation
 */
class CalcExplanationFormula extends React.PureComponent<CalcExplanationFormulaProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { header, children } = this.props;
        let className: string = "calcFormula formula";
        if (header) {
            className = "header";
        }
        return (
            <CalcExplanationEntry type="formula" className={className} lg={3}>
                {children}
            </CalcExplanationEntry>
        );
    }
}

type CalcExplanationOwnProps = {|
    children: React.Node,
    hasAdditionalCalculations?: boolean,
    additionalCalculationsToggle?: () => mixed,
    additionalCalculations?: boolean,
|};
type CalcExplanationReduxStateProps = {|
    rounding: boolean,
    formulaVisibility: boolean,
|};
type CalcExplanationReduxDispatchProps = {|
    onToggleRounding: () => mixed,
    onToggleFormulaVisibility: () => mixed,
|};
type CalcExplanationProps = {|
    ...CalcExplanationOwnProps,
    ...CalcExplanationReduxStateProps,
    ...CalcExplanationReduxDispatchProps,
|};
/**
 * Root element of a line of calculation explanations
 */
class CalcExplanationUnconnected extends React.PureComponent<CalcExplanationProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const checkboxes = [];
        let className: string = "calculator-descriptions";
        if (this.props.formulaVisibility === true) {
            className += " mobileOverflowScrolling";
        }
        checkboxes.push(
            <Label
                check
                className="roundingToggle ml-2 ml-sm-0"
                key="precision"
            >
                <Input
                    type="checkbox"
                    checked={!this.props.rounding}
                    onChange={this.props.onToggleRounding}
                />{" "}
                Vis tal med full presisjon
            </Label>,
        );
        checkboxes.push(
            <Label
                check
                className="ml-4 ml-sm-2 formulaVisibilityToggle"
                key="formulas"
            >
                <Input
                    type="checkbox"
                    checked={this.props.formulaVisibility}
                    onChange={this.props.onToggleFormulaVisibility}
                />{" "}
                Vis formlar
            </Label>,
        );
        if (this.props.hasAdditionalCalculations) {
            checkboxes.push(
                <Label
                    check
                    className="ml-4 ml-sm-2 additionalCalculationsToggle"
                    key="additionalCalculations"
                >
                    <Input
                        type="checkbox"
                        checked={this.props.additionalCalculations}
                        onChange={this.props.additionalCalculationsToggle}
                    />{" "}
                    Vis utrekning av «andre tidseiningar»
                </Label>,
            );
        }
        return (
            <div className={className}>
                <Row className="mt-5 text-muted">
                    <Col>
                        <h5>Utrekning</h5>
                        <small>
                            <Row>
                                <Col className="form-inline">{checkboxes}</Col>
                            </Row>
                            <CalcExplanationLine>
                                <CalcExplanationDescription header={true}>
                                    Skildring
                                </CalcExplanationDescription>
                                <CalcExplanationCalculation header={true}>
                                    Utrekning
                                </CalcExplanationCalculation>
                                <CalcExplanationFormula header={true}>
                                    Formel
                                </CalcExplanationFormula>
                            </CalcExplanationLine>
                            {this.props.children}
                        </small>
                    </Col>
                </Row>
            </div>
        );
    }
}
const CalcExplanation: React.AbstractComponent<CalcExplanationOwnProps> =
    connect<
        CalcExplanationProps,
        CalcExplanationOwnProps,
        CalcExplanationReduxStateProps,
        CalcExplanationReduxDispatchProps,
        _,
        _,
    >(
        (state): CalcExplanationReduxStateProps => {
            return {
                formulaVisibility: state.global.formulaVisibility,
                rounding: state.global.round,
            };
        },
        (dispatch): CalcExplanationReduxDispatchProps => {
            return {
                onToggleRounding: () => {
                    dispatch(toggleRounding());
                },
                onToggleFormulaVisibility: () => {
                    dispatch(toggleFormulaVisibility());
                },
            };
        },
    )(CalcExplanationUnconnected);

type CalcExplanationLineProps = {|
    children: React.Node,
|};
/**
 * A line containing calculation descriptions
 */
class CalcExplanationLine extends React.PureComponent<CalcExplanationLineProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        return <Row className="calcDescription-row">{this.props.children}</Row>;
    }
}

type FractionProps = {|
    top: number | string,
    bottom: number | string,
|};
/**
 * A fraction
 */
class Fraction extends React.PureComponent<FractionProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { top, bottom } = this.props;
        return (
            <div className="fraction">
                <div className="above">{top}</div>
                <div className="below">{bottom}</div>
            </div>
        );
    }
}

type FractionResultProps = {|
    children: React.Node,
|};

/**
 * The result of a fraction
 */
class FractionResult extends React.PureComponent<FractionResultProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        return <div className="fractionResult">{this.props.children}</div>;
    }
}

type FractionComponentProps = {|
    children: React.Node,
|};

/**
 * The result of a fraction
 */
class FractionComponent extends React.PureComponent<FractionComponentProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        return <div className="fractionComponent">{this.props.children}</div>;
    }
}

export {
    CalcExplanation,
    CalcExplanationLine,
    CalcExplanationDescription,
    CalcExplanationCalculation,
    CalcExplanationFormula,
    Fraction,
    FractionResult,
    FractionComponent,
};
