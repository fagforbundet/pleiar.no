/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { MainContainer, PageTitle } from './layout';
import { Row, Col } from 'reactstrap';
import { NotFound } from './shared';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { QuickRefIndex, QuickRefEntry } from '../data.quickref';
import { HandbookRender, HandbookLikeSearchResult } from './handbook';

/*
 * QuickRef is a simplified variant of the Handbook. It uses a lot of the
 * rendering code from the handbook, as well as subclasses of the datatypes for
 * the handbook.
 *
 * The main difference is that it only has a single section, and makes some
 * assumptions about this being true. Tests validate these assumptions.
 */

/**
 * The quickref search result renderer
 */
class QuickRefSearchResult extends React.PureComponent<{|entry: QuickRefEntry |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { entry } = this.props;
        return <HandbookLikeSearchResult entry={entry} prefix="Hurtigoppslag" readmore="Les mer i hurtigoppslaget" />;
    }
}

/**
 * The Pleiar.no quickref index/list renderer
 */
class QuickRefIndexRender extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const index = new QuickRefIndex();
        const rootChildren = index.tree;
        const result = [];
        for(const child of rootChildren)
        {
            result.push(
                <Link key={child.path.join('/')} to={child.url}><span>{child.title}</span></Link>
            );
        }
        return <div className="handbookIndex">
                <h1>Hurtigoppslag</h1>
                <div className="linkList autoWrapping">
                    {result}
                </div>
            </div>;
    }
}

/**
 * The QuickRef app component. Renders pages or the index. Reuses {@link
 * HandbookRender} from handbook.js for rendering
 */
class QuickRef extends React.Component<{|match: {| params: {| quickrefPath?: string|}|}|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const match = this.props.match;
        let subComponent: React.Node;
        let small: boolean = false;
        if(match.params.quickrefPath)
        {
            const entry = new QuickRefEntry( match.params.quickrefPath );
            if(entry.exists)
            {
                subComponent = <HandbookRender entry={entry} navBackLink="/oppslag" navMode="back" />;
            }
            else
            {
                subComponent = <NotFound root={false} />;
            }
        }
        else
        {
            subComponent = <QuickRefIndexRender />;
            small = true;
        }
        return  <MainContainer app="quickref" small={small}>
            <Row>
                <Col>
                    <PageTitle title="Hurtigoppslag" />
                    {subComponent}
                </Col>
            </Row>
        </MainContainer>;
    }
}

// Public exports
export default (withRouter(QuickRef): React.AbstractComponent<{| |}>);
export { QuickRefSearchResult };
// Testing exports
export { QuickRef as QuickRefRaw, QuickRefIndexRender };
