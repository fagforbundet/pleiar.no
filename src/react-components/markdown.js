/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import ReactMarkdown, { uriTransformer } from 'react-markdown';
import { Link } from 'react-router-dom';
import slug from '../helper.slugs.js';
import { Alert } from 'reactstrap';
import dictionary from '../../data/datasett/dictionary.json';
import labValues from '../../data/datasett/lab.json';
import { DictionaryEntry } from './dictionary';
import { LabValueTable, LabValueCategoryTable } from './labvalues';
import { ExternalLink } from './shared/links';
import { Table } from 'reactstrap';
import { HandbookIndex } from '../data.handbook';
import { HandbookIndexRenderer } from './handbook';
import remarkGFM from 'remark-gfm';
import type { DictionaryEntry as DictionaryEntryType } from '../types/data';

/**
 * A normal header generator that also adds a slug (`id`) if it can
 */
class MarkdownHeader extends React.PureComponent<{|level: number, children: React.Node|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const children = this.props.children;
        let id: string = '';
        if(Array.isArray(children))
        {
            const child = children[0];
            if(typeof(child) === "string")
            {
                id = slug.get(child);
            }
        }
        return React.createElement('h'+this.props.level,{id},this.props.children);
    }
}

/**
 * A <table> generator that uses reactstrap instead of bare <table> tags
 */
class MarkdownTable extends React.PureComponent<{| children: React.Node |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <Table hover size="sm" responsive>{this.props.children}</Table>;
    }
}

/**
 * A link renderer that builds a `<Link>` for local links, and `<ExternalLink>` for external ones.
 */
class MarkdownBasicLink extends React.PureComponent<{|href: string, forceExternal?: boolean, children: React.Node|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { href, children, forceExternal } = this.props;

        if ( href.substr(0,4) === 'http' || forceExternal === true )
        {
            return <ExternalLink href={href}>{children}</ExternalLink>;
        }
        else if( (href.substr(0,1) === '/' || href.indexOf(':') === -1) && href.substr(0,2) !== "//")
        {
            return <Link to={href}>{children}</Link>;
        }
        else
        {
            return <ExternalLink sameWindow={true} href={href}>{children}</ExternalLink>;
        }
    }
}

/**
 * Error message handler for `MarkdownExtendedLink`
 */
class MarkdownExtendedLinkError extends React.PureComponent<{|nodeType: string, nodeParams: Array<string>, message: string, additionalValues: {...}|}> // eslint-disable-line flowtype/require-exact-type
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <Alert color="danger">
            MarkdownExtendedLink: <b>{this.props.message}</b><br />
            nodeType: {this.props.nodeType}<br />
            nodeParams: {JSON.stringify(this.props.nodeParams)}<br />
            additionalValues: {JSON.stringify(this.props.additionalValues)}
        </Alert>;
    }
}

/**
 * A special markdown link renderer that handles the special `!component` syntax.
 * This syntax is fully documented in docs/ExtendedMarkdown.md
 */
class MarkdownExtendedLink extends React.PureComponent<{|href: string, children: React.Node|}>
{
    /**
     * Parses an extended link string and hands the result over to the handler
     * for that component type
     */
    extendedLinkNode (): React.Node
    {
        const nodeParams = this.props.href.substr(1).split('/');
        const nodeType = nodeParams.shift();
        if(nodeType === 'dict')
        {
            return this.extendedTypeDict(nodeParams);
        }
        else if(nodeType === "handbookIndexOpen")
        {
            return this.extendedTypeHandbookIndexOpen(nodeParams);
        }
        else if(nodeType === "handbookIndexPlain")
        {
            return this.extendedTypeHandbookIndexPlain(nodeParams);
        }
        else if(nodeType == 'labValues')
        {
            return this.extendedTypeLabValues(nodeParams);
        }
        else if(nodeType == 'vimeo')
        {
            return this.extendedTypeVimeo(nodeParams);
        }
        else if(nodeType == 'youtube')
        {
            return this.extendedTypeYoutube(nodeParams);
        }
        return <MarkdownExtendedLinkError nodeType={nodeType} nodeParams={nodeParams} message={"Unknown extended markdown node type: "+nodeType} additionalValues={{}} />;
    }

    /**
     * Handler for !labValues
     */
    extendedTypeLabValues (params: Array<string>): React.Node
    {
        if(params.length)
        {
            const category = decodeURIComponent(params.join('/') );
            if (!labValues.index[category])
            {
                return <MarkdownExtendedLinkError nodeType="labValues" nodeParams={params} message={"Kunne ikkje slå opp labverdi-kategori: "+category} additionalValues={{}} />;
            }
            else
            {
                return <LabValueCategoryTable inhibitTitle={true} values={labValues} category={category} />;
            }
        }
        else
        {
            return <LabValueTable values={labValues} />;
        }
    }

    /**
     * Handler for !dict
     */
    extendedTypeDict (params: Array<string>): React.Node
    {
        const name = decodeURIComponent(params[0]);
        // No simple way to type this variable, it's a DictionaryEntryType, but
        // it can also be an empty DictionaryEntryType
        // FIXME: There has to be a way to do this
        //
        // eslint-disable-next-line flowtype/require-variable-type
        let dictEntry;
        for(const entry of dictionary)
        {
            if(entry.expression === name)
            {
                dictEntry = (entry: DictionaryEntryType);
            }
        }
        if(dictEntry === undefined)
        {
            return <MarkdownExtendedLinkError nodeType="dict" nodeParams={params} message="Kunne ikkje slå opp oppføring i ordlista" additionalValues={{
                name
            }} />;
        }
        return <DictionaryEntry entry={dictEntry} inhibitTitle={true} forceAuth={true} />;
    }

    /**
     * Handler for !handbookIndexOpen
     */
    extendedTypeHandbookIndexOpen (params: Array<string>): React.Node
    {
        const indexEntry = new HandbookIndex( params.join('/') );
        if(! indexEntry.exists)
        {
            return <MarkdownExtendedLinkError nodeType="handbookIndexOpen" nodeParams={params} message="Kunne ikkje slå opp indeks" additionalValues={{}} />;
        }
        return <HandbookIndexRenderer index={indexEntry} title={""} skipToplevelArticles={true} alwaysOpen={true} />;
    }

    /**
     * Handler for !handbookIndexPlain
     */
    extendedTypeHandbookIndexPlain (params: Array<string>): React.Node
    {
        const indexEntry = new HandbookIndex( params.join('/') );
        if(! indexEntry.exists)
        {
            return <MarkdownExtendedLinkError nodeType="handbookIndexPlain" nodeParams={params} message="Kunne ikkje slå opp indeks" additionalValues={{}} />;
        }
        return <HandbookIndexRenderer index={indexEntry} title={""} plainList={true} />;
    }

    /**
     * Handler for !youtube
     */
    extendedTypeYoutube(params: Array<string>): React.Node
    {
        const id = params[0];
        if (id === null || id === undefined || id.length < 4)
        {
            return <MarkdownExtendedLinkError nodeType="youtube" nodeParams={params} message="Manglar ID" additionalValues={{}} />;
        }
        if(navigator.userAgent === "ReactSnap")
        {
            return <div style={{height: '360px'}} />;
        }
        return <span className="videoWrapper img-fluid">
            <iframe mozallowfullscreen="" webkitallowfullscreen="" allowFullScreen src={"https://www.youtube.com/embed/"+id} height="360" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; picture-in-picture; web-share"></iframe>
        </span>;
    }

    /**
     * Handler for !vimeo
     */
    extendedTypeVimeo (params: Array<string>): React.Node
    {
        const rawID = params[0];
        if (rawID === null || rawID === undefined)
        {
            return <MarkdownExtendedLinkError nodeType="vimeo" nodeParams={params} message="Manglar ID" additionalValues={{}} />;
        }
        const id = Number.parseInt(rawID);
        if ( Number.isNaN(id) || id <= 0)
        {
            return <MarkdownExtendedLinkError nodeType="vimeo" nodeParams={params} message="Ugyldig ID" additionalValues={{}} />;
        }
        if(navigator.userAgent === "ReactSnap")
        {
            return <div style={{height: '360px'}} />;
        }
        return <span className="videoWrapper img-fluid">
            <iframe mozallowfullscreen="" webkitallowfullscreen="" allowFullScreen src={"https://player.vimeo.com/video/"+id+"?title=0&quality=&byline=0&portrait=0&dnt=1"} height="360" frameBorder="0"></iframe>
        </span>;
    }

    /**
     * Parses an internal link string (:component/path) and returns a `Link`
     */
    internalLinkNode (): React.Node
    {
        let nodePath: Array<string> = this.props.href.substr(1).split('/');
        const nodeType = nodePath.shift();
        if(nodeType === 'handbook')
        {
            nodePath.unshift('handbok');
        }
        else if(nodeType === 'quickref')
        {
            nodePath.unshift('oppslag');
        }
        else if(nodeType === "elearn")
        {
            nodePath.unshift('elaering');
        }
        else if(nodeType === "dict")
        {
            nodePath = ['ordliste',nodePath.join('/')];
        }
        else
        {
            return <span>{this.props.children} (intern feil: lenkefeil)</span>;
        }
        return <Link to={"/"+nodePath.join('/')}>{this.props.children}</Link>;
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const typeChar = this.props.href.substr(0,1);
        if(typeChar === '!')
        {
            return this.extendedLinkNode();
        }
        else if(typeChar === ':')
        {
            return this.internalLinkNode();
        }
        else if(typeChar === '+')
        {
            return <MarkdownBasicLink forceExternal={true} href={this.props.href.substr(1)}>{this.props.children}</MarkdownBasicLink>;
        }
        return <MarkdownBasicLink href={this.props.href}>{this.props.children}</MarkdownBasicLink>;
    }
}

/**
 * A replacement for building `<p>` for markdown paragraphs that uses
 * a `<div class="div-like-p">` instead (because if extended link syntax
 * is enabled, then we can't use `<p>` since you're not suppose to have `<div>`
 * as a child of `<p>`, and the extended link syntax will emit `<div>`.
 */
class MarkdownExtendedParagraph extends React.PureComponent<{| children: React.Node |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <div className="div-like-p">{this.props.children}</div>;
    }
}

/**
 * Link handler that adds the `img-fluid` class
 */
class MarkdownImg extends React.PureComponent<{|src: string, alt: string|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        let alt: string = "";
        let imgText: string = "";
        if(this.props.alt.indexOf('#') === 0)
        {
            alt = this.props.alt.replace(/^#/,'');
        }
        else
        {
            imgText = this.props.alt;
            alt = this.props.alt;
        }
        const img = <img className="img-fluid" src={this.props.src} alt={alt} />;
        if(this.props.alt.length > 0)
        {
            return <div className="imageContainer">
                {img}
                <div className="img-text">
                    <Markdown content={imgText} />
                </div>
            </div>;
        }
        return img;
    }
}

type MarkdownRenderers = {|
    a: Class<MarkdownBasicLink | MarkdownExtendedLink>,
    h1: Class<MarkdownHeader>,
    h2: Class<MarkdownHeader>,
    h3: Class<MarkdownHeader>,
    h4: Class<MarkdownHeader>,
    h5: Class<MarkdownHeader>,
    img: Class<MarkdownImg>,
    table: Class<MarkdownTable>,
    p?: Class<MarkdownExtendedParagraph>
|};

/**
 * Our special markdown handler. A wrapper around `react-markdown`.
 *
 * Set `permitSpecialNodes: true` if you want to enable parsing of extended
 * markdown links (`!component`). Defaulst to being disabled.
 */
class Markdown extends React.PureComponent<{|content: string, permitSpecialNodes?: boolean|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const components: MarkdownRenderers = {
            a: MarkdownBasicLink,
            h1: MarkdownHeader,
            h2: MarkdownHeader,
            h3: MarkdownHeader,
            h4: MarkdownHeader,
            h5: MarkdownHeader,
            img: MarkdownImg,
            p: MarkdownExtendedParagraph,
            table: MarkdownTable
        };

        let transformer: typeof uriTransformer | null = uriTransformer;

        if(this.props.permitSpecialNodes === true)
        {
            components.a = MarkdownExtendedLink;
            // Trust URIs by disabling the transformer. Otherwise we can't
            // have :-prefixed URIs
            transformer = null;
        }

        slug.reset();
        return <ReactMarkdown components={components} remarkPlugins={[remarkGFM]} transformLinkUri={transformer}>{this.props.content}</ReactMarkdown>;
    }
}

/* Public exports */
export { Markdown };
/* Testing exports */
export { MarkdownHeader, MarkdownBasicLink, MarkdownExtendedLinkError, MarkdownExtendedLink, MarkdownExtendedParagraph, MarkdownImg, MarkdownTable };
