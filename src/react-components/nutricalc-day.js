/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
// $FlowIssue[cannot-resolve-module] react-select+flow temporarily broken, tracked as https://github.com/JedWatson/react-select/issues/3612
import Select from 'react-select';
import { Row, Col, Button, Collapse, Table} from 'reactstrap';
import { numbers } from '../helper.numbers';
import { reset, toggleFoodList, setEntryValue, removeEntry, setMinimum } from '../actions/nutricalc-day';
import XCircle from 'react-feather/dist/icons/x-circle';
import { connect } from 'react-redux';
//import 'react-select/dist/react-select.css';
import data from '../../data/datasett/nutricalc-data.json';
import { PageTitle } from './layout';
import { getValueFromLiteralOrEvent } from '../helper.general';
import type { ReactSelectOptions } from '../types/libs';

/**
 * The stored datatype for a singe line in the nutricalc-day calculator
 */
export type NutriCalcDayLineState = {|
    custom: boolean,
    food: string,
    kcal: number,
    items: number,
|};

/**
 * The signature for the callback called when a line is updated
 */
type lineUpdateCallback = (number, string, string) => void;

/**
 * This represents a single entry of food
 *
 * It lets a user modify it at will.
 *
 * The props are:
 * entry - the line number, used for keying
 * items - how many of this food item the user has chosen
 * kcal - how many kcal per item
 * food - string, the name of the food
 * onLineUpdate - the callback to call when something changes
 * onLineRemove - the callback to call when the whole line should be removed
 * custom - a boolean, true if this is a custom entry by the user
 */
class FoodLine extends React.PureComponent<{|entry: number, items: number, kcal: number, food: string, onLineUpdate: lineUpdateCallback, onLineRemove: (number) => mixed, autoFocus?: boolean, custom?: boolean|}>
{
    constructor(props) // eslint-disable-line require-jsdoc

    {
        super(props);
    }

    /**
     * Modifies a single value.
     * This is bound to this instance of `FoodLine` in the `constructor`.
     */
    changeValue (key,ev: SyntheticEvent<HTMLInputElement>)
    {
        this.props.onLineUpdate(this.props.entry,key,getValueFromLiteralOrEvent(ev));
    }

    /**
     * Removes this line.
     * This is bound to this instance of `FoodLine` in the `constructor`.
     */
    removeLine()
    {
        this.props.onLineRemove(this.props.entry);
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        let content: React.Node;
        let removeIcon: string | React.Node = '';
        // Disable certain aspects if this is a null entry
        let disabled: boolean = false;
        if(this.props.kcal == 0 && this.props.food == "" && !this.props.custom)
        {
            disabled = true;
        }
        // If we are NOT disabled, we should render a "remove" icon
        if (!disabled)
        {
            removeIcon = <div className="float-right d-none d-md-block"><XCircle onClick={() => { this.removeLine(); }} className="remove-icon" height="16" width="16" /></div>;
        }
        // If this is a "custom" entry we render using the custom renderer, otherwise we render using select
        if(this.props.custom)
        {
            content = this.renderCustom();
        }
        else
        {
            content = this.renderSelect();
        }
        let total: string | React.Node = "";
        if(this.props.items > 0 && this.props.kcal > 0)
        {
            total = numbers.format(this.props.items * this.props.kcal);
            total = <span> = {total} <span className="d-none d-md-inline">kcal</span></span>;
        }
        return <Row>
            <Col xs="2" className="nutriCalcDayItems">
                <input disabled={disabled} type="number" step="0.5" min="1" max="20" className="form-control" value={this.props.items} onChange={(e) => { this.changeValue("items",e); }} />
            </Col>
            <Col xs="7" sm="6" className="nutriCalcDayName">
                {content}
            </Col>
            <Col xs="3" sm="2" className="nutriCalcDayKcalBox">
                <input type="number" className="form-control" value={this.props.kcal} onChange={(e) => { this.changeValue("kcal",e); }} />
            </Col>
            <Col sm="2" className="align-self-center text-muted d-none d-sm-block">
                <small className="nutriCalcDayLineTotal">{total}</small>
                {removeIcon}
            </Col>
        </Row>;
    }

    /**
     * Builds a datastructore for react-select and returns a `<Select />`.
     */
    renderSelect (): React.Node
    {
        const options = [];
        let value: ReactSelectOptions;
        for(const entry in data.basicGroups)
        {
            const content = data.basicGroups[entry];
            const optEntry: ReactSelectOptions = { value: entry, label: content.name };
            if(entry === this.props.food)
            {
                value = optEntry;
            }
            options.push(optEntry);
        }
        return <Select
                    options={options}
					isClearable={false}
					value={value}
                    key={this.props.entry}
                    noOptionsMessage={ () => "Ingen matvarer funnet" }
                    placeholder="Vel…"
					isSearchable={true}
                    autoFocus={this.props.autoFocus}
                    onChange={
                        (e) =>
                        {
                            if(e !== null && e !== undefined && e.value && typeof(e.value) === "string")
                            {
                                this.changeValue("food",e.value);
                            }
                        }
                    }
                    classNamePrefix="react-select"
				/>;
    }

    /**
     * Renders a custom input field. This replaces the `<Select />` if the user
     * has chosen "custom" in the select box.
     */
    renderCustom (): React.Node
    {
        return <input type="text" autoFocus key={this.props.entry} className="form-control" value={this.props.food} onChange={(e) => { this.changeValue("food",e); }} />;
    }
}

/**
 * Renders a list of all foods in the DB
 */
class FoodList extends React.PureComponent<{|isOpen: boolean, toggleList: () => void|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const entries = [];
        let entryNo: number = 0;
        let label  : string = 'Sjå matvareliste';
        if(this.props.isOpen)
        {
            label = 'Gøym matvareliste';
        }
        for(const name in data.basicGroups)
        {
            if(name == data.customLabel)
            {
                continue;
            }
            const content = data.basicGroups[name];
            entryNo++;
            entries.push(
                <tr key={entryNo}>
                    <td>
                        {content.name}
                    </td>
                    <td>
                        {numbers.format(content.kcal)}
                    </td>
                </tr>
            );
        }
        // Note: using min-w-100 here to set the minimum width so that it expands on phones.
        // On desktop it will still use >100%, but that's still within the limits of its column
        // (affects margins)
        return <Row className="mt-3 min-w-100">
                <Button id="toggleFoodListButton" className="btn-block" size="sm" color="light" onClick={this.props.toggleList}>{label}</Button>
                <Collapse isOpen={this.props.isOpen} className="w-100">
                    <Table className="font-size-70-sm" size="sm">
                        <thead>
                            <tr>
                                <th>Matvare</th>
                                <th>kcal</th>
                            </tr>
                        </thead>
                        <tbody>
                            {entries}
                        </tbody>
                    </Table>
                </Collapse>
            </Row>;
    }
}

const FoodListContainer = connect(
    (state) =>
    {
        return {
            isOpen: state.nutriCalc.day.isFoodListOpen
        };
    },
    (dispatch) =>
    {
        return {
            toggleList: () =>
            {
                dispatch(toggleFoodList());
            }
        };
    })(FoodList);

/**
 * Renders the "meta" input, ie. where you can select the minimum kcal per day
 */
class FoodMeta extends React.PureComponent<{|minimum: number | "", onMinimumChange: (SyntheticEvent<HTMLInputElement>) => mixed|},{| isOpen: boolean |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        return <div>
                <Row>
                    <Col>
                        Minimum kcal/døgn:
                    </Col>
                    <Col>
                        <input type="number" id="minimumKcalDay" className="form-control" value={this.props.minimum} onChange={this.props.onMinimumChange} />
                    </Col>
                </Row>
                <FoodListContainer />
               </div>;
    }
}

type NutriCalcDayOwnProps = {|
|};
type NutriCalcDayReduxStateProps = {|
    entries: Array<NutriCalcDayLineState>,
    minimum: number | "",
|};
type NutriCalcDayReduxDispatchProps = {|
    onLineUpdate: (number, string, string) => void,
    onLineRemove: (number) => void,
    onMinimumChange: (SyntheticEvent<HTMLInputElement>) => void,
    onResetForm: () => void
|};
type NutriCalcDayProps = {|
    ...NutriCalcDayOwnProps,
    ...NutriCalcDayReduxStateProps,
    ...NutriCalcDayReduxDispatchProps
|};

/**
 * Root of the calculator.
 *
 * This maintains the state and renders everything else
 */
class NutriCalcDay extends React.PureComponent<NutriCalcDayProps>
{

    /**
     * Calculates the total sum of all lines
     */
    calcTotal (): number
    {
        let no: number = 0;
        for(const entry of this.props.entries)
        {
            // $FlowIssue[incompatible-call] FIXME: We should already know that we have floats at this point. But we don't.
            const total = numbers.parseFloat(entry.kcal)*numbers.parseFloat(entry.items);
            no += total;
        }
        return no;
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const renderEntries        = [];
        let stateEntry   : number  = -1;
        let lastWasCustom: boolean = false;
        // Generate all of the foodlines
        this.props.entries.forEach( (stateContent) =>
        {
            stateEntry++;
            renderEntries.push(<FoodLine items={stateContent.items} key={stateEntry} entry={stateEntry} food={stateContent.food} kcal={stateContent.kcal} custom={stateContent.custom} onLineRemove={this.props.onLineRemove} onLineUpdate={this.props.onLineUpdate} />);
            lastWasCustom = stateContent.custom;
        });
        stateEntry++;
        // Generate the "new item" line
        renderEntries.push(<FoodLine items={1} autoFocus={!lastWasCustom} key={stateEntry} entry={stateEntry} food="" kcal={0} onLineRemove={  this.props.onLineRemove} onLineUpdate={this.props.onLineUpdate} />);

        const total = this.calcTotal();

        // We accept an empty string as the minimum, so we pretend it's 0 if we
        // get an empty string
        const minimum = this.props.minimum === "" ? 0 : this.props.minimum;

        // Calculates how much more or less the person has eaten than the requirement
        const diff              = total - minimum;
        let diffColor  : string = 'text-secondary';
        let diffPrefix : string = '';
        if(diff > 0)
        {
            diffColor = 'text-success';
            diffPrefix = '+';
        }
        else if(diff < 0)
        {
            if(diff == (0-minimum))
            {
                diffColor = 'text-secondary';
            }
            else
            {
                diffColor = 'text-danger';
            }
        }

        return <div>
            <PageTitle title="Ernæringskalkulator - inntak" />
            <Row>
                <Col xs="8">
                    <h3 className="d-none d-md-block">Rekn ut dagsinntak i kcal.</h3>
                    <h3 className="d-md-none">kcal/dag</h3>
                </Col>
                <Col xs="4" className="align-self-center text-muted d-sm-none">
                    <Button className="reset-button float-right" size="sm" color="light" onClick={this.props.onResetForm}>Nullstill</Button>
                </Col>
            </Row>
            <Row>
                <Col xs="12" lg="9">
                    <Row>
                        <Col xs="2">
                            <div className="d-none d-md-block">
                                Mengd:
                            </div>
                        </Col>
                        <Col className="text-right" xs="7" sm="6">
                            Matvare:
                        </Col>
                        <Col xs="3" sm="2">
                            <div className="d-none d-md-block">
                                kcal pr. stk.:
                            </div>
                            <div className="d-md-none">
                                kcal/stk
                            </div>
                        </Col>
                        <Col sm="2" className="align-self-center text-muted d-none d-sm-block">
                            <Button className="reset-button" size="sm" color="light" onClick={this.props.onResetForm}>Nullstill</Button>
                        </Col>
                    </Row>
                    <div id="nutriCalcDayEntries">
                        {renderEntries}
                    </div>
                    <Row>
                        <Col xs="9" sm="10" className="text-right">
                            <b>Totalt:</b>
                        </Col>
                        <Col id="nutriCalcDayTotal">
                            {numbers.format(total)} kcal
                        </Col>
                    </Row>
                    <Row>
                        <Col className="text-right" xs="9" sm="10">
                            <b>Differanse:</b>
                        </Col>
                        <Col>
                            <span className={diffColor} id="nutriCalcDayDiff">{diffPrefix}{numbers.format(diff)}</span> <span className="d-none d-md-inline">kcal</span>
                        </Col>
                    </Row>
                </Col>
                <Col xs="12" lg="3">
                    <FoodMeta onMinimumChange={this.props.onMinimumChange} minimum={this.props.minimum} />
                </Col>
            </Row>
        </div>;
    }
}

const NutriCalcDayContainer: React.AbstractComponent<NutriCalcDayOwnProps> = connect<NutriCalcDayProps,NutriCalcDayOwnProps,NutriCalcDayReduxStateProps,NutriCalcDayReduxDispatchProps,_,_>(
    (state): NutriCalcDayReduxStateProps =>
    {
        return {
            entries: state.nutriCalc.day.entries,
            minimum: state.nutriCalc.day.minimum
        };
    },
    (dispatch): NutriCalcDayReduxDispatchProps =>
    {
        return {
            onLineUpdate: (entryNo: number, key: string, val: string) =>
            {
                if(key === "kcal")
                {
                    val = numbers.floatishInRangeFromThing(val,0,5000)+"";
                }
                else if(key === "items")
                {
                    val = numbers.floatishInRangeFromThing(val,1,20)+"";
                }
                dispatch(setEntryValue(entryNo,key,val));
            },
            onLineRemove: (entryNo: number) =>
            {
                dispatch(removeEntry(entryNo));
            },
            onMinimumChange (ev: SyntheticEvent<HTMLInputElement>)
            {
                const value = numbers.floatishInRangeFromThing(ev,0,18000);
                dispatch(setMinimum(value));
            },
            onResetForm: () =>
            {
                dispatch(reset());
            },
        };
    }
)(NutriCalcDay);

export default NutriCalcDayContainer;
