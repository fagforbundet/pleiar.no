/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019-2020
 * Copyright (C) Eskild Hustvedt 2017-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from 'react';
import { MainContainer, PageTitle } from './layout';
import { Row, Col } from 'reactstrap';
import { Markdown } from './markdown';
import { NotFound } from './shared';
import { withRouter } from 'react-router';
import { Link, Redirect } from 'react-router-dom';
// $FlowIssue[cannot-resolve-module] react-select+flow temporarily broken, tracked as https://github.com/JedWatson/react-select/issues/3612
import Select from 'react-select';
import slug from '../helper.slugs.js';
import { HandbookEntry, HandbookIndex } from '../data.handbook';
import type { QuickRefEntry } from '../data.quickref';
import { RequiresAuth } from './auth';
import SimpleAccordion from './shared/simple-accordion';
import type { ReactSelectOnChangeValue }  from '../types/libs.js';

/**
 * A rendering component that can render a `HandbookEntry` with a
 * searchSnippet, which is an array of `searchSnippetComponent`s that
 * `helper.lunrSnippet` has built.
 */
class HandbookLikeSearchResult extends React.PureComponent<{|entry: HandbookEntry | QuickRefEntry, prefix: string, readmore: string |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { entry, prefix, readmore } = this.props;
        const result = [];
        for(let componentI: number = 0; componentI < entry.searchSnippet.length; componentI++)
        {
            const component = entry.searchSnippet[componentI];
            if(component.type === "normal")
            {
                result.push(<span key={componentI}>{component.str}</span>);
            }
            else if(component.type === "highlight")
            {
                result.push(<b key={componentI}>{component.str}</b>);
            }
            else if(component.type === "delimiter")
            {
                result.push(<i className="handbookDelimiter" key={componentI}> (…) </i>);
            }
            else
            {
                throw('Unknown string type: '+component.type);
            }
        }
        return <Row className="handbookSearchEntry">
            <Col>
                <Link to={entry.url} className="linkWrapper">
                    <div className="handbookSearchTitle likeLink" target="_blank">{prefix}: {entry.title}</div>
                    {result}. <span className="likeLink">{readmore}.</span>
                </Link>
            </Col>
        </Row>;
    }
}

/**
 * A rendering component that can render a `HandbookEntry` with a
 * searchSnippet, which is an array of `searchSnippetComponent`s that
 * `helper.lunrSnippet` has built.
 */
class HandbookSearchResult extends React.PureComponent<{|entry: HandbookEntry |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { entry } = this.props;
        return <HandbookLikeSearchResult entry={entry} prefix="Håndbok" readmore="Les mer i håndboken" />;
    }
}

/**
 * The Pleiar.no handbook index/list renderer
 */
class HandbookIndexRenderer extends React.PureComponent<{| index?: HandbookIndex, title?: string, skipToplevelArticles?: boolean, alwaysOpen?: boolean, plainList?: boolean |}>
{
    /**
     * Recursively goes over the entire Handbook* tree and renders
     * them
     */
    recursiveRender (entry: HandbookEntry | HandbookIndex, suppressTitle: ?boolean): Array<React.Node>
    {
        const rendered = [];
        if(entry instanceof HandbookEntry)
        {
            rendered.push(<li key={entry.path.join('/')} className="topSection"><Link to={entry.url}>{entry.title}</Link></li>);
        }
        else
        {
            let children: Array<React.Node> = [];
            for(const child of entry.tree)
            {
                children = children.concat( this.recursiveRender( child ) );
            }
            if(!suppressTitle)
            {
                rendered.push(<li key={entry.path.join('/')} className="topSection"><Link to={entry.url}>{entry.title}</Link></li>);
            }
            rendered.push(<ul key={'sub-'+entry.path.join('/')}>{children}</ul>);
        }
        return rendered;
    }

    /**
     * Renders the index using a SimpleAccordion
     */
    renderWithAccordion (index: HandbookIndex, skipToplevelArticles: ?boolean, alwaysOpen: ?boolean): React.Node
    {
        const result = [];
        const rootChildren = index.tree;
        for(const child of rootChildren)
        {
            if(skipToplevelArticles === true && child instanceof HandbookEntry)
            {
                continue;
            }
            result.push({
                header: child.title,
                render: this.recursiveRender(child,true)
            });
        }
        return <SimpleAccordion key="idx-accordion" elements={result} alwaysOpen={ alwaysOpen === true } />;
    }

    /**
     * Renders the index as a basic <ul> list
     */
    renderAsUlList (index: HandbookIndex): React.Node
    {
        const result = [];
        const rootChildren = index.tree;
        for(const child of rootChildren)
        {
            result.push(this.recursiveRender(child,false));
        }
        return <ul key="hiUlList">
            {result}
        </ul>;
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        let index: ?HandbookIndex            = this.props.index;
        const title: ?string                 = this.props.title;
        const skipToplevelArticles: ?boolean = this.props.skipToplevelArticles;
        const alwaysOpen: ?boolean           = this.props.alwaysOpen;
        const render                         = [];
        const plainList                      = this.props.plainList === true;
        if(title === null || title === undefined)
        {
            render.push(<h1 key="header">Håndbok</h1>);
        }
        else if(title.length > 0)
        {
            render.push(<h1 key="header">Håndbok: {title}</h1>);
        }
        if (index === undefined || index === null)
        {
            index = new HandbookIndex();
        }
        if(plainList)
        {
            render.push(this.renderAsUlList(index));
        }
        else
        {
            render.push(this.renderWithAccordion(index, skipToplevelArticles, alwaysOpen));
        }
        return <div className="handbookIndex">
                {render}
            </div>;
    }
}

/**
 * Props for `HandbookTocRender`
 */
type HandbookTocRenderProps = {| toc: Array<string> |};

type HandbookNavLinkRenderMode = 'nav' | 'back';

type HandbookNavLinkType = "next" | "previous" | "toplevel";

/**
 * Renders the `<Select />` box that can be used to jump between headers
 * on a handbook page
 */
class HandbookTocRender extends React.Component<HandbookTocRenderProps>
{
    constructor (props: HandbookTocRenderProps) // eslint-disable-line require-jsdoc
    {
        super(props);
    }

    /**
     * Handles scrolling to a header.
     * This is bound to this instance of `HandbookTocRender` in the `constructor`.
     */
    onChange (target: ReactSelectOnChangeValue)
    {
        if(Array.isArray(target))
        {
            return;
        }
        if(target !== undefined && target !== null)
        {
            const scrollTo = document.getElementById(target.value);
            if(scrollTo)
            {
                window.scrollTo(0, scrollTo.offsetTop);
            }
        }
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        slug.reset();

        const options = [];
        for(const entry of this.props.toc)
        {
            options.push({
                label: entry,
                value: slug.get(entry)
            });
        }
        return <Select
                    options={options}
					isClearable={false}
                    value={null}
                    noResultsText="Ingen resultater funnet"
                    placeholder="Hopp til…"
					isSearchable={true}
                    autoFocus={false}
                    onChange={ (v: ReactSelectOnChangeValue) => this.onChange(v) }
                    classNamePrefix="react-select"
                    className="reactSelect"
				/>;
    }
}

/**
 * A navigation link in the handbook that links to the previous or next section
 * (page).  Also does some magic to render shorter link text (with only "next"
 * or "previous") on mobile, where space is limited.
 *
 * @param url The Url to render
 * @param title The title to render
 * @param type The type of rendering (next or previous)
 * @param [genericNext] (optional) Override the generic text for Next
 * @param [genericPrevious] (optional) Override the generic text for Previous
 */
class HandbookNavLinkRaw extends React.PureComponent<{| url: string, title: string, type: HandbookNavLinkType, genericNext?: string, genericPrevious?: string |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { url, title, type, genericNext, genericPrevious } = this.props;
        let prefix      : ?string = null;
        let postfix     : ?string = null;
        let genericText : ?string = null;
        if(type === "next")
        {
            postfix = "\u00A0»";
            genericText = genericNext ? genericNext : "Neste";
        }
        else if(type == "previous")
        {
            prefix = "«\u00A0";
            genericText = genericPrevious ? genericPrevious : "Forrige";
        }
        else if(type == "toplevel")
        {
            genericText = "↑";
        }
        return <Link to={url}>{prefix}<span className="d-md-none">{genericText}</span><span className="d-none d-md-inline">{title}</span>{postfix}</Link>;
    }
}

/**
 * A variant of @{link HandbookNavLinkRaw} that takes a @{link HandbookEntry}
 * instead of raw `url` and `title` parameters.
 *
 * @param {HandbookEntry} entry The HandbookEntry being rendered
 * @param type The type of rendering (next or previous)
 */
class HandbookNavLink extends React.PureComponent<{| entry: HandbookEntry | HandbookIndex, type: HandbookNavLinkType |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { entry, type } = this.props;
        return <HandbookNavLinkRaw url={entry.url} title={entry.title} type={type} />;
    }
}

/**
 * Renders a set of `HandbookNavLink`s. One for previous, one for next.
 * Usually there will be two `HandbookNavLinks` on any page, one at the top,
 * one at the bottom.
 */
class HandbookNavLinks extends React.PureComponent<{|entry: HandbookEntry, navBackLink?: string, navMode?: HandbookNavLinkRenderMode, hideToplevelLink?: boolean |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { entry, navMode, navBackLink, hideToplevelLink } = this.props;
        let previous : React.Node = null;
        let next     : React.Node = null;
        let toplevel : React.Node = null;
        if(navMode && navMode === "back" && navBackLink)
        {
            previous = <HandbookNavLinkRaw type="previous" url={navBackLink} title="Tilbake" genericPrevious="Tilbake" />;
        }
        else
        {
            if(entry.next)
            {
                next = <HandbookNavLink type="next" entry={entry.next} />;
            }
            if(entry.previous)
            {
                previous = <HandbookNavLink type="previous" entry={entry.previous} />;
            }
            if(hideToplevelLink !== true && entry.toplevelParent.displayIndexLink === true)
            {
                toplevel = <HandbookNavLink type="toplevel" entry={entry.toplevelParent} />;
            }
        }
        return <Row className="handbook-nav">
            <Col className="previous">
                {previous}
            </Col>
            <Col className="toplevel">
                {toplevel}
            </Col>
            <Col className="next">
                {next}
            </Col>
        </Row>;
    }
}

/**
 * Renders a single section in the handbook. Will 404 if the chapter or
 * section isn't found. Will redirect to the first section in a chapter if
 * only a chapter is provided.
 *
 * @param {HandbookEntry} entry The HandbookEntry to render
 * @param navMode The nav link mode (nav or back)
 * @param navBackLink The URL to link to in navMode=back
 */
class HandbookRender extends React.PureComponent<{|entry: HandbookEntry, navMode?: HandbookNavLinkRenderMode, navBackLink?: string |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { entry, navMode, navBackLink } = this.props;
        let toc : ?React.Node;
        let mediumWidth: number = 8;
        if (entry.hideTOC)
        {
            mediumWidth = 12;
        }
        else
        {
            toc = <Col md="4" className="order-md-12 conditionallySticky">
                <HandbookTocRender toc={entry.toc} />
            </Col>;
        }
        const article = <Row>
                {toc}
                <Col md={mediumWidth} xl="8" className="order-md-1">
                    <HandbookNavLinks entry={entry} navMode={navMode} navBackLink={navBackLink} />
                    <Markdown content={entry.content} permitSpecialNodes={true} />
                    <HandbookNavLinks entry={entry} navMode={navMode} navBackLink={navBackLink} hideToplevelLink={true} />
                </Col>
            </Row>;
        if(entry.public)
        {
            return article;
        }
        return <RequiresAuth type="article" title={entry.title}>
            {article}
        </RequiresAuth>;
    }
}

/**
 * The handbook app component. Renders pages or the index.
 */
class Handbook extends React.Component<{|match: {| params: {| handbookPath?: string|}|}|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const match = this.props.match;
        let subComponent: React.Node;
        let small: boolean = false;
        let title: string = "Handbok";
        if(match.params.handbookPath)
        {
            const entry = new HandbookEntry( match.params.handbookPath );
            if(entry.exists)
            {
                subComponent = <HandbookRender entry={entry} />;
                title += ' - '+entry.toplevelParent.title;
            }
            else
            {
                const index = new HandbookIndex( match.params.handbookPath );
                if(index.exists)
                {
                    subComponent = <Redirect to={index.firstChild.url} />;
                }
                else
                {
                    subComponent = <NotFound root={false} />;
                }
            }
        }
        else
        {
            subComponent = <HandbookIndexRenderer />;
            small = true;
        }
        return  <MainContainer app="handbook" small={small}>
            <Row>
                <Col>
                    <PageTitle title={title} />
                    {subComponent}
                </Col>
            </Row>
        </MainContainer>;
    }
}

// Public exports
export default (withRouter(Handbook): React.AbstractComponent<{| |}>);
export { HandbookSearchResult, HandbookLikeSearchResult, HandbookRender, HandbookIndexRenderer };
// Testing exports
export { Handbook as HandbookUnconnected, HandbookNavLinks, HandbookNavLink, HandbookTocRender,  HandbookNavLinkRaw };
