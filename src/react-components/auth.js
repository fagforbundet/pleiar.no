/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

// Let flow know about our AUTH_URL and AUTH_APPID globals (from webpack/data config)
declare var AUTH_URL:string;
declare var AUTH_APPID:string;

import * as React from 'react';
import { Button, Row, Col } from 'reactstrap';
import { Redirect } from 'react-router-dom';
import { StructuredData } from './shared/structured-data';
import { ExternalLink } from './shared/links';
import { MainContainer, PageTitle } from './layout';
import auth from '../auth';
import type { AuthError } from '../auth';

/**
 * Displays an "authentication failed" message
 */
class AuthFailed extends React.PureComponent<{| token: string |}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        const { token } = this.props;
        const error: AuthError | null = auth.getError();
        const errorCode: AuthError | "(ukjent)" = (error === null ? "(ukjent)" : error);
        return <MainContainer app="auth">
            <PageTitle />
            <Row>
                <Col>
                    <h3>Innloggingsfeil :(</h3>
                    Vi klarte ikke å logge deg inn. Du kan <ExternalLink sameWindow={true} href={getAuthURL()}>prøve på nytt</ExternalLink>.<br />
                    Hvis du fremdeles får feilmeldingen, send oss en e-post på <ExternalLink sameWindow={true} href="mailto:kontakt@pleiar.no">kontakt@pleiar.no</ExternalLink>. Ta gjerne med feilkoden som står nedenfor så skal vi undersøke og fikse problemet!<br /><br />
                </Col>
            </Row>
            <Row>
                <Col>
                    <code>
                        Feilkode: {errorCode} <span className="err-token">({token})</span>
                    </code>
                </Col>
            </Row>
        </MainContainer>;
    }
}

/**
 * Generates an authentication URL that can be used to log in
 */
function getAuthURL (): string
{
    return AUTH_URL+'?return_to='+location.origin+'/auth&app_id='+AUTH_APPID;
}

/**
 * Handler for /auth
 *
 * This handles passing JWT's to the auth object, and preforming redirects as
 * needed.
 */
class Authenticator extends React.PureComponent<{||}>
{
    /**
     * Redirects, using react-router-dom's `Redirect` to
     * localStorage.pleiarAuthRedirect or /
     */
    redirect (): React.Node
    {
        const redirect = window.localStorage.pleiarAuthRedirect;
        delete window.localStorage.pleiarAuthRedirect;
        // Verify that the redirect is valid(ish)
        if (/^\//.test(redirect) && /^(\/|\w|\d)*$/.test(redirect))
        {
            return <Redirect to={redirect} />;
        }
        else
        {
            return <Redirect to="/" />;
        }
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        // Hack to permit logging out
        if(/^\/auth\/logout/.test(location.pathname))
        {
            auth.invalidate();
            return this.redirect();
        }

        if(auth.isAuthenticated())
        {
            return this.redirect();
        }
        const token = location.href.replace(/.+\/auth\/?\?jwt=/,'');
        // eslint-disable-next-line security/detect-possible-timing-attacks
        if(token !== location.href)
        {
            auth.authenticate(token);
        }
        if(auth.isAuthenticated())
        {
            return this.redirect();
        }
        return <AuthFailed token={token} />;
    }
}

/**
 * Renders an authentication overlay, which requests that the user log in.
 */
class AuthOverlay extends React.PureComponent<{||}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        window.localStorage.pleiarAuthRedirect = location.pathname;
        const login_url = getAuthURL();
        return <div className="authOverlay">
            <ExternalLink basicStyle={true} href="https://www.fagforbundet.no/"><img className="img-fluid branding" src="/brand.png" alt="Fagforbundet" /></ExternalLink>
            <div className="loginBox">
                Pleiar.no er en tjeneste levert av Fagforbundet til sine medlemmer.<br />
                Er du medlem bruker du bare mobiltelefonen din eller BankID
                for å <ExternalLink sameWindow={true} href={login_url}>logge inn</ExternalLink>.
                Ikke medlem? <ExternalLink sameWindow={true}
                    href="https://www.fagforbundet.no/medlemskap-og-fordeler/">Meld
                    deg inn</ExternalLink>!<br /><br />
                <ExternalLink sameWindow={true} href={login_url}><Button color="primary">Logg inn</Button></ExternalLink>{' '}
                <ExternalLink sameWindow={true} href="https://www.fagforbundet.no/medlemskap-og-fordeler/"><Button color="secondary">Bli medlem</Button></ExternalLink>
            </div>
        </div>;
    }
}

/**
 * Renders a block of text usable as an "inline" paywall
 */
class InlineAuthBlock extends React.Component<{|attemptedAccess: string|}>
{
    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        window.localStorage.pleiarAuthRedirect = location.pathname;
        const login_url = AUTH_URL+'?return_to='+location.origin+'/auth&app_id='+AUTH_APPID;
        return <div className="authBlock">
            Tilgang til {this.props.attemptedAccess} er avgrenset til medlemmer
            av Fagforbundet. Hvis du er medlem kan du <ExternalLink sameWindow={true} href={login_url}>logge
            inn</ExternalLink>. Ellers er du hjertelig velkommen til å <ExternalLink
            href="https://www.fagforbundet.no/medlemskap-og-fordeler/">bli
            medlem</ExternalLink>
        </div>;
    }
}

/**
 * Props for RequiresAuth
 */
type RequiresAuthProps = {| children: React.Node |}
| {|
    children: React.Node,
    type: "article",
    title: string,
    image?: string,
    author?: string
|};

/**
 * This is a react wrapper around any component that requires the user to be
 * authenticated. It checks if the user has been authenticated. If they have,
 * it renders all the children directly. Otherwise it wraps the children in a
 * div, displays the auth overlay, and renders the children behind the overlay.
 */
class RequiresAuth extends React.Component<RequiresAuthProps>
{
    wrapperDiv: {| current: ?HTMLDivElement |};
    childrenDiv: {| current: ?HTMLDivElement |};
    observer: MutationObserver;

    constructor (props: RequiresAuthProps) // eslint-disable-line require-jsdoc
    {
        super(props);
        this.wrapperDiv = React.createRef();
        this.childrenDiv = React.createRef();
    }

    /**
    * Connect a MutationObserver to handle className changes when we mount
    */
    componentDidMount()
    {
        if(window.MutationObserver === undefined)
        {
            return;
        }
        const observer = new MutationObserver( (list) =>
        {
            for(const mutation of list)
            {
                if (mutation.type === 'attributes' && mutation.attributeName === "class")
                {
                    if(mutation.target === this.wrapperDiv.current && this.wrapperDiv.current != null && this.wrapperDiv.current.className !== "authn")
                    {
                        this.wrapperDiv.current.className = "authn";
                    }
                    else if(mutation.target === this.childrenDiv.current && this.childrenDiv.current != null && this.childrenDiv.current.className !== "requiresAuthentication auth-content")
                    {
                        this.childrenDiv.current.className = "requiresAuthentication auth-content";
                    }
                }
            }
        });
        this.observer = observer;
        if(this.wrapperDiv.current !== null && this.wrapperDiv.current !== undefined)
        {
            observer.observe(this.wrapperDiv.current, { attributes: true });
        }
        if(this.childrenDiv.current !== null && this.childrenDiv.current !== undefined)
        {
            observer.observe(this.childrenDiv.current, { attributes: true });
        }
    }

    /**
    * Disconnect our MutationObserver when we unmount
    */
    componentWillUnmount ()
    {
        if(this.observer !== undefined)
        {
            this.observer.disconnect();
        }
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node
    {
        let structured: React.Node;
        if(this.props.type && this.props.type === "article")
        {
            structured = <StructuredData key="StructuredData" walled={true} type="article" author={this.props.author} image={this.props.image} title={this.props.title} />;
        }
        else
        {
            structured = <StructuredData key="StructuredData" walled={true} type="auto" />;
        }
        if(auth.isAuthenticated())
        {
            return [
                <div key="auth-entry" className="auth-content">
                    {this.props.children}
                </div>,
                structured
            ];
        }
        else
        {
            return <div className="authn" ref={this.wrapperDiv}>
                <AuthOverlay />
                <div className="requiresAuthentication auth-content" ref={this.childrenDiv}>
                    {this.props.children}
                </div>
                {structured}
            </div>;
        }
    }
}

// Public exports
export { RequiresAuth, Authenticator, InlineAuthBlock, getAuthURL };
// Testing exports
export { AuthOverlay };
