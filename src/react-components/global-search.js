/**
 * @prettier
 */
/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow
import * as React from "react";
import { connect } from "react-redux";
import { MainContainer, PageTitle } from "./layout";
import { Row, Col, Input } from "reactstrap";
import { setGlobalSearchString } from "../actions/global-search";
import PleiarSearcher from "../searcher";
import { DictionaryEntry } from "./dictionary";
import { ExternalResourcesEntry } from "./external-resources.js";
import { LabValueEntryCleartext } from "./labvalues";
import { HandbookSearchResult } from "./handbook";
import { QuickRefSearchResult } from "./quickref";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import RoutingAssistant, { RoutingAssistantSyncRoutes } from "../routing";
import { getValueFromLiteralOrEvent } from "../helper.general";
import { HandbookEntry } from "../data.handbook";
import type { QuickRefEntry } from "../data.quickref";
import { ConditionalRender } from "./shared";
import { ToolsSearchRenderer } from "./tools";
import { ExternalLink } from "./shared/links";
import { StructuredData } from "./shared/structured-data";
import { NutritionSearchRenderer } from "./nutricalc";
import { setMedSynonymsSearchString } from "../actions/med-synonyms";
import type {
    ReduxEventOrStringCallback,
    ReduxEventOrString,
} from "../types/callbacks";
import type {
    DictionaryEntry as DictionaryEntryType,
    ExternalResourcesEntry as ExternalResourcesEntryType,
    LabValueEntry as LabValueEntryType,
    ExternallyIndexedItem,
    SearchMetadataEntry,
} from "../types/data";
import type { lunrResult, lunrMetadata } from "../types/libs";
import type { onSyncSignature } from "../routing";

import type {
    RouterHistory,
    Match as RouterMatch,
    Location,
} from "react-router-dom";

type TopSearchReduxStateProps = {|
    search: string,
|};
type TopSearchReduxDispatchProps = {|
    onSearchUpdate: ReduxEventOrStringCallback,
|};
type TopSearchPropsFromRouter = {|
    history: RouterHistory,
|};
/**
 * Props for `TopSearch`
 */
type TopSearchProps = {|
    ...TopSearchReduxStateProps,
    ...TopSearchReduxDispatchProps,
    ...TopSearchPropsFromRouter,
|};

/**
 * The datatype that `WhateverRenderer` accepts as its result prop
 */
export type WhateverResult =
    | string
    | {| dictionary: DictionaryEntryType |}
    | {| externalResources: ExternalResourcesEntryType |}
    | {| labValues: LabValueEntryType |}
    | {| quickref: QuickRefEntry |}
    | {| handbook: HandbookEntry |};

/**
 * The search bar on the top of all pages
 */
class TopSearch extends React.PureComponent<TopSearchProps> {
    changeId: number;

    // eslint-disable-next-line require-jsdoc
    constructor(props: TopSearchProps) {
        super(props);
    }

    /**
     * Callback, called when either the user types or clicks on a link that
     * updates the search value. This gets bound to this instance of
     * `TopSearch` in the `constructor`. It also handles changing the URL to
     * /search through the `RoutingAssistant`.
     */
    onChanged(ev: SyntheticEvent<HTMLInputElement>) {
        if (window.location.pathname.indexOf("/sok") !== 0) {
            RoutingAssistant.push("/sok");
        } else {
            const value = getValueFromLiteralOrEvent(ev);
            if (value === "") {
                RoutingAssistant.generateReplaceNOW(
                    "/",
                    getValueFromLiteralOrEvent(ev),
                    "auto",
                );
            } else {
                RoutingAssistant.generateReplace(
                    "/sok",
                    getValueFromLiteralOrEvent(ev),
                    "auto",
                );
            }
        }
        this.props.onSearchUpdate(ev);
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        return (
            <div className="topSearch">
                <Input
                    placeholder="Søk"
                    type="search"
                    value={this.props.search}
                    onChange={(e) => this.onChanged(e)}
                    name="search"
                    id="globalSearchField"
                />
            </div>
        );
    }
}

/**
 * Component that renders an externally indexed item as a search result
 * for global search. Used by {@link WhateverRenderer}
 */
class SearchMetaEntryRenderer extends React.PureComponent<{|
    entry: SearchMetadataEntry,
|}> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { entry } = this.props;
        let description: string = "Ta e-lærlingskurset «" + entry.title + "»";
        if (
            entry.description !== undefined &&
            entry.description !== null &&
            typeof entry.description === "string"
        ) {
            description = entry.description;
        }
        return (
            <Row className="searchMetaEntry">
                <Col>
                    <Link to={entry.url} className="linkWrapper">
                        <div className="searchTitle likeLink">
                            {entry.title}
                        </div>
                        {description}
                    </Link>
                </Col>
            </Row>
        );
    }
}

/**
 * Component that renders an externally indexed item as a search result
 * for global search. Used by {@link WhateverRenderer}
 */
class ExternalSearchRenderer extends React.PureComponent<{|
    entry: ExternallyIndexedItem,
|}> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { entry } = this.props;
        const itemFooter: Array<React.Node> = [];
        let prefix: string = "";
        if (entry.type === "fagprat") {
            prefix = "[Podkast] Fagprat: ";
        }
        return (
            <Row className="externallyIndexedEntry">
                <Col>
                    <ExternalLink href={entry.url} className="linkWrapper">
                        <div className="searchTitle likeLink">
                            {prefix + entry.title}
                        </div>
                        {entry.text} {itemFooter}
                    </ExternalLink>
                </Col>
            </Row>
        );
    }
}

/**
 * A rendering class that can be provided with "any" datatype that pleiar.no
 * uses, and will then do-the-right-thing™ to get that datatype to render.
 * For instance used by the `TopSearchContainer` to not have to care about
 * what kind of search result it got, it just passes it on to `WhateverRenderer`.
 */
class WhateverRenderer extends React.PureComponent<{|
    result: WhateverResult,
    type?: string,
    metadata?: lunrMetadata | null,
    forceAuth?: boolean,
    whitelistedAuthRequiredEntries?: number,
|}> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        let type: ?string;
        // FIXME: I've been unable to type this in a satisfactory manner
        // eslint-disable-next-line flowtype/require-variable-type
        let result = {};

        if (this.props.type) {
            type = this.props.type;
            result[type] = this.props.result;
        } else if (typeof this.props.result === "string") {
            const ref = this.props.result;
            result = PleiarSearcher.getResultFromRef(ref, this.props.metadata);
            type = ref.split("/")[0];
        }
        if (result.dictionary) {
            return (
                <DictionaryEntry
                    entry={result.dictionary}
                    forceAuth={this.props.forceAuth}
                    whitelistedAuthRequiredEntries={
                        this.props.whitelistedAuthRequiredEntries
                    }
                />
            );
        } else if (result.externalResources) {
            return <ExternalResourcesEntry entry={result.externalResources} />;
        } else if (result.labValues) {
            return <LabValueEntryCleartext entry={result.labValues} />;
        } else if (result.handbook) {
            return <HandbookSearchResult entry={result.handbook} />;
        } else if (result.tool) {
            return <ToolsSearchRenderer entry={result.tool} />;
        } else if (result.nutrition) {
            return <NutritionSearchRenderer entry={result.nutrition} />;
        } else if (result.external) {
            return <ExternalSearchRenderer entry={result.external} />;
        } else if (result.quickref) {
            return <QuickRefSearchResult entry={result.quickref} />;
        } else if (result.quiz) {
            return <SearchMetaEntryRenderer entry={result.quiz} />;
        } else {
            return (
                <span id="whateverRendererError">
                    No idea how to render {type} ({JSON.stringify(result)})
                </span>
            );
        }
    }
}

/**
 * A component that displays a suggestion to retry the search the user tried
 * globally on all datatypes. Uses `WhateverRenderer` to render the results.
 */
class GlobalSearchRedirect extends React.PureComponent<{| search: string |}> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        return (
            <div className="otherSearchSuggestions">
                <Link to={"/sok/" + this.props.search}>
                    Prøv å søke i heile pleiar.no
                </Link>
            </div>
        );
    }
}

type SearchResultRendererOwnProps = {|
    result: Array<lunrResult>,
    search: string,
    type?: string,
    suggestAlternatives?: boolean,
    suggestGlobalSearch?: boolean,
    whitelistedAuthRequiredEntries?: number,
|};
type SearchResultRendererReduxDispatchProps = {|
    onSetMedSearch: typeof setMedSynonymsSearchString,
|};
type SearchResultRendererProps = {|
    ...SearchResultRendererOwnProps,
    ...SearchResultRendererReduxDispatchProps,
|};
/**
 * Displays a set of search results. Can optionally display a suggestion for
 * alternate places to try the search if there were no results.
 */
class SearchResultRendererUnconnected extends React.Component<SearchResultRendererProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const rendered = [];
        if (this.props.result.length > 0) {
            const whitelistedAuthRequiredEntries = this.props
                .whitelistedAuthRequiredEntries
                ? this.props.whitelistedAuthRequiredEntries
                : 0;
            for (
                let elementNO: number = 0;
                elementNO < this.props.result.length;
                elementNO++
            ) {
                const forceAuth: boolean =
                    elementNO + 1 <= whitelistedAuthRequiredEntries;

                let element: string | lunrResult = this.props.result[elementNO];
                let metadata: ?lunrMetadata;
                if (
                    typeof element === "object" &&
                    !(element instanceof HandbookEntry) &&
                    element.ref
                ) {
                    if (element.matchData && element.matchData.metadata) {
                        metadata = element.matchData.metadata;
                    }
                    element = element.ref;
                }
                if (typeof element === "string") {
                    rendered.push(
                        <WhateverRenderer
                            key={element}
                            result={element}
                            metadata={metadata}
                            forceAuth={forceAuth}
                            whitelistedAuthRequiredEntries={
                                this.props.whitelistedAuthRequiredEntries
                            }
                        />,
                    );
                } else if (this.props.type) {
                    rendered.push(
                        <WhateverRenderer
                            key={elementNO + "-" + this.props.type}
                            /* $FlowIssue[incompatible-type] Actual flow bug, see PR 7298 on the flow repo */
                            result={element}
                            type={this.props.type}
                            metadata={metadata}
                            forceAuth={forceAuth}
                            whitelistedAuthRequiredEntries={
                                this.props.whitelistedAuthRequiredEntries
                            }
                        />,
                    );
                } else {
                    throw (
                        "SearchResultRenderer: got non-string element but no type: " +
                        JSON.stringify(element)
                    );
                }
            }
            return <div key="searchResultRenderer">{rendered}</div>;
        } else {
            const { onSetMedSearch, search } = this.props;
            let alternatives: React.Node = null;
            let missingContentString: string = "noe innhold";
            let newContentString: string = "nytt innhold";
            let upperBlock: React.Node;
            let lowerBlock: React.Node;
            if (this.props.type === "dictionary") {
                missingContentString = "noen ord eller uttrykk";
            } else if (this.props.type === "externalResources") {
                missingContentString = "noen nettsider";
                newContentString = "nye nettsider";
            }
            const missingContentContact: React.Node = (
                <span>
                    <br />
                    Er det {missingContentString} vi mangler? Ta kontakt med oss
                    og foreslå {newContentString}! Du kan nå oss på{" "}
                    <ExternalLink
                        sameWindow={true}
                        href="mailto:kontakt@pleiar.no"
                    >
                        e-post
                    </ExternalLink>
                    .
                </span>
            );
            if (this.props.suggestAlternatives) {
                const qString = encodeURIComponent(this.props.search);
                alternatives = (
                    <div className="otherSearchSuggestions">
                        Prøv søket andre steder:
                        <Link
                            to="/verktoy/medikament/synonymer"
                            onClick={() => {
                                onSetMedSearch(search);
                            }}
                        >
                            I byttelisten for legemiddel
                        </Link>
                        <ExternalLink
                            href={
                                "https://sok.helsebiblioteket.no/vivisimo/cgi-bin/query-meta?v%3Aproject=NOKC_SearchTab1&v%3Asources=SearchTab1&query=" +
                                qString
                            }
                        >
                            Helsebiblioteket
                        </ExternalLink>
                        <ExternalLink
                            href={
                                "https://sml.snl.no/.search?utf8=%E2%9C%93&query=" +
                                qString
                            }
                        >
                            Store Medisinske Leksikon (SML)
                        </ExternalLink>
                        <ExternalLink
                            href={
                                "https://www.felleskatalogen.no/medisin/sok?sokord=" +
                                qString
                            }
                        >
                            Felleskatalogen
                        </ExternalLink>
                        <ExternalLink
                            href={
                                "https://www.legemiddelsok.no/sider/default.aspx?searchquery=" +
                                qString +
                                "&f=Han;MtI;Vir;ATC;Var;Mar;Mid;Avr;gen;par;&pane=0"
                            }
                        >
                            Legemiddelsøk
                        </ExternalLink>
                        <ExternalLink
                            href={
                                "http://lvh.no/search?utf8=%E2%9C%93&q=" +
                                qString
                            }
                        >
                            Legevakthåndboken
                        </ExternalLink>
                        <ExternalLink
                            href={
                                "http://oncolex.no/FreeTextSearchResults.aspx?procedureSearchText=" +
                                qString +
                                "&MatchPhrase=false"
                            }
                        >
                            Oncolex
                        </ExternalLink>
                    </div>
                );
                upperBlock = (
                    <span className="d-none d-sm-block">{alternatives}</span>
                );
                lowerBlock = (
                    <span>
                        <span className="d-sm-none">{alternatives}</span>
                        {missingContentContact}
                    </span>
                );
            } else if (this.props.suggestGlobalSearch) {
                alternatives = (
                    <GlobalSearchRedirect search={this.props.search} />
                );
                upperBlock = (
                    <span>
                        {alternatives}
                        <span className="d-none d-sm-block">
                            {missingContentContact}
                        </span>
                    </span>
                );
                lowerBlock = (
                    <span className="d-sm-none">{missingContentContact}</span>
                );
            } else {
                lowerBlock = missingContentContact;
            }
            return (
                <div id="extNoResults">
                    <Row>
                        <Col xs="5" sm="8">
                            <b>Fant ingen treff for «{this.props.search}».</b>
                            {upperBlock}
                        </Col>
                        <Col xs="7" sm="4">
                            <img
                                src="/confused-beaver.svg"
                                width="200"
                                height="137"
                                alt=""
                            />
                        </Col>
                    </Row>
                    <Row>
                        <Col>{lowerBlock}</Col>
                    </Row>
                </div>
            );
        }
    }
}

/**
 * Connected version of {@link SearchResultRendererUnconnected}
 */
const SearchResultRenderer: SearchResultRenderer = connect<
    SearchResultRendererProps,
    SearchResultRendererOwnProps,
    {||},
    SearchResultRendererReduxDispatchProps,
    _,
    _,
>(
    null,
    ({
        onSetMedSearch: setMedSynonymsSearchString,
    }: SearchResultRendererReduxDispatchProps),
)(SearchResultRendererUnconnected);

type SearchCounterOwnProps = {|
    results: Array<lunrResult>,
    filter?: string,
    search: string,
|};

type SearchCounterProps = {|
    history: RouterHistory,
    ...SearchCounterOwnProps,
|};

/**
 * This renders a set of buttons that can be used to apply filters of which
 * search result type to display.  It will also display the total number of
 * results in that category.
 */
class SearchCounterUnconnected extends React.PureComponent<SearchCounterProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { filter, results, history } = this.props;
        const elementMap = {
            handbook: {
                name: "Håndbok",
                urlName: "handbok",
            },
            dictionary: {
                name: "Ordliste",
                redirect: "/ordliste",
            },
            externalResources: {
                name: "Andre sider",
                redirect: "/andresider",
            },
            labValue: {
                name: "Labverdier",
                urlName: "labverdier",
            },
            tool: {
                name: "Verktøy",
                urlName: "verktoy",
            },
            nutrition: {
                name: "Ernæringsinformasjon",
                urlName: "ernaering",
            },
            external: {
                name: "Eksternt",
                urlName: "eksternt",
            },
            quickref: {
                name: "Hurtigoppslag",
                urlName: "oppslag",
            },
            quiz: {
                name: "E-læring",
                urlName: "elaering",
            },
        };
        const elements = [];
        const entryTypes = {};
        for (const result of results) {
            const ref = result.ref.split("/")[0];
            if (entryTypes[ref] === undefined) {
                entryTypes[ref] = 0;
            }
            entryTypes[ref]++;
        }
        for (const type in entryTypes) {
            const element = elementMap[type];
            if (element === undefined) {
                throw "SearchCounter: unknown result: " + type;
            }
            const { name, redirect, urlName } = element;
            const className =
                "pill " +
                (filter === urlName && filter !== undefined
                    ? "pill-active"
                    : "");
            const onClick = () => {
                if (redirect !== undefined) {
                    history.push(redirect + "/" + this.props.search);
                } else {
                    if (filter === urlName) {
                        RoutingAssistant.generatePush(
                            "/sok",
                            this.props.search,
                            "auto",
                        );
                    } else {
                        RoutingAssistant.generatePush(
                            "/sok",
                            this.props.search,
                            "auto",
                            urlName,
                        );
                    }
                }
            };
            elements.push(
                <div key={type} onClick={onClick} className={className}>
                    {name} ({entryTypes[type]})
                </div>,
            );
        }
        return (
            <div
                className={
                    "searchResultTypeList link-pills " +
                    (filter !== undefined ? "filtered" : "")
                }
            >
                {elements}
            </div>
        );
    }
}
const SearchCounter: React.AbstractComponent<SearchCounterOwnProps> =
    withRouter(SearchCounterUnconnected);

/**
 * Props for InfiniteScrollSearchResultRenderer
 */
type InfiniteScrollSearchResultRendererOwnProps = {|
    result: Array<lunrResult>,
    search: string,
    searchPathPrefix: string,
    type?: string,
    // We don't actually use it, but it's used to force a re-render when the
    // location changes.
    location?: Location,
    // How many, if any, entries that actually require auth to view, to render
    // as if the user is authenticated
    whitelistedAuthRequiredEntries?: number,
    suggestAlternatives?: boolean,
    suggestGlobalSearch?: boolean,
|};
type InfiniteScrollSearchResultRendererProps = {|
    match: RouterMatch,
    ...InfiniteScrollSearchResultRendererOwnProps,
|};

/**
 * A variant of SearchResultRenderer that also does automatic infinite
 * scrolling.  This class will do the infinite scrolling, and then wrap a
 * `SearchResultRenderer` in that, so the `SearchResultRenderer` does not
 * need to know anything about the actual infinite scrolling.
 */
class InfiniteScrollSearchResultRendererUnRouted extends React.Component<
    InfiniteScrollSearchResultRendererProps,
    {| renderElements: number |},
> {
    bottomScrollElement: {| current: ?HTMLDivElement |};

    // eslint-disable-next-line require-jsdoc
    constructor(props: InfiniteScrollSearchResultRendererProps) {
        super(props);

        this.bottomScrollElement = React.createRef();
    }

    /**
     * Event handler that gets called whenever the user scrolls. Used to render
     * more elements if the user is getting close to the bottom.
     */
    onScrollEvent() {
        const renderElements = RoutingAssistant.getAutoRenderElements();
        if (
            this.bottomScrollElement.current === undefined ||
            this.bottomScrollElement.current === null
        ) {
            return;
        }
        if (
            renderElements < this.props.result.length &&
            this.bottomScrollElement.current.getBoundingClientRect().bottom <
                1500
        ) {
            RoutingAssistant.generateReplaceNOW(
                this.props.searchPathPrefix,
                this.props.search,
                renderElements + 10,
                this.props.match.params.filter,
            );
        }
    }

    /**
     * Attach scroll event listener
     */
    componentDidMount() {
        window.addEventListener("scroll", () => this.onScrollEvent());
    }

    /**
     * Detach scroll event listener
     */
    componentWillUnmount() {
        window.removeEventListener("scroll", () => this.onScrollEvent());
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const renderElements = RoutingAssistant.getAutoRenderElements();
        const result = this.props.result.slice(0, renderElements);
        return (
            <div ref={this.bottomScrollElement}>
                <SearchResultRenderer
                    result={result}
                    search={this.props.search}
                    type={this.props.type}
                    suggestAlternatives={this.props.suggestAlternatives}
                    suggestGlobalSearch={this.props.suggestGlobalSearch}
                    whitelistedAuthRequiredEntries={
                        this.props.whitelistedAuthRequiredEntries
                    }
                />
            </div>
        );
    }
}
const InfiniteScrollSearchResultRenderer: React.AbstractComponent<InfiniteScrollSearchResultRendererOwnProps> =
    withRouter(InfiniteScrollSearchResultRendererUnRouted);

/**
 * Props for `GlobalSearch`
 */
type GlobalSearchProps = {|
    search: string,
    location?: Location,
    match: RouterMatch,
    onSearchUpdate: ReduxEventOrStringCallback,
    history: RouterHistory,
|};
/**
 * Renders the search result page for the global search
 */
class GlobalSearch extends React.Component<GlobalSearchProps> {
    // eslint-disable-next-line require-jsdoc
    constructor(props: GlobalSearchProps) {
        super(props);
    }

    /**
     * Performs a search and returns the result.
     */
    getResults(): Array<lunrResult> {
        const search = this.props.search;
        if (search === null || search === "" || !/\w/.test(search)) {
            return [];
        } else {
            return PleiarSearcher.safePartialSearch(search);
        }
    }

    /**
     * We clear the search field when we unmount
     */
    componentWillUnmount() {
        this.props.onSearchUpdate("");
    }

    /**
     * Applies a filtering reducer to our search result
     */
    applyReducer(result: Array<lunrResult>): {|
        result: Array<lunrResult>,
        filter?: string,
    |} {
        const filterMap = {
            labverdier: "labValue",
            handbok: "handbook",
            verktoy: "tool",
            ernaering: "nutrition",
            eksternt: "external",
            oppslag: "quickref",
            elaering: "quiz",
        };
        const filter = this.props.match.params.filter;
        if (
            filter === undefined ||
            filter === null ||
            filter === "" ||
            filterMap[filter] === undefined
        ) {
            return { result };
        }
        const onlyInclude = filterMap[filter];
        const reduced = result.reduce((acc, cur) => {
            if (cur.ref.indexOf(onlyInclude + "/") === 0) {
                acc.push(cur);
            }
            return acc;
        }, []);
        return { result: reduced, filter };
    }

    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { search } = this.props;
        const entries = this.getResults();
        const filtered = this.applyReducer(entries);
        const filteredEntries = filtered.result;
        let results: React.Node = null;
        if (search !== null && search !== undefined && search !== "") {
            results = (
                <InfiniteScrollSearchResultRenderer
                    result={filteredEntries}
                    search={this.props.search}
                    suggestAlternatives={true}
                    location={this.props.location}
                    searchPathPrefix="/sok"
                />
            );
        }
        return (
            <Row>
                <Col lg={{ offset: 2, size: 8 }}>
                    <PageTitle title="Søk" />
                    <h2>Søkeresultater</h2>
                    <hr />
                    <SearchCounter
                        search={this.props.search}
                        results={entries}
                        filter={filtered.filter}
                    />
                    {results}
                </Col>
                <StructuredData type="auto" walled={true} />
            </Row>
        );
    }
}

// eslint-disable-next-line flowtype/require-exact-type
type RequiresPleiarSearcherProps = {
    component: React.ComponentType<{||}>,
    app: string,
    onRouteSync?: onSyncSignature,
};

/**
 * This wraps {@link ConditionalRender}. `component` will be rendered once
 * @{link PleiarSearcher} has been initialized. It will wrap a `<MainContainer>`
 * around `<ConditionalRender>`, using the `app` prop supplied. It will also perform
 * `RoutingAssistantSyncRoutes` if the `onRouteSync` prop is supplied.
 */
class RequiresPleiarSearcher extends React.Component<RequiresPleiarSearcherProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { component, app, onRouteSync, ...additionalProps } = this.props;
        let routingAssistant: React.Node = null;
        if (onRouteSync) {
            routingAssistant = (
                <RoutingAssistantSyncRoutes onSync={onRouteSync} />
            );
        }
        return (
            <MainContainer app={app}>
                {routingAssistant}
                {/* $FlowFixMe[cannot-spread-inexact] ConditionalRender needs some work to type properly - #117 */}
                <ConditionalRender
                    component={component}
                    test={() => PleiarSearcher.hasInitialized()}
                    resolve={() => PleiarSearcher.initialize()}
                    {...additionalProps}
                />
            </MainContainer>
        );
    }
}

const GlobalSearchContainer = connect(
    (state) => {
        return {
            search: state.globalSearch.search,
        };
    },
    (dispatch) => {
        return {
            onSearchUpdate(ev: ReduxEventOrString) {
                dispatch(setGlobalSearchString(getValueFromLiteralOrEvent(ev)));
            },
        };
    },
)(withRouter(GlobalSearch));

const TopSearchContainer: React.AbstractComponent<{||}> = connect<
    TopSearchProps,
    TopSearchPropsFromRouter,
    TopSearchReduxStateProps,
    TopSearchReduxDispatchProps,
    _,
    _,
>(
    (state): TopSearchReduxStateProps => {
        return {
            search: state.globalSearch.search,
        };
    },
    (dispatch): TopSearchReduxDispatchProps => {
        return {
            onSearchUpdate(ev: ReduxEventOrString) {
                dispatch(setGlobalSearchString(getValueFromLiteralOrEvent(ev)));
            },
        };
    },
)(withRouter(TopSearch));

type GlobalSearchWrapperProps = {| onRouteSync: onSyncSignature |};
/**
 * This is a wrapper around GlobalSearch. It wraps it in a
 * RequiresPleiarSearcher component to ensure PleiarSearcher is initialized
 * once GlobalSearch starts rendering.
 */
class GlobalSearchWrapper extends React.Component<GlobalSearchWrapperProps> {
    // eslint-disable-next-line require-jsdoc
    render(): React.Node {
        const { onRouteSync, ...additionalProps } = this.props;
        return (
            <RequiresPleiarSearcher
                component={GlobalSearchContainer}
                onRouteSync={onRouteSync}
                app="search"
                {...additionalProps}
            />
        );
    }
}
const GlobalSearchWrapperContainer: Class<GlobalSearchWrapper> = connect<
    GlobalSearchWrapperProps,
    {||},
    _,
    GlobalSearchWrapperProps,
    _,
    _,
>(
    () => {
        return {};
    },
    (dispatch): GlobalSearchWrapperProps => {
        return {
            onRouteSync(search) {
                dispatch(setGlobalSearchString(search));
            },
        };
    },
)(GlobalSearchWrapper);

export default GlobalSearchWrapperContainer;
export {
    SearchResultRenderer,
    TopSearchContainer as TopSearch,
    InfiniteScrollSearchResultRenderer,
    RequiresPleiarSearcher,
};
// Testing exports
export {
    TopSearch as TopSearchUnconnected,
    GlobalSearchRedirect,
    WhateverRenderer,
    GlobalSearch as GlobalSearchContainerRaw,
    InfiniteScrollSearchResultRendererUnRouted,
    SearchCounter,
    SearchCounterUnconnected,
    ExternalSearchRenderer,
    SearchResultRendererUnconnected,
};
