/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 * Copyright (C) Eskild Hustvedt 2017-2018
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// @flow

/**
 * Helper function that retrieves a string value, given either a string, or a
 * SyntheticEvent.  Used to be able to treat an event or a string
 * interchangably, so that for instance a function can accept both an event
 * (ie. be triggered by a input field) and a string (value provided by us by
 * manually calling it).
 */
function getValueFromLiteralOrEvent (thing: string | SyntheticEvent<HTMLInputElement>): string
{
    let value: string;
    if(typeof(thing) == "string")
    {
        value = thing;
    }
    else
    {
        value = thing.currentTarget.value;
    }
    return value;
}

export { getValueFromLiteralOrEvent };
