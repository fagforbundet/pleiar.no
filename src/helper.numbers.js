/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019-2021
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// @flow

import { getValueFromLiteralOrEvent } from './helper.general';
import type { ReduxEventOrString } from './types/callbacks';

/**
 * This provides some static helper functions for formatting and
 * parsing numbers.
 */
const numbers = {
    /**
     * This parses a ReduxEventOrString and then hands it off to
     * `numbers.parseFloatish`.
     */
    parseFloatishFromThing (thing: ReduxEventOrString): "" | number
    {
        return numbers.parseFloatish( getValueFromLiteralOrEvent(thing) );
    },
    /**
     * This parses a ReduxEventOrString, hands it off to numbers.parseFloatish
     * and then ensures that it is within the range supplied. If it is below the range,
     * it sets it to `min`, if it is above the range it sets it to `max`.
     * If it's an empty string, that's just returned directly.
     */
    floatishInRangeFromThing(thing: ReduxEventOrString | number, min: number, max: number): "" | number
    {
        let value: number | "";
        if(typeof(thing) === "number")
        {
            value = thing;
        }
        else
        {
            value = numbers.parseFloatishFromThing(thing);
        }
        if(value === "")
        {
            return value;
        }
        if(value > max)
        {
            value = max;
        }
        else if(value < min)
        {
            value = min;
        }
        return value;
    },
    /**
     * This does basically the same thing as parseFloatishFromThing, but
     * it guarantees that it will return a number (or NaN).
     */
    getFloatishFromThing (thing: number | ReduxEventOrString): number
    {
        if(typeof(thing) === "number")
        {
            return thing;
        }
        const value = getValueFromLiteralOrEvent(thing);
        if(value === "")
        {
            throw('getFloatishFromThing: got empty string');
        }
        return numbers.parseFloat(value);
    },
    /**
     * A third variant of parseFloatishFromThing. This one will return zero
     * if it ends up with a string
     */
    getFloatishOrZeroFromThing (thing: number | string | SyntheticEvent<HTMLInputElement>): number
    {
        if(typeof(thing) === "number")
        {
            return thing;
        }
        const valueFromThing = getValueFromLiteralOrEvent(thing);
        const result = numbers.parseFloat(valueFromThing);
        if(Number.isNaN(result))
        {
            return 0;
        }
        return result;
    },
    /**
     * A function that either returns an empty string, or the result from
     * `numbers.parseFloat`
     */
    parseFloatish (toParse: string): "" | number
    {
        if(toParse === "")
        {
            return "";
        }
        return numbers.parseFloat(toParse);
    },
    /**
     * A `Number.parseFloat` wrapper that handles both . and ,-delimited numbers.
     */
    parseFloat (toParse: string): number
    {
        if(toParse.replace !== undefined)
        {
            toParse = toParse.replace(/,/g,'.');
        }
        return Number.parseFloat(toParse);
    },
    /**
     * Formats a number according to the locale (no)
     */
    format(number: number): string
    {
        return Number(number).toLocaleString("no");
    },
    /**
     * Rounds a number to its closest 10
     */
    roundNearestTen(number: number): number
    {
        return Math.round(number / 10) * 10;
    },
    /**
     * Rounds a number to a fixed maximum of decimal places
     */
    round(no: number, roundTo: number = 4): number
    {
        return (+Number(no).toFixed(roundTo));
    },
    /**
     * Variant of `.round()` that only applies rounding if the roundingEnabled
     * parameter is true
     */
    conditionalRound(no: number, roundingEnabled: boolean, roundTo: number = 4): number
    {
        if(roundingEnabled)
        {
            return numbers.round(no,roundTo);
        }
        return no;
    },
    /**
     * Rounds a number to its simplest significant decimal.
     */
    roundToSimplestSignificant(no: number): number
    {
        for(const attempt of [0, 2, 3])
        {
            const result = numbers.round(no,attempt);
            if(result > 0)
            {
                return result;
            }
        }
        return no;
    },
    /**
     * Variant of `.roundToSimplestSignificant` that only applies rounding if
     * the roundingEnabled parameter is true
     */
    conditionalRoundToSimplestSignificant(no: number, roundingEnabled: boolean): number
    {
        if(roundingEnabled)
        {
            return numbers.roundToSimplestSignificant(no);
        }
        return no;
    }
};

export { numbers };
