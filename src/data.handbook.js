/*
 * Handbook data wrapper object
 *
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import handbook from '../data/handbok/handbok.json';
import type { HandbookPath, HandbookDataStructure, HandbookDataEntry } from './types/data';
import type { searchSnippetComponent } from './helper.lunrSnippet';

/**
 * Base class for Handbook* objects. Provides shared methods for looking up
 * objects.
 */
class HandbookBase
{
    _toplevelParent: ?HandbookIndex;

    /**
     * Internal warning emitter for Handbook* objects
     *
     * XXX: Should be able to differentiate between various subclasses
     */
    _warn(message: string)
    {
        // eslint-disable-next-line no-console
        console.log('HandbookEntry: '+message);
    }

    /**
     * Retrieves the root datastructure for this Handbook class. You can override
     * this in a subclass to override the datastructure that is being used.
     */
    _rootDatastructure (): HandbookDataStructure
    {
        return handbook;
    }

    /**
     * Retrives the HandbookIndex variant for this Handbook variant. You can override
     * this in a subclass to override which object gets created.
     */
    _newHandbookIndex (path: HandbookPath): HandbookIndex
    {
        return new HandbookIndex(path);
    }

    /**
     * Retrives the HandbookEntry variant for this Handbook variant. You can override
     * this in a subclass to override which object gets created.
     */
    _newHandbookEntry (path: HandbookPath): HandbookEntry
    {
        return new HandbookEntry(path);
    }

    /**
     * Retrieves the root path for this Handbook variant
     */
    _rootPath(): string
    {
        return '/handbok/';
    }

    /**
     * Looks up a single entry from the handbook based upon a path.
     * Will always either return the entry or null, it will not return
     * an index entry (use _lookupIndex for that)
     */
    _lookupEntry(path: HandbookPath): HandbookDataEntry | null
    {
        let index: HandbookDataEntry | HandbookDataStructure | null = this.__lookup(path);
        if(index !== null && index.content !== undefined && index.order === undefined)
        {
            index = ((index: $UnsafeRecast): HandbookDataEntry);
            return index;
        }
        if(index !== null && index.article !== undefined)
        {
            return index.article;
        }
        return null;
    }

    /**
     * Looks up a single index entry from the handbook based upon a path.
     * Will always either return the entry or null, it will not return
     * an actual entry (use _lookupEntry for that)
     */
    _lookupIndex(path: HandbookPath): HandbookDataStructure | null
    {
        const index = this.__lookup(path);
        if(index !== null && index.content === undefined && index.order !== undefined && index.tree !== undefined)
        {
            return index;
        }
        return null;
    }

    /**
     * The method that powers the other _lookup* methods. Will iterate through
     * the datastructure to find 'path' and return it. It is up to the caller
     * to determine if the path is a HandbookDataEntry or a HandbookDataStructure.
     *
     * This method should not be called by any subclasses directly.
     */
    __lookup(path: HandbookPath): HandbookDataEntry | HandbookDataStructure | null
    {
        // Make a copy of the path array, so that we can do destructive
        // modifications to it
        const lookupPath  : HandbookPath          = [].concat(path);
        let   currentTree : HandbookDataStructure = this._rootDatastructure();
        let result: ?HandbookDataEntry | ?HandbookDataStructure;

        while(lookupPath.length)
        {
            const current = lookupPath.shift();
            const entry = currentTree.tree[current];

            if(entry === undefined)
            {
                this._warn(this.constructor.name+': Unable to lookup path: '+lookupPath.join('/')+' - '+current+' does not exist');
                return null;
            }

            if(entry.tree === undefined && entry.content !== undefined)
            {
                // We use UnsafeRecast because flow has problems knowing that
                // if tree is undefined and content defined, that means we have a
                // HandbookDataEntry, not another HandbookDataStructure
                result = ((entry: $UnsafeRecast): HandbookDataEntry);
            }
            else
            {
                // We use UnsafeRecast because flow has problems knowing that
                // if tree is defined and content undefined, that means we have a
                // HandbookDataEntry, not another HandbookDataStructure
                currentTree = ((entry: $UnsafeRecast): HandbookDataStructure);
            }
        }
        if(result === undefined)
        {
            return currentTree;
        }
        return result;
    }

    /**
     * Locates the toplevel parent of this entry. Toplevel parent is the
     * highest entry in the tree *except* the root element.
     *
     * Returns `this` if it is the toplevel parent.
     */
    get toplevelParent(): HandbookIndex
    {
        // We cache the toplevel parent if it's not ourself
        let parent: ?HandbookIndex = this._toplevelParent;
        if (parent === null || parent === undefined)
        {
            if (this instanceof HandbookIndex && this.path.length == 1)
            {
                parent = this;
            }
            else
            {
                const path = this.path[0];
                parent = new HandbookIndex(path);
                this._toplevelParent = parent;
            }
        }
        return parent;
    }

    /**
     * Stubbed path
     */
    get path(): HandbookPath
    {
        throw("Subclass of HandbookBase MUST override path!");
    }
}

/**
 * Represents a single "index" entry in the handbook. That is, an entry that
 * contains other entries.
*/
class HandbookIndex extends HandbookBase
{
    exists: boolean;
    _entry: HandbookDataStructure;
    _firstChild: HandbookEntry | HandbookIndex;
    path: HandbookPath;

    constructor(path?: HandbookPath | string) // eslint-disable-line require-jsdoc
    {
        super();
        if(path === undefined)
        {
            this.exists = true;
            this._entry = this._rootDatastructure();
            this.path = [];
            return;
        }
        if(typeof(path) === "string")
        {
            path = path.split('/');
        }
        const lookup = this._lookupIndex(path);
        this.path = path;
        if(lookup === null)
        {
            this.exists = false;
        }
        else
        {
            this._entry = lookup;
            this.exists = true;
        }
    }

    /**
     * Retrieves the HandbookDataStructure for this entry
     */
    get value(): HandbookDataStructure
    {
        if (!this.exists)
        {
            throw('Attempt to retrieve .value from a non-existing HandbookIndex: '+this.path.join('/'));
        }
        return this._entry;
    }

    /**
     * Retrieves the article for this entry, if it exists
     */
    get article (): HandbookEntry | null
    {
        if (!this.exists)
        {
            throw('Attempt to retrieve .article from a non-existing HandbookIndex: '+this.path.join('/'));
        }
        if(this._entry.article !== undefined)
        {
            return this._newHandbookEntry(this.path);
        }
        return null;
    }

    /**
     * Retrieves the title for this entry
     */
    get title(): string
    {
        return this.value.title;
    }

    /**
     * Retrieves the displayIndexLink for this entry
     */
    get displayIndexLink(): boolean
    {
        return this.value.displayIndexLink === true;
    }

    /**
     * Retrieves the URL for this entry
     */
    get url(): string
    {
        if (!this.exists)
        {
            throw('Can not retrieve .url for non-existing object');
        }
        return this._rootPath()+this.path.join('/');
    }

    /**
     * Retrieves the list of entries contained within this one
     */
    get tree(): Array<HandbookEntry | HandbookIndex>
    {
        const tree = [];
        for(const entry of this.value.order)
        {
            if(this.value.tree[entry] === undefined)
            {
                throw('tree(): undefined entry listed in order');
            }
            const path = [].concat(this.path,entry);
            if(this.value.tree[entry].tree !== undefined)
            {
                tree.push(
                    this._newHandbookIndex(path)
                );
            }
            else
            {
                tree.push(
                    this._newHandbookEntry(path)
                );
            }
        }
        return tree;
    }

    /**
     * Retrieves the first child contained within this one
     *
     * This is essentially the same as `tree.shift()`, but is slightly optimized
     * because it doesn't have to iterate through everything
     */
    get firstChild(): HandbookEntry | HandbookIndex
    {
        if(this._firstChild === undefined)
        {
            const entry = this.value.order[0];
            if(this.value.tree[entry] === undefined)
            {
                throw('firstChild(): undefined entry listed in order');
            }
            const path = [].concat(this.path,entry);
            if(this.value.tree[entry].tree !== undefined)
            {
                this._firstChild = this._newHandbookIndex(path);
            }
            else
            {
                this._firstChild = this._newHandbookEntry(path);
            }
        }
        return this._firstChild;
    }
}

/**
 * Represents a single entry in the handbook. Use this instead of referencing
 * the handbook data structure directly, whenever possible. It makes looking up
 * prev/next entries etc. much easier.
 */
class HandbookEntry extends HandbookBase
{
    _entry: HandbookDataEntry;
    _next: HandbookEntry | null;
    _previous: HandbookEntry | null;
    _path: HandbookPath;
    searchSnippet: Array<searchSnippetComponent>;
    exists: boolean;

    constructor(path: HandbookPath | string) // eslint-disable-line require-jsdoc
    {
        super();
        if(typeof(path) === "string")
        {
            path = path.split('/');
        }
        this._path = path;
        const lookup = this._lookupEntry(path);
        if(lookup === null)
        {
            this.exists = false;
        }
        else
        {
            this._entry = lookup;
            this.exists = true;
        }
    }

    /**
     * Retrieves the HandbookDataEntry for this entry
     */
    get value(): HandbookDataEntry
    {
        this._verifySelf('value');
        return this._entry;
    }

    /**
     * Retrieves the toc for this entry
     */
    get toc(): Array<string>
    {
        return this.value.toc;
    }

    /**
     * Retrieves the "hide TOC" value for this entry (usually false)
     */
    get hideTOC(): boolean
    {
        return !!this.value.hideTOC;
    }

    /**
     * Retrieves the content for this entry
     */
    get content(): string
    {
        return this.value.content;
    }

    /**
     * Retrieves the title for this entry
     */
    get title(): string
    {
        return this.value.title;
    }

    /**
     * Retrieves the path for this entry
     */
    get path(): HandbookPath
    {
        return this.value.path;
    }

    /**
     * Retrieves the fully-qualified URL for this entry
     */
    get url(): string
    {
        this._verifySelf('url');
        if(this._entry.customURL !== undefined)
        {
            return this._entry.customURL;
        }
        return this._rootPath()+this.path.join('/');
    }

    /**
     * Determines if this entry has been whitelisted for public access
     */
    get public(): boolean
    {
        return this.value.public;
    }

    /**
     * Retrieves the next HandbookDataEntry or null
     */
    get next(): HandbookEntry | null
    {
        this._verifySelf('next');

        if(this._next !== undefined)
        {
            return this._next;
        }
        if(this._entry.next === undefined || !this.exists)
        {
            return null;
        }
        const lookup = this._newHandbookEntry( this._entry.next );
        if(!lookup.exists)
        {
            let path: string = '';
            if(this._entry.next !== undefined)
            {
                path = this._entry.next.join('/');
            }
            throw('Failed to look up '+path);
        }
        this._next = lookup;
        return this._next;
    }

    /**
     * Retrieves the previous HandbookDataEntry or null
     */
    get previous(): HandbookEntry | null
    {
        this._verifySelf('previous');

        if(this._previous !== undefined)
        {
            return this._previous;
        }
        if(this._entry.previous === undefined || !this.exists)
        {
            return null;
        }
        const lookup = this._newHandbookEntry( this._entry.previous );
        if(!lookup.exists)
        {
            let path: string = '';
            if(this._entry.previous !== undefined)
            {
                path = this._entry.previous.join('/');
            }
            throw('Failed to look up '+path);
        }
        this._previous = lookup;
        return this._previous;
    }

    /**
     * Private function that is used to verify that this object is valid (ie.
     * that it `.exists`). If it isn't valid, then we throw()
     */
    _verifySelf(caller: string): null
    {
        if (!this.exists)
        {
            throw('Attempt to retrieve .'+caller+' from a non-existing '+this.constructor.name+': '+this._path.join('/'));
        }
        return null;
    }
}

export default HandbookEntry;
export { HandbookEntry, HandbookIndex };
