/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import lunr from 'lunr';
import lunrStemmer from 'lunr-languages/lunr.stemmer.support.js';
import lunrNO from 'lunr-languages/lunr.no.js';

/*
 * The lunr warn function crashes in strict mode, so we override it with this
 * super-simplistic variant
 */
lunr.utils.warn = (message) =>
{
    /* eslint-disable no-console */
    console.log(message);
    /* eslint-enable no-console */
};

lunrStemmer(lunr);
lunrNO(lunr);

export default lunr;
