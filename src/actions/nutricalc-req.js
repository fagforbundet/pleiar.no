/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import type { nutriCalcReqToggleAction, nutriCalcReqAction, nutriCalcReqAgeGroup, nutriCalcReqPatientState } from '../reducers/nutricalc-req';

/**
 * Redux action that sets the value of one of the input fields in the nutricalc-req calculator
 */
function setValue (field: 'ageGroup' | 'patientState' | 'temp', value: number | "" | nutriCalcReqAgeGroup | nutriCalcReqPatientState): nutriCalcReqAction
{
    // XXX: To be fair, this is poor design and should probably be split into seperate actions
    // $FlowIssue[incompatible-return] Flow seems to think the fields are incompatible with each other
    return {
        type: 'NUTRICALC_REQ_SET',
        field, value
    };
}

/**
 * Redux action that sets the value of one of the boolean fields in the nutricalc-req calculator
 */
function toggleValue (field: 'febrile' | 'skinny'): nutriCalcReqToggleAction
{
    // $FlowIssue[incompatible-return] Flow seems to think the fields are incompatible with each other
    return {
        type: 'NUTRICALC_REQ_TOGGLE',
        field
    };
}

export { setValue, toggleValue };
