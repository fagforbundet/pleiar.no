/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2020
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import type { newsAction } from '../reducers/news';
import { BPValues, pulseValues, tempValues, RFValues, SpO2Values, consciousnessValues } from '../reducers/news';

/**
 * Redux action. Sets the BP value for the NEWS calculator
 */
function setNewsBP(BP: string): newsAction
{
    const validated = BPValues[BP];
    if (validated)
    {
        return {
            type: 'NEWS_SET_BP',
            BP: validated
        };
    }
    else
    {
        throw('Invalid BP value: '+BP);
    }
}

/**
 * Redux action. Sets the BP value for the NEWS calculator
 */
function setNewsPulse(pulse: string): newsAction
{
    const validated = pulseValues[pulse];
    if (validated)
    {
        return {
            type: 'NEWS_SET_PULSE',
            pulse: validated
        };
    }
    else
    {
        throw('Invalid pulse value: '+pulse);
    }
}

/**
 * Redux action. Sets the BP value for the NEWS calculator
 */
function setNewsTemp(temp: string): newsAction
{
    const validated = tempValues[temp];
    if (validated)
    {
        return {
            type: 'NEWS_SET_TEMP',
            temp: validated
        };
    }
    else
    {
        throw('Invalid temp value: '+temp);
    }
}

/**
 * Redux action. Sets the RF value for the NEWS calculator
 */
function setNewsRF(RF: string): newsAction
{
    const validated = RFValues[RF];
    if (validated)
    {
        return {
            type: 'NEWS_SET_RF',
            RF: validated
        };
    }
    else
    {
        throw('Invalid RF value: '+RF);
    }
}

/**
 * Redux action. Sets the SpO2 value for the NEWS calculator
 */
function setNewsSpO2(SpO2: string | null): newsAction
{
    if(SpO2 == null)
    {
        return {
            type: 'NEWS_SET_SPO2',
            SpO2: null
        };
    }
    else
    {
        const validated = SpO2Values[SpO2];
        if (validated)
        {
            return {
                type: 'NEWS_SET_SPO2',
                SpO2: validated
            };
        }
    }
    throw('Invalid SpO2 value: '+SpO2);
}

/**
 * Redux action. Sets the consciousness state value for the NEWS calculator
 */
function setNewsConsciousness(consciousness: string): newsAction
{
    const validated = consciousnessValues[consciousness];
    if (validated)
    {
        return {
            type: 'NEWS_SET_CONSCIOUSNESS',
            consciousness: validated
        };
    }
    else
    {
        throw('Invalid consciousness value: '+consciousness);
    }
}

/**
 * Redux action. Toggles use of the alternate O2 table for NEWS
 */
function toggleNewsAlternateO2Table (): newsAction
{
    return {
        type: 'NEWS_TOGGLE_ALTERNATEO2TABLE'
    };
}

/**
 * Redux action. Toggles the O2 state for NEWS.
 */
function toggleNewsO2 (): newsAction
{
    return {
        type: 'NEWS_TOGGLE_O2'
    };
}

export { setNewsBP, setNewsConsciousness, setNewsRF, setNewsSpO2, setNewsTemp, setNewsPulse, toggleNewsO2, toggleNewsAlternateO2Table};
