/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import type { nutriCalcDayAction } from '../reducers/nutricalc-day';

/**
 * Redux action that toggles display of the food list in the nutricalc
 */
function toggleFoodList (): nutriCalcDayAction
{
    return {
        type: 'NUTRICALC_DAY_TOGGLE_FOODLIST'
    };
}

/**
 * Redux action that sets the value of a line in the nutrition calculator
 */
function setEntryValue (entryNo: number, key: string, val: string): nutriCalcDayAction
{
    return {
        type: 'NUTRICALC_DAY_SET_ENTRY_VALUE',
        entryNo, key, val
    };
}

/**
 * Redux action that removes a line from the nutrition calculator
 */
function removeEntry (entryNo: number): nutriCalcDayAction
{
    return {
        type: 'NUTRICALC_DAY_REMOVE_ENTRY',
        entryNo
    };
}

/**
 * Redux action that changes the minimum kcal required in the nutrition calculator
 */
function setMinimum (minimum: number | ""): nutriCalcDayAction
{
    return {
        type: 'NUTRICALC_DAY_SET_MINIMUM',
        minimum
    };
}

/**
 * Redux action that resets the entire nutrition calculator
 */
function reset (): nutriCalcDayAction
{
    return {
        type: 'NUTRICALC_DAY_RESET'
    };
}

export { toggleFoodList, setEntryValue, removeEntry, setMinimum, reset };
