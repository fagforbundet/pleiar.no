/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Eskild Hustvedt 2017-2018
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import type { medCalcInfusionAction } from '../reducers/medcalc-infusion';

/**
 * Redux action that sets the content (in ml) of the infusion
 */
function setContent (content: number | ''): medCalcInfusionAction
{
    return {
        type: 'MEDCALC_INFUSION_SET_CONTENT',
        content
    };
}
/**
 * Redux action that sets the number of minutes the infusion is to be given over
 */
function setMinutes (minutes: number | ''): medCalcInfusionAction
{
    return {
        type: 'MEDCALC_INFUSION_SET_MINUTES',
        minutes
    };
}
/**
 * Redux action that sets the number of hours the infusion is to be given over
 */
function setHours (hours: number | ''): medCalcInfusionAction
{
    return {
        type: 'MEDCALC_INFUSION_SET_HOURS',
        hours
    };
}
/**
 * Redux action that changes the mode of the calculator
 */
function setCalculatorMode (mode: "ml/t" | "dråpar/sek"): medCalcInfusionAction
{
    return {
        type: 'MEDCALC_INFUSION_SET_MODE',
        mode,
    };
}
/**
 * Redux action that toggles the visibility of "additional calculations"
 */
function toggleAdditionalCalculationsVisibility (): medCalcInfusionAction
{
    return {
        type: 'MEDCALC_INFUSION_TOGGLE_ADDITIONAL_CALCULATIONS'
    };
}

export { setContent, setMinutes, setHours, setCalculatorMode, toggleAdditionalCalculationsVisibility };
