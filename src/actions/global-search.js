/*
 * Part of Pleiar.no - a collection of tools for nurses
 *
 * Copyright (C) Fagforbundet 2019
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

// @flow

import type { globalSearchAction } from '../reducers/global-search';

/**
 * Redux action that sets the search string for searches using the global
 * search function
 */
function setGlobalSearchString(search: string): globalSearchAction
{
    return {
        type: 'GLOBAL_SEARCH_SET_SEARCH_STRING',
        search
    };
}

export { setGlobalSearchString };
